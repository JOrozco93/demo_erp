# puertas_garaje

HS Puertas de Garaje

* Actualizaciones

- Cotizaciones
  - Alta
    - Ajustes a la vista
    - Refactorización de Js
    - Validaciones
      - Puertas
      - Componentes
      - Kits
    - Notificaciones
    - Añadir componentes a la lista
    - Eliminar componentes de la lista
    
- Panel de dashboard
  - Bloques de status
  - Grafica de ventas
  - Divisas
  - Multiplicadores
  - Listados de cotizaciones
  
- Clientes
  - perfil con listado de cotizaciones, depositos, incidencias
  
- Catalogos
  - Puertas
  - Componentes
  - Kits
  - Ventanas

* Pendientes
  - Proceso para cancelar una cotización
  - Mostrar una cotización
  - Editar una cotización
