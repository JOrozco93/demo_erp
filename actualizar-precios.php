<?php

if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");

$puertas = getPuertasActualizarPrecios();
$componentes = getComponentesActualizarPrecios();
$kits = getKitsActualizarPrecios();
$ventanas = getVentanasActualizarPrecios();

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->              
        <?php include ("includes/css.php"); ?>
        <!--<link rel="shortcut icon" href="favicon.ico"/>-->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>
        
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Actualizar precios
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="inicio.php">
                                        Inicio
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Actualizar precios
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>Lista de precios</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form id="precios-form" method="post" action="alta/actualizar-precios-save.php">
                                        <!-- puertas -->
                                        <div class="row" style="margin-bottom: 25px;">
                                            <div class="col-md-12">
                                                <div class="well well-sm">
                                                    <h4>Puertas</h4>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="table-puertas" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="100px;">Metros</th>
                                                                <th width="100px;">Pies</th>
                                                                <th width="100px;">Tipo</th>
                                                                <th width="100px;">VA</th>
                                                                <th width="100px;">VB</th>
                                                                <th width="100px;">VC</th>
                                                                <th width="100px;">VD</th>
                                                                <th width="100px;">VE</th>
                                                                <th width="100px;">VF</th>
                                                                <th width="100px;">Precio</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php for ($i = 0; $i < count($puertas); $i++): ?>
                                                                <tr>
                                                                    <td>
                                                                        <input type="hidden" name="<?php echo "puertas[$i][id_puerta]"; ?>" value="<?php echo $puertas[$i]['id_puerta']; ?>" class="form-control" />
                                                                        <?php echo $puertas[$i]['m_ancho'] . ' x ' . $puertas[$i]['m_alto']; ?>
                                                                    </td>
                                                                    <td><?php echo $puertas[$i]['f_ancho'] . '\'' . $puertas[$i]['in_ancho'] . '" x ' . $puertas[$i]['f_alto'] . '\'' . $puertas[$i]['in_alto'] . '"'; ?></td>
                                                                    <td><?php echo $puertas[$i]['nombre_tipo']; ?></td>
                                                                    <td><input type="text" name="<?php echo "puertas[$i][VA]"; ?>" value="<?php echo $puertas[$i]['VA']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "puertas[$i][VB]"; ?>" value="<?php echo $puertas[$i]['VB']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "puertas[$i][VC]"; ?>" value="<?php echo $puertas[$i]['VC']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "puertas[$i][VD]"; ?>" value="<?php echo $puertas[$i]['VD']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "puertas[$i][VE]"; ?>" value="<?php echo $puertas[$i]['VE']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "puertas[$i][VF]"; ?>" value="<?php echo $puertas[$i]['VF']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "puertas[$i][precio]"; ?>" value="<?php echo $puertas[$i]['precio']; ?>" class="form-control number" /></td>
                                                                </tr>
                                                            <?php endfor; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- puertas -->

                                        <!-- componentes -->
                                        <div class="row" style="margin-bottom: 25px;">
                                            <div class="col-md-12">
                                                <div class="well well-sm">
                                                    <h4>Componentes</h4>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="table-componentes" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="150px;">Grupo del componente</th>
                                                                <th width="150px;">Nombre del componente</th>
                                                                <th width="100px;">VA</th>
                                                                <th width="100px;">VB</th>
                                                                <th width="100px;">VC</th>
                                                                <th width="100px;">VD</th>
                                                                <th width="100px;">VE</th>
                                                                <th width="100px;">VF</th>
                                                                <th width="100px;">Precio</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php for ($i = 0; $i < count($componentes); $i++): ?>
                                                                <tr>
                                                                    <td>
                                                                        <input type="hidden" name="<?php echo "componentes[$i][id_componente]"; ?>" value="<?php echo $componentes[$i]['id_componente']; ?>" class="form-control" />
                                                                        <?php echo $componentes[$i]['grupo_componente']; ?>
                                                                    </td>
                                                                    <td><?php echo $componentes[$i]['nombre_componente']; ?></td>
                                                                    <td><input type="text" name="<?php echo "componentes[$i][VA]"; ?>" value="<?php echo $componentes[$i]['VA']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "componentes[$i][VB]"; ?>" value="<?php echo $componentes[$i]['VB']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "componentes[$i][VC]"; ?>" value="<?php echo $componentes[$i]['VC']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "componentes[$i][VD]"; ?>" value="<?php echo $componentes[$i]['VD']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "componentes[$i][VE]"; ?>" value="<?php echo $componentes[$i]['VE']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "componentes[$i][VF]"; ?>" value="<?php echo $componentes[$i]['VF']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "componentes[$i][precio]"; ?>" value="<?php echo $componentes[$i]['precio']; ?>" class="form-control number" /></td>
                                                                </tr>
                                                            <?php endfor; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- componentes -->

                                        <!-- kits -->
                                        <div class="row" style="margin-bottom: 25px;">
                                            <div class="col-md-12">
                                                <div class="well well-sm">
                                                    <h4>Kits</h4>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="table-kits" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="300px;">Nombre del kit</th>
                                                                <th width="100px;">VA</th>
                                                                <th width="100px;">VB</th>
                                                                <th width="100px;">VC</th>
                                                                <th width="100px;">VD</th>
                                                                <th width="100px;">VE</th>
                                                                <th width="100px;">VF</th>
                                                                <th width="100px;">Precio</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php for ($i = 0; $i < count($kits); $i++): ?>
                                                                <tr>
                                                                    <td>
                                                                        <input type="hidden" name="<?php echo "kits[$i][id_kit]"; ?>" value="<?php echo $kits[$i]['id_kit']; ?>" class="form-control" />
                                                                        <?php echo $kits[$i]['nombre_kit']; ?>
                                                                    </td>
                                                                    <td><input type="text" name="<?php echo "kits[$i][VA]"; ?>" value="<?php echo $kits[$i]['VA']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "kits[$i][VB]"; ?>" value="<?php echo $kits[$i]['VB']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "kits[$i][VC]"; ?>" value="<?php echo $kits[$i]['VC']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "kits[$i][VD]"; ?>" value="<?php echo $kits[$i]['VD']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "kits[$i][VE]"; ?>" value="<?php echo $kits[$i]['VE']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "kits[$i][VF]"; ?>" value="<?php echo $kits[$i]['VF']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "kits[$i][precio]"; ?>" value="<?php echo $kits[$i]['precio']; ?>" class="form-control number" /></td>
                                                                </tr>
                                                            <?php endfor; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- kits -->

                                        <!-- ventanas -->
                                        <div class="row" style="margin-bottom: 25px;">
                                            <div class="col-md-12">
                                                <div class="well well-sm">
                                                    <h4>Ventanas</h4>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="table-kits" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th width="300px;">Nombre de la ventana</th>
                                                                <th width="100px;">VA</th>
                                                                <th width="100px;">VB</th>
                                                                <th width="100px;">VC</th>
                                                                <th width="100px;">VD</th>
                                                                <th width="100px;">VE</th>
                                                                <th width="100px;">VF</th>
                                                                <th width="100px;">Precio</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php for ($i = 0; $i < count($ventanas); $i++): ?>
                                                                <tr>
                                                                    <td>
                                                                        <input type="hidden" name="<?php echo "ventanas[$i][id_ventana]"; ?>" value="<?php echo $ventanas[$i]['id_ventana']; ?>" class="form-control" />
                                                                        <?php echo $ventanas[$i]['nombre_ventana']; ?>
                                                                    </td>
                                                                    <td><input type="text" name="<?php echo "ventanas[$i][VA]"; ?>" value="<?php echo $ventanas[$i]['VA']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "ventanas[$i][VB]"; ?>" value="<?php echo $ventanas[$i]['VB']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "ventanas[$i][VC]"; ?>" value="<?php echo $ventanas[$i]['VC']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "ventanas[$i][VD]"; ?>" value="<?php echo $ventanas[$i]['VD']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "ventanas[$i][VE]"; ?>" value="<?php echo $ventanas[$i]['VE']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "ventanas[$i][VF]"; ?>" value="<?php echo $ventanas[$i]['VF']; ?>" class="form-control number" /></td>
                                                                    <td><input type="text" name="<?php echo "ventanas[$i][precio]"; ?>" value="<?php echo $ventanas[$i]['precio']; ?>" class="form-control number" /></td>
                                                                </tr>
                                                            <?php endfor; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ventanas -->

                                        <!-- form actions -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" id="precios-save" class="btn btn-success pull-right"><i class="fa fa-save"></i> Guardar</button>
                                            </div>
                                        </div>
                                        <!-- form actions -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- page-content -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

        <!-- footer -->
        <div class="footer">
            <div class="footer-inner">
                2015 &copy; HS Puertas de Garage.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- footer -->

        <!-- notifications -->
        <div class='notifications bottom-right'></div>
        <!-- notifications -->
        
        <!-- BEGIN JAVASCRIPTS -->
        <?php include ("includes/js.php") ?>;

<!-- BEGIN GRAPHS -->
<script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<!-- END GRAPHS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="scripts/custom/index.js" type="text/javascript"></script>

<script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="plugins/select2/select2.min.js"></script>

<script>
    jQuery(document).ready(function () {
        App.init();

        function notify(message) {
            $('.bottom-right').notify({
                message: { text: message },
                type: 'bangTidy'
            }).show(); 
        }

        function validateInputs() {
            var inputs = $('input[type=text].number');
            var valid = true;

            $.each(inputs, function(index, elem) {
                var val = $(elem).val();

                if (isNaN(val) || val == '') {
                    valid = false;

                    return false;
                }
            });

            return valid;
        }

        $('#precios-save').click(function() {
            if (validateInputs()) {
                $('#precios-form').submit();
            } else {
                notify('Advertencia: Algunos datos no son validos');
            }
        });
    });

</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>