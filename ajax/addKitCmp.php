<?php
header("Cache-Control: no-cache, must-revalidate");

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];
$idk = $_POST['idk'];
$fnKC = $_POST['fn'];
if ($fnKC == "4") { # Eliminar kit_componente
  $idKC = $_POST['idKC'];
  $sql_drop = sprintf("UPDATE `kit_componente` SET `activo` = 0 WHERE id_kit_componente = %s", GetSQLValueString($idKC, "int"));
  $result = mysql_query($sql_drop, $db_con) or die("Problemas en la consulta" . $sql_drop);

  $detalle = "El usuario con el id " . $id_usuario . "  eliminó un kit_componente con el id " . $idKC . " ";
  $evento = "Eliminar";
  $id_log = actualizalog($id_usuario, $evento, $detalle);
  $sql_update = sprintf("UPDATE `kit_componente` SET `id_log`=%s WHERE id_kit_componente =%s", GetSQLValueString($id_log, "text"), GetSQLValueString($idKC, "int")
  );
  $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta" . $sql_update);

}
else if ($fnKC == "2") { // Guardar cantidad
  $cant = $_POST['cant'];
  $idKC = $_POST['id_KC'];
  $sql_upd = sprintf("UPDATE `kit_componente` SET cantidad_kit_componente = %s WHERE id_kit_componente = %s", GetSQLValueString($cant, "int"), GetSQLValueString($idKC, "int"));
  $result = mysql_query($sql_upd, $db_con) or die("Problemas en la consulta" . $sql_upd);

  echo $cant;
}
else if ($fnKC == "3") { // Modificar cantidad
    $idKC = $_POST['id_KC'];
    $sqlKC1 = "SELECT * FROM kit_componente WHERE activo = 1 AND id_kit_componente = ". $idKC;
    $rowKC1 = mysql_fetch_array(consulta($sqlKC1));
    $sqlC1 = "SELECT * FROM componentes WHERE activo=1 AND id_componente=" . $rowKC1['id_componente'];
    $rowC1 = mysql_fetch_array(consulta($sqlC1)); // Nombre del componente
?>
    <td>
      <div class="input-group">
        <input type="number" name="cantidad_kit_componente_<?php echo $rowKC1['id_kit_componente']; ?>" class="form-control input-sm" value="<?php echo $rowKC1['cantidad_kit_componente']; ?>" min="1" placeholder="Cantidad" />
        <span class="input-group-btn">
          <button data-id="<?php echo $rowKC1['id_kit_componente']; ?>" class="btn btn-info btn-sm saveKC" type="button" data-placement="top" data-toggle="tooltip" title="Guardar cantidad"><i class="fa fa-floppy-o"></i></button>
        </span>
      </div>
    </td>
    <td><?php echo htmlentities($rowC1['nombre_componente']); ?></td>
    <td class="hidden"><?php echo $rowC1['precio']; ?></td>
    <td><a data-placement="right" data-toggle="tooltip" title="Remover componente" href="#" class="btn btn-danger btn-sm delKC" data-id="<?php echo $rowKC1['id_kit_componente']; ?>"><i class="fa fa-minus"></i></a></td>
<?php

} else if ($fnKC == "1") { # Agregar kit_componente
  $idC = $_POST['idC'];
  $sql_insert = sprintf("INSERT INTO `kit_componente`(`id_kit`, `id_componente`) VALUES (%s, %s)", GetSQLValueString($idk, "int"), GetSQLValueString($idC, "int"));
  $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta" . $sql_insert);

  $idKC = mysql_insert_id();
  $detalle = "El usuario con el id " . $id_usuario . "  creo un kit_componente con el id" . $idKC . " ";
  $evento = "Alta";
  $id_log = actualizalog($id_usuario, $evento, $detalle);
  $sql_update = sprintf("UPDATE `kit_componente` SET `id_log`=%s WHERE id_kit_componente =%s", GetSQLValueString($id_log, "text"), GetSQLValueString($idKC, "text")
  );
  $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta" . $sql_update);

  $sqlKC1 = "SELECT * FROM kit_componente WHERE activo = 1 AND id_kit_componente = ". $idKC;
  $queryKC1 = consulta($sqlKC1);
  $rowKC1 = mysql_fetch_array($queryKC1);
  $sqlC1 = "SELECT * FROM componentes WHERE activo=1 AND id_componente=" . $rowKC1['id_componente'];
  $rowC1 = mysql_fetch_array(consulta($sqlC1)); // Nombre del componente
?>
  <tr id="trComp_<?php echo $rowKC1['id_kit_componente']; ?>">
    <td>
      <div class="input-group">
        <input type="number" name="cantidad_kit_componente_<?php echo $rowKC1['id_kit_componente']; ?>" class="form-control input-sm" value="1" min="1" placeholder="Cantidad" />
        <span class="input-group-btn">
          <button data-id="<?php echo $rowKC1['id_kit_componente']; ?>" class="btn btn-info btn-sm saveKC" type="button" data-placement="top" data-toggle="tooltip" title="Guardar cantidad"><i class="fa fa-floppy-o"></i></button>
        </span>
      </div>
    </td>
    <td><?php echo htmlentities($rowC1['nombre_componente']); ?></td>
    <td class="hidden"><?php echo $rowC1['precio']; ?></td>
    <td><a data-placement="right" data-toggle="tooltip" title="Remover componente" href="#" class="btn btn-danger btn-sm delKC" data-id="<?php echo $rowKC1['id_kit_componente']; ?>"><i class="fa fa-minus"></i></a></td>
  </tr>
<?php }
?>
