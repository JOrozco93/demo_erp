<?php
if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cliente = $_POST['id_cliente'];
$id_impuesto = $_POST['id_impuesto'];
	
try {
	$stmt = $pdo->query("SELECT * FROM impuestos WHERE id_impuesto = $id_impuesto LIMIT 1");  
	$row_count = $stmt->rowCount();

	if ($row_count) {
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$porcentaje = $row['porcentaje'];

		$response = [
			'status' => 'success',
			'porcentaje' => $porcentaje
		];
	} else {
		$response = [
			'status' => 'error',
			'message' => 'No se ha localizado el impuesto'
		];
	}
	
	echo $_GET['callback'] . "(" . json_encode($response) . ");";
} catch(PDOException $e) {
	echo '{"error":{"text":'. $e->getMessage() .'}}'; 
}

?>