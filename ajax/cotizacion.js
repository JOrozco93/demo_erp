$(document).ready(function () {

    var total_cotizacion = {
        subtotal: 0,
        impuestos: 0,
        total: 0
    };

    function notify(message) {
        $('.bottom-right').notify({
            message: { text: message },
            type: 'bangTidy'
        }).show(); 
    }

    function checkInfo() {
        id_cliente = $('#id_cliente').val();
        id_divisa = $('#id_divisa').val();
        id_impuesto = $('#id_impuesto').val();
        id_despacho = $('#id_despacho').val();
        descripcion = $('#descripcion').val();

        if (id_cliente == '0') {
            notify('No se ha seleccionado un cliente');

            return false;
        }

        if (id_divisa == '0') {
            notify('No se ha seleccionado una divisa');

            return false;
        }

        if (id_impuesto == '0') {
            notify('No se ha seleccionado un impuesto');

            return false;
        }

        if (id_despacho == '0') {
            notify('No se ha seleccionado una opción de despacho');

            return false;
        }

        if (descripcion == '') {
            notify('No se ha ingresado una descripción');
            
            return false;
        }

        return true;
    }

    function search_id_door_in_m(id_ptipo, m_ancho, m_alto) {
        id_door = 0;

        $.ajax({
            url: './alta/conversor.php',
            type: 'post',
            dataType: 'jsonp',
            data: {
                ban : 0,
                id_ptipo: id_ptipo,
                m_ancho: m_ancho,
                m_alto: m_alto
            },
            async: false
        }).done(function(data) {
            if (!data.error) {
                id_door = data;        
            }
        });

        return id_door;
    }

    function search_id_door_in_f(id_ptipo, f_ancho, in_ancho, f_alto, in_alto) {
        id_door = 0;

        $.ajax({
            url: './alta/conversor.php',
            type: 'post',
            dataType: 'jsonp',
            data: {
                ban : 1,
                id_ptipo: id_ptipo,
                f_ancho: f_ancho,
                in_ancho: in_ancho,
                f_alto: f_alto,
                in_alto: in_alto
            },
            async: false
        }).done(function(data) {
            if (!data.error) {
                id_door = data;
            }
        });

        return id_door;
    }

    function calculopuerta(id_cliente, id_divisa, id_impuesto, id_ptipo, id_puerta, id_pmodelo, id_pcolor, id_pmovimiento, id_textura, id_sello, id_motor, id_ventana, id_vcolor, nventanas) {
        return $.ajax({
            url: './alta/calculoPuerta.php',
            type: 'post',
            dataType: 'jsonp',
            async: false,
            data: {
                id_cliente: id_cliente,
                id_divisa: id_divisa,
                id_impuesto: id_impuesto,
                id_ptipo: id_ptipo,
                id_puerta: id_puerta,
                id_pmodelo: id_pmodelo,
                id_pcolor: id_pcolor,
                id_pmovimiento: id_pmovimiento,
                id_textura: id_textura,
                id_sello: id_sello,
                id_motor: id_motor,
                id_ventana: id_ventana,
                id_vcolor: id_vcolor,
                nventanas: nventanas
            }
        });
    }

    function calculokit(id_cliente, id_divisa, id_impuesto, id_kit) {
        return $.ajax({
            url: './alta/calculoKit.php',
            type: 'post',
            dataType: 'jsonp',
            async: false,
            data: {
                id_cliente: id_cliente,
                id_divisa: id_divisa,
                id_impuesto: id_impuesto,
                id_kit: id_kit
            }
        });
    }

    function calculocomponente(id_cliente, id_divisa, id_impuesto, id_componente) {
        return $.ajax({
            url: './alta/calculoComponente.php',
            type: 'post',
            dataType: 'jsonp',
            async: false,
            data: {
                id_cliente: id_cliente,
                id_divisa: id_divisa,
                id_impuesto: id_impuesto,
                id_componente: id_componente
            }
        });
    }

    function getConceptos() {
        var id_cotizacion = $("#id_cotizacion").val();

        $.ajax({
            url: './ajax/getConcepts.php',
            type: 'post',
            data: {
                id_cotizacion: id_cotizacion
            },
            async: false
        }).done(function(response) {
            if (response.error) {
                notify(response.error)
                
                return false;
            }

            $("#concepts-section").html(response);

            subtotal();
            impuestos();
            total();

            resgistrartotales();
        });
    }  

    function subtotal() {
        var id_cotizacion = $("#id_cotizacion").val();
        
        $.ajax({
            url: './alta/totalCotizacion.php',
            type: 'post',
            dataType: 'jsonp',
            data: {
                id_cotizacion: id_cotizacion
            },
            async: false
        }).done(function(data) {
            if (!data.error) {
                $('#subtotal').val(parseFloat(data).toFixed(2));
            }
        });
    }

    function impuestos() {
        var id_cliente = $("#id_cliente").val();
        var id_impuesto = $("#id_impuesto").val();
            
        $.ajax({
            url: './ajax/calculofinal.php',
            type: 'post',
            dataType: 'jsonp',
            data: {
                id_cliente: id_cliente,
                id_impuesto: id_impuesto
            },
            async: false
        }).done(function(data) {
            if (data.status == 'error') {
                notify(data.message);
            }

            if (data.status == 'success') {
                $.each(data, function(index, ele) {
                    var porcentaje = data.porcentaje;

                    var subtotal = $("#subtotal").val();
                    var impuestos = (subtotal * porcentaje) / 100;

                    $('#impuestos').val(parseFloat(impuestos).toFixed(2));
                });
            }
        });
    }

    function total() {
        var subtotal = $("#subtotal").val();
        var impuestos = $('#impuestos').val();

        var total = parseFloat(subtotal) + parseFloat(impuestos);
        
        $('#total').val(parseFloat(total).toFixed(2));
    }

    function resgistrartotales() {
        total_cotizacion.subtotal = $('#subtotal').val();
        total_cotizacion.impuestos = $('#impuestos').val();
        total_cotizacion.total = $('#total').val();
    }
        
    App.init();
    TableAdvanced.init();

    if ($('#id_divisa').val() == 0) {
        $('#id_divisa').val(2);
    }

    if ($('#id_impuesto').val() == 0) {
        $('#id_impuesto').val(1);
    }

    if ($('#id_despacho').val() == 0) {
        $('#id_despacho').val(1);
    }

    $('#configuracionPuerta').hide();
    $('#configuracionComponente').hide();
    $('#configuracionKit').hide();
        
    function calcularsello(id_sello) {
        sello = id_sello;
        csello = 0;
        
        $.ajax({
            url: './ajax/sello.php',
            type: 'post',
            dataType: 'jsonp',
            data: { sello: sello }
        }).done(function(data) {
            result = data; 
        
            if (!data.error) {
                $.each(data, function(index, ele) {
                    csello = ele.precio;
                    parseInt(csello);
                });
            } else {
            }
            
            //return csello;*/
        });
    }

    $('#elemento').on("change", function() {
        var option = $(this).val();

        if (option == 0) {
            $('#configuracionPuerta').hide();
            $('#configuracionComponente').hide();
            $('#configuracionKit').hide();
        }

        if (option == 1) {
            $('#configuracionPuerta').show();
            $('#configuracionComponente').hide();
            $('#configuracionKit').hide();
        }

        if (option == 2) {
            $('#configuracionPuerta').hide();
            $('#configuracionKit').hide();
            $('#configuracionComponente').show();
        }

        if (option == 3) {
            $('#configuracionPuerta').hide();
            $('#configuracionComponente').hide();
            $('#configuracionKit').show();
        }
    });

    $("#id_ptipo").on("change", function() {
        var id_ptipo = $(this).val();

        $('#medida').hide();

        if ((id_ptipo == 0) || (id_ptipo == '')) {
            $('#id_puerta').html('<option value="">--------------------</option>');
            $('#id_pmodelo').html('<option value="">--------------------</option>');
            $('#id_pcolor').html('<option value="">--------------------</option>');
            $('#id_pmovimiento').html('<option value="">--------------------</option>');
            
            $('#id_ptextura').val(1);
            $('#id_psello').val(1);
            $('#id_pmotor').val(1);

            $('#id_ventana').html('<option value="">--------------------</option>');
            $('#id_vcolor').html('<option value="">--------------------</option>');

            $('#nventanas').val('');
            $('#ventanas').hide();

            return false;
        }

        $.ajax({
            url: 'ajax/tipopuerta-medida.php',
            type: 'post',
            data: {
                id: id_ptipo
            },
            success: function(datos) {
                $("#id_puerta").html(datos);
            },
            async: false
        });

        $.ajax({
            url: 'ajax/tipopuerta-modelo.php',
            type: 'post',
            data: {
                id: id_ptipo
            },
            success: function(datos) {
                $("#id_pmodelo").html(datos);
            },
            async: false
        });

        $.ajax({
            url: 'ajax/tipopuerta-color.php',
            type: 'post',
            data: {
                id: id_ptipo
            },
            success: function(datos) {
                $("#id_pcolor").html(datos);
            },
            async: false
        });

        $.ajax({
            url: 'ajax/tipopuerta-movimiento.php',
            type: 'post',
            data: {
                id: id_ptipo
            },
            success: function(datos) {
                $("#id_pmovimiento").html(datos);
            },
            async: false
        });

        $('#id_ptextura').val(1);
        $('#id_psello').val(1);
        $('#id_pmotor').val(1);

        $.ajax({
            url: 'ajax/tipopuerta-vmodelo.php',
            type: 'post',
            data: {
                id: id_ptipo
            },
            success: function(datos) {
                $("#id_ventana").html(datos);
            },
            async: false
        });

        $.ajax({
            url: 'ajax/tipopuerta-vcolor.php',
            type: 'post',
            data: {
                id: id_ptipo
            },
            success: function(datos) {
                $("#id_vcolor").html(datos);
            },
            async: false
        });

        $('#nventanas').val('');
        $('#ventanas').hide();
    });

    $("#id_puerta").on("change", function() {
        var id_puerta = $(this).val();

        if (id_puerta == 0) {
            $('#medida').show();
        }

        if (id_puerta != 0) {
            $('#medida').hide();
        }

        if ((id_puerta == 0) || (id_puerta == '')) {
            $('#nventanas').val('');

            return false;
        }

        $.ajax({
            url: 'ajax/tipopuerta-ventanas.php',
            type: 'post',
            data: {
                id: id_puerta
            },
            dataType: 'json',
            async: false,
            success: function(datos) {
                if (datos.error) {
                    alert(datos.message);    
                }

                if (datos.status == 'success') {
                    $('#nventanas').val(datos.nventanas);
                }
            }
        });
    });

    $("#id_modelo").on("change", function() {
        var id_modelo = $(this).val();

        if (id_modelo == '0') {
            $('#metros').hide();
            $('#pies').hide();
        }

        if (id_modelo == 'm') {
            $('#metros').show();
            $('#pies').hide();
        }

        if (id_modelo == 'p') {
            $('#pies').show();
            $('#metros').hide();
        }
    });

    $("#id_ventana").on("change", function() {
        var id_ventana = $(this).val();
        
        if (id_ventana == '') {
            $('#id_vcolor').val('');
            $('#ventanas').hide();

            return;
        }

        if (id_ventana == 0) {
            $('#id_vcolor').val(0);
            $('#ventanas').hide();

            return;
        }

        $('#ventanas').show();
    });

    $("#id_grupo_componente").on("change", function() {
        id_grupo_componente = $(this).val();

        if ((id_grupo_componente == 0) || (id_grupo_componente == null)) {
            $('#id_componente').html('<option value="">--------------------</option>');
            
            return false;
        }

        $.ajax({
            url: './ajax/dropDownComponente.php',
            type: 'post',
            data: {
                idgc: id_grupo_componente
            },
            success: function(datos) {
                $("#id_componente").html(datos);
            }
        });
    });

    // saveP
    $("#saveP").click(function() {
        if (!checkInfo()) {
            return false;
        }

        var id_cotizacion = $('#id_cotizacion').val();
        
        var id_cliente = $('#id_cliente').val();
        var id_divisa = $('#id_divisa').val();
        var id_impuesto = $('#id_impuesto').val();

        var cantidad = $('#cantidadP').val();
        var id_ptipo = $('#id_ptipo').val();
        var id_puerta = $('#id_puerta').val();
        
        var id_modelo = $('#id_modelo').val();

        var id_pmodelo = $('#id_pmodelo').val();
        var id_pcolor = $('#id_pcolor').val();
        var id_pmovimiento = $('#id_pmovimiento').val();
        
        var id_ptextura = $('#id_ptextura').val();
        var id_psello = $('#id_psello').val();
        var id_pmotor = $('#id_pmotor').val();

        var id_ventana = $('#id_ventana').val();
        var id_vcolor = $('#id_vcolor').val();
        var nventanas = $('#nventanas').val();

        if (cantidad == '' || cantidad == 0) {
            notify('No se ha ingresado la cantidad');
            return false;
        }

        if (id_ptipo == '') {
            notify('No se ha seleccionado un tipo de puerta');
            return false;
        }

        if (id_puerta == '') {
            notify('No se ha seleccionado una medida');
            return false;
        }

        // en caso de seleccionar una puerta a la medida --------
        if (id_puerta == 0 && id_modelo == 0) {
            notify('no se ha seleccionado una unidad de medida');
            return false;
        }
        // ------------------------------------------------------

        if (id_pmodelo == '') {
            notify('No se ha seleccionado un modelo');
            return false;
        }

        if (id_pcolor == '') {
            notify('No se ha seleccionado un color');
            return false;
        }

        if (id_pmovimiento == '') {
            notify('No se ha seleccionado un movimiento');
            return false;
        }

        if (id_ventana == '') {
            notify('No se ha seleccionado un modelo de ventana');
            return false;
        }

        if (id_vcolor == '') {
            notify('No se ha seleccionado un color de ventana');
            return false;
        }

        if (id_ventana > 0 && (nventanas == '' || nventanas == 0)) {
            notify('No se ha ingresado el numero de ventanas');
            return false;   
        }

        if (id_puerta != '' && id_puerta > 0 && id_ventana == 0) {
            var nventanas_orginal;

            $.ajax({
                url: 'ajax/tipopuerta-ventanas.php',
                type: 'post',
                data: {
                    id: id_puerta
                },
                dataType: 'json',
                async: false,
                success: function(datos) {
                    if (datos.error) {
                        nventanas_original = 0;
                    }

                    if (datos.status == 'success') {
                        nventanas_original = datos.nventanas;
                    }
                }
            });

            if (nventanas < nventanas_original) {
                notify('No se puede ingresar un numero menor de ventanas');
                return false;
            }
        }

        // calcular el precio de la puerta

        // medidas en metros
        if (id_puerta == 0 && id_modelo == 'm') {
            var m_ancho = $('#m_ancho').val();
            var m_alto = $('#m_alto').val();

            if (m_ancho == '' || m_alto == '') {
                notify('No se han ingresado las medidas en metros');
                
                return false;
            }

            var id_puerta_compatible = search_id_door_in_m(id_ptipo, m_ancho, m_alto);

            if (id_puerta_compatible) {
                var precio = calculopuerta(id_cliente, id_divisa, id_impuesto, id_ptipo, id_puerta_compatible, id_pmodelo, id_pcolor, id_pmovimiento, id_ptextura, id_psello, id_pmotor, id_ventana, id_vcolor, nventanas);
            } else {
                notify('No se encontro una puerta compatible en metros con las medidas introducidas');

                return false;
            }
        }

        // medidas en pies
        if (id_puerta == 0 && id_modelo == 'p') {
            var f_ancho = $('#f_ancho').val();
            var in_ancho = $('#in_ancho').val();
            var f_alto = $('#f_alto').val();
            var in_alto = $('#in_alto').val();

            if (f_ancho == '' || in_ancho == '' || f_alto == '' || in_alto == '') {
                notify('no se han ingresado las medidas en pies');
                
                return false;
            }

            var id_puerta_compatible = search_id_door_in_f(id_ptipo, f_ancho, in_ancho, f_alto, in_alto);

            if (id_puerta_compatible) {
                var precio = calculopuerta(id_cliente, id_divisa, id_impuesto, id_ptipo, id_puerta_compatible, id_pmodelo, id_pcolor, id_pmovimiento, id_ptextura, id_psello, id_pmotor, id_ventana, id_vcolor, nventanas);
            } else {
                notify('No se encontro una puerta compatible en pies con las medidas introducidas');

                return false;
            }
        }

        // medidas de una puerta en base de datos
        if (id_puerta > 0) {
            var precio = calculopuerta(id_cliente, id_divisa, id_impuesto, id_ptipo, id_puerta, id_pmodelo, id_pcolor, id_pmovimiento, id_ptextura, id_psello, id_pmotor, id_ventana, id_vcolor, nventanas); 
        }

        // registrar el concepto
        precio.success(function(data) {
            var costo_puerta = data;

            params = {
                "id_cotizacion": id_cotizacion,
                "id_cliente": id_cliente,
                "id_divisa": id_divisa,  
                "cantidad": cantidad,
                "ptipo": id_ptipo,
                "puerta": id_puerta_compatible || id_puerta,
                "m_ancho": (id_puerta == 0 && id_modelo == 'm') ? $('#m_ancho').val() : 0,
                "m_alto": (id_puerta == 0 && id_modelo == 'm') ? $('#m_alto').val() : 0,
                "f_ancho": (id_puerta == 0 && id_modelo == 'p') ? $('#f_ancho').val() : 0,
                "in_ancho": (id_puerta == 0 && id_modelo == 'p') ? $('#in_ancho').val() : 0,
                "f_alto": (id_puerta == 0 && id_modelo == 'p') ? $('#f_alto').val() : 0,
                "in_alto": (id_puerta == 0 && id_modelo == 'p') ? $('#in_alto').val() : 0,
                "pmodelo": id_pmodelo,
                "pcolor": id_pcolor,
                "pmovimiento": id_pmovimiento,
                "ptextura": id_ptextura,
                "psello": id_psello,
                "pmotor": id_pmotor,
                "ventana": id_ventana,
                "vcolor": id_vcolor,
                "nventanas": nventanas,
                "costo": costo_puerta
            };

            $.ajax({
                data: params,
                url: './ajax/addConcept.php',
                type: 'post',
                beforeSend: function() {
                    $("#cantidadP").parent().removeClass("has-error");
                    $("#iAddP").removeClass("fa-plus");
                    $("#iAddP").addClass("fa-spinner");
                    $("#iAddP").addClass("fa-spin");
                    $("#saveP").text("Agregando...");
                },
                success: function(response) {
                    subtotal();
                    impuestos();
                    total();

                    resgistrartotales();
                    
                    $("#iAddP").removeClass("fa-spinner");
                    $("#iAddP").removeClass("fa-spin");
                    $("#iAddP").addClass("fa-plus");
                    $("#saveP").text("Agregar");

                    $("#cantidadP").val('');
                    $("#id_ptipo").val('');
                    $("#id_puerta").html('<option value="">--------------------</option>');
                    
                    $("#id_pmodelo").html('<option value="">--------------------</option>');
                    $("#id_pcolor").html('<option value="">--------------------</option>');
                    $("#id_pmovimiento").html('<option value="">--------------------</option>');

                    $('#metros').hide();
                    $('#pies').hide()

                    $('#id_modelo').val(0);
                    $('#medida').hide();

                    $("#id_ptextura").val(1);
                    $("#id_psello").val(1);
                    $("#id_pmotor").val(1);

                    $('#id_ventana').html('<option value="">--------------------</option>');
                    $('#id_vcolor').html('<option value="">--------------------</option>');

                    $('#nventanas').val('');
                    $('#ventanas').hide();

                    $("#concepts-section").html(response);

                    $('html, body').animate({
                        scrollTop: $("#concepts-section").offset().top - 125
                    }, 500);
                }
            });
        });
    });
    // saveP

    // SaveC
    $("#saveC").click(function() {
        if (!checkInfo()) {
            return false;
        }

        var id_cotizacion = $('#id_cotizacion').val();

        var id_cliente = $('#id_cliente').val();
        var id_divisa = $('#id_divisa').val();
        var id_impuesto = $('#id_impuesto').val();

        var cantidad = $('#cantidadC').val();
        var id_grupo_componente = $('#id_grupo_componente').val();
        var id_componente = $('#id_componente').val();

        if (cantidad == '') {
            notify('No se ha ingresado la cantidad de componentes');
            
            return false;
        }

        if (id_grupo_componente == '') {
            notify('No se ha seleccionado un grupo de componentes');
            
            return false;
        }

        if (id_componente == '') {
            notify('No se ha seleccionado un componente');
            
            return false;
        }

        var precio = calculocomponente(id_cliente, id_divisa, id_impuesto, id_componente);
        
        precio.success(function(data) {
            costo_componente = data;

            params = {
                "id_cotizacion": id_cotizacion,
                "id_cliente": id_cliente,
                "id_divisa": id_divisa, 
                "cantidad": cantidad,
                "gcomp": id_grupo_componente,
                "comp": id_componente,
                "costo": costo_componente
            };

            $.ajax({
                url: './ajax/addConcept.php',
                type: 'post',
                data: params,
                beforeSend: function() {
                    $("#cantidadC").parent().removeClass("has-error");
                    $("#iAddC").removeClass("fa-plus");
                    $("#iAddC").addClass("fa-spinner");
                    $("#iAddC").addClass("fa-spin");
                    $("#saveC").text("Agregando...");
                },
                success: function(response) {
                    subtotal();
                    impuestos();
                    total();

                    resgistrartotales();
                    
                    $("#iAddC").removeClass("fa-spinner");
                    $("#iAddC").removeClass("fa-spin");
                    $("#iAddC").addClass("fa-plus");
                    $("#saveC").text("Agregar");

                    $("#cantidadC").val('');
                    $("#id_grupo_componente").val('');
                    $("#id_componente").html('<option value="">--------------------</option>');

                    $("#concepts-section").html(response);
                    
                    $('html, body').animate({
                        scrollTop: $("#concepts-section").offset().top - 125
                    }, 500);
                }
            });
        });
    });
    // SaveC

    // SaveK
    $("#saveK").click(function() {
        if (!checkInfo()) {
            return false;
        }

        var id_cotizacion = $("#id_cotizacion").val();

        var id_cliente = $('#id_cliente').val();
        var id_divisa = $('#id_divisa').val();
        var id_impuesto = $('#id_impuesto').val();

        var cantidad = $('#cantidadK').val();
        var id_kit = $('#id_kit').val();

        if (cantidad == '') {
            notify('No se ha ingresado la cantidad de kits');

            return false;
        }

        if (id_kit == '') {
            notify('No se ha seleccionado un kit');

            return false;
        }

        var precio = calculokit(id_cliente, id_divisa, id_impuesto, id_kit);
      
        precio.success(function (data) {
            costo_kit = data;

            params = {
                "id_cotizacion": id_cotizacion,
                "id_cliente": id_cliente,
                "id_divisa": id_divisa,
                "cantidad": cantidad,
                "kit": id_kit,
                "costo": costo_kit
            };

            $.ajax({
                url: './ajax/addConcept.php',
                type: 'post',
                data: params,
                beforeSend: function () {
                    $("#cantidadK").parent().removeClass("has-error");
                    $("#iAddK").removeClass("fa-plus");
                    $("#iAddK").addClass("fa-spinner");
                    $("#iAddK").addClass("fa-spin");
                    $("#saveK").text("Agregando...");
                },
                success: function(response) {
                    subtotal();
                    impuestos();
                    total();

                    resgistrartotales();
                    
                    $("#iAddK").removeClass("fa-spinner");
                    $("#iAddK").removeClass("fa-spin");
                    $("#iAddK").addClass("fa-plus");
                    $("#saveK").text("Agregar");

                    $("#cantidadK").val('');
                    $("#id_kit").val('');

                    $("#concepts-section").html(response);
                    
                    $('html, body').animate({
                        scrollTop: $("#concepts-section").offset().top - 125
                    }, 500);
                }
            });
        });
    });
    // SaveK

    // chgC
    $(document).on('click', '.chgC', function() {
        var id_concepto = $(this).data('id-con');
        var id_cotizacion = $('#id_cotizacion').val();

        var params = {
            'id_concepto': id_concepto,
            'id_cotizacion': id_cotizacion
        }

        $.ajax({
            url: './ajax/getConcept.php',
            type: 'post',
            dataType: 'json',
            data: params,
            success: function(response) {
                $('#modal_change_concept input[id=id_concepto]').val(response.id_concepto);
                $('#modal_change_concept input[id=id_cotizacion]').val(response.id_cotizacion);
                $('#modal_change_concept input[id=precio_unitario]').val(response.precio_unitario);

                $('#modal_change_concept').data('precio', response.precio_unitario);

                $('#modal_change_concept').modal('show');
            }
        });       
    });

    $(document).on('click', '#modal_change_concept button[id=save]', function() {
        var precio = $('#modal_change_concept').data('precio');

        var id_concepto = $('#modal_change_concept input[id=id_concepto]').val();
        var id_cotizacion = $('#modal_change_concept input[id=id_cotizacion]').val();
        var precio_unitario = $('#modal_change_concept input[id=precio_unitario]').val();

        if (precio_unitario == '') {
            notify('Introduzca un valor para el precio unitario');

            return false;
        }

        if (isNaN(precio_unitario)) {
            notify('Se requiere de un valor numerico para el precio unitario');

            return false;            
        }

        if (precio_unitario < precio) {
            notify('No se puede introducir una cantidad menor al precio original');

            return false;
        }

        var params = {
            'id_concepto': id_concepto,
            'id_cotizacion': id_cotizacion,
            'precio_unitario': precio_unitario
        }

        $.ajax({
            url: './ajax/updateConcept.php',
            type: 'post',
            dataType: 'json',
            data: params,
            success: function(response) {
                if (response.error) {
                    notify(response.error);

                    return false;
                }

                $('#modal_change_concept').modal('hide');

                getConceptos();

                // subtotal();
                // impuestos();
                // total();
            }
        });
    });

    // delC
    $(document).on('click', '.delC', function() {
        var idCon = $(this).data('id-con');
        var id_cotizacion = $("#id_cotizacion").val();
        
        var params = {
            'idCon': idCon,
            'id_cotizacion': id_cotizacion
        };

        $.ajax({
            url: './ajax/addConcept.php',
            type: 'post',
            data: params,
            success: function(response) {
                subtotal();
                impuestos();
                total();

                resgistrartotales();

                $('#concepts-section').html(response);
                
                $('html, body').animate({
                    scrollTop: $('#table-concepts').offset().top - 125
                }, 500);
            }
        });
    });
    // delC

    // save-quotation
    $("#save-quotation").click(function() {
        if (!checkInfo()) {
            return false;
        }

        if ($('#table-concepts > tbody > tr').length == 0) {
            notify('No se han agregado conceptos a la cotización');

            return false;
        }

        if ($('#id_termino_venta').val() == 0) {
            notify('Seleccione un término de venta');

            return false;
        }

        var cotizacion_subtotal = parseFloat(total_cotizacion.subtotal);
        var cotizacion_impuestos = parseFloat(total_cotizacion.impuestos);
        var cotizacion_total = parseFloat(total_cotizacion.total);
        
        var subtotal = parseFloat($('#subtotal').val());
        var impuestos = parseFloat($('#impuestos').val());
        var total = parseFloat($('#total').val());

        if (subtotal < cotizacion_subtotal) {
            notify('El subtotal no puede ser menor al calculado previamente');

            return;
        }

        if (impuestos < cotizacion_impuestos) {
            notify('El impuesto no puede ser menor al calculado previamente');

            return;
        }

        if (total < cotizacion_total) {
            notify('El total no puede ser menor al calculado previamente');

            return;
        }

        id_cotizacion = $('#id_cotizacion').val();

        document.forms[0].submit();
    });

    $('#cancel-quotation').click(function() {
        var id_cotizacion = $("#id_cotizacion").val();

        $.ajax({
            url: './ajax/deleteQuotation.php',
            type: 'post',
            data: { id_cotizacion: id_cotizacion },
            dataType: 'json',
            success: function(response) {
                if (response.error) {
                    console.log(response.error);
                    return false;
                }

                console.log(response.success);

                location.href = "listado-cotizaciones.php";
            },
            async: false
        });
    });

});

function mostrar(tipo){
    //alert(tipo);
    $('#imagen').attr('src','img/galeria-'+tipo+'.jpg');
    $('#modal_imagen').modal('show');
}

function validateMail(idMail) {
    //Creamos un objeto
    object = document.getElementById(idMail);
    valueForm = object.value;

    // Patron para el correo
    var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    if (valueForm.search(patron) == 0){
        //Mail correcto
        object.style.color = "#000";
        return;
    }
    //Mail incorrecto
    object.style.color = "#f00";
}

// Obtiene la direccion del cliente
function datosCliente(id_cliente) {
    if ((id_cliente == 0) || (id_cliente == null)) {
        $("#direccion-cliente").html('');
        return false;
    }

    $('#direccion-cliente').html('<div style="text-align:center; padding:20px;"><img src="img/loading.gif" alt="Espere por favor..." height="8" width="26" /></div>');

    $.ajax({
        type: 'post',
        url: 'ajax/datos-cliente-for-selects.php',
        data: 'id=' + id_cliente,
        dataType: 'json',
        async: false,
        success: function(datos) {
            if (datos.status == 'success') {
                $('#id_divisa').val(datos.cliente.divisa);
            }
        }
    });

    $.ajax({
        type: "POST",
        url: "ajax/datos-cliente.php",
        data: "id=" + id_cliente,
        async: false,
        success: function(datos) {
            $("#direccion-cliente").html(datos);
        }
    });
}

// Obtiene los terminos de venta
function terminosVenta(id_termino_venta) {
    if (id_termino_venta == 0) {
        $('#termino_venta').val('');

        return false;
    }

    $.ajax({
        type: 'post',
        url: 'ajax/getTerminosVentaById.php',
        data: { id_termino_venta: id_termino_venta },
        dataType: 'json',
        async: false,
        success: function(response) {
            if (response.error) {
                alert(response.error);

                return false;
            }

            console.log(response);

            $('#termino_venta').val(response.descripcion);
        }
    });
}

function clean() {
    $("#direccion-cliente").html('');
    $('#id_cliente option[value="0"]').attr("selected","selected"); 
}