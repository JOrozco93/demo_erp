<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST["id"]) && $_POST["id"] != "") {
    $id_cliente = $_POST["id"];
    $activo = 1;

    $stmt = $pdo->prepare("SELECT * FROM clientes WHERE id_cliente = :id_cliente AND activo = :activo");

    $stmt->bindParam(':id_cliente', $id_cliente);
    $stmt->bindParam(':activo', $activo);

    $result = $stmt->execute();

    if ($result) {
        $cliente = $stmt->fetch(PDO::FETCH_ASSOC);

        $response = [
            'status' => 'success',
            'message' => 'Cliente localizado',
            'cliente' => [
                'divisa' => $cliente['divisa_cliente']
            ]
        ];
    } else {
        $response = [
            'status' => 'error',
            'message' => 'No se ha podido localizar el cliente'
        ];
    }
} else {
    $response = [
        'status' => 'error',
        'message' => 'No se ha seleccionado un cliente'
    ];
}

echo json_encode($response);

?>
