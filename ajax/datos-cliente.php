<?php
require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST["id"])) {
    $id = $_POST["id"];
    if ($id != '') {
        $sql = 'SELECT * FROM clientes AS c, domicilios AS d WHERE c.id_cliente ='.$id.' AND c.id_domicilio = d.id_domicilio';
        $edit = mysql_fetch_array(consulta($sql));
    }     
    $pais = datoRapido("SELECT nombre AS dato FROM pais WHERE id_pais= ".$edit['id_pais']);   
    $divisa = datoRapido("SELECT CONCAT(abbrev,' - ',nombre) AS dato FROM divisas WHERE id_divisa= ".$edit['divisa_cliente']);
?>
     <div class="col-md-12" style="background-color:#eee;">
        <div class="col-md-4 "> 
            <h4>Datos Cliente</h4>                                                                              
            <p>
                <i class="fa fa-user"></i> &nbsp; Contacto: <?php echo $edit['nombre_contacto']; ?><br/>
                <i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit['telefono_contacto']; ?> <?php if($edit['ext_contacto']!=''){ ?> ext <?php echo $edit['ext_contacto']; } ?> <br/>   
                <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit['email_contacto']; ?>
            </p>
            <p><i class="fa fa-users"></i> &nbsp; Tipo: <?php echo ($edit['tipo']==1)?'Prospecto':'Cliente'; ?></p> 
            <?php if($edit['nombre_autorizado'] || $edit['telefono_autorizado'] || $edit['ext_autorizado'] || $edit['email_autorizado']){ ?>
            </div>
            <div class="col-md-4 ">  
            <h4>Personal autorizado para hacer pedidos</h4>                                    
            <p>
                <i class="fa fa-user"></i> &nbsp; Contacto: <?php echo $edit['nombre_autorizado']; ?><br/>
                <i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit['telefono_autorizado']; ?> <?php if($edit['ext_autorizado']!=''){ ?> ext <?php echo $edit['ext_autorizado']; } ?> <br/>   
                <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit['email_autorizado']; ?>
            </p>
            <?php } ?>
            </div>
            <div class="col-md-4 ">  
            <h4><i class="fa fa-map-pin"></i> &nbsp;<?php if($edit['calle'] || $edit['no_exterior'] || $edit['no_interior']) { echo $edit['calle']." ".$edit['no_exterior']." ".$edit['no_interior']; } ?></h4> 
            <?php if($edit['colonia']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['colonia']; ?></p>  <?php } ?>  
            <?php if($edit['referencia']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['referencia']; ?></p><?php } ?>  
            <?php if($edit['localidad'] || $edit['municipio'] || $edit['estado']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['localidad']." ".$edit['municipio']." ".$edit['estado']; ?></p><?php } ?>  
            <p> &nbsp; &nbsp; &nbsp; <?php echo $pais; ?> <?php if($edit['codigo_postal']){ echo "CP ".$edit['codigo_postal']; } ?></p>
            <!-- <span style="cursor:pointer" class="pull-right"><i class="fa fa-times"  onclick="clean();"></i></span> -->
        </div><!-- /.col-->                                      
    </div>
<?php  } ?>
