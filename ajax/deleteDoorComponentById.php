<?php

	if (!isset($_SESSION)) {
    	session_start();
	}

	include("../config.php");
	require_once("../includes/validacion.php");
	require_once("../includes/funciones_BD.php");

	$id_usuario = $_SESSION['id_usuario'];
	$id_puerta_componente = $_GET['id'];
	$result = deleteDoorComponentById($id_puerta_componente);
	echo json_encode([ 'result' => $result]);

?>