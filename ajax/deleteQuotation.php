<?php

	if (!isset($_SESSION)) {
    	session_start();
	}

	include("../config.php");
	require_once("../includes/validacion.php");
	require_once("../includes/funciones_BD.php");

	$id_usuario = $_SESSION['id_usuario'];

	$id_cotizacion = $_POST['id_cotizacion'];

	try {
		$rows_affected = $pdo->exec("DELETE FROM cotizaciones WHERE id_cotizacion = $id_cotizacion");  

		if ($rows_affected) {
			$pdo->exec("DELETE FROM conceptos WHERE id_cotizacion = $id_cotizacion");
		}

		$response = array(
			'success' => 'La cotización ha sido eliminada con exito'
		);
	} catch (PDOException $e) {
		$response = array(
			'error' => 'Ha ocurrido una error durante la eliminación de la cotización: ' . $e->getMessage()
		);
	}

	echo json_encode($response);

?>