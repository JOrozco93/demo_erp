<?php
if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$idgc = isset($_POST['idgc']) ? $_POST['idgc'] : "";

$sql = 'SELECT * FROM componentes WHERE  activo=1 AND id_componente_grupo = '.$idgc.' ORDER BY id_componente';
$components = consulta($sql);
?>
<select class="form-control" id="id_componente" name="id_componente">
    <option value="">--------------------</option>
    <?php while ($row1 = mysql_fetch_array($components)) { ?>
        <option value="<?php echo $row1['id_componente']; ?>"><?php echo $row1['nombre_componente']; ?></option>
    <?php } ?>
</select>
