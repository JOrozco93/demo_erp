<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");

$id_usuario = $_SESSION['id_usuario'];

$id_concepto = $_POST['id_concepto'];
$id_cotizacion = $_POST['id_cotizacion'];
	
$sql = "SELECT * FROM conceptos WHERE id_cotizacion = $id_cotizacion AND id_concepto = $id_concepto";

try {
	$stmt = $pdo->query($sql);

	if ($stmt->rowCount()) {
		$concepto = $stmt->fetch(PDO::FETCH_ASSOC);

		echo json_encode($concepto);
	} else {
		echo '{ "error": { "text": "No se ha localizado al cliente" } }';
	}
} catch(PDOException $e) {
	echo '{ "error": { "text": ' . $e->getMessage() . '} }'; 
}

?>