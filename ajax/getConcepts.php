<?php

session_start();

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cotizacion = isset($_POST['id_cotizacion']) ? $_POST['id_cotizacion'] : 0;

$sql = 'SELECT * FROM conceptos AS c WHERE c.id_cotizacion =' . $id_cotizacion . ' ORDER BY c.id_concepto';
$concepts = consulta($sql);
$numC = mysql_num_rows($concepts);
?>
<div>
    <?php if ($numC > 0): ?>
        <div class="table-responsive">
            <table id="table-concepts" class='table table-striped table-bordered'>
                <tr>
                    <th style="width: 10%;">Nombre</th>
                    <th style="width: 10%;">Cantidad</th>
                    <th style="width: 50%;">Descripción</th>
                    <th style="width: 10%;">Precio Unitario</th>
                    <th style="width: 10%;">Importe</th>
                    <th style="width: 10%;">Acciones</th>
                </tr>
                <?php while ($row1 = mysql_fetch_array($concepts)): ?>
                    <tr>
                        <?php
                            # Puerta
                            if (isset($row1['id_puerta'])) {
                                $sql2 = "SELECT * FROM puerta_tipo WHERE activo=1 AND id_ptipo = ". $row1['id_ptipo'];
                                $ptipo = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM puerta WHERE activo = 1 AND id_puerta = ". $row1['id_puerta'];
                                $puerta = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM puerta_modelo WHERE activo=1 AND id_pmodelo = ". $row1['id_pmodelo'];
                                $pmodelo = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM puerta_color WHERE activo=1 AND id_pcolor = ". $row1['id_pcolor'];
                                $pcolor = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM puerta_movimiento WHERE activo=1 AND id_pmovimiento = ". $row1['id_pmovimiento'];
                                $pmovimiento = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM puerta_textura WHERE activo=1 AND id_ptextura = ". $row1['id_ptextura'];
                                $ptextura = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM puerta_sello WHERE activo=1 AND id_psello = ". $row1['id_psello'];
                                $psello = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM puerta_motor WHERE activo=1 AND id_pmotor = ". $row1['id_pmotor'];
                                $pmotor = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM ventana WHERE id_ventana = {$row1['id_ventana']} AND activo = 1";
                                $pventana = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM ventana_color WHERE id_vcolor = {$row1['id_vcolor']} AND activo = 1";
                                $pvcolor = mysql_fetch_array(consulta($sql2));
                                $nventanas = $row1['nventanas'];
                        ?>
                            <td>Puerta</td>
                            <td><?php echo $row1['cantidad']; ?></td>
                            <td>
                                <strong>Tipo:</strong> <?php echo $ptipo['nombre_tipo']; ?> |
                                <strong>Medidas:</strong> 

                                <?php if ($row1['m_ancho'] == 0 && $row1['m_alto'] == 0 && $row1['f_ancho'] == 0 && $row1['in_ancho'] == 0 && $row1['f_alto'] == 0 && $row1['in_alto'] == 0) { ?>
                                    <?php echo $puerta['m_ancho'] . 'm' . 'x' . $puerta['m_alto'] . 'm'; ?>
                                <?php } ?>

                                <?php if ($row1['m_ancho'] != 0 || $row1['m_alto'] != 0) { ?>
                                    <?php echo $row1['m_ancho'] . 'm' . 'x' . $row1['m_alto'] . 'm'; ?>
                                <?php } ?>

                                <?php if ($row1['f_ancho'] != 0 || $row1['in_ancho'] != 0 || $row1['f_alto'] != 0 || $row1['in_alto'] != 0) { ?>
                                    <?php echo $row1['f_ancho'] . '\'' . $row1['in_ancho'] . '"' . 'x' . $row1['f_alto'] . '\'' . $row1['in_alto'] . "\""; ?>
                                <?php } ?>

                                <strong>Modelo:</strong> <?php echo $pmodelo['nombre_modelo']; ?> |
                                <strong>Color:</strong> <?php echo $pcolor['nombre_color']; ?> |
                                <strong>Movimiento:</strong> <?php echo $pmovimiento['nombre_movimiento']; ?> |
                                <strong>Textura:</strong> <?php echo $ptextura['nombre_textura']; ?> |
                                <strong>Sello:</strong> <?php echo $psello['nombre_sello']; ?> |
                                <strong>Motor:</strong> <?php echo $pmotor['nombre_motor']; ?> |
                                <strong>Ventana:</strong> <?php echo $pventana['nombre_ventana'] ? $pventana['nombre_ventana'] : 'Sin Ventana'; ?> |
                                <strong>Color ventana:</strong> <?php echo $pvcolor['nombre_color'] ? $pvcolor['nombre_color'] : 'Sin Color'; ?> |

                                <?php if ($nventanas > 0): ?>
                                    <strong># ventanas:</strong> <?php echo $nventanas ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo number_format($row1['precio_unitario'], 2, '.', ','); ?>
                            </td>
                            <td>
                                <?php echo number_format($row1['importe'], 2, '.', ','); ?>
                            </td>
                        <?php
                            # Componente
                            } elseif (isset($row1['id_componente'])) {
                                $sql2 = "SELECT * FROM componentes_grupos WHERE activo=1 AND id_grupo_componente = ". $row1['id_grupo_componente'];
                                $gcomp = mysql_fetch_array(consulta($sql2));
                                $sql2 = "SELECT * FROM componentes WHERE activo=1 AND id_componente = ". $row1['id_componente'];
                                $ccomp = mysql_fetch_array(consulta($sql2));
                        ?>
                            <td>Componente</td>
                            <td><?php echo $row1['cantidad']; ?></td>
                            <td>
                                <strong>Grupo:</strong> <?php echo $gcomp['grupo_componente']; ?> |
                                <strong>Componente:</strong> <?php echo htmlentities($ccomp['nombre_componente']); ?>
                            </td>
                            <td>
                                <?php echo number_format($row1['precio_unitario'], 2, '.', ','); ?>
                            </td>
                            <td>
                                <?php echo number_format($row1['importe'], 2, '.', ','); ?>
                            </td>
                        <?php
                            # Kit
                            } elseif (isset($row1['id_kit'])) {
                                $sql2 = "SELECT * FROM kit WHERE activo=1 AND id_kit = ". $row1['id_kit'];
                                $kkit = mysql_fetch_array(consulta($sql2));
                        ?>
                            <td>Kit</td>
                            <td><?php echo $row1['cantidad']; ?></td>
                            <td>
                                <strong>Kit:</strong> <?php echo $kkit['nombre_kit']; ?>
                            </td>
                            <td>
                                <?php echo number_format($row1['precio_unitario'], 2, '.', ','); ?>
                            </td>
                            <td>
                                <?php echo number_format($row1['importe'], 2, '.', ','); ?>
                            </td>
                        <?php } ?>
                        <td>
                            <button type="button" data-id-con="<?php echo $row1['id_concepto']; ?>" class="btn btn-warning chgC"><i class='fa fa-exchange'></i></button>
                            <button type="button" data-id-con="<?php echo $row1['id_concepto']; ?>" class="btn btn-danger delC"><i class='fa fa-times'></i></button>
                        </td>
                     </tr>
                <?php endwhile; ?>
            </table>
        </div>
    <?php else: ?>
        <p>No se han agregado conceptos a esta cotización</p>
    <?php endif; ?>
</div>
