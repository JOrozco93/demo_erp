<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cotizacion = $_POST['id_cotizacion'];
	
$sql = "SELECT pagoconsaldo, url_pago, comentarios_cotizacion, aprobada_motivo FROM cotizaciones WHERE id_cotizacion = $id_cotizacion";

try {
	$stmt = $pdo->query($sql);

	if ($stmt->rowCount()) {
		$cotizacion = $stmt->fetch(PDO::FETCH_ASSOC);

		echo $_GET['callback'] . "(" . json_encode($cotizacion) . ");";
	} else {
		echo '{ "error": { "text": "No se ha localizado al cliente" } }';
	}
} catch(PDOException $e) {
	echo '{ "error": { "text": ' . $e->getMessage() . '} }'; 
}

?>