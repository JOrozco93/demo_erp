<?php

	if (!isset($_SESSION)) {
    	session_start();
	}

	include("../config.php");
	require_once("../includes/validacion.php");
	require_once("../includes/funciones_BD.php");

	$id_usuario = $_SESSION['id_usuario'];
	
	$id_puerta_componente = $_GET['id'];

	$puerta_componente = getDoorComponentById($id_puerta_componente);

	echo json_encode($puerta_componente);

?>