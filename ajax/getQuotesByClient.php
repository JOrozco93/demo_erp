<?php

	if (!isset($_SESSION)) {
    	session_start();
	}

	include("../config.php");
	require_once("../includes/validacion.php");
	require_once("../includes/funciones_BD.php");

	$id_usuario = $_SESSION['id_usuario'];
	
	$id_cliente = (int) $_POST['id_client'];

	function getQuotesByIdClient($id_cliente) {
		global $pdo;

		$sql = "SELECT id_cotizacion, numero_cotizacion FROM cotizaciones WHERE activo = 1 AND id_cliente = $id_cliente";
		$result = $pdo->query($sql);
		
		return $result->fetchAll(PDO::FETCH_ASSOC);
	}


	$quotes = getQuotesByIdClient($id_cliente);

	echo json_encode($quotes);

?>