<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cliente = $_POST['id_cliente'];
	
$sql = "SELECT saldo FROM clientes WHERE id_cliente = $id_cliente";

try {
	$stmt = $pdo->query($sql);

	if ($stmt->rowCount()) {
		$cliente = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($cliente['saldo'] != 0) {
			echo $_GET['callback'] . "(" . json_encode([ 'saldo' => $cliente['saldo'] ]) . ");";
		} else {
			echo $_GET['callback'] . "(" . json_encode([ 'saldo' => 0 ]) . ");";
		}
	} else {
		echo '{ "error": { "text": "No se ha localizado al cliente" } }';
	}
} catch(PDOException $e) {
	echo '{ "error": { "text": ' . $e->getMessage() . '} }'; 
}

?>