<?php

	if (!isset($_SESSION)) {
    	session_start();
	}

	include("../config.php");
	require_once("../includes/validacion.php");
	require_once("../includes/funciones_BD.php");

	$id_usuario = $_SESSION['id_usuario'];
	
	$id_termino_venta = (int) $_POST['id_termino_venta'];

	$sql = "SELECT * FROM terminos_venta WHERE id_termino_venta = $id_termino_venta";
		
	$result = $pdo->query($sql);

	if ($result->rowCount()) {
		$response = $result->fetch(PDO::FETCH_ASSOC);
	} else {
		$response = array("error" => "Ha ocurrido un error durante la obtención del termino de venta");
	}

	echo json_encode($response);

?>