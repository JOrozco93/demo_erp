<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cotizacion = $_POST['id_cotizacion'];
	
$sql = "SELECT total FROM cotizaciones WHERE id_cotizacion = $id_cotizacion";

try {
	$stmt = $pdo->query($sql);

	if ($stmt->rowCount()) {
		$cotizacion = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($cotizacion['total'] != 0) {
			echo $_GET['callback'] . "(" . json_encode([ 'total' => $cotizacion['total'] ]) . ");";
		} else {
			echo $_GET['callback'] . "(" . json_encode([ 'total' => 0 ]) . ");";
		}
	} else {
		echo '{ "error": { "text": "No se ha localizado la cotización" } }';
	}
} catch(PDOException $e) {
	echo '{ "error": { "text": ' . $e->getMessage() . '} }';
}

?>