<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

// include 'config.php';

$id_cliente = $_POST['id_cliente'];
$id_impuesto = $_POST['id_impuesto'];

$sql = "SELECT * FROM impuestos WHERE id_impuesto = $id_impuesto";

try {
	$dbh = new PDO("mysql:host=$host;dbname=$db", $user, $password);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = $dbh->query($sql);  
	$rows = $stmt->fetchAll(PDO::FETCH_OBJ);
	$dbh = null;

	echo $_GET['callback'] . "(" . json_encode($rows) . ");";
} catch(PDOException $e) {
	echo '{"error":{"text":'. $e->getMessage() .'}}'; 
}

?>