<?php
header("Cache-Control: no-cache, must-revalidate");

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$idk = $_POST['idk'];

$sql_drop = sprintf("UPDATE `kit` SET `activo`=0 WHERE id_kit = %s", GetSQLValueString($idk, "int"));
$result = mysql_query($sql_drop, $db_con) or die("Problemas en la consulta" . $sql_drop);

$detalle = "El usuario con el id " . $id_usuario . "  eliminó un kit con el id " . $idk . " ";
$evento = "Eliminar";
$id_log = actualizalog($id_usuario, $evento, $detalle);
$sql_update = sprintf("UPDATE `kit` SET `id_log`=%s WHERE id_kit =%s", GetSQLValueString($id_log, "text"), GetSQLValueString($idk, "int"));
$result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta" . $sql_update);

$sql_KC = sprintf("SELECT * FROM `kit_componente` WHERE `activo`=1 AND id_kit = %s", GetSQLValueString($idk, "int"));
$num_KC = mysql_num_rows(consulta($sql_KC));
if ($num_KC > 0) {
  while ($row_KC = mysql_fetch_array(consulta($sql_KC))) {
    $detalle = "El usuario con el id " . $id_usuario . "  eliminó un kit_componente con el id " . $row_KC['id_kit_componente'] . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `kit_componente` SET activo=0 AND `id_log`=%s WHERE id_kit =%s", GetSQLValueString($id_log, "text"), GetSQLValueString($idk, "int"));
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta" . $sql_update);
  }
}
?>
"1"
