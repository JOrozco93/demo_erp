<?php
header("Cache-Control: no-cache, must-revalidate");

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];
$idk = $_POST['idk'];
$sqlCc = "SELECT * FROM componentes c WHERE c.activo = 1 AND c.id_componente NOT IN (SELECT kc.id_componente FROM kit_componente kc WHERE kc.activo = 1 AND kc.id_kit = ". $idk .")";
$queryCc = consulta($sqlCc);
$numCc = mysql_num_rows($queryCc);
if ($numCc > 0) {
    ?>
    <table class="table tblCmp">
      <thead>
        <tr>
          <th></th>
          <th>Nombre</th>
          <th>Precio</th>
          <th>Impuesto</th>
        </tr>
      </thead>

    <?php
    while ($rowCc = mysql_fetch_array($queryCc)) {
      ?>
      <tr id="trCmp_<?php echo $rowCc['id_componente']; ?>">
        <td>
          <a data-placement="left" data-toggle="tooltip" title="Agregar" href="#" class="btn btn-primary btn-sm addKC" data-id="<?php echo $rowCc['id_componente'] ?>"><i class="fa fa-plus"></i></a>
        </td>
        <td><?php echo htmlentities($rowCc['nombre_componente']); ?></td>
        <td>$ <?php echo number_format($rowCc['precio'], 2, '.', ','); ?></td>
        <td><?php echo $rowCc['impuesto']; ?></td>
      </tr>
      <?php
    }
    ?>
    </table>
    <?php
} else {
  ?>
    <p><em>No hay componentes en esta lista</em></p>
  <?php
}
