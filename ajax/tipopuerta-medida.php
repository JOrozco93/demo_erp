<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST["id"])) {
    $id = $_POST["id"];
    
    if ($id != '') {
        $sql = 'SELECT * FROM puerta WHERE activo = 1 AND id_ptipo = ' . $id . '';
        $dbh = new PDO("mysql:host=$host;dbname=$db", $user, $password);    
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $dbh->query($sql);  
        $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    }  
    
?>
                
<option value="">Selecciona una medida ...</option>
<option value="">--------------------</option>
<option value="0">Puerta a la Medida</option>
<option value="">--------------------</option>
<?php foreach ($rows as $row) { ?>
    <option value="<?php echo $row->id_puerta; ?>"><?php echo $row->m_ancho.'m';?>x<?php echo $row->m_alto.'m';?>/<?php echo $row->f_ancho."'";?><?php echo $row->in_ancho.'"';?>x<?php echo $row->f_alto."'";?><?php echo $row->in_alto.'"';?></option>
<?php } ?>

<?php } ?>