<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST["id"])) {
    $id = $_POST["id"];
    
    if ($id != '') {
        $stmt = $pdo->query("SELECT * FROM puerta_modelo WHERE activo = 1 AND id_ptipo = $id");  
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }  
    
?>
                
<option value="">Selecciona una modelo ...</option>
<?php foreach ($rows as $row) { ?>
    <option value="<?php echo $row['id_pmodelo']; ?>" <?php if ($row['por_default']) { echo 'selected'; } ?>><?php echo $row['nombre_modelo']; ?></option>
<?php } ?>

<?php } ?>
