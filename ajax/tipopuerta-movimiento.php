<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST['id']) && $_POST['id'] != '') {
	$id = $_POST['id'];

    $stmt = $pdo->query("SELECT * FROM puerta_movimiento WHERE activo = 1 AND id_ptipo = $id");  
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

	<option value="">Selecciona una modelo ...</option>
	<?php foreach ($rows as $row) { ?>
	    <option value="<?php echo $row['id_pmovimiento']; ?>" <?php if ($row['por_default']) { echo 'selected'; } ?>><?php echo $row['nombre_movimiento']; ?></option>
	<?php } ?>

<?php } ?>
