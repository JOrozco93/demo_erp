<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST['id']) && $_POST['id'] != null) {
    $id = $_POST['id'];

    $stmt = $pdo->query("SELECT * FROM ventana_color WHERE activo = 1");
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<option value="">Selecciona un color de ventana ...</option>
<option value="">--------------------</option>
<option value="0">Sin color</option>
<option value="">--------------------</option>
<?php foreach ($rows as $row) { ?>
    <option value="<?php echo $row['id_vcolor']; ?>"><?php echo $row['nombre_color']; ?></option>
<?php } ?>

<?php } ?>