<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST['id']) && $_POST['id'] != null) {
    $id_puerta = $_POST['id'];

    $stmt = $pdo->query("SELECT * FROM puerta WHERE id_puerta = $id_puerta LIMIT 1");
	$puerta = $stmt->fetch(PDO::FETCH_ASSOC);

	$response = [
		'status' => 'success',
		'nventanas' => $puerta['nventanas']
	];
} else {
	$response = [
		'status' => 'error',
		'message' => 'No se ha proporcionado un id para buscar la puerta'
	];
}

echo json_encode($response);

?>