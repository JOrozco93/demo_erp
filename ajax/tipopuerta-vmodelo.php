<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");

if (isset($_POST['id']) && $_POST['id'] != null) {
    $id = $_POST['id'];

    $stmt = $pdo->query("SELECT * FROM ventana WHERE activo = 1 AND id_ptipo = $id");
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<option value="">Selecciona un modelo de ventana ...</option>
<option value="">--------------------</option>
<option value="0">Sin ventana</option>
<option value="">--------------------</option>
<?php foreach ($rows as $row) { ?>
    <option value="<?php echo $row['id_ventana']; ?>"><?php echo $row['nombre_ventana']; ?></option>
<?php } ?>

<?php } ?>