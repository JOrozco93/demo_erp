<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");
require_once("../includes/validacion.php");

$id_sello = $_POST['id_sello'];
$fpago = $_POST['fpago'];
$cantidad = $_POST['cantidad'];
$pta_medida = $_POST['pta_medida'];

if ($id_sello != 0) {
    $sql = 'SELECT * FROM `puerta_medida` WHERE `id_medida` =' . $pta_medida;
    $edit = mysql_fetch_array(consulta($sql));

    $sql1 = 'SELECT ' . $fpago . ' FROM `componentes` WHERE `id_componente` =' . $id_sello;
    $edit1 = mysql_fetch_array(consulta($sql1));
    if ($id_sello == 7) {
        $total_medida = ((int) $edit['metros_alto']) * ((int) $edit['metros_ancho']) * ((int) $edit['metros_alto']);
        $ttPago = (((int) $edit1[$fpago]) * ((int) $total_medida)) * ((int) $cantidad);
    } else {
        $total_medida = ((int) $edit['metros_ancho']);
        $ttPago = (((int) $edit1[$fpago]) * ((int) $total_medida)) * ((int) $cantidad);
    }
    echo (is_numeric($ttPago)) ? $ttPago = number_format($ttPago, 2, '.', ',') : $ttPago = '0';
} else {
    echo $ttPago = 0;
}
?>