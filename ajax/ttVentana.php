<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");
require_once("../includes/validacion.php");

$id_ventana = $_POST['id_ventana'];
$fpago = $_POST['fpago'];
$pta_medida = $_POST['pta_medida'];
$tipo_puerta = $_POST['tipo_puerta'];
$cantidad = $_POST['cantidad'];

if ($id_ventana != 0) {
    $sql = 'SELECT cantidad FROM `ventana_medida` WHERE `id_medida` =' . $pta_medida;
    $edit = mysql_fetch_array(consulta($sql));
    
    $sql1 = 'SELECT ' . $fpago . ' FROM `ventana_tipo` WHERE `id_tipo` =' . $tipo_puerta;
    $edit1 = mysql_fetch_array(consulta($sql1));
    $ttPago = (((int) $edit1[$fpago]) * ((int) $edit['cantidad']))*((int)$cantidad);
    echo (is_numeric($ttPago)) ? $ttPago = number_format($ttPago, 2, '.', ',') : $ttPago = '0';
} else {
    echo $ttPago = 0;
}
?>