<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");

$id_usuario = $_SESSION['id_usuario'];

$id_concepto = $_POST['id_concepto'];
$id_cotizacion = $_POST['id_cotizacion'];
$precio_unitario = $_POST['precio_unitario'];
	
$sql = "SELECT * FROM conceptos WHERE id_cotizacion = $id_cotizacion AND id_concepto = $id_concepto";

try {
	$stmt = $pdo->query($sql);

	if ($stmt->rowCount()) {
		$concepto = $stmt->fetch(PDO::FETCH_ASSOC);

		$cantidad = $concepto['cantidad'];
		$punitario = $precio_unitario;
		$importe = $cantidad * $punitario;

		$pdo->query("UPDATE conceptos SET precio_unitario = $punitario, importe = $importe WHERE id_cotizacion = $id_cotizacion AND id_concepto = $id_concepto");

		echo json_encode(["status" => "success"]);
	} else {
		echo json_encode(["error" => "No se ha localizado el concepto"]);
		// echo '{ "error": { "text": "No se ha localizado al cliente" } }';
	}
} catch(PDOException $e) {
	echo json_encode(["error" => $e->getMessage()]);
	// echo '{ "error": { "text": ' . $e->getMessage() . '} }'; 
}

?>