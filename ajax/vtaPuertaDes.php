<?php
require_once("../config.php");
require_once("../includes/funciones_BD.php");
require_once("../includes/validacion.php");

$tpuerta = $_POST['id'];
$opt_med = $_POST['opt_med'];
?>
<script>
    $(function () {

        //on KeyPress, integer value validation
        $('.integer_fields').keypress(function (event)
        {
            //alert("123");
            var validKey = [8, 9, 13];
            var charCode = (event.which) ? event.which : event.keyCode;
//		alert(charCode);
            if (validKey.indexOf(charCode) != -1)
                return true;
            if (charCode < 48 || charCode > 57)
            {
                event.preventDefault();
            }
        });

        //on KeyPress, float value validation
        $('.decimal_fields').keypress(function (event) {
            var validKey = [8, 9, 13];
            var charCode = (event.which) ? event.which : event.keyCode;
//		alert(charCode);
            if (validKey.indexOf(charCode) != -1)
                return true;
            if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57))
            {
                event.preventDefault();
            }
        });
    });
</script>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-3">
            <label>
                Cantidad:
            </label>
            <input class="form-control integer_fields" name="cantidad" id="cantidad"/>
        </div>
        <div class="col-sm-3">
            <label>
                Modelo:
            </label>
            <select class="form-control" name="pta_modelo" id="pta_modelo" required="">
                <option value="">[Seleccione un Modelo]</option>
                <?php
                $sql_ptaModelo = "SELECT * FROM puerta_modelo WHERE activo=1 AND id_tipo=" . $tpuerta . " ORDER BY id_modelo";
                $query_ptaModelo = consulta($sql_ptaModelo);
                $num_ptaModelo = mysql_num_rows($query_ptaModelo);
                if ($num_ptaModelo > 0) {
                    while ($row_ptaModelo = mysql_fetch_array($query_ptaModelo)) {
                        ?>
                        <option value="<?php echo $row_ptaModelo['id_modelo']; ?>"><?php echo $row_ptaModelo['nom_modelo']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>


        <?php if ($opt_med == '1') { ?>
            <div class="col-sm-3">
                <label>
                    Medida (Metros/Pies):
                </label>
                <select class="form-control" name="pta_medida" id="pta_medida" required="" onchange="ttPuerta(this.value, $('#fpago').val(), $('#cantidad').val());">
                    <option value="">[Seleccione una Medida]</option>
                    <?php
                    $sql_ptaMed = "SELECT * FROM puerta_medida WHERE activo=1 AND id_tipo=" . $tpuerta . " ORDER BY id_medida";
                    $query_ptaMed = consulta($sql_ptaMed);
                    $num_ptaMed = mysql_num_rows($query_ptaMed);
                    if ($num_ptaMed > 0) {
                        while ($row_ptaMed = mysql_fetch_array($query_ptaMed)) {
                            ?>
                            <option value="<?php echo $row_ptaMed['id_medida']; ?>"><?php echo $row_ptaMed['metros_ancho'] . 'x' . $row_ptaMed['metros_alto'] . ' / ' . $row_ptaMed['pies_ancho'] . "'" . $row_ptaMed['pies_alto'] . '"'; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <input name="med1" id="med1" type="hidden" value=""/>
                <input name="med2" id="med2" type="hidden" value=""/>
                <input name="md" id="md" type="hidden" value=""/>
            </div>
        <?php } else if ($opt_med == '2') { ?>
            <div class="col-sm-3">
                <label>
                    Medida (Metros/Pies):
                </label>
                <select class="form-control" name="md" id="md" onchange="medPta($('#med1').val(), $('#med2').val(), this.value,<?php echo $tpuerta ?>);">
                    <option value="">[Seleccione una Medida]</option>
                    <option value="1">Metros</option>
                    <option value="2">Pies</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label>Ingrese la Medida:</label>
                <div id="verMts" style="display: none">
                    <div class="form-group">
                        <!--<label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>-->
                        <div class="input-group">                        
                            <input type="text" class="form-control" name="med1" id="med1" name="med2" id="med2" onchange="medPta(this.value, this.value, $('#md').val(),<?php echo $tpuerta ?>);"/>
                            <div class="input-group-addon">x</div>                        
                            <input type="text" class="form-control" name="med2" id="med2" onchange="medPta($('#med1').val(), this.value, $('#md').val(),<?php echo $tpuerta ?>);">
                            <!--<div class="input-group-addon">.00</div>-->
                        </div>
                    </div>
                </div>
                <div id="verPies" style="display: none">
                    <div class="form-group">
                        <!--<label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>-->
                        <div class="input-group">                        
                            <input type="text" class="form-control" name="med1" id="med1" name="med2" id="med2" onchange="medPta(this.value, this.value, $('#md').val(),<?php echo $tpuerta ?>);"/>
                            <div class="input-group-addon">'</div>                        
                            <input type="text" class="form-control" name="med2" id="med2" onchange="medPta($('#med1').val(), this.value, $('#md').val(),<?php echo $tpuerta ?>);">
                            <div class="input-group-addon">"</div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="pta_medida" id="pta_medida" />
            </div>
        <?php } ?>



    </div>
</div>
<br/>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-3">
            <label>
                Textura:
            </label>
            <select class="form-control" name="pta_textura" id="pta_textura" required="">
                <option value="">[Seleccione una Textura]</option>
                <?php
                $sql_ptaTex = "SELECT * FROM puerta_textura WHERE activo=1 ORDER BY id_textura";
                $query_ptaTex = consulta($sql_ptaTex);
                $num_ptaTex = mysql_num_rows($query_ptaTex);
                if ($num_ptaTex > 0) {
                    while ($row_ptaTex = mysql_fetch_array($query_ptaTex)) {
                        ?>
                        <option value="<?php echo $row_ptaTex['id_textura']; ?>"><?php echo $row_ptaTex['nom_textura']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-sm-3">
            <label>
                Movimiento o Elevaci&oacute;n:
            </label>
            <select class="form-control" name="pta_mov" id="pta_mov" required="">
                <option value="">[Seleccione una Movimiento o Elevaci&oacute;n]</option>
                <?php
                $sql_ptaMov = "SELECT * FROM puerta_mov WHERE activo=1 AND id_tipo=" . $tpuerta . " ORDER BY id_mov";
                $query_ptaMov = consulta($sql_ptaMov);
                $num_ptaMov = mysql_num_rows($query_ptaMov);
                if ($num_ptaMov > 0) {
                    while ($row_ptaMov = mysql_fetch_array($query_ptaMov)) {
                        ?>
                        <option value="<?php echo $row_ptaMov['id_mov']; ?>"><?php echo $row_ptaMov['nom_mov']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-sm-3">
            <label>
                Color:
            </label>
            <select class="form-control" name="pta_color" id="pta_color" required="">
                <option value="">[Seleccione una Color]</option>
                <?php
                $sql_ptaColor = "SELECT * FROM puerta_color WHERE activo=1 ORDER BY id_color";
                $query_ptaColor = consulta($sql_ptaColor);
                $num_ptaColor = mysql_num_rows($query_ptaColor);
                if ($num_ptaColor > 0) {
                    while ($row_ptaColor = mysql_fetch_array($query_ptaColor)) {
                        ?>
                        <option value="<?php echo $row_ptaColor['id_color']; ?>"><?php echo $row_ptaColor['nom_color']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
            <label>Total</label>
            <div class="input-group">
                <div class="input-group-addon">
                    $
                </div>
                <input type="text" class="form-control" id="total_puerta" name="total_puerta" onchange="valPuerta(this.value, $('#pta_medida').val(), $('#fpago').val(), $('#cantidad').val())
                                ;"/>
            </div>
        </div>
    </div>
</div>