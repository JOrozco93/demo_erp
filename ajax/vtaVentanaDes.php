<?php
require_once("../config.php");
require_once("../includes/funciones_BD.php");
require_once("../includes/validacion.php");

$tpuerta = $_POST['id'];
?>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-3">
            <label>
                Modelo:
            </label>
            <select class="form-control" name="vta_modelo" id="vta_modelo" required="" onchange="ttVentana(this.value, $('#fpago').val(), $('#pta_medida').val(), $('#tipo_puerta').val(), $('#cantidad').val());">
                <option value="">[Seleccione un Modelo]</option>
                <option value="0">Sin Ventana</option>
                <?php
                $sql_vtaModelo = "SELECT * FROM ventana_modelo WHERE activo=1 AND id_tipo=" . $tpuerta . " ORDER BY id_modelo";
                $query_vtaModelo = consulta($sql_vtaModelo);
                $num_vtaModelo = mysql_num_rows($query_vtaModelo);
                if ($num_vtaModelo > 0) {
                    while ($row_vtaModelo = mysql_fetch_array($query_vtaModelo)) {
                        ?>
                        <option value="<?php echo $row_vtaModelo['id_modelo']; ?>"><?php echo $row_vtaModelo['nom_modelo']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-lg-3">
            <label>
                Color:
            </label>
            <select class="form-control" name="vta_color" id="vta_color" required="">
                <option value="">[Seleccione el Color de Ventana]</option>
                <option value="0">Sin Ventanas</option>
                <?php
                $sql_vtaColor = "SELECT * FROM ventana_color WHERE activo=1 ORDER BY id_color";
                $query_vtaColor = consulta($sql_vtaColor);
                $num_vtaColor = mysql_num_rows($query_vtaColor);
                if ($num_vtaColor > 0) {
                    while ($row_vtaColor = mysql_fetch_array($query_vtaColor)) {
                        ?>
                        <option value="<?php echo $row_vtaColor['id_color']; ?>"><?php echo $row_vtaColor['nom_color']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6"></div>
        <div class="col-lg-6">
            <label>Total</label>
            <div class="input-group">
                <div class="input-group-addon">
                    $
                </div>
                <input type="text" class="form-control" id="total_ventana" name="total_ventana" onchange="valVentana(this.value, $('#vta_modelo').val(), $('#fpago').val(), $('#pta_medida').val(), $('#tipo_puerta').val(), $('#cantidad').val());"/>
            </div>
        </div>
    </div>
</div>