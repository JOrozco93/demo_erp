<?php

    error_reporting(E_ALL & ~E_NOTICE);

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == NULL)) {
        header("Location: logout.php");
    }

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    $id_usuario = $_SESSION['id_usuario'];

    if (isset($_GET["tipo"])) {
        $tipo = $_GET['tipo'];
    } else {
        $tipo = NULL;
    }

    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        
        if ($id != 0) {
            $sql = 'SELECT * FROM clientes AS c, domicilios AS d WHERE c.id_cliente ='.$id.' AND c.id_domicilio = d.id_domicilio';
            $edit = mysql_fetch_assoc(consulta($sql));
        }
    }

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $name; ?></title>
    <?php include ("includes/css.php"); ?>
    <link href="plugins/data-tables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <link href="css/jquery-ui.min.css" rel="stylesheet">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <?php include ("includes/header.php"); ?>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php include ("includes/menu.php"); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="theme-panel hidden-xs hidden-sm">

                    <div class="toggler-close">
                    </div>

                </div>
                <!-- END STYLE CUSTOMIZER -->
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title"> Alta de <?php echo ($tipo =='prospecto') ?'Prospectos' : 'Clientes'; ?> </h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="listado-clientes.php">
                                    Clientes
                                </a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <?php if($tipo =='prospecto'){ ?>
                            <li><a href="listado-prospectos.php">Prospectos</a><i class="fa fa-angle-right"></i></li>
                            <?php } else { ?>
                            <li><a href="listado-clientes.php">Clientes</a><i class="fa fa-angle-right"></i></li>
                            <?php } ?>
                            <li><a href="#">Alta</a></li>                            
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN VALIDATION STATES-->
                        <div class="portlet box blue">
                            <!--<div class="portlet-title"></div>-->
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form action="alta/cliente.php" id="formPrincipal" name="formPrincipal" class="form-horizontal" method="POST">  
                                    <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />   
                                    <input type="hidden" id="id_domicilio" name="id_domicilio" value="<?php echo $edit['id_domicilio']; ?>" /> 
                                    <input type="hidden" id="btn" name="btn" value="<?php echo ($id==0)?'guardar':'modificar';?>" />            
                                    <div class="portlet box blue">
                                        <div class="portlet-title">                            
                                        </div>
                                        <div class="portlet-body form">
                                            <br/>
                                            <div class="row">
                                                <div class="col-sm-5">                                                           
                                                    <div class="col-sm-12">
                                                        <div class="input-group">   
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-building"></i>
                                                            </div>                                      
                                                            <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" required placeholder="Nombre del <?php echo ($tipo =='prospecto') ? 'prospecto':'cliente'; ?>" value="<?php echo ($id != '') ? $edit['nombre_cliente'] : ''; ?>">
                                                        </div>  <p>&nbsp;</p>                                       
                                                        <?php if ($id != 0){?><input type="hidden" name="tipo" value="<?php echo $edit['tipo'];?>" />
                                                        <?php } else if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2')) { ?>

                                                        <select class="form-control" id="tipo" name="tipo" <?php echo ($id != 0) ? 'disabled=""' : ''; ?> >
                                                            <option <?php echo ($tipo == 'prospecto') ? 'selected=""':($edit['tipo']=='1') ? 'selected=""' : ''; ?> value="1">Prospecto</option>
                                                            <option <?php echo ($tipo == 'cliente') ? 'selected=""' : ($edit['tipo']=='2') ? 'selected=""' : ''; ?> value="2">Cliente</option>
                                                        </select><br />
                                                        <?php } else { ?>
                                                        <input type="hidden" name="tipo" value="1" />
                                                        <?php } ?>
                                                        <label>Divisa</label>
                                                         <select class="form-control" id="id_divisa" name="id_divisa">
                                                            <option value="<?php echo $GLOBAL_id_divisa; ?>"><?php echo $GLOBAL_divisa; ?></option>
                                                            <option value="" disabled="">--------------------</option>
                                                            <?php
                                                            $sql1 = "SELECT * FROM divisas ORDER BY abbrev ASC";
                                                            $query1 = consulta($sql1);
                                                            $num1 = mysql_num_rows($query1);
                                                            if ($num1 > 0) {
                                                                while ($row1 = mysql_fetch_array($query1)) {
                                                                    ?>
                                                                    <option <?php echo ($id != '') ? ($row1['id_divisa'] == $edit['id_divisa']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_divisa']; ?>"><?php echo $row1['abbrev']; ?> - <?php echo $row1['nombre']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <br/>
                                                        <label>Tipo de costo</label>
                                                        <select class="form-control" id="tipo_costo" name="tipo_costo" <?php echo ($edit['aprobado'] == '1' && !($_SESSION["permiso_usuario"] == 1 || $_SESSION["permiso_usuario"]== 2 || $_SESSION["permiso_usuario"] == 4)) ? 'disabled:""' : ''; ?>>                                                           
                                                            <option value="VA" <?php if ($edit['tipo_costo'] == 'VA') { echo 'selected'; } ?>>VA</option>
                                                            <option value="VB" <?php if ($edit['tipo_costo'] == 'VB') { echo 'selected'; } ?>>VB</option>
                                                            <option value="VC" <?php if ($edit['tipo_costo'] == 'VC') { echo 'selected'; } ?>>VC</option>
                                                            <option value="VD" <?php if ($edit['tipo_costo'] == 'VD') { echo 'selected'; } ?>>VD</option>
                                                            <option value="VE" <?php if ($edit['tipo_costo'] == 'VE') { echo 'selected'; } ?>>VE</option>
                                                            <option value="VF" <?php if ($edit['tipo_costo'] == 'VF') { echo 'selected'; } ?>>VF</option>
                                                        </select>
                                                        <br />
                                                        <label class="control-label">Motivo del costo</label>
                                                        <br />
                                                        <textarea class="form-control" id="motivo_costo" name="motivo_costo" rows="4" placeholder="Motivo"><?php echo ($id != '') ? $edit['motivo_costo'] : ''; ?></textarea>
                                                    </div>                         
                                                    <p>&nbsp;</p>
                                                    <div class="col-sm-12">
                                                        <h3>Contacto</h3>
                                                        <div class="input-group">   
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>                                      
                                                            <input type="text" class="form-control" id="nombre_contacto" name="nombre_contacto" required placeholder="Nombre del contacto" value="<?php echo ($id != '') ? $edit['nombre_contacto'] : ''; ?>">
                                                        </div>  
                                                        <p>&nbsp;</p>  
                                                        <div class="input-group">   
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-envelope"></i>
                                                            </div> 
                                                            <input type="text" class="form-control" id="email_contacto" name="email_contacto" required placeholder="Email" value="<?php echo ($id != '') ? $edit['email_contacto'] : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">                                        
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-phone"></i>
                                                            </div>
                                                            <input data-inputmask='"mask": "99 9999 9999"' data-mask type="text" placeholder="12 3456 7890"  class="form-control" id="telefono_contacto" name="telefono_contacto" value="<?php echo ($id != '') ? $edit['telefono_contacto'] : ''; ?>"/>
                                                        </div>
                                                    </div>                                    
                                                    <div class="col-sm-4">                                                                        
                                                        <input class="form-control" id="ext_contacto" name="ext_contacto" placeholder="Extensi&oacute;n" value="<?php echo ($id != '') ? $edit['ext_contacto'] : ''; ?>"/>
                                                    </div>   
                                                    <p>&nbsp;</p>

                                                    <div class="col-sm-12">
                                                        <h3>Personal Autorizado</h3>
                                                        <div class="input-group">   
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>                                      
                                                            <input type="text" class="form-control" id="nombre_autorizado" name="nombre_autorizado"  placeholder="Nombre de persona autorizada a hacer pedidos" value="<?php echo ($id != '') ? $edit['nombre_autorizado'] : ''; ?>">
                                                        </div>  
                                                        <p>&nbsp;</p>  
                                                        <div class="input-group">   
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-envelope"></i>
                                                            </div> 
                                                            <input type="text" class="form-control" id="email_autorizado" name="email_autorizado"  placeholder="Email" value="<?php echo ($id != '') ? $edit['email_autorizado'] : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">                                        
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-phone"></i>
                                                            </div>
                                                            <input data-inputmask='"mask": "99 9999 9999"' data-mask type="text" placeholder="12 3456 7890"  class="form-control" id="telefono_autorizado" name="telefono_autorizado" value="<?php echo ($id != '') ? $edit['telefono_autorizado'] : ''; ?>"/>
                                                        </div>
                                                    </div>                                    
                                                    <div class="col-sm-4">                                                                        
                                                        <input class="form-control" id="ext_autorizado" name="ext_autorizado" placeholder="Extensi&oacute;n" value="<?php echo ($id != '') ? $edit['ext_autorizado'] : ''; ?>"/>
                                                    </div> 
                                                    <p>&nbsp;</p>                     
                                                </div>                     

                                                <div class="col-sm-5 col-sm-offset-1 well">                                 
                                                    <div class="col-sm-12">                                                      
                                                        <h2>Datos fiscales</h2>
                                                        <label>Razón Social</label>
                                                        <input type="text" class="form-control" id="razon_social" name="razon_social"  placeholder="Razon Social" value="<?php echo ($id != '') ? $edit['razon_social'] : ''; ?>" >
                                                        <br />
                                                        <label>No. Registro Fiscal</label>
                                                        <input type="text" class="form-control" id="rfc" name="rfc"  placeholder="No. Registro Fiscal" maxlength="15" value="<?php echo ($id != '') ? $edit['rfc'] : ''; ?>" >
                                                        <br />
                                                        <label>Calle</label>
                                                        <input type="text" class="form-control" id="calle" name="calle"  placeholder="Calle" value="<?php echo ($id != '') ? $edit['calle'] : ''; ?>" >
                                                    </div>                                    
                                                    <div class="col-sm-6">
                                                        <br />
                                                        <label> # Exterior</label>
                                                        <input type="text" class="form-control" id="no_exterior" name="no_exterior"  placeholder="# Exterior" value="<?php echo ($id != '') ? $edit['no_exterior'] : ''; ?>">
                                                    </div>                                  
                                                    <div class="col-sm-6">
                                                        <br />
                                                        <label># Interior</label>
                                                        <input type="text" class="form-control" id="no_interior" name="no_interior"  placeholder="# Interior" value="<?php echo ($id != '') ? $edit['no_interior'] : ''; ?>">
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <br /> 
                                                        <label> Colonia </label>                                                                           
                                                        <input type="text" class="form-control" id="colonia" name="colonia"  placeholder="Colonia" value="<?php echo ($id != '') ? $edit['colonia'] : ''; ?>">                                      
                                                    </div>    
                                                    <div class="col-sm-12">
                                                        <br />
                                                        <label>Referencia</label>                                                                           
                                                        <input type="text" class="form-control" id="referencia" name="referencia"  placeholder="Referencia" value="<?php echo ($id != '') ? $edit['referencia'] : ''; ?>">                                                                              
                                                    </div>                              
                                                    <div class="col-sm-6">
                                                        <br /> 
                                                        <label>Localidad</label>                                                                             
                                                        <input type="text" class="form-control" id="localidad" name="localidad"  placeholder="Localidad" value="<?php echo ($id != '') ? $edit['localidad'] : ''; ?>">
                                                    </div>                                                                             
                                                    <div class="col-sm-6" >
                                                        <br /> 
                                                        <label>Municipio</label>                                                                               
                                                        <input type="text" class="form-control" id="municipio" name="municipio"  placeholder="Municipio" value="<?php echo ($id != '') ? $edit['municipio'] : ''; ?>">                                       
                                                    </div>       
                                                    <div class="col-sm-6">
                                                        <br />  
                                                        <label>Estado</label>                                                                            
                                                        <input type="text" class="form-control" id="estado" name="estado"  placeholder="Estado" value="<?php echo ($id != '') ? $edit['estado'] : ''; ?>">                                       
                                                    </div>  
                                                    <div class="col-sm-6">
                                                        <br />  
                                                        <label>Código Postal</label>
                                                        <input class="form-control integer_fields"  id="codigo_postal" name="codigo_postal" data-inputmask='"mask": "99999"' data-mask type="text" placeholder="12345" value="<?php echo ($id != '') ? $edit['codigo_postal'] : ''; ?>"/>                                           
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <br />
                                                        <label>País</label>
                                                         <select class="form-control" id="id_pais" name="id_pais">
                                                            <option value="<?php echo $GLOBAL_id_pais; ?>"><?php echo $GLOBAL_pais; ?></option>
                                                            <option value="" disabled="">--------------------</option>
                                                            <?php
                                                            $sql3 = "SELECT * FROM pais ORDER BY nombre ASC";
                                                            $query3 = consulta($sql3);
                                                            $num3 = mysql_num_rows($query3);
                                                            if ($num3 > 0) {
                                                                while ($row3 = mysql_fetch_array($query3)) {
                                                                    ?>
                                                                    <option <?php echo ($id != '') ? ($row3['id_pais'] == $edit['id_pais']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row3['id_pais']; ?>"><?php echo $row3['nombre']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>  
                                                </div>                              
                                            </div>

                                            <div class="form-actions fluid">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6">
                                                    <?php if ($tipo == 'prospecto' || $edit['tipo'] ==1) { ?> 
                                                        <a class="btn btn-danger" href="listado-prospectos.php"><i class="fa fa-times"></i> Cancelar</a>
                                                    <?php } else  { ?>
                                                        <a class="btn btn-danger" href="listado-clientes.php"><i class="fa fa-times"></i> Cancelar</a>
                                                    <?php } ?>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <?php if ($id != 0) { ?>                                            
                                                        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-pencil"></i> Modificar</button>
                                                        <?php } else  { ?>
                                                        <button type="submit" class="btn blue btn-success pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- portlet-body -->
                                    </div><!-- portlet box blue -->
                                </form><!-- END FORM-->
                            </div>
                        </div><!-- END VALIDATION STATES-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include ("includes/footer.php"); ?>
    <?php include ("includes/js.php"); ?>
    <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
    <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
    <script src="scripts/custom/table-advanced.js"></script>
    <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins
        });

        function validateMail(idMail){
            //Creamos un objeto 
            object = document.getElementById(idMail);
            valueForm = object.value;
            // Patron para el correo
            var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

            if (valueForm.search(patron) == 0){
                //Mail correcto
                object.style.color = "#000";
                return;
            }
            //Mail incorrecto
            object.style.color = "#f00";
        }
    </script><!-- END JAVASCRIPTS -->
    </body><!-- END BODY -->
</html>