<?php

if (!isset($_SESSION)) {
    session_start();
}

// if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
// }

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM ventana_color WHERE id_vcolor =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));
    }
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Color
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-ventanas.php">
                                        Colores
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del color
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->    
                    <form action="alta/color.php" id="colorForm" name="colorForm" class="form-horizontal" method="POST">  
                        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />   
                        <input type="hidden" id="btn" name="btn" value="" />            
                        <div class="portlet box blue">
                            <div class="portlet-title">                            
                            </div>
                            <div class="portlet-body form">
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">  
                                            <span style="color:red;">*</span> <label>Nombre color:</label>
                                            <input type="text" class="form-control" name="nombre_color" id="nombre_color" placeholder="Nombre" value="<?php echo ($id != '') ? $edit_usu['nombre_color'] : ''; ?>">
                                        </div>   
                                        <div class="col-sm-4">  
                                            <span style="color:red;">*</span> <label>Descripción:</label>
                                            <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción" value="<?php echo ($id != '') ? $edit_usu['descripcion'] : ''; ?>">
                                        </div>  
                                    </div>
                                </div>                                                             
                                <br/>
                                <div class="form-actions fluid">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <a class="btn btn-danger" href="catalogo-ventanas.php"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php if ($id != 0) { ?>                                            
                                                <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                            <?php } else { ?>
                                                <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    App.init();
                    TableAdvanced.init();
                });

                function validar_vacios() {
                    if (document.getElementById('nombre_color').value == '') {
                        alert('Introduzca el nombre del color');
                        return false;
                    }

                    if (document.getElementById('descripcion').value == '') {
                        alert('Introduzca una descripción');
                        return false;
                    }
                    
                    <?php if ($id != 0) { ?>
                        document.forms[0].btn.value = "modificar";
                    <?php } else { ?>
                        document.forms[0].btn.value = "guardar";
                    <?php } ?>

                    document.forms[0].submit();
                }

                $(document).ready(function () {
                    $(":input").inputmask();
                });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>









