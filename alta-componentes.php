<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];
if (isset($_GET["idgc"])) {
  $id_gc = $_GET["idgc"];
    if ($id_gc != '') {
        $sql_gc = 'SELECT * FROM componentes_grupos WHERE id_grupo_componente =' . $id_gc;
        $gcomp = mysql_fetch_array(consulta($sql_gc));
    }
}

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM componentes WHERE id_componente =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));
    }
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Componente
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-componentes.php">
                                        Componentes
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del componente
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <form action="alta/componentes.php" id="componentesForm" name="componentesForm" class="form-horizontal" method="POST">
                        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
                        <input type="hidden" id="idgc" name="id_grupo_componente" value="<?php echo $gcomp["id_grupo_componente"]; ?>" />
                        <input type="hidden" id="btn" name="btn" value="" />
                        <div class="portlet box blue">
                            <div class="portlet-title">
                            </div>
                            <div class="portlet-body form">
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <span style="color:red;">*</span><label>Nombre:</label>
                                            <input type="text" class="form-control" name="nombre_componente" id="nombre_componente" placeholder="Nombre" value="<?php echo ($id != '') ? htmlentities($edit_usu['nombre_componente']) : ''; ?>">
                                        </div>
                                        <div class="col-sm-4">
                                            <label><span style="color:red">*</span>Tipo de Componente</label>
                                            <select class="form-control" name="id_componente_grupo" id="idgc">
                                                <option value="">[Seleccione un Tipo]</option>
                                                <?php
                                                $sql1 = "SELECT * FROM componentes_grupos WHERE activo=1 ORDER BY id_grupo_componente";
                                                $query1 = consulta($sql1);
                                                $row1 = mysql_num_rows($query1);
                                                if ($row1 > 0) {
                                                    while ($rowEst1 = mysql_fetch_array($query1)) {
                                                        ?>
                                                        <option <?php echo ($id != '') ? ($rowEst1['id_grupo_componente'] == $gcomp["id_grupo_componente"]) ? 'selected=""' : '' : ''; ?> value="<?php echo $rowEst1['id_grupo_componente']; ?>"><?php echo $rowEst1['grupo_componente']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <span style="color:red;">*</span><label>Precio:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" class="form-control" name="precio" id="precio" placeholder="Precio" value="<?php echo ($id != '') ? $edit_usu['precio'] : ''; ?>">
                                            </div>
                                        </div>
                                        <!--
                                        <div class="col-sm-4">
                                            <span style="color:red;">*</span><label>Impuesto:</label>
                                            <div class="input-group">
                                                <input type="number" min="0" class="form-control" name="impuesto" id="impuesto" placeholder="Impuesto" value="<?php echo ($id != '') ? $edit_usu['impuesto'] : ''; ?>">
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                        -->
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-2">  
                                            <span style="color:red;">*</span><label>VA:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" name="VA" id="VA" value="<?php echo ($id != '') ? $edit_usu['VA'] : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">  
                                            <span style="color:red;">*</span><label>VB:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" name="VB" id="VB" value="<?php echo ($id != '') ? $edit_usu['VB'] : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">  
                                            <span style="color:red;">*</span><label>VC:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" name="VC" id="VC" value="<?php echo ($id != '') ? $edit_usu['VC'] : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">  
                                            <span style="color:red;">*</span><label>VD:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" name="VD" id="VD" value="<?php echo ($id != '') ? $edit_usu['VD'] : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">  
                                            <span style="color:red;">*</span><label>VE:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" name="VE" id="VE" value="<?php echo ($id != '') ? $edit_usu['VE'] : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">  
                                            <span style="color:red;">*</span><label>VF:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" name="VF" id="VF" value="<?php echo ($id != '') ? $edit_usu['VF'] : ''; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-actions fluid">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <a class="btn btn-danger" href="catalogo-componentes.php"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php if ($id != 0) { ?>
                                                <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                            <?php } else { ?>
                                                <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    App.init(); // initlayout and core plugins
                    TableAdvanced.init();
                });

                function validar_vacios() {
                    if (document.getElementById('nombre_componente').value == '') {
                        alert('Introduzca el nombre del componente');
                        return false;
                    }

                    if (document.getElementById('precio').value == '') {
                        alert('Introduzca el precio');
                        return false;
                    }
                    
                    /*
                    if (document.getElementById('impuesto').value == '') {
                        alert('Introduzca el impuesto');
                        return false;
                    }
                    */
                    
                    <?php if ($id != 0) { ?>
                        document.forms[0].btn.value = "modificar";
                    <?php } else { ?>
                        document.forms[0].btn.value = "guardar";
                    <?php } ?>
                    document.forms[0].submit();
                }

                $(document).ready(function () {
                    $(":input").inputmask();
                });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
