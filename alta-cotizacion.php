<?php

if (!isset($_SESSION)) {
    session_start();
}

// if (($_SESSION['id_usuario'] == NULL)) {
//     header("Location: logout.php");
// }

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = 0;
}

if (isset($_GET['id_cliente'])) {
    $id_cliente = $_GET['id_cliente'];
} else {
    $id_cliente = 0;
}

$cliente = $pdo->query("SELECT * FROM clientes WHERE id_cliente = $id_cliente LIMIT 1")->fetch(PDO::FETCH_ASSOC);

$id_cliente = $cliente['id_cliente'];
$id_divisa = $cliente['divisa_cliente'];

if (isset($id_usuario) && $id_usuario != 0) {
    
    $sql1 = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
    $result = mysql_query($sql1, $db_con);
    $editu = mysql_fetch_assoc($result);

    if ($id != 0) {
        $sql = "SELECT * FROM cotizaciones WHERE id_cotizacion = $id";
        $edit = mysql_fetch_assoc(consulta($sql));
    } else {
        $sql = 'INSERT INTO cotizaciones (id_vendedor) VALUES (' . $id_usuario . ')';
        $insert = mysql_query($sql, $db_con) or die('error en la consulta: ' . $sql);

        $sql = 'SELECT * FROM cotizaciones WHERE id_vendedor = '. $id_usuario . ' order by id_cotizacion desc limit 1';
        $edit = mysql_fetch_assoc(consulta($sql));
    }

}

$clientes = $pdo->query("SELECT * FROM clientes  WHERE activo = 1 AND aprobado = 1 ORDER BY nombre_cliente ASC")->fetchAll(PDO::FETCH_ASSOC);
$divisas = $pdo->query("SELECT * FROM divisas WHERE activo = 1 ORDER BY abbrev ASC")->fetchAll(PDO::FETCH_ASSOC);
$impuestos = $pdo->query("SELECT * FROM impuestos WHERE activo = 1 ORDER BY nombre ASC")->fetchAll(PDO::FETCH_ASSOC);
$terminos_venta = $pdo->query("SELECT * FROM terminos_venta ORDER BY id_termino_venta ASC")->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $name; ?></title>
    <?php include ("includes/css.php"); ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <?php include ("includes/header.php"); ?>
    <div class="clearfix">
    </div>
    
    <!-- page-container -->
    <div class="page-container">
        <?php include ("includes/menu.php"); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="theme-panel hidden-xs hidden-sm">
                    <div class="toggler-close">
                    </div>
                </div>
                <!-- END STYLE CUSTOMIZER -->
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            Cotización
                        </h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-list-alt"></i>
                                <a href="listado-cotizaciones.php">
                                    Cotizaciones
                                </a>
                                <i class="fa fa-angle-right"></i>
                            </li>

                            <li>
                                <a href="#">
                                    Alta de cotización
                                </a>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <form action="alta/addCotizacion.php" id="formPrincipal" name="formPrincipal" class="form-horizontal" method="POST">
                    <input type="hidden" id="id_cotizacion" name="id_cotizacion" value="<?php echo $edit['id_cotizacion']; ?>">
                    <!-- <input type="hidden" id="precio_pies" name="precio_pies" value=""> -->
                    <!-- <input type="hidden" id="btn" name="btn" value="">-->
                    
                    <div class="portlet box blue">
                        <!-- portlet-title -->
                        <div class="portlet-title">
                            <!-- content -->
                        </div>
                        <!-- portlet-title -->

                        <!-- portlet-body -->
                        <div class="portlet-body" style="padding: 25px;">
                            <div class="">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <?php // echo '<pre>' . print_r($edit, 1) . '</pre>'; ?>
                                            <div style="margin-bottom: 10px;">
                                                <label for="id_cliente">Cliente</label>
                                                <select class="form-control" id="id_cliente" name="id_cliente" onchange="datosCliente(this.value);">
                                                    <option value="0">Selecciona un cliente o prospecto ...</option>
                                                    <?php foreach($clientes as $cliente): ?>
                                                        <option value="<?php echo $cliente['id_cliente']; ?>" <?php if ($cliente['id_cliente'] == $id_cliente) echo 'selected' ?>><?php echo $cliente['nombre_cliente']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <div style="margin-bottom: 10px;">
                                                <label for="id_divisa">Divisa</label>
                                                <select class="form-control" id="id_divisa" name="id_divisa">
                                                    <option value="0">Seleccione una divisa ...</option>
                                                    <?php foreach($divisas as $divisa): ?>
                                                        <option value="<?php echo $divisa['id_divisa']; ?>" <?php if ($divisa['id_divisa'] == $id_divisa) echo 'selected' ?>><?php echo "{$divisa['abbrev']} - {$divisa['nombre']} - {$divisa['tipo_cambio']}"; ?></option>
                                                    <?php endforeach; ?>    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <div style="margin-bottom: 10px;">
                                                <label for="id_impuesto">Impuesto</label>
                                                <select class="form-control" id="id_impuesto" name="id_impuesto">
                                                    <option value="0">Seleccione un impuesto ...</option>
                                                    <?php foreach($impuestos as $impuesto): ?>
                                                        <option value="<?php echo $impuesto['id_impuesto']; ?>"><?php echo $impuesto['nombre']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <div style="margin-bottom: 10px;">
                                                <label for="id_despacho">Despacho</label>
                                                <select class="form-control" id="id_despacho" name="id_despacho">
                                                    <option value="0">Seleccione una opción ...</option>
                                                    <option value="1">Envío a cliente</option>
                                                    <option value="2">Recolección en oficina</option>
                                                    <option value="3">Instalación HS</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div style="margin-bottom: 10px;">
                                                <label>Descripción de la cotización</label>
                                                <input type="text" id="descripcion" name="descripcion" class="form-control" placeholder="Descripción">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="direccion-cliente"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-4 col-md-offset-4">
                                        <div class="well" style="background: #4d90fe;">
                                            <label for="elemento" style="color: #fff;">Agregar elemento</label>
                                            <select id="elemento" name="elemento" class="form-control">
                                                <option value="0">Seleccione un elemento ...</option>
                                                <option value="1">Puerta</option>
                                                <option value="2">Componente</option>
                                                <option value="3">Kit</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        
                                        <!-- configuracionPuerta -->
                                        <div id="configuracionPuerta" >
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label for="cantidadP">Cantidad</label>
                                                        <input type="text" class="form-control" id="cantidadP" name="cantidad" placeholder="Cantidad" value="" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Tipo</label>
                                                        <select class="form-control" id="id_ptipo" name="id_ptipo">
                                                            <option value="">--------------------</option>
                                                            <?php
                                                            $sql1 = "SELECT * FROM puerta_tipo WHERE activo = 1";
                                                            $query1 = consulta($sql1);
                                                            $num1 = mysql_num_rows($query1);
                                                            if ($num1 > 0) {
                                                                while ($row1 = mysql_fetch_array($query1)) {
                                                                    ?>
                                                                        <option <?php echo ($id != '0') ? ($row1['id_ptipo'] == $edit['id_ptipo']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_ptipo']; ?>"><?php echo $row1['nombre_tipo']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Medida</label>
                                                        <select class="form-control" id="id_puerta" name="id_puerta">
                                                            <option value="">--------------------</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row" id="medida" style="display:none;">
                                                    <div class="col-sm-4">
                                                        <select id="id_modelo" name="id_modelo" onchange="$('#precio_pies').val('');" class="form-control">
                                                            <option value="0">Selecciona unidad ...</option>
                                                            <option value="m">Metros (m)</option>
                                                            <option value="p">Pies/Pulgadas</option>
                                                        </select>
                                                    </div>
                                                    
                                                    <div id="metros" style="display:none;">
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="m_ancho" name="m_ancho" placeholder="m" value="" />
                                                                <div class="input-group-addon">x</div>
                                                                <input type="text" class="form-control" id="m_alto" name="m_alto" placeholder="m" value="" />
                                                                <div class="input-group-addon">m</div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="pies" style="display:none;">
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="f_ancho" name="f_ancho" placeholder="pies" value="" />
                                                                <div class="input-group-addon">'</div>
                                                                <input type="text" class="form-control" id="in_ancho" name="in_ancho" placeholder="pulgadas" value="" />
                                                                <div class="input-group-addon">"</div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="f_alto" name="f_alto" placeholder="pies" value="" />
                                                                <div class="input-group-addon">'</div>
                                                                <input type="text" class="form-control" id="in_alto" name="in_alto" placeholder="pulgadas" value="" />
                                                                <div class="input-group-addon">"</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Modelo</label>
                                                        <div class="input-group">
                                                            <select class="form-control" id="id_pmodelo" name="id_pmodelo">
                                                                <option value="">--------------------</option>
                                                            </select>
                                                            <span class="input-group-btn">
                                                                <a class="btn btn-info" onclick="mostrar('puertas');"><i class="fa fa-file-photo-o"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Color</label>
                                                        <div class="input-group">
                                                            <select class="form-control" id="id_pcolor" name="id_pcolor">
                                                                <option value="">--------------------</option>
                                                            </select>
                                                            <span class="input-group-btn">
                                                                <a class="btn btn-info" onclick="mostrar('colores');"><i class="fa fa-file-photo-o"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Movimiento</label>
                                                        <div class="input-group">
                                                            <select class="form-control" id="id_pmovimiento" name="id_pmovimiento">
                                                                <option value="">--------------------</option>
                                                            </select>
                                                            <span class="input-group-btn">
                                                                <a class="btn btn-info" onclick="mostrar('movimiento');"><i class="fa fa-file-photo-o"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Textura</label>
                                                        <select class="form-control" id="id_ptextura" name="id_ptextura">
                                                            <?php
                                                            $sql1 = "SELECT * FROM puerta_textura WHERE activo =1";
                                                            $query1 = consulta($sql1);
                                                            $num1 = mysql_num_rows($query1);
                                                            if ($num1 > 0) {
                                                                while ($row1 = mysql_fetch_array($query1)) {
                                                                    ?>
                                                                    <option <?php echo ($id != '0') ? ($row1['id_ptextura'] == $edit['id_ptextura']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_ptextura']; ?>"><?php echo $row1['nombre_textura']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Sellos</label>
                                                        <select class="form-control" id="id_psello" name="id_psello">
                                                            <?php
                                                            $sql1 = "SELECT * FROM puerta_sello WHERE activo =1";
                                                            $query1 = consulta($sql1);
                                                            $num1 = mysql_num_rows($query1);
                                                            if ($num1 > 0) {
                                                                while ($row1 = mysql_fetch_array($query1)) {
                                                                    ?>
                                                                    <option <?php echo ($id != '0') ? ($row1['id_psello'] == $edit['id_psello']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_psello']; ?>"><?php echo $row1['nombre_sello']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Motor</label>
                                                        <select class="form-control" id="id_pmotor" name="id_pmotor">
                                                            <?php
                                                            $sql1 = "SELECT * FROM puerta_motor WHERE activo =1";
                                                            $query1 = consulta($sql1);
                                                            $num1 = mysql_num_rows($query1);
                                                            if ($num1 > 0) {
                                                                while ($row1 = mysql_fetch_array($query1)) {
                                                                    ?>
                                                                    <option <?php echo ($id != '0') ? ($row1['id_pmotor'] == $edit['id_pmotor']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_pmotor']; ?>"><?php echo $row1['nombre_motor']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Modelo de ventana</label>
                                                        <div class="input-group">
                                                            <select class="form-control" id="id_ventana" name="id_ventana">
                                                                <option value="">--------------------</option>
                                                            </select>
                                                            <span class="input-group-btn">
                                                                <a id="muestraModelos" class="btn btn-info" onclick="mostrar('ventanas');"><i class="fa fa-file-photo-o"></i></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-4">
                                                        <label>Color de ventana</label>
                                                        <select class="form-control" id="id_vcolor" name="id_vcolor">
                                                            <option value="">--------------------</option>
                                                        </select>
                                                    </div>

                                                    <div id="ventanas" style="display:none;">
                                                        <div class="col-sm-4">
                                                            <label for="nventanas">Numero de ventanas</label>
                                                            <input type="text" class="form-control" id="nventanas" name="nventanas" placeholder="Numero de ventanas" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row"> 
                                                    <div class="col-sm-12" style="margin-top:20px">
                                                        <button type="button" id="saveP" class="btn btn-info pull-right"><i id="iAddP" class="fa fa-plus"></i> Agregar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- configuracionPuerta -->

                                        <!-- configuracionComponente -->
                                        <div id="configuracionComponente">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Cantidad</label>
                                                        <input type="text" class="form-control" id="cantidadC" name="cantidad" placeholder="Cantidad" value="" />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Grupo</label>
                                                        <select class="form-control" id="id_grupo_componente" name="id_grupo_componente">
                                                            <option value="">--------------------</option>
                                                            <?php
                                                            $sql1 = "SELECT * FROM componentes_grupos WHERE activo =1";
                                                            $query1 = consulta($sql1);
                                                            $num1 = mysql_num_rows($query1);
                                                            if ($num1 > 0) {
                                                                while ($row1 = mysql_fetch_array($query1)) {
                                                                    ?>
                                                                    <option <?php echo ($id != '0') ? ($row1['id_grupo_componente'] == $edit['id_grupo_componente']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_grupo_componente']; ?>"><?php echo $row1['grupo_componente']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Componente</label>
                                                        <select class="form-control" id="id_componente" name="id_componente">
                                                            <option value="">--------------------</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12" style="margin-top:20px">
                                                        <button type="button" id="saveC" class="btn btn-info pull-right"><i id="iAddC" class="fa fa-plus"></i> Agregar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- configuracionComponente -->
                            
                                        <!-- configuracionKit -->
                                        <div id="configuracionKit">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Cantidad</label>
                                                        <input type="text" class="form-control" id="cantidadK" name="cantidad" placeholder="Cantidad" value="" />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label>Kit</label>
                                                        <select class="form-control" id="id_kit" name="id_kit">
                                                            <option value="">--------------------</option>
                                                            <?php
                                                            $sql1 = "SELECT * FROM kit WHERE activo =1";
                                                            $query1 = consulta($sql1);
                                                            $num1 = mysql_num_rows($query1);
                                                            if ($num1 > 0) {
                                                                while ($row1 = mysql_fetch_array($query1)) {
                                                                    ?>
                                                                    <option <?php echo ($id != '0') ? ($row1['id_kit'] == $edit['id_kit']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_kit']; ?>"><?php echo $row1['nombre_kit']; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12" style="margin-top:20px">
                                                        <button type="button" id="saveK" class="btn btn-info pull-right"><i id="iAddK" class="fa fa-plus"></i> Agregar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- configuracionKit -->

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="well">
                                            <h4>Elementos cotizados</h4>
                                            <div id="concepts-section">
                                                <p>No se han agregado conceptos a esta cotización</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-4">
                                        <h4>Términos de venta.</h4>
                                        <select id="id_termino_venta" name="id_termino_venta" onchange="terminosVenta(this.value);"class="form-control">
                                            <option value="0">Seleccione un término de venta ...</option>
                                            <?php foreach ($terminos_venta as $tv) { ?>
                                                <option value="<?php echo $tv['id_termino_venta']; ?>"><?php echo $tv['termino_venta']; ?></option> 
                                            <?php } ?>
                                        </select>
                                        <br>
                                        <textarea id="termino_venta" class="form-control" style="height: 200px;" disabled></textarea>
                                        <!--
                                        <ul>
                                            <li>Todos los modelos con textura de madera en BN.</li>
                                            <li>Para ver las especificaciones técnicas de cada puerta, consultar <a href="http://hspuertasdegaraje.com" title="HS Puertas de Garaje" target="_blank">http://hspuertasdegaraje.com</a></li>
                                            <li>No incluye envío.</li>
                                            <li>Puertas completas, paneles, herrajes calibre 14 y resorte acorde al tamaño de la puerta.</li>
                                            <li>Modelo con cuadros, con cuadro de ajuste.</li>
                                            <li>Puertas fabricadas a la medida con tiempo de envió a 10 días hábiles.</li>
                                            <li>Precios en MXN</li>
                                            <li>IVA incluido.</li>
                                            <li>Elegantti y exquisite comienzan en precio VE.</li>
                                            <li>Pago de contado.</li>
                                            <li>Validez de 15 días hábiles.</li>
                                            <li>Una vez realizado el pedido, no hay cambios o devoluciones.</li>
                                        </ul>
                                        -->
                                    </div>
                                    <div class="col-md-4 col-md-offset-4">
                                        <div class="row">
                                            <div class="col-sm-4"><h5>Subtotal</h5></div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" id="subtotal" name="subtotal" value="0" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4"><h5>Impuestos</h5></div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" id="impuestos" name="impuestos" value="0" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4"><h4>Total</h4></div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" id="total" name="total" value="0" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button type="button" id="cancel-quotation" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" id="save-quotation" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Agregar</button>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- portlet-body -->

                    </div>
                </form>
            
            </div>
        </div>
    </div>
    <div class='notifications bottom-right'></div>

    <?php include ("modal/imagen.php"); ?>
    <?php include ("modal/change-concept.php"); ?>
    <?php include ("includes/footer.php"); ?>
    <?php include ("includes/js.php"); ?>

    <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
    <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
    <script src="scripts/custom/table-advanced.js"></script>
    <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
    <script src="plugins/numeral.min.js" type="text/javascript" ></script>

    <script src="ajax/cotizacion.js" type="text/javascript" ></script>

</body>
</html>
