<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    if ($id != '') {
        $sql = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
        $user = mysql_fetch_array(consulta($sql));
        $nom = $user["username"];
    }
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <link href="plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />

    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Deposito
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-usd"></i>
                                    <a href="inicio.php">
                                        Listado Depositos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>  
                                <li>
                                    <a href="alta-incidencia.php?id=0">
                                        Datos del deposito
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->    
                    <form action="alta/depositos.php" id="depositosForm" name="depositosForm" class="form-horizontal" method="POST">  
                        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">             
                        <input type="hidden" class="form-control" id="id_usuario_alta" name="id_usuario_alta" value="<?php echo $id_usuario; ?>">
                        <input type="hidden" id="btn" name="btn" value="guardar" />           
                        <div class="portlet box blue">
                            <div class="portlet-title">                            
                            </div>
                            <div class="portlet-body form">
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">  
                                            <label>Cliente:</label>
                                            <select class="form-control" id="id_cliente" name="id_cliente">
                                                <option value="0">Seleccione un cliente ...</option>
                                                <?php
                                                $sql2 = "SELECT id_cliente, nombre_cliente, aprobado FROM clientes WHERE activo = 1 AND aprobado = 1 ORDER BY id_cliente";
                                                $query2 = consulta($sql2);
                                                $num2 = mysql_num_rows($query2);
                                                if ($num2 > 0) {
                                                    while ($row2 = mysql_fetch_array($query2)) {
                                                        ?>
                                                        <option value="<?php echo $row2['id_cliente']; ?>" <?php echo ($row2['id_cliente'] == $id)? 'selected' : ''; ?>><?php echo $row2['nombre_cliente']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">  
                                            <label>Fecha:</label>
                                            <input data-provide="datepicker" class="form-control" data-date-format="yyyy-mm-dd" id="fecha_deposito" name="fecha_deposito" type="text" value="" placeholder="AAAA-mm-dd"/>                                            
                                        </div>
                                        <div class="col-md-4">  
                                            <label>Monto a depositar:</label>
                                            <div class="input-group">
                                                <span style="background:#4d90fe; color:white;" class="input-group-addon">$</span>
                                                <input type="text" class="form-control" placeholder="100000" name="monto_deposito" id="monto_deposito" value=""/>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>                                                           
                                <div class="form-actions fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <a class="btn btn-danger" href="listado-depositos.php"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-md-6">
                                           <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    App.init(); // initlayout and core plugins
                    TableAdvanced.init();
                });

                function validar_vacios() {
                    if (document.getElementById('id_cliente').value == 0) {
                        alert("Introduzca el nombre del cliente");
                        
                        return false;
                    }
                    
                    if (document.getElementById('fecha_deposito').value == '') {
                        alert("Introduzca la fecha del deposito");
                        return false;
                    }
                    
                    if (document.getElementById('monto_deposito').value == '') {
                        alert("Introduzca el monto");
                        return false;
                    }
                    
                    document.forms[0].submit();
                }
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>









