<?php

if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];

    if ($id != '') {
        $sql_usu = "
            SELECT
                incidencias.id_incidencia,
                clientes.id_cliente,
                cotizaciones.id_cotizacion,
                incidencias.descripcion
            FROM incidencias
            INNER JOIN cotizaciones ON incidencias.id_cotizacion = cotizaciones.id_cotizacion
            INNER JOIN clientes ON cotizaciones.id_cliente = clientes.id_cliente
            WHERE id_incidencia = $id
        ";

        $edit_usu = mysql_fetch_assoc(consulta($sql_usu));

        // echo '<pre>' . print_r($edit_usu, 1) . '</pre>';
        // exit;

        $sql = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
        $user = mysql_fetch_array(consulta($sql));
        $nom = $user["username"];
    }
} else {
    $id = 0;
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>

    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Incidencia
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Listado Incidencias
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>  
                                <li>
                                    <a href="alta-incidencia.php?id=0">
                                        Datos de la incidencia
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->    
                    <form action="alta/incidencias.php" id="incidenciasForm" name="incidenciasForm" class="form-horizontal" method="POST">  
                        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" class="form-control" />             
                        <input type="hidden" id="id_usuario_alta" name="id_usuario_alta" value="<?php echo $id_usuario ?>" class="form-control" />
                        <input type="hidden" id="btn" name="btn" value="" />           
                        <div class="portlet box blue">
                            <div class="portlet-title">                            
                            </div>
                            <div class="portlet-body form">
                                <br/>                             
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label>Cliente:</label>
                                            <select class="form-control" id="id_cliente" name="id_cliente">
                                                <option value="0">Seleccione un cliente ...</option>
                                                <?php
                                                    $sql = "SELECT id_cliente, nombre_cliente FROM clientes WHERE activo = 1 ORDER BY nombre_cliente";
                                                    $clientes = consulta($sql);
                                                ?>
                                                <?php while ($cliente = mysql_fetch_assoc($clientes)) { ?>
                                                    <?php if ($edit_usu['id_cliente'] == $cliente['id_cliente']) { ?>
                                                        <option value="<?php echo $cliente['id_cliente']; ?>" selected><?php echo $cliente['nombre_cliente']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $cliente['id_cliente']; ?>"><?php echo $cliente['nombre_cliente']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Cotizacion:</label>
                                            <select class="form-control" id="id_cotizacion" name="id_cotizacion">
                                                <option value="0">Seleccione una cotización ...</option>
                                                <?php
                                                    $tmp_id_cliente = (int) $edit_usu['id_cliente'];
                                                    $sql = "SELECT id_cotizacion, numero_cotizacion FROM cotizaciones WHERE activo = 1 AND id_cliente = $tmp_id_cliente ORDER BY numero_cotizacion";
                                                    $cotizaciones = consulta($sql);
                                                ?>
                                                <?php while ($cotizacion = mysql_fetch_assoc($cotizaciones)) { ?>
                                                    <?php if ($edit_usu['id_cotizacion'] == $cotizacion['id_cotizacion']) { ?>
                                                        <option value="<?php echo $cotizacion['id_cotizacion']; ?>" selected><?php echo $cotizacion['numero_cotizacion']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $cotizacion['id_cotizacion']; ?>"><?php echo $cotizacion['numero_cotizacion']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-sm-12">  
                                            <label>Descripcion:</label>
                                            <?php $incidencia_descripcion = isset($edit_usu) ? $edit_usu['descripcion'] : NULL; ?>
                                            <textarea rows="2" placeholder="Su descripcion" name="descripcion" id="descripcion" class="form-control"><?php echo $incidencia_descripcion; ?></textarea>
                                        </div> 
                                    </div>
                                </div>                              
                                <div class="form-actions fluid">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <a class="btn btn-danger" href="listado-incidencias.php"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php if ($id != 0) { ?>                        
                                                <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                            <?php } else { ?>
                                                <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">
                $(document).ready(function() {
                    App.init();
                    TableAdvanced.init();

                    $(":input").inputmask();

                    $('#id_cliente').change(function(e) {
                        var option = e.target.value;

                        if (option == 0) {
                            $('#id_cotizacion').html('<option value="0">Seleccione una cotización ...</option>');
                        }

                        if (option > 0) {
                            $.ajax({
                                url: './ajax/getQuotesByClient.php',
                                type: 'post',
                                dataType: 'json',
                                data: {
                                    id_client: option
                                },
                                success: function(data) {
                                    var html = '<option value="0">Seleccione una cotización ...</option>';

                                    $.each(data, function(index, object) {
                                            html += '<option value="' + object.id_cotizacion + '">' + object.numero_cotizacion + '</option>'; 
                                    });

                                    $('#id_cotizacion').html(html);
                                },
                                async: false
                            });
                        }
                    });
                });

                function validar_vacios() {
                    <?php if ($id != 0) { ?>
                        document.forms[0].btn.value = "modificar";
                    <?php } else { ?>
                        document.forms[0].btn.value = "guardar";
                    <?php } ?>
                    
                    document.forms[0].submit();
                }
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>









