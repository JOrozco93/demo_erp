<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '0') {
        $sql_k = 'SELECT * FROM kit WHERE id_kit =' . $id;
        $edit_k = mysql_fetch_array(consulta($sql_k));
    }
    else {
      $sql_ins = "INSERT INTO `kit` (`nombre_kit`, `id_proveedor`, `precio`, `impuesto`) VALUES ('', '', '', '')";
      $result = mysql_query($sql_ins, $db_con) or die("Problemas en la consulta: " . $sql_insert);
      $id = mysql_insert_id();
      $sql_k = 'SELECT * FROM kit WHERE id_kit =' . $id;
      $edit_k = mysql_fetch_array(consulta($sql_k));
    }
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Nuevo Kit Herrer&iacute;a
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-herreria.php">
                                        Listado de Kit Herrer&iacute;a
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del Kit
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <form action="alta/kit.php" id="kForm" name="kForm" class="form-horizontal" method="POST">
                        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
                        <input type="hidden" id="btn" name="btn" value="" />
                        <div class="portlet box blue">
                            <div class="portlet-title">
                            </div>
                            <div class="portlet-body form">
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <span style="color:red;">*</span><label>Nombre:</label>
                                            <input type="text" class="form-control" name="nombre_kit" id="nombre_kit" placeholder="Nombre del kit" value="<?php echo ($id != '') ? $edit_k['nombre_kit'] : ''; ?>">
                                        </div>
                                        <div class="col-sm-4">
                                            <span style="color:red;">*</span><label>Proveedor:</label>
                                            <select class="form-control" id="id_proveedor" name="id_proveedor">
                                                <?php
                                                $sql1 = "SELECT * FROM proveedores WHERE activo =1 ORDER BY id_proveedor";
                                                $query1 = consulta($sql1);
                                                $num1 = mysql_num_rows($query1);
                                                if ($num1 > 0) {
                                                    while ($row1 = mysql_fetch_array($query1)) {
                                                        ?>
                                                        <option <?php echo ($id != '') ? ($row1['id_proveedor'] == $edit_k['id_proveedor']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_proveedor']; ?>"><?php echo $row1['nombre']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                          <span style="color:red;">*</span><label>Precio:</label>
                                          <div class="input-group">
                                              <span class="input-group-addon">$</span>
                                              <input type="text" class="form-control" name="precio" id="precio" placeholder="Precio" value="<?php echo ($id != '') ? $edit_k['precio'] : ''; ?>">
                                          </div>
                                      </div>
                                    </div>
                                </div>
                                <br />

                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VA:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VA" id="VA" value="<?php echo ($id != '') ? $edit_k['VA'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VB:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VB" id="VB" value="<?php echo ($id != '') ? $edit_k['VB'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VC:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VC" id="VC" value="<?php echo ($id != '') ? $edit_k['VC'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VD:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VD" id="VD" value="<?php echo ($id != '') ? $edit_k['VD'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VE:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VE" id="VE" value="<?php echo ($id != '') ? $edit_k['VE'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VF:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VF" id="VF" value="<?php echo ($id != '') ? $edit_k['VF'] : ''; ?>" />
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <br/>

                                <!--
                                <div class="row">
                                  <div class="col-sm-12">
                                      <div class="col-sm-4">
                                          <span style="color:red;">*</span><label>Impuesto:</label>
                                          <div class="input-group">
                                              <input type="number" class="form-control" name="impuesto" id="impuesto" placeholder="Impuesto" min="1" value="<?php echo ($id != '') ? $edit_k['impuesto'] : ''; ?>">
                                              <span class="input-group-addon">%</span>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <br />
                                -->

                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="col-sm-6">
                                      <div class="well">
                                        <h3>Componentes añadidos</h3>
                                        <div id="tblComA">
                                          <?php
                                          $sqlKC1 = "SELECT * FROM kit_componente WHERE activo = 1 AND id_kit = ". $edit_k['id_kit'];
                                          $queryKC1 = consulta($sqlKC1);
                                          $numKC1 = mysql_num_rows($queryKC1);
                                          if ($numKC1 > 0) {
                                            $display_tbl = "table";
                                            $display_msg = "none";
                                          } else {
                                            $display_tbl = "none";
                                            $display_msg = "block";
                                          }
                                              ?>
                                          <p style="display: <?php echo $display_msg; ?>"><em>No hay componentes en esta lista</em></p>
                                          <table class="table" style="display: <?php echo $display_tbl; ?>">
                                            <thead>
                                              <tr>
                                                <th class="col-sm-3">Cantidad</th>
                                                <th class="col-sm-6">Nombre</th>
                                                <th class="col-sm-3"></th>
                                              </tr>
                                            </thead>
                                            <tbody id="tblBody">
                                              <?php
                                              if ($numKC1 > 0) {
                                                while ($rowKC1 = mysql_fetch_array($queryKC1)) { // Por cada componente en el kit
                                                  $sqlC1 = "SELECT * FROM componentes WHERE activo = 1 AND id_componente = " . $rowKC1['id_componente'];
                                                  $rowC1 = mysql_fetch_array(consulta($sqlC1));  // Nombre del componente
                                                  ?>
                                                  <tr id="trComp_<?php echo $rowKC1['id_kit_componente']; ?>">
                                                    <td><?php echo $rowKC1['cantidad_kit_componente']; ?></td>
                                                    <td><?php echo htmlentities($rowC1['nombre_componente']); ?></td>
                                                    <td class="hidden"><?php echo $rowC1['precio']; ?></td>
                                                    <td>
                                                      <div class="btn btn-group">
                                                        <a data-placement="top" data-toggle="tooltip" title="Modificar cantidad" href="#" class="btn btn-info btn-sm updKC" data-id="<?php echo $rowKC1['id_kit_componente']; ?>"><i class="fa fa-pencil"></i></a>
                                                        <a data-placement="right" data-toggle="tooltip" title="Remover componente" href="#" class="btn btn-danger btn-sm delKC" data-id="<?php echo $rowKC1['id_kit_componente']; ?>" data-idc="<?php echo $rowC1['id_componente'] ?>"><i class="fa fa-minus"></i></a>
                                                      </div>
                                                    </td>
                                                  </tr>
                                                  <?php
                                                }
                                              }
                                                ?>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <div class="well">
                                        <h3>Componentes por añadir</h3>
                                        <div id="tblComP">
                                          <?php
                                          $sqlCc = "SELECT * FROM componentes c WHERE c.activo = 1 AND c.id_componente NOT IN (SELECT kc.id_componente FROM kit_componente kc WHERE kc.activo = 1 AND kc.id_kit = ". $edit_k['id_kit'] .")";
                                          $queryCc = consulta($sqlCc);
                                          $numCc = mysql_num_rows($queryCc);
                                          if ($numCc > 0) {
                                              ?>
                                              <table class="table tblCmp">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Nombre</th>
                                                    <th>Precio</th>
                                                  </tr>
                                                </thead>
                                              <?php
                                              while ($rowCc = mysql_fetch_array($queryCc)) {
                                                ?>
                                                <tr id="trCmp_<?php echo $rowCc['id_componente']; ?>">
                                                  <td>
                                                    <a data-placement="left" data-toggle="tooltip" title="Agregar" href="#" class="btn btn-primary btn-sm addKC" data-id="<?php echo $rowCc['id_componente'] ?>"><i class="fa fa-plus"></i></a>
                                                  </td>
                                                  <td><?php echo htmlentities($rowCc['nombre_componente']); ?></td>
                                                  <td>$ <?php echo number_format($rowCc['precio'], 2, '.', ','); ?></td>
                                                </tr>
                                                <?php
                                              }
                                              ?>
                                              </table>
                                              <?php
                                          } else {
                                            ?>
                                              <p><em>No hay componentes en esta lista</em></p>
                                            <?php
                                          }
                                          ?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <br/>
                                <div class="form-actions fluid">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <a id="btnCancel" href="catalogo-herreria.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php if ($edit_k['nombre_kit'] != '') { ?>
                                                <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                            <?php } else { ?>
                                                <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="hidden">
                      <input type="hidden" name="oldPrecio" />
                    </div>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">

              jQuery(document).ready(function () {
                  App.init();
                  TableAdvanced.init();

                  $("[data-toggle='tooltip']").tooltip();

                  $(".delKC").live("click", function(){
                    id = $(this).data("id");
                    idC = $(this).data("idc");
                    idK = $("#id").val();

                    $.ajax({
                      data:  {"fn": "4", "idk": idK, "idKC": id},
                      url:   './ajax/addKitCmp.php?rnd='+getRandomInt(),
                      type:  'post',
                      beforeSend: function () {
                        val = parseFloat($("#precio").val());
                        c = parseFloat( ($("#trComp_"+id+" td input").size() > 0) ? $("#trComp_"+id+" td input").val() : $("#trComp_"+id+" td").first().text());
                        p = parseFloat($("#trComp_"+id+" td").first().next().next().text());
                        t = c * p;
                        if ((val - t) > 0) {
                          $("#precio").val((val - t).toFixed(2));
                        } else {
                          oldVal = parseFloat($("input[name='oldPrecio']").val());
                          $("#precio").val(oldVal.toFixed(2));

                        }

                      },
                      success:  function (response) {
                        $("#trComp_" + id).fadeOut();
                        $("#trComp_" + id).remove();
                        if ($("#tblComA table tr").size() <= 1) {
                          $("#tblComA table").fadeOut();
                          $("#tblComA p").fadeIn();
                        }
                      }
                    }).done(function(){
                      $.ajax({
                        data:  {"idk": idK},
                        url:   './ajax/list-componentes.php?rnd='+getRandomInt(),
                        type:  'post',
                        beforeSend: function () {
                        },
                        success:  function (response) {
                          $("#tblComP").html(response);
                        }
                      });
                      $("[data-toggle='tooltip']").tooltip();
                    });
                  });

                  $(".addKC").live("click", function(){
                    idC = $(this).data("id");
                    idK = $("#id").val();

                    $.ajax({
                      data:  {"fn": "1", "idk": idK, "idC": idC},
                      url:   './ajax/addKitCmp.php?rnd='+getRandomInt(),
                      type:  'post',
                      beforeSend: function () {
                      },
                      success:  function (response) {
                        $("#tblComA p").hide();
                        $("#tblComA table").css("display", "table");
                        $("#tblBody").append(response);
                      }
                    }).done(function(){
                      $.ajax({
                        data:  {"idk": idK},
                        url:   './ajax/list-componentes.php?rnd='+getRandomInt(),
                        type:  'post',
                        beforeSend: function () {
                        },
                        success:  function (response) {
                          $("#tblComP").html(response);
                        }
                      });

                      // id = $("#tblBody tr").last().attr("id").split("_")[1];
                      // assignPrice(id);
                      $("[data-toggle='tooltip']").tooltip();
                    });
                  });

                  $(".saveKC").live("click", function(){
                    id_KC = $(this).data("id");
                    idK = $("#id").val();
                    cant = $("input[name='cantidad_kit_componente_"+ id_KC +"']").val();

                    $.ajax({
                      data:  {"fn": "2","idk": idK, "id_KC": id_KC, "cant" : cant},
                      url:   './ajax/addKitCmp.php',
                      type:  'post',
                      beforeSend: function () {
                      },
                      success:  function (response) {
                        $("#trComp_" + id_KC + " td").first().html(response);
                        $("#trComp_" + id_KC + " td").last().html("<div class='btn btn-group'><a data-placement='top' data-toggle='tooltip' title='Modificar cantidad' href='#' class='btn btn-info btn-sm updKC' data-id='"+ id_KC +"'><i class='fa fa-pencil'></i></a><a data-placement='right' data-toggle='tooltip' title='Remover componente' href='#' class='btn btn-danger btn-sm delKC' data-id='"+ id_KC +"'><i class='fa fa-minus'></i></a></div>");

                        arr = $("[id^='trComp_']:not(#trComp_"+ id_KC +")");
                        if (arr.size() > 0) {
                          precioAnt = 0.0;
                          for (var i = 0; i < arr.size(); i++) {
                            v1 = arr[i].cells[0].innerText.trim();
                            if (v1 != "") {
                              p1 = parseFloat(arr[i].cells[2].innerText.trim());
                              c1 = parseFloat(v1)
                              precioAnt += (c1 * p1);
                            }
                          }
                          $("#precio").val(precioAnt);
                        } else {
                          $("#precio").val(0);
                        }
                        assignPrice(id_KC);
                      }
                    }).done(function(){
                      $("[data-toggle='tooltip']").tooltip();
                    });
                  });

                  $(".updKC").live("click", function(){
                    id_KC = $(this).data("id");
                    idK = $("#id").val();

                    $.ajax({
                      data:  {"fn": "3","idk": idK, "id_KC": id_KC},
                      url:   './ajax/addKitCmp.php',
                      type:  'post',
                      beforeSend: function () {
                      },
                      success:  function (response) {
                        $("#trComp_" + id_KC).html(response);
                      }
                    }).done(function(){
                      $("[data-toggle='tooltip']").tooltip();
                    });
                  });

                  /*
                  $("#btnCancel").on("click", function(){
                    idK = $("#id").val();
                    res = confirm("¿Está seguro de que desea cancelar la creación este kit?");
                    if (res) {
                      $.ajax({
                        data: { "idk": idK},
                        url: './ajax/kitRemove.php',
                        type: 'post',
                        success:  function (response) {
                          window.open("catalogo-herreria.php", "_self");
                        }
                      });
                    }
                  });
                  */

                  $("input[name='oldPrecio']").val($("#precio").val());
              });

              function validar_vacios() {
                  if (document.getElementById('nombre_kit').value == '') {
                      alert('Introduzca el nombre del kit');
                      return false;
                  }

                  if (document.getElementById('id_proveedor').value == '') {
                      alert('Elija a un proveedor');
                      return false;
                  }

                  if (document.getElementById('precio').value == '' ) {
                      alert('Introduzca el precio del kit');
                      return false;
                  } else if (!$.isNumeric($("#precio").val()) || ( $.isNumeric($("#precio").val()) && parseFloat($("#precio").val()) < 1) ) {
                    alert('El precio del kit debe ser un número mayor que cero');
                    return false;
                  }

                  /*
                  if (document.getElementById('impuesto').value == '') {
                      alert('Introduzca el impuesto del kit');
                      return false;
                  } else if (!$.isNumeric($("#impuesto").val()) || ( $.isNumeric($("#impuesto").val()) && parseFloat($("#impuesto").val()) < 1) ) {
                    alert('El impuesto del kit debe ser un número mayor que cero');
                    return false;
                  }
                  */

                  document.forms[0].btn.value = "modificar";
                  document.forms[0].submit();
              }

              function getRandomInt() {
                return Math.floor(Math.random() * Math.pow(10,6));
              }

              function assignPrice(idKC) {
                c = parseFloat( ($("#trComp_"+idKC+" td input").size() > 0) ? $("#trComp_"+idKC+" td input").val() : $("#trComp_"+idKC+" td").first().text());
                p = parseFloat($("#trComp_"+idKC+" td").first().next().next().text());
                t = c * p;
                val = parseFloat($("#precio").val());
                $("#precio").val((val + t).toFixed(2));
                return;
              }

              $(document).ready(function () {
                  $(":input").inputmask();
              });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
