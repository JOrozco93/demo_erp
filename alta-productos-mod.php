<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];
$id_prod = $_GET["modificar"];

$sql_prod = "SELECT * FROM productos WHERE id = $id_prod";
$prods = mysql_fetch_array(consulta($sql_prod));

if ($prods["tasa_IVA"] == 1) {
    $iva = "Sin IVA";
}
else if ($prods["tasa_IVA"] == 2) {
    $iva = "16%";
}
else if ($prods["tasa_IVA"] == 3) {
    $iva = "11%";
}
else if ($prods["tasa_IVA"] == 4) {
    $iva = "0%";
}

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM usuarios WHERE id_usuario =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));

        if($edit_usu['permiso_usuario']==5 && $edit_usu['id_cliente']){
            $sql_usu = 'SELECT nombre_cliente AS nombre FROM clientes WHERE id_cliente =' . $edit_usu['id_cliente'];
            $cliente = mysql_fetch_array(consulta($sql_usu));
            $edit_usu['nombre_usuario'] = $cliente['nombre'];
        }
    }
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <script src="scripts/validaNumeros.js" type="text/javascript" ></script>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Productos
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-productos.php">
                                        Productos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del Producto
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <form action="alta/modificarProducto.php" id="productosForm" name="productosForm" class="form-horizontal" method="POST">
                    <input type="hidden" id="id" name="id" value="<?php echo $id_prod; ?>" />
                    <input type="hidden" id="btn" name="btn" value="" />
                    <div class="portlet box blue">
                        <div class="portlet-title">
                        </div>
                        <div class="portlet-body form">
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span> Nombre del producto
                                        </label>
                                        <input type="text" class="form-control" id="nombre_producto" value="<?php echo $prods["nombre"];?>" name="nombre_producto"  placeholder="Nombre">
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span> Descripción:
                                        </label>
                                        <input class="form-control" id="desc_producto" name="desc_producto" placeholder="Descripcion" value="<?php echo $prods["descripcion"];?>">
                                    </div><!-- /.col-->
                                    <div class="col-sm-4">
                                        <label>
                                             Tasa de IVA:
                                        </label>
                                        <select class="form-control" id="iva_producto" name="iva_producto" >
                                            <option value="<?php echo $prods["tasa_IVA"]; ?>"><?php echo "$iva"?></option>
                                            <option value="1">Sin IVA</option>
                                            <option value="2">16</option>
                                            <option value="3">11</option>
                                            <option value="4">0</option>
                                        </select>
                                    </div><!-- /.col-->

                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span>Precio Unitario
                                        </label>
                                        <input type="text" class="form-control" id="precio_unitario" name="precio_unitario"  placeholder="Precio Unitario" value="<?php echo $prods["precio_Unitario"];?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span>Unidad
                                        </label>
                                        <input type="text" class="form-control" id="unidad" name="unidad"  placeholder="Unidad" value="<?php echo $prods["unidad"];?>">
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                                <div class="form-actions fluid">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <a class="btn btn-danger" href="catalogo-productos.php"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-sm-6">
                                                <button type="submit" class="btn blue btn-success pull-right" onClick="validar_campos();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                        </div>
                                     </div>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    App.init();
                    TableAdvanced.init();
                });

                function validar_vacios() {
                    var usuarios = [
                        <?php
                            // Creamos un arreglo en PHP  con todos los nombres de usuario y los metemos en un arreglo.

                            $sql6 = "SELECT username FROM usuarios";
                            $query6 = consulta($sql6);

                            $num6 = mysql_num_rows($query6);

                            if ($num6 > 0) {
                                while ($row6 = mysql_fetch_array($query6)) {
                                    echo '"' . $row6['username'] . '", ';
                                }
                            }
                        ?>
                        "admin"
                    ];

                    <?php if ($id == 0) { ?>
                        if ($.inArray($('#nombre').val(), usuarios) != -1) {
                            alert("El usuario ya se encuentra registrado");
                            return false;
                        }
                    <?php } ?>

                    if (document.getElementById('id_permiso').value == '') {
                        alert("Seleccione un permiso para el usuario");
                        return false;
                    }

                    if (document.getElementById('id_permiso').value > 5) {
                        if (document.getElementById('num_empleado').value == '') {
                            alert("Ingrese un Numero de Empleado");
                            return false;
                        }
                    }
                    if (document.getElementById('nom_usuario').value == '') {
                        alert("Ingrese un Nombre de Usuario");
                        return false;
                    } else if (document.getElementById('password').value == '') {
                        alert("Ingrese un Password");
                        return false;
                    } else if (document.getElementById('email').value == '') {
                        alert("Ingrese un Email o Correo Electronico");
                        return false;
                    } else if (document.getElementById('nombre').value == '') {
                        alert("Ingrese Nombre(s)");
                        return false;
                    }  else {
            <?php if ($id != 0) { ?>
                    document.forms[0].btn.value = "modificar";
            <?php } else  { ?>
                    document.forms[0].btn.value = "guardar";
            <?php } ?>
                    document.forms[0].submit();
                    }
                }

                $(document).ready(function () {
                    $(":input").inputmask();
                });

                function validateMail(idMail) {
                    //Creamos un objeto
                    object = document.getElementById(idMail);
                    valueForm = object.value;

                    // Patron para el correo
                    var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

                    if (valueForm.search(patron) == 0){
                        //Mail correcto
                        object.style.color = "#000";
                        return;
                    }
                    //Mail incorrecto
                    object.style.color = "#f00";
                }
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
