<?php

    error_reporting(E_ALL & ~E_NOTICE);

    if (!isset($_SESSION)) {
        session_start();
    }

    //if (($_SESSION['id_usuario'] == NULL)) {
    //    header("Location: logout.php");
    //}

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");
    $id_usuario = $_SESSION['id_usuario'];

    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        if ($id != 0) {
            $sql = 'SELECT * FROM proveedores AS p, domicilios AS d WHERE p.id_proveedor ='.$id.' AND p.id_domicilio = d.id_domicilio';
            $edit = mysql_fetch_array(consulta($sql));
            //print_r($edit);
        }    
    }
    
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Proveedor
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catálogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-proveedores.php">
                                        Proveedores
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del Proveedor
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->    
                    <form action="alta/proveedor.php" id="formPrincipal" name="formPrincipal" class="form-horizontal" method="POST">  
                    <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />   
                    <input type="hidden" id="id_domicilio" name="id_domicilio" value="<?php echo $edit['id_domicilio']; ?>" /> 
                    <input type="hidden" id="btn" name="btn" value="" />            
                    <div class="portlet box blue">
                        <div class="portlet-title">                            
                        </div>
                        <div class="portlet-body form">
                            <br/>
                            <div class="row">
                            <div class="col-sm-5">                                                           
                                <div class="col-sm-12">
                                    <div class="input-group">   
                                        <div class="input-group-addon">
                                            <i class="fa fa-building"></i>
                                        </div>                                      
                                        <input type="text" class="form-control" id="nombre_proveedor" name="nombre_proveedor"  placeholder="Nombre del proveedor" value="<?php echo ($id != '') ? $edit['nombre'] : ''; ?>">
                                    </div>                                         
                                </div>
                                <p>&nbsp;</p>
                                <div class="col-sm-12">
                                   
                                    <select class="form-control" id="tipo" name="tipo" >
                                        <option <?php echo ($id == 0) ? 'selected=""':($edit['tipo']=='Nacional') ? 'selected=""' : ''; ?> value="Nacional">Nacional</option>
                                        <option <?php echo ($id != 0) ? ($edit['tipo']=='Importado') ? 'selected=""' : '':''; ?> value="Importado">Importado</option>
                                    </select>
                                </div>                         
                                <p>&nbsp;</p>
                                <div class="col-sm-12">
                                    <div class="input-group">   
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>                                      
                                        <input type="text" class="form-control" id="nombre_contacto" name="nombre_contacto"  placeholder="Nombre del contacto" value="<?php echo ($id != '') ? $edit['nombre_contacto'] : ''; ?>">
                                    </div>                                         
                                </div>
                                <p>&nbsp;</p>
                                <div class="col-sm-12">
                                     
                                        <div class="input-group">   
                                        <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div> 
                                        <input type="text" class="form-control" id="email" name="email"  placeholder="Email" value="<?php echo ($id != '') ? $edit['email_contacto'] : ''; ?>">
                                    </div>
                                </div>
                          <p>&nbsp;</p>
                                <div class="col-sm-8">
                                   
                                    <div class="input-group">                                        
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input data-inputmask='"mask": "99 9999 9999"' data-mask type="text" placeholder="12 3456 7890"  class="form-control" id="telefono" name="telefono" value="<?php echo ($id != '') ? $edit['telefono_contacto'] : ''; ?>"/>
                                        
                                    </div>
                                </div>                                    
                                <div class="col-sm-4">                                                                        
                                    <input class="form-control" id="ext" name="ext" placeholder="Extensi&oacute;n" value="<?php echo ($id != '') ? $edit['ext_contacto'] : ''; ?>"/>
                                </div>                           
                        </div>                     
                           
                        <div class="col-sm-5 col-sm-offset-1 well">                                 
                                    <div class="col-sm-12">  
                                    <br />
                                        <label>
                                            Calle
                                        </label>
                                        <input type="text" class="form-control" id="calle" name="calle"  placeholder="Calle" value="<?php echo ($id != '') ? $edit['calle'] : ''; ?>" >
                                    </div>                                    
                                    <div class="col-sm-6">
                                    <br />
                                        <label>
                                            # Exterior
                                        </label>
                                        <input type="text" class="form-control" id="no_exterior" name="no_exterior"  placeholder="# Exterior" value="<?php echo ($id != '') ? $edit['no_exterior'] : ''; ?>">
                                    </div>                                  
                                    <div class="col-sm-6">
                                    <br />
                                        <label>
                                            # Interior
                                        </label>
                                        <input type="text" class="form-control" id="no_interior" name="no_interior"  placeholder="# Interior" value="<?php echo ($id != '') ? $edit['no_interior'] : ''; ?>">
                                    </div>
                                    <div class="col-sm-12">
                                       <br /> <label>
                                           Colonia
                                        </label>                                                                           
                                            <input type="text" class="form-control" id="colonia" name="colonia"  placeholder="Colonia" value="<?php echo ($id != '') ? $edit['colonia'] : ''; ?>">                                      
                                    </div>    
                                    <div class="col-sm-12">
                                        <br /><label>
                                           Referencia
                                        </label>                                                                           
                                            <input type="text" class="form-control" id="referencia" name="referencia"  placeholder="Referencia" value="<?php echo ($id != '') ? $edit['referencia'] : ''; ?>">                                                                              
                                    </div>                              
                                    <div class="col-sm-6">
                                       <br /> <label>
                                           Localidad
                                        </label>                                                                             
                                            <input type="text" class="form-control" id="localidad" name="localidad"  placeholder="Localidad" value="<?php echo ($id != '') ? $edit['localidad'] : ''; ?>">
                                    </div>                                                                             
                                    <div class="col-sm-6" >
                                        <br /> <label>Municipio</label>                                                                               
                                        <input type="text" class="form-control" id="municipio" name="municipio"  placeholder="Municipio" value="<?php echo ($id != '') ? $edit['municipio'] : ''; ?>">                                       
                                    </div>       
                                    <div class="col-sm-6">
                                      <br />  <label>
                                           Estado
                                        </label>                                                                            
                                            <input type="text" class="form-control" id="estado" name="estado"  placeholder="Estado" value="<?php echo ($id != '') ? $edit['estado'] : ''; ?>">                                       
                                    </div>  
                                    <div class="col-sm-6">
                                      <br />  <label>
                                           Código Postal
                                        </label>
                                        <div class="input-group">                                        
                                            <input class="form-control integer_fields"  id="codigo_postal" name="codigo_postal" data-inputmask='"mask": "99999"' data-mask type="text" placeholder="12345" value="<?php echo ($id != '') ? $edit['codigo_postal'] : ''; ?>"/>                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                     <br />  <label>
                                           País
                                        </label>
                                        <select class="form-control" id="id_pais" name="id_pais">
                                            <option value="<?php echo $GLOBAL_id_pais; ?>"><?php echo $GLOBAL_pais; ?></option>
                                            <option value="" disabled="">--------------------</option>
                                            <?php
                                            $sql3 = "SELECT * FROM pais ORDER BY nombre";
                                            $query3 = consulta($sql3);
                                            $num3 = mysql_num_rows($query3);
                                            if ($num3 > 0) {
                                                while ($row3 = mysql_fetch_array($query3)) {
                                                    ?>
                                                    <option <?php echo ($id != '') ? ($row3['id_pais'] == $edit['id_pais']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row3['id_pais']; ?>"><?php echo $row3['nombre']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>  
                            </div>                              
                        </div>
                                                                                
                        <div class="form-actions fluid">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <a class="btn btn-danger" href="catalogo-proveedores.php"><i class="fa fa-times"></i> Cancelar</a>
                                </div>
                                <div class="col-sm-6">
                                    <?php if ($id != 0) { ?>                                            
                                        <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                    <?php } else  { ?>
                                        <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                    <?php } ?>
                                </div>
                             </div>
                        </div>
                        </div>
                    </div>
                    </form>
                    </div>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    App.init(); // initlayout and core plugins
                    TableAdvanced.init();
                        });       
                function validar_vacios() {                     
                    if (document.getElementById('nombre_proveedor').value == '') {
                        alert("Ingrese un Nombre del Proveedor");
                        return false;
                    }  else {
                    <?php if ($id != 0) { ?>
                            document.forms[0].btn.value = "modificar";
                    <?php } else  { ?>
                            document.forms[0].btn.value = "guardar";
                    <?php } ?>
                            document.forms[0].submit();
                        }
                    }

                        $(document).ready(function () {
                            $(":input").inputmask();
                        });

                        function validateMail(idMail) {
                            //Creamos un objeto 
                            object = document.getElementById(idMail);
                            valueForm = object.value;

                            // Patron para el correo
                            var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

                            if (valueForm.search(patron) == 0){
                                //Mail correcto
                                object.style.color = "#000";
                                return;
                            }
                            //Mail incorrecto
                            object.style.color = "#f00";
                        }
           </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>









