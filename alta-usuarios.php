<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM usuarios WHERE id_usuario =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));

        if($edit_usu['permiso_usuario']==5 && $edit_usu['id_cliente']){
            $sql_usu = 'SELECT nombre_cliente AS nombre FROM clientes WHERE id_cliente =' . $edit_usu['id_cliente'];
            $cliente = mysql_fetch_array(consulta($sql_usu));
            $edit_usu['nombre_usuario'] = $cliente['nombre'];
        }
    }
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Usuario
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-usuarios.php">
                                        Usuarios
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del Usuario
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <form action="alta/usuario.php" id="usuarioForm" name="usuarioForm" class="form-horizontal" method="POST">
                    <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
                    <input type="hidden" id="btn" name="btn" value="" />
                    <div class="portlet box blue">
                        <div class="portlet-title">
                        </div>
                        <div class="portlet-body form">
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span> Nombre completo
                                        </label>
                                        <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Nombre" value="<?php echo ($id != '') ? $edit_usu['nombre_usuario'] : ''; ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span> Permiso:
                                        </label>
                                        <select class="form-control" id="id_permiso" name="id_permiso" onchange="muestraCliente(this.value)">
                                            <option <?php echo ($id != '') ? ($edit_usu['permiso_usuario'] == "" || $edit_usu['permiso_usuario'] == NULL) ? 'selected=""' : '' : ''; ?> value="">[Seleccione Tipo de Permiso]</option>
                                            <?php
                                            $sql3 = "SELECT * FROM permisos WHERE activo=1 ORDER BY id_permiso";
                                            $query3 = consulta($sql3);
                                            $num3 = mysql_num_rows($query3);
                                            if ($num3 > 0) {
                                                while ($row3 = mysql_fetch_array($query3)) {
                                                    ?>
                                                    <option <?php echo ($id != '') ? ($row3['id_permiso'] == $edit_usu['permiso_usuario']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row3['id_permiso']; ?>"><?php echo $row3['descripcion']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div><!-- /.col-->
                                    <div class="col-sm-4">
                                        <label>
                                             Divisa:
                                        </label>
                                        <select class="form-control" id="id_divisa" name="id_divisa" >
                                            <option <?php echo ($id != '') ? ($edit_usu['id_divisa'] == "" || $edit_usu['id_divisa'] == NULL) ? 'selected=""' : '' : ''; ?> value="">[Seleccione Tipo de Divisa]</option>
                                            <?php
                                            $sql4 = "SELECT * FROM divisas WHERE activo=1 ORDER BY id_divisa";
                                            $query4 = consulta($sql4);
                                            $num4 = mysql_num_rows($query4);
                                            if ($num4 > 0) {
                                                while ($row4 = mysql_fetch_array($query4)) {
                                                    ?>
                                                    <option <?php echo ($id != '') ? ($row4['id_divisa'] == $edit_usu['id_divisa']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row4['id_divisa']; ?>"><?php echo $row4['abbrev']." - ".$row4['nombre']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div><!-- /.col-->

                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span>Cuenta
                                        </label>
                                        <input type="text" class="form-control" id="nom_usuario" name="nom_usuario"  placeholder="Nombre usuario" value="<?php echo ($id != '') ? $edit_usu['username'] : ''; ?>" <?php if($id !=0){ echo 'disabled=""'; } ?>>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span>Contraseña
                                        </label>
                                        <input type="text" class="form-control" id="password" name="password"  placeholder="Contraseña" value="<?php echo ($id != '') ? $edit_usu['pwd'] : ''; ?>">
                                    </div>

                                    <div class="col-sm-4" <?php if ($edit_usu['permiso_usuario'] < 6) { echo 'style="display:none;"'; } ?> id="campo_num">
                                        <label>
                                           Número de Empleado
                                        </label>
                                        <div class="input-group">
                                            <input class="form-control integer_fields"  id="num_empleado" name="num_empleado" data-inputmask='"mask": "9999"' data-mask type="text" placeholder="1234" value="<?php echo ($id != '') ? $edit_usu['num_empleado'] : ''; ?>"/>

                                        </div>
                                    </div>

                                   <div class="col-sm-4" <?php if($edit_usu['permiso_usuario'] != 5){ echo 'style="display:none;"'; } ?> id="campo_cliente">
                                        <label>
                                             Cliente Relacionado:
                                        </label>
                                        <select class="form-control" id="id_cliente" name="id_cliente" >
                                            <option <?php echo ($id != '') ? ($edit_usu['id_cliente'] == "" || $edit_usu['id_cliente'] == NULL) ? 'selected=""' : '' : ''; ?> value="">[Seleccione el cliente relacionado]</option>
                                            <?php
                                            $sql5 = "SELECT id_cliente, nombre_cliente FROM clientes WHERE activo=1 ORDER BY nombre_cliente ASC";
                                            $query5 = consulta($sql5);
                                            $num5 = mysql_num_rows($query5);
                                            if ($num5 > 0) {
                                                while ($row5 = mysql_fetch_array($query5)) {
                                                    ?>
                                                    <option <?php echo ($id != '') ? ($row5['id_cliente'] == $edit_usu['id_cliente']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row5['id_cliente']; ?>"><?php echo $row5['nombre_cliente']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                 <div class="col-sm-4">
                                        <label>
                                            <span style="color:red;">*</span>Email
                                        </label>
                                        <input type="text" class="form-control" onKeyUp="javascript:validateMail('email')" id="email" name="email"  placeholder="Email" value="<?php echo ($id != '') ? $edit_usu['email_usuario'] : ''; ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            T&eacute;lefono
                                        </label>
                                        <div class="input-group">
                                            <input data-inputmask='"mask": "99-99999999"' data-mask type="text" placeholder="12-34567890"  class="form-control integer_fields" id="telefono" name="telefono" value="<?php echo ($id != '') ? $edit_usu['telefono_usuario'] : ''; ?>"/>
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>
                                            Extensi&oacute;n
                                        </label>
                                        <input class="form-control integer_fields" id="ext" name="ext" placeholder="Extensi&oacute;n" value="<?php echo ($id != '') ? $edit_usu['ext_usuario'] : ''; ?>"/>
                                    </div>

                                </div>
                            </div>
                            <br/>
                                <div class="form-actions fluid">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <a class="btn btn-danger" href="catalogo-usuarios.php"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php if ($id != 0) { ?>
                                                <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                            <?php } else  { ?>
                                                <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                            <?php } ?>
                                        </div>
                                     </div>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    App.init();
                    TableAdvanced.init();
                });

                function validar_vacios() {
                    var usuarios = [
                        <?php
                            // Creamos un arreglo en PHP  con todos los nombres de usuario y los metemos en un arreglo.

                            $sql6 = "SELECT username FROM usuarios";
                            $query6 = consulta($sql6);

                            $num6 = mysql_num_rows($query6);

                            if ($num6 > 0) {
                                while ($row6 = mysql_fetch_array($query6)) {
                                    echo '"' . $row6['username'] . '", ';
                                }
                            }
                        ?>
                        "admin"
                    ];

                    <?php if ($id == 0) { ?>
                        if ($.inArray($('#nombre').val(), usuarios) != -1) {
                            alert("El usuario ya se encuentra registrado");
                            return false;
                        }
                    <?php } ?>

                    if (document.getElementById('id_permiso').value == '') {
                        alert("Seleccione un permiso para el usuario");
                        return false;
                    }

                    if (document.getElementById('id_permiso').value > 5) {
                        if (document.getElementById('num_empleado').value == '') {
                            alert("Ingrese un Numero de Empleado");
                            return false;
                        }
                    }
                    if (document.getElementById('nom_usuario').value == '') {
                        alert("Ingrese un Nombre de Usuario");
                        return false;
                    } else if (document.getElementById('password').value == '') {
                        alert("Ingrese un Password");
                        return false;
                    } else if (document.getElementById('email').value == '') {
                        alert("Ingrese un Email o Correo Electronico");
                        return false;
                    } else if (document.getElementById('nombre').value == '') {
                        alert("Ingrese Nombre(s)");
                        return false;
                    }  else {
            <?php if ($id != 0) { ?>
                    document.forms[0].btn.value = "modificar";
            <?php } else  { ?>
                    document.forms[0].btn.value = "guardar";
            <?php } ?>
                    document.forms[0].submit();
                    }
                }

                $(document).ready(function () {
                    $(":input").inputmask();
                });

                function validateMail(idMail) {
                    //Creamos un objeto
                    object = document.getElementById(idMail);
                    valueForm = object.value;

                    // Patron para el correo
                    var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

                    if (valueForm.search(patron) == 0){
                        //Mail correcto
                        object.style.color = "#000";
                        return;
                    }
                    //Mail incorrecto
                    object.style.color = "#f00";
                }

                function muestraCliente(permiso) {
                    console.log(permiso);

                    if (permiso == 5) {
                        $("#campo_cliente").show();
                        $("#campo_num").hide();
                    } else if (permiso > 5) {
                         $("#campo_num").show();
                         $("#campo_cliente").hide();
                    } else {
                        $("#campo_cliente").hide();
                        $("#campo_num").hide();
                    }
                }
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
