<?php

if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    if ($id != '') {
        $sql_mul = 'SELECT * FROM ventana WHERE id_ventana =' . $id;
        $edit_mul = mysql_fetch_array(consulta($sql_mul));
    }
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Ventana
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-ventanas.php">
                                        Ventanas
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos de la Ventana
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->    
                    <form action="alta/ventanas.php" id="ventanasForm" name="ventanasForm" class="form-horizontal" method="POST">  
                        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />   
                        <input type="hidden" id="btn" name="btn" value="" />            
                        <div class="portlet box blue">
                            <div class="portlet-title">                            
                            </div>
                            <div class="portlet-body">
                                <br/>
                                <div class="row">
                                    <div class="col-sm-4">  
                                        <span style="color:red;">*</span> <label>Nombre ventana:</label>
                                        <input type="text" class="form-control" id="nombre_ventana" name="nombre_ventana" placeholder="Nombre" value="<?php echo ($id != '') ? $edit_mul['nombre_ventana'] : ''; ?>">
                                    </div>   
                                    <div class="col-sm-4">  
                                        <span style="color:red;">*</span> <label>Descripción:</label>
                                        <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" value="<?php echo ($id != '') ? $edit_mul['descripcion'] : ''; ?>">
                                    </div>  
                                    <div class="col-sm-4">  
                                        <span style="color:red;">*</span> <label>Tipo</label>
                                        <select class="form-control" id="tipo" name="tipo">
                                            <option value="">Seleccione un tipo ...</option>                                            
                                            <?php
                                                $sql1 = "SELECT * FROM puerta_tipo WHERE activo = 1";
                                                $query1 = consulta($sql1);
                                                $num1 = mysql_num_rows($query1);
                                                if ($num1 > 0) {
                                                    while ($row1 = mysql_fetch_array($query1)) { ?>
                                                        <option <?php echo ($id != '') ? ($row1['id_ptipo'] == $edit_mul['id_ptipo']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row1['id_ptipo']; ?>"><?php echo $row1['nombre_tipo']; ?></option>
                                                    <?php }
                                                }
                                            ?>
                                        </select>
                                    </div> 
                                </div>
                                <br/>
                                <div class="row">
                                    <!--
                                    <div class="col-sm-4">  
                                        <span style="color:red;">*</span> <label>Impuesto:</label>
                                        <input type="text" class="form-control" id="impuesto" name="impuesto" placeholder="Impuesto" value="<?php echo ($id != '') ? $edit_mul['impuesto'] : ''; ?>">
                                    </div>
                                    -->
                                    <div class="col-sm-4">  
                                        <span style="color:red;">*</span> <label>Precio:</label>
                                        <input type="text" class="form-control" id="precio" name="precio" placeholder="Precio" value="<?php echo ($id != '') ? $edit_mul['precio'] : ''; ?>">
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VA:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VA" id="VA" value="<?php echo ($id != '') ? $edit_mul['VA'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VB:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VB" id="VB" value="<?php echo ($id != '') ? $edit_mul['VB'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VC:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VC" id="VC" value="<?php echo ($id != '') ? $edit_mul['VC'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VD:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VD" id="VD" value="<?php echo ($id != '') ? $edit_mul['VD'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VE:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VE" id="VE" value="<?php echo ($id != '') ? $edit_mul['VE'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-2">  
                                        <span style="color:red;">*</span><label>VF:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" name="VF" id="VF" value="<?php echo ($id != '') ? $edit_mul['VF'] : ''; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-actions fluid">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <a class="btn btn-danger" href="catalogo-ventanas.php"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php if ($id != 0) { ?>                        
                                                <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                            <?php } else { ?>
                                                <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    App.init();
                    TableAdvanced.init();
                });

                function validar_vacios() {
                    if (document.getElementById('nombre_ventana').value == '') {
                        alert("Introduzca el nombre de la ventana");
                        return false;
                    }

                    if (document.getElementById('descripcion').value == '') {
                        alert("Introduzca una descripción");
                        return false;
                    }

                    if (document.getElementById('tipo').value == '') {
                        alert("Seleccione un tipo");
                        return false;
                    }

                    if (document.getElementById('precio').value == '') {
                        alert("Introduzca el precio");
                        return false;
                    }

                    /*
                    if (document.getElementById('impuesto').value == '') {
                        alert("Introduzca el impuesto");
                        return false;
                    }
                    */

                    <?php if ($id != 0) { ?>
                        document.forms[0].btn.value = "modificar";
                    <?php } else { ?>
                        document.forms[0].btn.value = "guardar";
                    <?php } ?>
                    
                    document.forms[0].submit();
                }

                // $(document).ready(function () {
                //  $(":input").inputmask();
                // });
            </script>
            <!-- END JAVASCRIPTS -->

    </body>
    <!-- END BODY -->
</html>









