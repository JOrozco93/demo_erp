<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

function updatePreciosPuerta($puerta) {
    global $db_con;

    $sql = "
        UPDATE
            puerta
        SET
            VA = {$puerta['VA']},
            VB = {$puerta['VB']},
            VC = {$puerta['VC']},
            VD = {$puerta['VD']},
            VE = {$puerta['VE']},
            VF = {$puerta['VF']},
            precio = {$puerta['precio']}
        WHERE
            id_puerta = {$puerta['id_puerta']}
    ";

    // comprobamos que el string para la consulta este bien construido
    // echo $sql . '<br/>';

    $result = mysql_query($sql, $db_con);

    // revisamos el resultado de la consulta
    // echo ($result == 1 ? 'true' : 'false') . '<br/>';
}

function updatePreciosComponente($componente) {
    global $db_con;

    $sql = "
        UPDATE
            componentes
        SET
            VA = {$componente['VA']},
            VB = {$componente['VB']},
            VC = {$componente['VC']},
            VD = {$componente['VD']},
            VE = {$componente['VE']},
            VF = {$componente['VF']},
            precio = {$componente['precio']}
        WHERE
            id_componente = {$componente['id_componente']}
    ";

    // comprobamos que el string para la consulta este bien construido
    // echo $sql . '<br/>';

    $result = mysql_query($sql, $db_con);

    // revisamos el resultado de la consulta
    // echo ($result == 1 ? 'true' : 'false') . '<br/>';
}

function updatePreciosKit($kit) {
    global $db_con;

    $sql = "
        UPDATE
            kit
        SET
            VA = {$kit['VA']},
            VB = {$kit['VB']},
            VC = {$kit['VC']},
            VD = {$kit['VD']},
            VE = {$kit['VE']},
            VF = {$kit['VF']},
            precio = {$kit['precio']}
        WHERE
            id_kit = {$kit['id_kit']}
    ";

    // comprobamos que el string para la consulta este bien construido
    // echo $sql . '<br/>';

    $result = mysql_query($sql, $db_con);

    // revisamos el resultado de la consulta
    // echo ($result == 1 ? 'true' : 'false') . '<br/>';
}

function updatePreciosVentana($ventana) {
    global $db_con;

    $sql = "
        UPDATE
            ventana
        SET
            VA = {$ventana['VA']},
            VB = {$ventana['VB']},
            VC = {$ventana['VC']},
            VD = {$ventana['VD']},
            VE = {$ventana['VE']},
            VF = {$ventana['VF']},
            precio = {$ventana['precio']}
        WHERE
            id_ventana = {$ventana['id_ventana']}
    ";

    // comprobamos que el string para la consulta este bien construido
    // echo $sql . '<br/>';

    $result = mysql_query($sql, $db_con);

    // revisamos el resultado de la consulta
    // echo ($result == 1 ? 'true' : 'false') . '<br/>';
}

$id_usuario = $_SESSION['id_usuario'];

$puertas = isset($_POST['puertas']) ? $_POST['puertas'] : [];
$componentes = isset($_POST['componentes']) ? $_POST['componentes'] : [];
$kits = isset($_POST['kits']) ? $_POST['kits'] : [];
$ventanas = isset($_POST['ventanas']) ? $_POST['ventanas'] : [];


// echo '<pre>' . print_r($_POST, 1) . '</pre>';

// echo count($puertas) .'<br>';
// echo count($componentes) . '<br>';
// echo count($kits) . '<br>';
// echo count($ventanas) . '<br>';

// exit;

foreach ($puertas as $puerta) {
    updatePreciosPuerta($puerta);
}

foreach ($componentes as $componente) {
    updatePreciosComponente($componente);
}

foreach ($kits as $kit) {
    updatePreciosKit($kit);
}

foreach ($ventanas as $ventana) {
    updatePreciosVentana($ventana);
}

echo "
    <script>
        alert('Los datos han sido actualizados con exito');
        location.href = '../actualizar-precios.php';
    </script>
";

?>