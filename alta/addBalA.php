<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <link href="plugins/data-tables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <link href="css/jquery-ui.min.css" rel="stylesheet">
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menuLateralGeneral.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">

                        <div class="toggler-close">
                        </div>

                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Balances
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">

                                <li>
                                    <i class="fa fa-usd"></i>
                                    <a href="#">
                                        Balances
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="balanceA.php">
                                        Balances General Ortiz
                                    </a>
                                    
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="" id="" name="" method="POST">
                                        <div class="row">
                                            <div class="col-lg-12"><div class="col-lg-12"><h3> Balances General Ortiz</h3></div></div>
                                            <br/>
                                            <div class="col-lg-12">
                                                <div class="col-md-4">
                                                    <label><span style="color:red">*</span>Tipo de Gastos:</label>
                                                    <select class="form-control" id="tipo_gasto" name="tipo_gasto">
                                                        <option value="">[Seleccione Tipo de Gasto]</option>
                                                        <option value="va">Inversiones</option>
                                                        <option value="vb">Inventarios</option>
                                                        <option value="vc">Operativos</option>
                                                        <option value="vd">Rentas</option>
                                                        <option value="ve">Bancos</option>
                                                        <option value="vf">Publicidad y Comercializacion</option>
                                                        <option value="vg">Costos Financieros</option>
                                                        <option value="vh">Salarios y Comiciones Nominales</option>
                                                        <option value="vi">Cheques Devueltos</option>
                                                        <option value="vi">Devueluciones</option>
                                                        <option value="vi">Colombia</option>
                                                        <option value="vi">Peru</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><span style="color:red">*</span>Monto de Gasto:</label>
                                                    <input class="form-control" id="valor_gasto" name="valor_gasto" type="text">
                                                </div>
                                                <div class="col-md-4">
                                                    <button  title="Agregar Gasto" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>   
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-sm-12">                                            
                                                <div class="col-sm-4 col-sm-offset-8">
                                                    <label>Total de Gastos:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            $
                                                        </div>
                                                        <input class="form-control" id="total_gastos" name="total_gastos" disabled="" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-md-4">
                                                    <label><span style="color:red">*</span>Tipo de Venta:</label>
                                                    <select class="form-control" id="tipo_gasto" name="tipo_gasto">
                                                        <option value="">[Seleccione Tipo de Venta]</option>
                                                        <option value="va">Reposicion de Cheques Devueltos</option>
                                                        <option value="vb">Prestamos de Inversiones Ortiz</option>
                                                        <option value="vc">Feria de Compras</option>
                                                        <option value="vd">Prestamos de Inversiones Otros</option>
                                                        <option value="ve">Devoluciones</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><span style="color:red">*</span>Monto de Venta:</label>
                                                    <input class="form-control" id="valor_venta" name="valor_venta" type="text">
                                                </div>
                                                <div class="col-md-4">
                                                    <button  title="Agregar Venta" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">                                            
                                                <div class="col-sm-4 col-sm-offset-8">
                                                    <label>Total de Ventas:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            $
                                                        </div>
                                                        <input class="form-control" id="total_venta" name="total_venta" disabled="" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-sm-12">                                            
                                                <div class="col-sm-4 col-sm-offset-8">
                                                    <label>Total de Gastos - Total de Ventas:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            $
                                                        </div>
                                                        <input class="form-control" id="total_general" name="total_general" disabled="" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions fluid">
                                            <div class="col-lg-12">
                                                <div class="col-lg-6">
                                                    <button type="button" class="btn btn-warning" onclick="window.location.href = 'balanceA.php'"><i class="fa fa-arrow-circle-left"></i> Regresar a la Lista</button>
                                                </div>
                                                <div class="col-lg-6">

                                                    <button type="button" class="btn btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-save"></i> Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="btn" name="btn" />
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                            <!-- END VALIDATION STATES-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("includes/footer.php"); ?>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
        <?php include ("includes/js.php"); ?>


        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
                                                        jQuery(document).ready(function () {
                                                            App.init(); // initlayout and core plugins
                                                        });
                                                        $(document).ready(function () {
                                                            $("#pais").autocomplete({
                                                                source: 'autocompletar_pais.php',
                                                                minLength: 1,
                                                                select: function (event, ui) {
                                                                    //                        alert(ui.item.imdbid);
                                                                    document.forms[0].id_pais.value = ui.item.imdbid;
                                                                }
                                                            });
                                                            $("#divisas").autocomplete({
                                                                source: 'autocompletar_divisas.php',
                                                                minLength: 1,
                                                                select: function (event, ui) {
                                                                    //                        alert(ui.item.imdbid);
                                                                    document.forms[0].id_divisas.value = ui.item.imdbid;
                                                                }
                                                            });
                                                        });
                                                        function validar_vacios() {
                                                            if (document.getElementById('tipo_costo').value == '') {
                                                                alert("Seleccione un TIpo de Costo");
                                                                return false;
                                                            } else if (document.getElementById('nom_cliente').value == '') {
                                                                alert("Introduzca el Nombre del Empresa del Cliente o del Cliente");
                                                                return false;

                                                            } else if (document.getElementById('email').value == '') {
                                                                alert("Introduzca el Correo Electronico o Email del Cliente");
                                                                return false;

                                                            } else if (document.getElementById('nom_contacto').value == '') {
                                                                alert("Introduzca el Nombre del Autorizado o Asistente del Cliente");
                                                                return false;

                                                            } else if (document.getElementById('email2').value == '') {
                                                                alert("Introduzca el Correo Electronico o Email del Autorizado o Asistente del Cliente");
                                                                return false;

                                                            } else {
                                                                (document.getElementById('option1').checked) ? document.forms[0].prospecto.value = 1 : (document.getElementById('option2').checked) ? document.forms[0].prospecto.value = 2 : document.forms[0].prospecto.value = 1;
                                                                //                    alert(document.forms[0].prospecto.value);
<?php if ($id_cliente != '') { ?>
                                                                    document.forms[0].btn.value = "modificar";
<?php } else if ($id_cliente == '') { ?>
                                                                    document.forms[0].btn.value = "guardar";
<?php } ?>
                                                                document.forms[0].submit();
                                                            }
                                                        }

                                                        //            function validar_vacios1() {
                                                        //                document.forms[0].btn.value = "modificar";
                                                        //                document.forms[0].submit();
                                                        //            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>