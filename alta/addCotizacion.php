<?php

require_once("../config.php");
require_once("../includes/funciones_BD.php");
require_once("../includes/validacion.php");

$id_cotizacion = $_POST['id_cotizacion'];
$id_cliente = $_POST['id_cliente'];
$id_divisa = $_POST['id_divisa'];
$id_impuesto = $_POST['id_impuesto'];
$tipo_despacho = $_POST['id_despacho'];
$descripcion = $_POST['descripcion'];
$id_termino_venta = $_POST['id_termino_venta'];

$subtotal = $_POST['subtotal'];
$impuestos = $_POST['impuestos'];
$total = $_POST['total'];

$caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
$desordenada = str_shuffle($caracteres);
$num = substr($desordenada, 2, 9);

$numero_cotizacion = date('Ymd') . 'HS' . $num;

/*
$sql_update=sprintf("UPDATE `cotizaciones` SET 
`id_cliente`='$id_cliente',
`numero_cotizacion`='$numero_cotizacion',
`descripcion`='$descripcion',
`autorizada`='$autorizada',
`autorizada_rechazo`='$autorizada_rechazo',
`pagada`='$pagada',
`pagada_rechazo`='$pagada_rechazo',
`pagada_comprobante`='$pagada_comprobante',
`precio_venta`='$precio_venta',
`id_divisa`='$id_divisa',
`tipo_cambio`='$tipo_cambio',
`tipo_despacho`='$tipo_despacho',
`impuestos`='$impuestos',
`total`='$total',
`id_vendedor`='$id_vendedor',
`activo`='$activo',
`id_log`='$id_log' WHERE id_cotizacion='$id_cotizacion'");
*/

$sql_update = sprintf("
	UPDATE
		cotizaciones
	SET
		id_cliente = $id_cliente,
		numero_cotizacion = '$numero_cotizacion',
		id_divisa = $id_divisa,
		id_impuesto = $id_impuesto,
		tipo_despacho = $tipo_despacho,
		descripcion = '$descripcion',
		id_termino_venta = $id_termino_venta,
		subtotal = $subtotal,
		impuestos = $impuestos,
		total = $total,
		activo = 1
 	WHERE
 		id_cotizacion = $id_cotizacion
 ");

$result = mysql_query($sql_update, $db_con) or die("Problemas en la consulta" . $sql_update);

echo "<script>alert('Los datos fueron guardados con \u00e9xito'); location.href='../listado-cotizaciones.php'</script>";

?>