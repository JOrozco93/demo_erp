<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id =$_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM usuarios WHERE id_usuario =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));
        $id_divisa = $edit_usu['id_divisa'];
    }
} else if ($_GET['id'] == '0') {
    $id = '';
    $id_divisa = 0;
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>  
        <link href="plugins/data-tables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" type="text/css" href="css/style-custom.css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <link href="css/jquery-ui.min.css" rel="stylesheet">
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menuLateralGeneral.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Usuario
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogo
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="usuarios.php">
                                        Usuarios
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del Usuario
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->                   
                    <form action="alta/altaUsuario.php" id="usuarioForm" name="usuarioForm" class="form-horizontal" method="POST">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                            </div>
                            <div class="portlet-body form">                           

                                <br/>
                                <div class="row">
                                    <div class="col-lg-12">                                        
                                        <div class="col-lg-3">
                                            <label>
                                                <span style="color:red;">*</span>Puesto:
                                            </label>
                                            <select class="form-control" id="id_tpusuario" name="id_tpusuario" onchange="view(this.value);">
                                                <option <?php echo ($id_usuarioDes != '') ? ($edit_usu['id_tpusuario'] == "" || $edit_usu['id_tpusuario'] == NULL) ? 'selected=""' : '' : ''; ?> value="">[Seleccione Tipo de Permiso]</option>
                                                <?php
                                                $sql3 = "SELECT * FROM tp_usuario WHERE activo=1 ORDER BY id_tpusuario";
                                                $query3 = consulta($sql3);
                                                $num3 = mysql_num_rows($query3);
                                                if ($num3 > 0) {
                                                    while ($row3 = mysql_fetch_array($query3)) {
                                                        ?>
                                                        <option <?php echo ($id_usuarioDes != '') ? ($row3['id_tpusuario'] == $edit_usu['id_tpusuario']) ? 'selected=""' : '' : ''; ?> value="<?php echo $row3['id_tpusuario']; ?>"><?php echo $row3['descripcion']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div><!-- /.col--> 
                                        <div class="col-lg-3" id="view_num_empleado">                                            
                                            <label>
                                                <span style="color:red;">*</span>Numero de Empleado 
                                            </label>
                                            <input type="text" class="form-control" id="num_empleado" name="num_empleado" placeholder="Numero Empleado" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['num_empleado'] : ''; ?>">                                                                                            
                                        </div>
                                        <div class="col-lg-3">  
                                            <label>
                                                <span style="color:red;">*</span>Nombre de Usuario
                                            </label>
                                            <input type="text" class="form-control" id="nom_usuario" name="nom_usuario" placeholder="Nombre Usuario" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['nom_usuario'] : ''; ?>">
                                        </div>
                                        <div class="col-lg-3">
                                            <label>
                                                <span style="color:red;">*</span>Password
                                            </label>
                                            <input type="text" class="form-control pass" id="password" name="password" placeholder="Password" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['password'] : ''; ?>">
                                        </div>                                        
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-lg-12">      
                                        <div class="col-lg-4">
                                            <label>
                                                <span style="color:red;">*</span>País
                                            </label>
                                            <select class="form-control"  id="pais" name="id_pais">
                                                <?php
                                                    $sql2 = "SELECT * FROM pais ORDER BY nombre";
                                                    $query2 = consulta($sql2);
                                                    $num2 = mysql_num_rows($query2);
                                                    $x = 0;
                                                    $row2 = "";
                                                    if ($num2 > 0) {
                                                    while ($row2 = mysql_fetch_array($query2)) {
                                                ?>
                                                <option value="<?php echo $row2['id_pais']; ?>" 
                                                    <?php
                                                        echo ($pais_id == $row2['id_pais'] ? 'selected' : '');  
                                                    ?>
                                                    > 
                                                    <?php echo $row2['nombre']; ?>
                                                </option>
                                                <?php
                                                        $x++;
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-4" id="view_divisas">
                                            <label>
                                                <span style="color:red;">*</span>Divisas
                                            </label>
                                            <select class="form-control" id="id_divisas" name="id_divisas">
                                                <?php
                                                    $sql4 = "SELECT * FROM divisas ORDER BY abbrev";
                                                    $query4 = consulta($sql4);
                                                    $num4 = mysql_num_rows($query4);
                                                    $x = 0;
                                                    $row4 = "";
                                                    if ($num4 > 0) {
                                                    while ($row4 = mysql_fetch_array($query4)) {
                                                ?>
                                                <option value="<?php echo $row4['id_divisas']; ?>" 
                                                    <?php
                                                        echo ($divisas_id == $row4['id_divisas'] ? 'selected' : '');  
                                                    ?>
                                                    > 
                                                    <?php echo $row4['abbrev']; ?>
                                                </option>
                                                <?php
                                                        $x++;
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <!-- <input type="hidden"  id="id_divisas" name="id_divisas"/> -->
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                <span style="color:red;">*</span>Email
                                            </label>
                                            <input type="text" onKeyUp="javascript:validateMail('email')" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['email'] : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <label>
                                                <span style="color:red;">*</span>Nombre(s)
                                            </label>
                                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['nombre'] : ''; ?>">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                <span style="color:red;">*</span>Apellido Paterno
                                            </label>
                                            <input type="text" class="form-control" id="ap_paterno" name="ap_paterno" placeholder="Apellido Paterno" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['ap_paterno'] : ''; ?>">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                Apellido Materno
                                            </label>
                                            <input type="text" class="form-control" id="ap_materno" name="ap_materno" placeholder="Apellido Materno" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['ap_materno'] : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <label>
                                                T&eacute;lefono de Casa
                                            </label>
                                            <div class="input-group">                                        
                                                <input data-mask="" class="form-control integer_fields" id="tel1" name="tel1" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['tel1'] : ''; ?>" data-inputmask='"mask": "99-99999999"' data-mask type="text" placeholder="12-34567890"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                            </div>

                                            <!--                                            <div class="input-group">                                        
                                                                                            <input data-inputmask='"mask": "99-99999999"' data-mask type="text" placeholder="12-34567890"  class="form-control integer_fields" id="tel1" name="tel1" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['tel1'] : ''; ?>"/>
                                                                                            <div class="input-group-addon">
                                                                                                <i class="fa fa-phone"></i>
                                                                                            </div>
                                                                                        </div>-->
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                T&eacute;lefono de Oficina
                                            </label>
                                            <div class="input-group">                                        
                                                <input data-mask="" class="form-control integer_fields" id="tel2" name="tel2" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['tel2'] : ''; ?>" data-inputmask='"mask": "99-99999999"' data-mask type="text" placeholder="12-34567890"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                            </div>

                                            <!--                                            <div class="input-group">                                        
                                                                                            <input class="form-control integer_fields" id="tel2" name="tel2" data-inputmask='"mask": "99-99999999"' data-mask type="text" placeholder="12-34567890" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['tel2'] : ''; ?>"/>
                                                                                            <div class="input-group-addon">
                                                                                                <i class="fa fa-phone"></i>
                                                                                            </div>
                                                                                        </div>-->
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                Extenci&oacute;n
                                            </label>                                     
                                            <input class="form-control integer_fields" id="ext" name="ext" placeholder="Extenci&oacute;n" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['ext'] : ''; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">                                    
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <label>
                                                RFC
                                            </label>                                    
                                            <input class="form-control" id="rfc" name="rfc" placeholder="RFC" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['rfc'] : ''; ?>"/>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                CURP
                                            </label>                                    
                                            <input class="form-control" id="curp" name="curp" placeholder="CURP" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['curp'] : ''; ?>"/>
                                        </div>
                                        <div class="col-lg-4">
                                            <label>
                                                NSS
                                            </label>                                    
                                            <input class="form-control" id="nss" name="nss" placeholder="NSS" value="<?php echo ($id_usuarioDes != '') ? $edit_usu['nss'] : ''; ?>"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions fluid">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <button type="button" class="btn btn-danger" onclick="window.location.href = 'usuarios.php'"><i class="fa fa-times"></i> Cancelar</button>
                                        </div>
                                        <div class="col-lg-6">
                                            <?php if ($id_usuarioDes != '') { ?>                                            
                                                <button type="button" class="btn btn-info pull-right" onclick="validar_vacios();"><i class="fa fa-pencil"></i> Modificar</button>
                                            <?php } else if ($id_usuarioDes == '') { ?>
                                                <button type="button" class="btn blue btn-success pull-right" onclick="validar_vacios();"><i class="fa fa-floppy-o"></i> Guardar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="btn" name="btn"/>

                            </div>
                        </div>
                    </form>
                </div>
            </div>  
        </div>
        <?php include ("includes/footer.php"); ?>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
        <?php // include ("includes/js.php"); ?>


        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="scripts/core/app.js" type="text/javascript"></script>
        <!--<script src="assets/scripts/custom/tasks.js" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL SCRIPTS -->


        <!--<script src="plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>-->
        <script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="scripts/validaNumeros.js" type="text/javascript"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <!--<script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->
        <!--<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>-->
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>


        <!-- input Masck -->
        <script src="plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <!--<script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->
        <script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>

                                                    jQuery(document).ready(function () {
                                                        App.init(); // initlayout and core plugins
                                                    });
                                                    $(document).ready(function () {
                                                        $("#pais").autocomplete({
                                                            source: 'autocompletar_pais.php',
                                                            minLength: 1,
                                                            select: function (event, ui) {
                                                                //                        alert(ui.item.imdbid);
                                                                document.forms[0].id_pais.value = ui.item.imdbid;
                                                            }
                                                        });
                                                        $("#divisas").autocomplete({
                                                            source: 'autocompletar_divisas.php',
                                                            minLength: 1,
                                                            select: function (event, ui) {
                                                                //                        alert(ui.item.imdbid);
                                                                document.forms[0].id_divisas.value = ui.item.imdbid;
                                                            }
                                                        });
                                                    });

                                                    function view(value) {
                                                        if (value < 3) {
                                                            document.getElementById('view_num_empleado').style.display = "none";
                                                        } else if (value > 3) {
                                                            document.getElementById('view_num_empleado').style.display = "block";
                                                        }
                                                        if (value < 2) {
                                                            document.getElementById('view_divisas').style.display = "none";
                                                        } else if (value > 2) {
                                                            document.getElementById('view_divisas').style.display = "block";
                                                        }
                                                    }

                                                    var valida = 0;
                                                    function validar_vacios() {
//                                                    if (document.getElementById('id_tpusuario').value == '') {
//                                                        alert("Seleccione un Tipo de Usuario");
//                                                        return false;
//                                                    } else if (document.getElementById('id_tpusuario').value != 1) {
//                                                        if (document.getElementById('num_empleado').value < 3) {
//                                                            alert("Ingrese un Numero de Empleado");
//                                                            return false;
//                                                        }
//                                                    }
//                                                    if (document.getElementById('id_tpusuario').value == '') {
//                                                        alert("Seleccione un Tipo de Usuario");
//                                                        return false;
//                                                    } else if (document.getElementById('id_tpusuario').value > 3) {
//                                                        if (document.getElementById('num_empleado').value == '') {
//                                                            alert("Ingrese un Numero de Empleado");
//                                                            return false;
//                                                        }
//                                                    }
                                                        if (document.getElementById('id_tpusuario').value == '') {
                                                            alert("Seleccione un Tipo de Usuario");
                                                            return false;
                                                        }
                                                        if (document.getElementById('id_tpusuario').value > 4) {
                                                            if (document.getElementById('num_empleado').value == '') {
                                                                alert("Ingrese un Numero de Empleado");
                                                                return false;
                                                            }
                                                        }
//                                                    if (document.getElementById('id_tpusuario').value == '') {
//                                                        alert("Seleccione un Tipo de Usuario");
//                                                        return false;
//                                                    } else if (document.getElementById('id_tpusuario').value != 1) {
//                                                        if (document.getElementById('num_empleado').value == '') {
//                                                            alert("Ingrese un Numero de Empleado");
//                                                            return false;
//                                                        }
//                                                    }
                                                        if (document.getElementById('nom_usuario').value == '') {
                                                            alert("Ingrese un Nombre de Usuario");
                                                            return false;
                                                        } else if (document.getElementById('password').value == '') {
                                                            alert("Ingrese un Password");
                                                            return false;
                                                        } else if (document.getElementById('pais').value == '') {
                                                            alert("Ingrese un Pais");
                                                            return false;
                                                        /*} else if (document.getElementById('divisas').value > 2) {
                                                            alert("Ingrese una Divisa");
                                                            return false;*/
                                                        } else if (document.getElementById('email').value == '') {
                                                            alert("Ingrese un Email o Correo Electronico");
                                                            return false;
                                                        } else if (document.getElementById('nombre').value == '') {
                                                            alert("Ingrese Nombre(s)");
                                                            return false;
                                                        } else if (document.getElementById('ap_paterno').value == '') {
                                                            alert("Ingrese el Apellido Paterno");
                                                            return false;
//<?php
        if ($id_usuarioDes != '') {
            $sql_usu = "SELECT * FROM usuario WHERE activo=1 ORDER BY id_usuario";
            $query_usu = consulta($sql_usu);
            $num_usu = mysql_num_rows($query_usu);
            if ($num_usu > 0) {
                while ($row_usu = mysql_fetch_array($query_usu)) {

                    $sql_tp = 'SELECT * FROM tp_usuario WHERE id_tpusuario =' . $row_usu['id_tpusuario'];
                    $edit_tp = mysql_fetch_array(consulta($sql_tp));
                    ?>////
                                                                    } else if ((document.getElementById('nom_usuario').value == '<?php echo $row_usu['nom_usuario']; ?>' ) && (document.getElementById('id_tpusuario').value == '<?php echo $row_usu['id_tpusuario'] ?>' ) ) {
                                                                        alert("El Nombre: " + document.getElementById('nom_usuario').value + " ya existe para el Tipo de Usuario:" + '<?php echo $edit_tp['descripcion']; ?>');
                                                                        document.getElementById('nombre').value = "";
                                                                        document.getElementById('tp_usuario').value = "";
                                                                        return false;
                                                                    } else if ((document.getElementById('nom_usuario').value == '<?php echo $row_usu['nom_usuario']; ?>' ) ) {
                                                                        alert("El Nombre: " + document.getElementById('nom_usuario').value + " ya existe");
                                                                        document.getElementById('nombre').value = "";
                                                                        return false;
            <?php
        }
    }
}
?>////
                                                        } else {
<?php if ($id_usuarioDes == '') { ?>
                                                                document.forms[0].btn.value = "guardar";
<?php } else if ($id_usuarioDes != '') { ?>
                                                                document.forms[0].btn.value = "modificar";
<?php } ?>
                                                            document.forms[0].submit();
                                                        }
                                                    }

                                                    $(document).ready(function () {
                                                        $(":input").inputmask();
                                                    });

                                                    function validateMail(idMail)

                                                    {

                                                        //Creamos un objeto 

                                                        object = document.getElementById(idMail);

                                                        valueForm = object.value;



                                                        // Patron para el correo

                                                        var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

                                                        if (valueForm.search(patron) == 0)

                                                        {

                                                            //Mail correcto

                                                            object.style.color = "#000";

                                                            return;

                                                        }

                                                        //Mail incorrecto

                                                        object.style.color = "#f00";

                                                    }
        </script>

        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>