<?php
if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cliente = (isset($_POST['id_cliente'])) ? $_POST['id_cliente'] : null;
$id_divisa = (isset($_POST['id_divisa'])) ? $_POST['id_divisa'] : null;
$id_impuesto = (isset($_POST['id_impuesto'])) ? $_POST['id_impuesto'] : null;

$id_componente = $_POST['id_componente'];

// obtenet los datos del cliente
$cliente = $pdo->query("SELECT * FROM clientes WHERE id_cliente = $id_cliente LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$cliente_tipo_costo = $cliente['tipo_costo'];  // id tipo de costo

// obtener la divisa
$divisa = $pdo->query("SELECT * FROM divisas WHERE id_divisa = $id_divisa LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$divisa_tipo_cambio = $divisa['tipo_cambio']; // valor de la divisa

// obtener el impuesto
$impuesto = $pdo->query("SELECT * FROM impuestos WHERE id_impuesto = $id_impuesto LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$impuesto_porcentaje = $impuesto['porcentaje']; // porcentaje del impuesto

// obtener el precio del componente
$componente = $pdo->query("SELECT * FROM componentes WHERE id_componente = $id_componente LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$componente_precio = $componente[$cliente_tipo_costo]; // precio del componente

// CALCULAR EL COSTO TOTAL DEL COMPONENTE
$total = $componente_precio * $divisa_tipo_cambio; // agregar la divisa

// devolver el total del componente
echo $_GET['callback'] . "(" . json_encode($total) . ");";

?>