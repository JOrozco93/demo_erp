<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cliente = (isset($_POST['id_cliente'])) ? $_POST['id_cliente'] : null;
$id_divisa = (isset($_POST['id_divisa'])) ? $_POST['id_divisa'] : null;
$id_impuesto = (isset($_POST['id_impuesto'])) ? $_POST['id_impuesto'] : null;

$id_ptipo = $_POST['id_ptipo'];
$id_puerta = $_POST['id_puerta'];
$id_pmodelo = $_POST['id_pmodelo'];
$id_pcolor = $_POST['id_pcolor'];
$id_pmovimiento = $_POST['id_pmovimiento'];
$id_textura = $_POST['id_textura'];
$id_sello = $_POST['id_sello'];
$id_motor = $_POST['id_motor'];
$id_ventana = $_POST['id_ventana'];
$id_vcolor = $_POST['id_vcolor'];
$nventanas = $_POST['nventanas'];

// obtenet los datos del cliente
$cliente = $pdo->query("SELECT * FROM clientes WHERE id_cliente = $id_cliente LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$cliente_tipo_costo = $cliente['tipo_costo'];  // id tipo de costo

// obtener la divisa
$divisa = $pdo->query("SELECT * FROM divisas WHERE id_divisa = $id_divisa LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$divisa_tipo_cambio = $divisa['tipo_cambio']; // valor de la divisa

// obtener el impuesto
$impuesto = $pdo->query("SELECT * FROM impuestos WHERE id_impuesto = $id_impuesto LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$impuesto_porcentaje = $impuesto['porcentaje']; // porcentaje del impuesto

// obtener el precio de la puerta
$puerta = $pdo->query("SELECT * FROM puerta WHERE id_puerta = $id_puerta LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$puerta_alto = $puerta['m_alto'];
$puerta_ancho = $puerta['m_ancho'];
$puerta_perimetro = $puerta_alto + $puerta_ancho + $puerta_alto;
$puerta_precio = $puerta[$cliente_tipo_costo]; // precio de la puerta

// obtener el modelo de la puerta
$puerta_modelo = $pdo->query("SELECT * FROM puerta_modelo WHERE id_pmodelo = $id_pmodelo LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$puerta_modelo_porcentaje = $puerta_modelo['porcentaje'];
$puerta_modelo_costo = ($puerta_precio * $puerta_modelo_porcentaje) / 100; // costo del modelo de puerta

// obtener el color de la puerta
$puerta_color = $pdo->query("SELECT * FROM puerta_color WHERE id_pcolor = $id_pcolor LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$puerta_color_porcentaje = $puerta_color['porcentaje'];
$puerta_color_costo = ($puerta_precio * $puerta_color_porcentaje) / 100; // costo del color para la puerta

// obtener el movimiento de la puerta
$puerta_movimiento = $pdo->query("SELECT * FROM puerta_movimiento WHERE id_pmovimiento = $id_pmovimiento LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$puerta_movimiento_porcentaje = $puerta_movimiento['porcentaje'];
$puerta_movimiento_costo = ($puerta_precio * $puerta_movimiento_porcentaje) / 100;

// obtener la textura
$puerta_textura = $pdo->query("SELECT * FROM puerta_textura WHERE id_ptextura = $id_textura LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$puerta_textura_costo = $puerta_textura['precio'];

// obtener el sello
$puerta_sello = $pdo->query("SELECT * FROM puerta_sello WHERE id_psello = $id_sello LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$puerta_sello_nombre = $puerta_sello['nombre_sello'];
$puerta_sello_precio = $puerta_sello['precio'];

if (strtolower($puerta_sello_nombre) == 'perimetral') {
	$puerta_sello_costo = $puerta_perimetro * $puerta_sello_precio;
} else {
	$puerta_sello_costo = $puerta_sello_precio;
}

// obtener el motor
$puerta_motor = $pdo->query("SELECT * FROM puerta_motor WHERE id_pmotor = $id_motor LIMIT 1")->fetch(PDO::FETCH_ASSOC);
$puerta_motor_costo = $puerta_motor['precio'];

// obtener la ventana
if ($id_ventana > 0) {
	$puerta_ventana = $pdo->query("SELECT * FROM ventana WHERE id_ventana = $id_ventana LIMIT 1")->fetch(PDO::FETCH_ASSOC);
	$puerta_ventana_precio = $puerta_ventana[$cliente_tipo_costo];
	$puerta_ventana_costo = $puerta_ventana_precio * $nventanas;
} else {
	$puerta_ventana_costo = 0;
}

// obtener el color de la ventana
if ($id_vcolor > 0) {
	$puerta_vcolor = $pdo->query("SELECT * FROM ventana_color WHERE id_vcolor = $id_vcolor LIMIT 1")->fetch(PDO::FETCH_ASSOC);
	$puerta_vcolor_costo = $puerta_vcolor['precio'];
} else {
	$puerta_vcolor_costo = 0;
}

// CALCULAR EL COSTO TOTAL DE LA PUERTA
$total = $puerta_precio + $puerta_modelo_costo + $puerta_color_costo + $puerta_movimiento_costo + $puerta_textura_costo + $puerta_sello_costo + $puerta_motor_costo + $puerta_ventana_costo + $puerta_vcolor_costo;

$total = $total * $divisa_tipo_cambio; // agregar la divisa

// devolver el total de la puerta
echo $_GET['callback'] . "(" . json_encode($total) . ");";

?>