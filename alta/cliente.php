<?php
if (!isset($_SESSION)) {
    session_start();
}
require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");
$id_usuario = $_SESSION['id_usuario'];

//Datos de control
$btn = $_POST['btn'];
$id = $_POST['id'];
$variable ='';

//Datos del cliente
$nombre_cliente = $_POST['nombre_cliente'];
$tipo = $_POST['tipo'];
$id_divisa = $_POST['id_divisa'];
$tipo_costo = $_POST['tipo_costo'];
$motivo_costo = $_POST['motivo_costo'];
$rfc = $_POST['rfc'];
$razon_social = $_POST['razon_social'];

//Datos de contacto
$nombre_contacto = $_POST['nombre_contacto'];
$email_contacto = $_POST['email_contacto'];
$telefono_contacto = $_POST['telefono_contacto'];
$ext_contacto = $_POST['ext_contacto'];

//Datos de personal autorizado
$nombre_autorizado = $_POST['nombre_autorizado'];
$email_autorizado = $_POST['email_autorizado'];
$telefono_autorizado = $_POST['telefono_autorizado'];
$ext_autorizado = $_POST['ext_autorizado'];

//Domicilio
$calle = $_POST['calle'];
$no_interior = $_POST['no_interior'];
$no_exterior = $_POST['no_exterior'];
$colonia = $_POST['colonia'];
$referencia = $_POST['referencia'];
$localidad = $_POST['localidad'];
$municipio = $_POST['municipio'];
$estado = $_POST['estado'];
$codigo_postal = $_POST['codigo_postal'];
$id_pais = $_POST['id_pais'];

if ($btn == "guardar") {

    $id_domicilio = insertaDomicilio($calle, $no_interior, $no_exterior, $colonia, $referencia, $localidad, $municipio, $estado, $codigo_postal, $id_pais);
    $sql_insert = sprintf("INSERT INTO `clientes` (`tipo`, `nombre_cliente`, `divisa_cliente`, `tipo_costo`,  `motivo_costo`, 
        `nombre_contacto`, `email_contacto`, `telefono_contacto`,  `ext_contacto`,  `razon_social`,  `rfc`, `id_domicilio`,  
        `nombre_autorizado`, `email_autorizado`, `telefono_autorizado`,  `ext_autorizado`, `id_usuario_alta`) 
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
                        GetSQLValueString($tipo, "int"),
                        GetSQLValueString($nombre_cliente, "text"),
                        GetSQLValueString($id_divisa, "int"),  
                        GetSQLValueString($tipo_costo, "text"), 
                        GetSQLValueString($motivo_costo, "text"), 
                        GetSQLValueString($nombre_contacto, "text"), 
                        GetSQLValueString($email_contacto, "text"), 
                        GetSQLValueString($telefono_contacto, "text"),
                        GetSQLValueString($ext_contacto, "text"),
                        GetSQLValueString($razon_social, "text"), 
                        GetSQLValueString($rfc, "text"),  
                        GetSQLValueString($id_domicilio, "int"),
                        GetSQLValueString($nombre_autorizado, "text"), 
                        GetSQLValueString($email_autorizado, "text"), 
                        GetSQLValueString($telefono_autorizado, "text"),
                        GetSQLValueString($ext_autorizado, "text"),
                        GetSQLValueString($id_usuario, "int")
    );

    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_lastInsert = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un cliente con el id: " . $id_lastInsert . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `clientes` SET `id_log`=%s WHERE id_cliente =%s", GetSQLValueString($id_log, "int"), GetSQLValueString($id_lastInsert, "int")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}
if ($btn == "modificar") {
    $id_domicilio = GetSQLValueString($_POST['id_domicilio'], "int");
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico al cliente con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $id_domicilio = actualizaDomicilio($id_domicilio, $calle, $no_interior, $no_exterior, $colonia, $referencia, $localidad, $municipio, $estado, $codigo_postal, $id_pais);
    $sql_update = sprintf("UPDATE `clientes` SET  `nombre_cliente`=%s,  `aprobado`=0,  `divisa_cliente`=%s, `tipo_costo`=%s,  `motivo_costo`=%s,  
        `nombre_contacto`=%s, `email_contacto`=%s, `telefono_contacto`=%s, `ext_contacto`=%s, `razon_social`=%s, `rfc`=%s, 
        `nombre_autorizado`=%s, `email_autorizado`=%s, `telefono_autorizado`=%s, `ext_autorizado`=%s, `id_log`=%s WHERE id_cliente=%s", 
                        GetSQLValueString($nombre_cliente, "text"),
                        GetSQLValueString($id_divisa, "int"),  
                        GetSQLValueString($tipo_costo, "text"), 
                        GetSQLValueString($motivo_costo, "text"), 
                        GetSQLValueString($nombre_contacto, "text"), 
                        GetSQLValueString($email_contacto, "text"), 
                        GetSQLValueString($telefono_contacto, "text"),
                        GetSQLValueString($ext_contacto, "text"),
                        GetSQLValueString($razon_social, "text"), 
                        GetSQLValueString($rfc, "text"),  
                        GetSQLValueString($nombre_autorizado, "text"), 
                        GetSQLValueString($email_autorizado, "text"), 
                        GetSQLValueString($telefono_autorizado, "text"),
                        GetSQLValueString($ext_autorizado, "text"),
                        GetSQLValueString($id_log, "int"), 
                        GetSQLValueString($id, "int")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    if($tipo==1){ //Prospecto
        header("Location: ../listado-prospectos.php");
    }else{
        header("Location: ../listado-clientes.php");
    }
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    if($tipo==1){ //Prospecto
        echo "<script>alert('Los datos del prospecto fueron cambiados con \u00e9xito'); location.href='../detalle-prospecto.php?id=".$id."'</script>";
    }else{
        echo "<script>alert('Los datos del cliente fueron cambiados con \u00e9xito'); location.href='../detalle-cliente.php?id=".$id."'</script>";
    }
}
?>