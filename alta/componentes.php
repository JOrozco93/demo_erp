<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$nombre_componente = $_POST['nombre_componente'];
$idcp = $_POST['id_componente_grupo'];
$precio = $_POST['precio'];

$VA = $_POST['VA'];
$VB = $_POST['VB'];
$VC = $_POST['VC'];
$VD = $_POST['VD'];
$VE = $_POST['VE'];
$VF = $_POST['VF'];

// $impuesto = $_POST['impuesto'];
$impuesto = 0;

$btn = $_POST['btn'];
$variable;

if ($btn == "guardar") {
    $sql_insert = sprintf("INSERT INTO `componentes` (`precio`, `nombre_componente`, `impuesto`, `id_componente_grupo`, `VA`, `VB`, `VC`, `VD`, `VE`, `VF`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", GetSQLValueString($precio, "text"), GetSQLValueString($nombre_componente, "text"), GetSQLValueString($impuesto, "text"), GetSQLValueString($idcp, "text"), GetSQLValueString($VA, "text"), GetSQLValueString($VB, "text"), GetSQLValueString($VC, "text"), GetSQLValueString($VD, "text"), GetSQLValueString($VE, "text"), GetSQLValueString($VF, "text"));

    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_usuarioDes = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un componente con el id: " . $id . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `componentes` SET `id_log`=%s WHERE id_componente=%s", GetSQLValueString($id_log, "text"), GetSQLValueString($id, "text")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}
if ($btn == "modificar") {
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico el componente con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);

    $sql_update = sprintf("UPDATE `componentes` SET `precio`=%s, `nombre_componente`=%s, `impuesto`=%s, `id_log`=%s, `VA`=%s, `VB`=%s, `VC`=%s, `VD`=%s, `VE`=%s, `VF`=%s WHERE id_componente=%s", GetSQLValueString($precio, "text"), GetSQLValueString($nombre_componente, "text"), GetSQLValueString($impuesto, "text"), GetSQLValueString($id_log, "int"), GetSQLValueString($VA, "text"), GetSQLValueString($VB, "text"), GetSQLValueString($VC, "text"), GetSQLValueString($VD, "text"), GetSQLValueString($VE, "text"), GetSQLValueString($VF, "text"), GetSQLValueString($id, "text"));

    // echo $sql_update;
    // exit;

    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../catalogo-componentes.php");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../catalogo-componentes.php'</script>";
}
?>
