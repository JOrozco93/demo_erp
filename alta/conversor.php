<?php

if (!isset($_SESSION)) {
    session_start();
}

include("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

// include 'config.php';

//$cantidad=intval($_POST['cantidad']);

$ban = $_POST['ban'];
$id_ptipo = $_POST['id_ptipo'];

if ($ban == 0) {
	$ancho = $_POST['m_ancho'];
	$alto = $_POST['m_alto'];
} else {
	$f_ancho = $_POST['f_ancho'];
	$in_ancho = $_POST['in_ancho'];
	$f_alto = $_POST['f_alto'];
	$in_alto = $_POST['in_alto'];

	// COVERTIR A METROS
	
	$ancho = (((($f_ancho * 12) * 2.54) + ($in_ancho * 2.54)) / 100);
	$alto = (((($f_alto * 12) * 2.54) + ($in_alto * 2.54)) / 100);
}

$query_select = "SELECT * FROM puerta WHERE id_ptipo = $id_ptipo ORDER BY m_ancho, m_alto ASC";
$result_select = mysql_query($query_select) or die(mysql_error());

$rows = array();

while($row = mysql_fetch_array($result_select)) {
    $rows[] = $row;
}

foreach ($rows as $row) {
	$anchor = 0;
	$altor = 0;

	if ($row['m_ancho'] >= $ancho) {
		$anchor = $row['m_ancho'];
	}

	if ($row['m_alto'] >= $alto) {
		$altor = $row['m_alto'];
	}

	if (!empty($anchor) && !empty($altor)) {
		echo $_GET['callback'] . "(" . json_encode($row['id_puerta']) . ");";
		break;
	}
}

?>