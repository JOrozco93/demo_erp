<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");
$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$grupo_componente = $_POST['grupo_componente'];
$btn = $_POST['btn'];
$variable;

if ($btn == "guardar") {
    $sql_insert = sprintf("INSERT INTO `componentes_grupos` (`grupo_componente`) VALUES (%s)", GetSQLValueString($grupo_componente, "text"));
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_grupo_componente = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un grupo_componente con el id: " . $id_grupo_componente . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `componentes_grupos` SET `id_log`=%s WHERE id_grupo_componente=%s", GetSQLValueString($id_log, "text"), GetSQLValueString($id_grupo_componente, "text")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}
if ($btn == "modificar") {
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico el grupo_componente con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `componentes_grupos` SET  `grupo_componente`=%s, `id_log`=%s WHERE id_grupo_componente=%s", GetSQLValueString($grupo_componente, "text"), GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../catalogo-componentes.php");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../catalogo-componentes.php'</script>";
}
?>
