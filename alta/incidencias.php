<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$sqlu = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
$user = mysql_fetch_array(consulta($sqlu));

$id = $_POST['id'];
$id_usuario_alta = $user['id_usuario'];

$descripcion = $_POST['descripcion'];
$id_cotizacion = $_POST['id_cotizacion'];
$btn = $_POST['btn'];

$variable = '';

if ($btn == "guardar") {
    $sql_insert = sprintf("INSERT INTO `incidencias` (`descripcion`, `id_usuario_alta`, `id_cotizacion`) VALUES (%s, %s, %s)", GetSQLValueString($descripcion, "text"), GetSQLValueString($id_usuario_alta, "text"), GetSQLValueString($id_cotizacion, "text")
    );
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_usuarioDes = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo una incidencia con el id: " . $id . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `incidencias` SET `id_log` = %s WHERE id_incidencia = %s", GetSQLValueString($id_log, "text"), GetSQLValueString($id, "text")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);

    $variable = "agregar";
}

if ($btn == "modificar") {
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico la incidencia con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `incidencias` SET `descripcion` = %s, `id_usuario_alta` = %s, `id_cotizacion`= %s, `id_log` = %s WHERE id_incidencia = %s", GetSQLValueString($descripcion, "text"), GetSQLValueString($id_usuario_alta, "text"), GetSQLValueString($id_cotizacion, "text"), GetSQLValueString($id_log, "text"), GetSQLValueString($id, "text")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);

    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../listado-incidencias.php");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../listado-incidencias.php'</script>";
}

?>