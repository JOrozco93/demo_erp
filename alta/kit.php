<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$nombre_kit = $_POST['nombre_kit'];
$id_proveedor = $_POST['id_proveedor'];
$precio = $_POST['precio'];

$VA = $_POST['VA'];
$VB = $_POST['VB'];
$VC = $_POST['VC'];
$VD = $_POST['VD'];
$VE = $_POST['VE'];
$VF = $_POST['VF'];

//$impuesto = $_POST['impuesto'];
$impuesto = 0;
$btn = $_POST['btn'];

$variable;

$detalle = "El usuario con el id: " . $id_usuario . "  modifico el kit con el id: " . $id . " ";
$evento = "Modificar";
$id_log = actualizalog($id_usuario, $evento, $detalle);

$sql_update = sprintf("UPDATE `kit` SET  `nombre_kit`=%s, `id_proveedor`=%s, `precio`=%s, `VA`=%s, `VB`=%s, `VC`=%s, `VD`=%s, `VE`=%s, `VF`=%s, `impuesto`=%s, `id_log`=%s WHERE id_kit=%s", GetSQLValueString($nombre_kit, "text"), GetSQLValueString($id_proveedor, "int"), GetSQLValueString($precio, "text"), GetSQLValueString($VA, "text"), GetSQLValueString($VB, "text"), GetSQLValueString($VC, "text"), GetSQLValueString($VD, "text"), GetSQLValueString($VE, "text"), GetSQLValueString($VF, "text"), GetSQLValueString($impuesto, "text"), GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int"));

// echo $sql_update;
// exit;

$result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
$variable = "modificar";

if (!isset($_POST['noreload'])) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../catalogo-herreria.php'</script>";
}

?>
