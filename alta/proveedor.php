<?php
if (!isset($_SESSION)) {
    session_start();
}
require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");
$id_usuario = $_SESSION['id_usuario'];

//Datos de control
$btn = $_POST['btn'];
$id = $_POST['id'];
$variable ='';
//Datos del proveedor
$nombre_proveedor = $_POST['nombre_proveedor'];
$tipo = $_POST['tipo'];
$nombre_contacto = $_POST['nombre_contacto'];
$email = $_POST['email'];
$telefono = $_POST['telefono'];
$ext = $_POST['ext'];


//Domicilio
$calle = $_POST['calle'];
$no_interior = $_POST['no_interior'];
$no_exterior = $_POST['no_exterior'];
$colonia = $_POST['colonia'];
$referencia = $_POST['referencia'];
$localidad = $_POST['localidad'];
$municipio = $_POST['municipio'];
$estado = $_POST['estado'];
$codigo_postal = $_POST['codigo_postal'];
$id_pais = $_POST['id_pais'];

if ($btn == "guardar") {

    $id_domicilio = insertaDomicilio($calle, $no_interior, $no_exterior, $colonia, $referencia, $localidad, $municipio, $estado, $codigo_postal, $id_pais);
    $sql_insert = sprintf("INSERT INTO `proveedores` (`nombre`, `tipo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`,  `ext_contacto`, `id_domicilio`, `id_usuario_alta`) 
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", 
                        GetSQLValueString($nombre_proveedor, "text"), 
                        GetSQLValueString($tipo, "text"), 
                        GetSQLValueString($nombre_contacto, "text"), 
                        GetSQLValueString($email, "text"), 
                        GetSQLValueString($telefono, "text"),
                        GetSQLValueString($ext, "text"), 
                        GetSQLValueString($id_domicilio, "int"),
                        GetSQLValueString($id_usuario, "int")
    );
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_lastInsert = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un proveedor con el id: " . $id_lastInsert . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `proveedores` SET `id_log`=%s WHERE id_proveedor =%s", GetSQLValueString($id_log, "int"), GetSQLValueString($id_lastInsert, "int")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}
if ($btn == "modificar") {
    $id_domicilio = GetSQLValueString($_POST['id_domicilio'], "int");
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico al proveedor con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $id_domicilio = actualizaDomicilio($id_domicilio, $calle, $no_interior, $no_exterior, $colonia, $referencia, $localidad, $municipio, $estado, $codigo_postal, $id_pais);
    $sql_update = sprintf("UPDATE `proveedores` SET  `nombre`=%s, `tipo`=%s,  `nombre_contacto`=%s, `email_contacto`=%s, `telefono_contacto`=%s, `ext_contacto`=%s,  `id_log`=%s WHERE id_proveedor=%s", 
                        GetSQLValueString($nombre_proveedor, "text"), 
                        GetSQLValueString($tipo, "text"),  
                        GetSQLValueString($nombre_contacto, "text"), 
                        GetSQLValueString($email, "text"), 
                        GetSQLValueString($telefono, "text"), 
                        GetSQLValueString($ext, "text"), 
                        GetSQLValueString($id_log, "int"), 
                        GetSQLValueString($id, "int")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../catalogo-proveedores.php");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../detalle-proveedor.php?id=".$id."'</script>";
}
?>