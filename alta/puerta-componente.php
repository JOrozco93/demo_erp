<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$id_puerta = $_POST['id_puerta'];
$id_componente = $_POST['id_componente'];
$cantidad = $_POST['cantidad'];

$sql_search = sprintf("SELECT * FROM puerta_componente WHERE id_puerta = %s AND id_componente = %s", GetSQLValueString($id_puerta, "int"), GetSQLValueString($id_componente, "int"));
$result = mysql_query($sql_search, $db_con);

if (mysql_num_rows($result)) {
    $row = mysql_fetch_assoc($result);

    $id = $row['id'];
    $cantidad = $row['cantidad'] + $cantidad;

    $sql_update = sprintf("UPDATE puerta_componente SET cantidad = %s WHERE id = %s", GetSQLValueString($cantidad, "int"), GetSQLValueString($id, "int"));
    $result = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $detalle = "El usuario con el id: " . $id_usuario . "  actualizo la cantidad de un componente para puerta con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE puerta_componente SET id_log = %s WHERE id = %s", GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int"));
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
} else {
    $sql_insert = sprintf("INSERT INTO puerta_componente (id_puerta, id_componente, cantidad) VALUES (%s, %s, %s)", GetSQLValueString($id_puerta, "int"), GetSQLValueString($id_componente, "int"), GetSQLValueString($cantidad, "int"));
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_usuarioDes = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un componente con el id: " . $id . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE puerta_componente SET id_log = %s WHERE id = %s", GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int"));
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);    
}

header("Location: ../detalle-puerta.php?id=$id_puerta");

?>