<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$nombre_movimiento = $_POST['nombre_movimiento'];
$id_ptipo = $_POST['id_ptipo'];
$btn = $_POST['btn'];
$variable;

if ($btn == "guardar") {
    $sql_insert = sprintf("INSERT INTO puerta_movimiento (nombre_movimiento, id_ptipo) VALUES (%s, %s)", GetSQLValueString($nombre_movimiento, "text"), GetSQLValueString($id_ptipo, "int"));
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_usuarioDes = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un movimiento de puerta con el id: " . $id . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE puerta_movimiento SET id_log = %s WHERE id_pmovimiento = %s", GetSQLValueString($id_log, "text"), GetSQLValueString($id, "text"));
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}

if ($btn == "modificar") {
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico un movimiento de puerta con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE puerta_movimiento SET nombre_movimiento = %s, id_ptipo = %s, id_log = %s WHERE id_pmovimiento = %s", GetSQLValueString($nombre_movimiento, "text"), GetSQLValueString($id_ptipo, "int"), GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int"));
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../catalogo-puertas.php");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../catalogo-puertas.php'</script>";
}

?>