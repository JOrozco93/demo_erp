<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$m_ancho = $_POST['m_ancho'];
$m_alto = $_POST['m_alto'];
$f_ancho = $_POST['f_ancho'];
$f_alto = $_POST['f_alto'];
$in_ancho = $_POST['in_ancho'];
$in_alto = $_POST['in_alto'];
$precio = $_POST['precio'];

$VA = $_POST['VA'];
$VB = $_POST['VB'];
$VC = $_POST['VC'];
$VD = $_POST['VD'];
$VE = $_POST['VE'];
$VF = $_POST['VF'];

//$impuesto = $_POST['impuesto'];
$impuesto = 0;
$tipo = $_POST['id_ptipo'];
$btn = $_POST['btn'];
$variable;

if ($btn == "guardar") {
    $sql_insert = sprintf("INSERT INTO `puerta` (`m_ancho`, `m_alto`, `f_ancho`, `in_ancho`, `f_alto`,  `in_alto`, `precio`, `VA`, `VB`, `VC`, `VD`, `VE`, `VF`, `impuesto`, `id_ptipo`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", GetSQLValueString($m_ancho, "text"), GetSQLValueString($m_alto, "text"), GetSQLValueString($f_ancho, "text"), GetSQLValueString($in_ancho, "text"), GetSQLValueString($f_alto, "text"), GetSQLValueString($in_alto, "text"), GetSQLValueString($precio, "text"), GetSQLValueString($VA, "text"), GetSQLValueString($VB, "text"), GetSQLValueString($VC, "text"), GetSQLValueString($VD, "text"), GetSQLValueString($VE, "text"), GetSQLValueString($VF, "text"), GetSQLValueString($impuesto, "text"), GetSQLValueString($tipo, "int"));

    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_inserted = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un componente con el id: " . $id . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `puerta` SET `id_log`=%s WHERE id_puerta=%s", GetSQLValueString($id_log, "text"), GetSQLValueString($id, "text")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}
if ($btn == "modificar") {
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico el componente con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    
    $sql_update = sprintf("UPDATE `puerta` SET `m_ancho`=%s, `m_alto`=%s, `f_ancho`=%s, `f_alto`=%s, `in_ancho`=%s, `in_alto`=%s, `precio`=%s, `VA`=%s, `VB`=%s, `VC`=%s, `VD`=%s, `VE`=%s, `VF`=%s, `impuesto`=%s, `id_ptipo`=%s, `id_log`=%s WHERE id_puerta=%s", GetSQLValueString($m_ancho, "text"), GetSQLValueString($m_alto, "text"), GetSQLValueString($f_ancho, "text"), GetSQLValueString($f_alto, "text"), GetSQLValueString($in_ancho, "text"), GetSQLValueString($in_alto, "text"), GetSQLValueString($precio, "text"), GetSQLValueString($VA, "text"), GetSQLValueString($VB, "text"), GetSQLValueString($VC, "text"), GetSQLValueString($VD, "text"), GetSQLValueString($VE, "text"), GetSQLValueString($VF, "text"), GetSQLValueString($impuesto, "text"), GetSQLValueString($tipo, "int"), GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int"));

    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $id_updated = $id;
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../detalle-puerta.php?id=$id_inserted");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../detalle-puerta.php?id=$id_updated'</script>";
}

?>