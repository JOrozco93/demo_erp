<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");
$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$status = $_POST['status'];
$motivo = $_POST['motivo'];
$tipo = $_POST['tipo'];

$cambiaprospecto = '';
if($status == 1){
	$cambiaprospecto = 'tipo = 2,';

}

$detalle = "El usuario con el id: " . $id_usuario . "  aprobo al cliente con el id: " . $id . " ";
$evento = "Aprobar";
$id_log = actualizalog($id_usuario, $evento, $detalle);
$sql_update = sprintf("UPDATE `clientes` SET `aprobado`=%s, `motivo_aprobado`=%s, %s `id_log`=%s WHERE id_cliente=%s", 
    GetSQLValueString($status, "int"), GetSQLValueString($motivo, "text"), $cambiaprospecto, GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int")
);

$result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);

header("location: ../autorizacion.php");

?>