<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id_cotizacion = $_POST['id_cotizacion'];
$id_cliente = $_POST['id_cliente'];
$status = $_POST['status'];
$motivo = $_POST['motivo'];
$fecha = date("Y-m-d H:i:s");

$sql_cotizacion = "SELECT * FROM cotizaciones WHERE id_cotizacion = $id_cotizacion";
$result = mysql_query($sql_cotizacion, $db_con) or die("Problemas en la consulta: " . $sql_update);
$cotizacion = mysql_fetch_assoc($result);

$pagoconsaldo = $cotizacion['pagoconsaldo'];

$detalle = "El usuario con el id: " . $id_usuario . "  aprobo al cliente con el id: " . $id_cliente . " ";
$evento = "Aprobar";
$id_log = actualizalog($id_usuario, $evento, $detalle);

if ($status == 1) {
	$sql_update = sprintf("UPDATE cotizaciones SET autorizada = 0, pagada = 1, id_usuario_pendiente = $id_usuario, pendiente_fecha = '$fecha', pendiente_motivo = '$motivo', pagoconsaldo = 0, url_pago = '' WHERE id_cotizacion = $id_cotizacion");
	$result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);

	$sql = "UPDATE clientes SET saldo = saldo + $pagoconsaldo WHERE id_cliente = $id_cliente";
	$result = mysql_query($sql, $db_con) or die("Problemas en la consulta: " . $sql);
}

if ($status == 2) {
	$sql_update = sprintf("UPDATE cotizaciones SET autorizada = 1, pagada = 2, id_usuario_pagada = $id_usuario, pagada_fecha = '$fecha', aprobada_motivo = '$motivo' WHERE id_cotizacion = $id_cotizacion");
	$result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
}

if ($status == 3) {
	$sql_update = sprintf("UPDATE cotizaciones SET autorizada = 0, pagada = 3, id_usuario_pagada_rechazo = $id_usuario, pagada_rechazo_fecha = '$fecha', rechazada_motivo = '$motivo', pagoconsaldo = 0, url_pago = '' WHERE id_cotizacion = $id_cotizacion");
	$result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);

	$sql = "UPDATE clientes SET saldo = saldo + $pagoconsaldo WHERE id_cliente = $id_cliente";
	$result = mysql_query($sql, $db_con) or die("Problemas en la consulta: " . $sql);
}

if($status == 2){
    header("Location: ../listado-produccion.php");
} else {
    header("Location: ../listado-ventas.php");
}
?>