<?php
	
if (!isset($_SESSION)) {
	session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$protocol   = 'http://';

if (isset($_SERVER['HTTPS'])) $protocol = 'https://';

$host = $_SERVER['HTTP_HOST'];

$path = rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\');

$url_path = 'comprobantes';

$id_cotizacion = $_POST['id_cotizacion'];
$id_cliente = $_POST['id_cliente'];
$pagoconsaldo = (isset($_POST['pagoconsaldo'])) ? $_POST['pagoconsaldo'] : 0;
$comentarios = $_POST['comentarios'];

$fecha = date("Y-m-d H:i:s");

if (!is_dir('../comprobantes/cotizacion_'.$id_cotizacion)) {
	mkdir('../comprobantes/cotizacion_'.$id_cotizacion);
}

$url_path_archivo = NULL;

if ($_FILES['file']['error'] > 0) {
    echo 'Error: ' . $_FILES['file']['error'] . '<br>';
} else {
    $ext = explode('.', $_FILES['file']['name']);

	$extension = $ext[1];
	$newname = 'comprobante' . $id_cotizacion;
	$full_local_path = '../comprobantes/cotizacion_' . $id_cotizacion . '/' . $newname . '.' . $extension;

    move_uploaded_file($_FILES['file']['tmp_name'], $full_local_path);
    
    $url_path_archivo = $url_path . '/cotizacion_' . $id_cotizacion . '/' . $newname . '.' . $extension;
}

$sql = "UPDATE cotizaciones SET pagoconsaldo = $pagoconsaldo, url_pago = '$url_path_archivo', id_usuario_pagada = $id_usuario, pagada_fecha = '$fecha', comentarios_cotizacion = '$comentarios', autorizada = 1 WHERE id_cotizacion = $id_cotizacion";
$result = mysql_query($sql, $db_con) or die("Problemas en la consulta: " . $sql);

$sql = "UPDATE clientes SET saldo = saldo - $pagoconsaldo WHERE id_cliente = $id_cliente";
$result = mysql_query($sql, $db_con) or die("Problemas en la consulta: " . $sql);

// echo $url_path_archivo;
   
// array_values() removes the original keys and replaces
// with plain consecutive numbers

header("location: ../listado-ventas.php");

?>