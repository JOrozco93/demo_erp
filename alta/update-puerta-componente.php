<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$id_puerta = $_POST['id_puerta'];
$cantidad = $_POST['cantidad'];

$detalle = "El usuario con el id: " . $id_usuario . "  modifico el componente con el id: " . $id . " ";
$evento = "Modificar";
$id_log = actualizalog($id_usuario, $evento, $detalle);
$sql_update = sprintf("UPDATE puerta_componente SET cantidad = %s, id_log = %s WHERE id = %s", GetSQLValueString($cantidad, "int"), GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int"));
$result = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);

header("Location: ../detalle-puerta.php?id=" . $id_puerta);

?>