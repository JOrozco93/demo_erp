<?php
if (!isset($_SESSION)) {
    session_start();
}
require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");
$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$username = $_POST['nom_usuario'];
$num_empleado = $_POST['num_empleado'];
$password = $_POST['password'];
$nombre = $_POST['nombre'];
$email = $_POST['email'];
$telefono = $_POST['telefono'];
$ext = $_POST['ext'];
$btn = $_POST['btn'];

$id_divisa = $_POST['id_divisa'];
$id_permiso = $_POST['id_permiso'];
$id_cliente = $_POST['id_cliente'];
$variable;

if ($btn == "guardar") {

$nom_usuario = $_POST['nom_usuario'];

    $sql_insert = sprintf("INSERT INTO `usuarios` (`username`, `pwd`,  `id_divisa`, `id_cliente`, `nombre_usuario`,  `email_usuario`, `telefono_usuario`,  `ext_usuario`, `num_empleado`, `permiso_usuario`) 
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
                        GetSQLValueString($username, "text"), 
                        GetSQLValueString($password, "text"), 
                        GetSQLValueString($id_divisa, "int"), 
                        GetSQLValueString($id_cliente, "int"), 
                        GetSQLValueString($nombre, "text"),
                        GetSQLValueString($email, "text"), 
                        GetSQLValueString($telefono, "text"),
                        GetSQLValueString($ext, "text"), 
                        GetSQLValueString($num_empleado, "text"),  
                        GetSQLValueString($id_permiso, "int")
    );
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_usuarioDes = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo un usuario con el id: " . $id_usuarioDes . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `usuarios` SET `id_log`=%s WHERE id_usuario =%s", GetSQLValueString($id_log, "text"), GetSQLValueString($id_usuarioDes, "text")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}
if ($btn == "modificar") {
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico al usuario con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `usuarios` SET  `id_divisa`=%s, `id_cliente`=%s,  `pwd`=%s, `nombre_usuario`=%s, `email_usuario`=%s, `telefono_usuario`=%s,  `ext_usuario`=%s,  `num_empleado`=%s, `permiso_usuario`=%s,  `id_log`=%s WHERE id_usuario=%s", 
                        GetSQLValueString($id_divisa, "int"), 
                        GetSQLValueString($id_cliente, "int"), 
                        GetSQLValueString($password, "text"), 
                        GetSQLValueString($nombre, "text"), 
                        GetSQLValueString($email, "text"), 
                        GetSQLValueString($telefono, "text"), 
                        GetSQLValueString($ext, "text"), 
                        GetSQLValueString($num_empleado, "text"), 
                        GetSQLValueString($id_permiso, "int"),  
                        GetSQLValueString($id_log, "int"), 
                        GetSQLValueString($id, "int")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../catalogo-usuarios.php");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../detalle-usuarios.php?id=".$id."'</script>";
}
?>