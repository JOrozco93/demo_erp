<?php

if (!isset($_SESSION)) {
    session_start();
}

require_once("../config.php");
require_once("../includes/validacion.php");
require_once("../includes/funciones_BD.php");

$id_usuario = $_SESSION['id_usuario'];

$id = $_POST['id'];
$nombre_ventana = $_POST['nombre_ventana'];
$descripcion = $_POST['descripcion'];
$tipo = $_POST['tipo'];
$precio = $_POST['precio'];

$VA = $_POST['VA'];
$VB = $_POST['VB'];
$VC = $_POST['VC'];
$VD = $_POST['VD'];
$VE = $_POST['VE'];
$VF = $_POST['VF'];

// $impuesto = $_POST['impuesto'];
$impuesto = 0;
$btn = $_POST['btn'];

$variable;

if ($btn == "guardar") {
    $sql_insert = sprintf("INSERT INTO `ventana` (`nombre_ventana`, `descripcion`, `id_ptipo`, `precio`, `VA`, `VB`, `VC`, `VD`, `VE`, `VF`, `impuesto`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", GetSQLValueString($nombre_ventana, "text"), GetSQLValueString($descripcion, "text"), GetSQLValueString($tipo, "text"), GetSQLValueString($precio, "text"), GetSQLValueString($VA, "text"), GetSQLValueString($VB, "text"), GetSQLValueString($VC, "text"), GetSQLValueString($VD, "text"), GetSQLValueString($VE, "text"), GetSQLValueString($VF, "text"), GetSQLValueString($impuesto, "text"));
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_usuarioDes = mysql_insert_id();
    $detalle = "El usuario con el id: " . $id_usuario . "  creo una ventana con el id: " . $id . " ";
    $evento = "Alta";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `ventana` SET `id_log`=%s WHERE id_ventana=%s", GetSQLValueString($id_log, "text"), GetSQLValueString($id, "text"));
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "agregar";
}
if ($btn == "modificar") {
    $detalle = "El usuario con el id: " . $id_usuario . "  modifico la ventana con el id: " . $id . " ";
    $evento = "Modificar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $sql_update = sprintf("UPDATE `ventana` SET  `nombre_ventana`=%s, `descripcion`=%s, `id_ptipo`=%s, `precio`=%s, `VA`=%s, `VB`=%s, `VC`=%s, `VD`=%s, `VE`=%s, `VF`=%s, `impuesto`=%s, `id_log`=%s WHERE id_ventana=%s", GetSQLValueString($nombre_ventana, "text"), GetSQLValueString($descripcion, "text"), GetSQLValueString($tipo, "text"), GetSQLValueString($precio, "text"), GetSQLValueString($VA, "text"), GetSQLValueString($VB, "text"), GetSQLValueString($VC, "text"), GetSQLValueString($VD, "text"), GetSQLValueString($VE, "text"), GetSQLValueString($VF, "text"), GetSQLValueString($impuesto, "text"), GetSQLValueString($id_log, "int"), GetSQLValueString($id, "int"));
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $variable = "modificar";
}

if ((!isset($_POST['noreload'])) && ($variable == 'agregar')) {
    header("Location: ../catalogo-ventanas.php");
} else if ((!isset($_POST['noreload'])) && ($variable == 'modificar')) {
    echo "<script>alert('Los datos fueron cambiados con \u00e9xito'); location.href='../catalogo-ventanas.php'</script>";
}

?>