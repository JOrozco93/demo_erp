<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>HS PUERTAS DE GARAJE</title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>

        <!-- Date Picker -->
        <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Balances<small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-usd"></i>
                                    <a href="">
                                        Balances
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Listado de Balances General Ortiz
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-11"></div>
                                            <div class="col-lg-1">
                                                <button onclick="window.location.href = 'addBalA.php?id=0'"  title="Crear Nuevo Balance" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-4">
                                                <label>Fecha Diaria:</label>
                                                <div class="input-group">
                                                    <input data-provide="datepicker" class="form-control" data-date-format="yyyy-mm-dd" id="fecha_inicio" name="fecha_inicio" type="text" placeholder="AAAA-mm-dd"/>
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div><!-- /.input group -->
                                            </div>
                                            <div class="col-sm-4 col-sm-offset-4">
                                                <a>
                                                    <img src="img/excel.png" class="botonExcel" style="width:50px;"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>

                                    <form method="POST" id="balAForm" name="balAForm" action="includes/balAForm.php">
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Total de Gastos
                                                    </th>
                                                    <th>
                                                        Total de Ventas
                                                    </th>
                                                    <th>
                                                        Total de Gastos - Total de Ventas
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" id="bal-0" name="bal-0" value="0"/>
                                                        <a onclick="ir($('#bal-0').val());">
                                                            2015-08-18 06:11:00
                                                        </a>
                                                    </td>
                                                    <td>
                                                        $0
                                                    </td>
                                                    <td>
                                                        $0
                                                    </td>
                                                    <td>
                                                        $0
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button onclick="" title="Eliminar Balance" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Total de Gastos
                                                    </th>
                                                    <th>
                                                        Total de Ventas
                                                    </th>
                                                    <th>
                                                        Total de Gastos - Total de Ventas
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <input type="hidden" id="bals" name="bals"/>
                                    </form>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-4">
                                                <label>Fecha Mensual:</label>
                                                <select class="form-control" >
                                                    <option>Enero</option>
                                                    <option>Febrero</option>
                                                    <option>Marzo</option>
                                                    <option>Abril</option>
                                                    <option>Mayo</option>
                                                    <option>Junio</option>
                                                    <option>Julio</option>
                                                    <option>Agosto</option>
                                                    <option>Septiembre</option>
                                                    <option>Octubre</option>
                                                    <option>Noviembre</option>
                                                    <option>Diciembre</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4 col-sm-offset-4">
                                                <a>
                                                    <img src="img/excel.png" class="botonExcel" style="width:50px;"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>

                                    <form method="POST" id="balAForm" name="balAForm" action="includes/balAForm.php">
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Total de Gastos
                                                    </th>
                                                    <th>
                                                        Total de Ventas
                                                    </th>
                                                    <th>
                                                        Total de Gastos - Total de Ventas
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" id="bal-0" name="bal-0" value="0"/>
                                                        <a onclick="ir($('#bal-0').val());">
                                                            Agosto
                                                        </a>
                                                    </td>
                                                    <td>
                                                        $0
                                                    </td>
                                                    <td>
                                                        $0
                                                    </td>
                                                    <td>
                                                        $0
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button onclick="" title="Eliminar Balance" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Total de Gastos
                                                    </th>
                                                    <th>
                                                        Total de Ventas
                                                    </th>
                                                    <th>
                                                        Total de Gastos - Total de Ventas
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <input type="hidden" id="bals" name="bals"/>
                                    </form>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script src="scripts/custom/form-samples.js"></script>

        <!-- datepicker -->
        <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <script>
                                                            jQuery(document).ready(function () {
                                                                App.init(); // initlayout and core plugins
                                                                //                                                                    TableAdvanced.init();
                                                                FormSamples.init();
                                                            });
                                                            $(document).ready(function () {
                                                                for (i = 1; i <= 2; i++) {
                                                                    var oTable = $('#sample_' + i).dataTable({
                                                                        "aoColumnDefs": [
                                                                            {"aTargets": [0]}
                                                                        ],
                                                                        "aaSorting": [[1, 'asc']],
                                                                        "aLengthMenu": [
                                                                            [5, 15, 20, -1],
                                                                            [5, 15, 20, "All"] // change per page values here
                                                                        ],
                                                                        // set the initial value
                                                                        "iDisplayLength": 10,
                                                                    });

                                                                    jQuery('#sample_' + i + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
                                                                    jQuery('#sample_' + i + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
                                                                    jQuery('#sample_' + i + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

                                                                    $('#sample_' + i + '_column_toggler input[type="checkbox"]').change(function () {
                                                                        /* Get the DataTables object again - this is not a recreation, just a get of the object */
                                                                        var iCol = parseInt($(this).attr("data-column"));
                                                                        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
                                                                        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
                                                                    });
                                                                }
                                                            });

                                                            function  ir(no_cot) {
                                                                document.forms["vtasCotForm"]["cots"].value = no_cot;
                                                                document.forms["vtasCotForm"].submit();
                                                            }

                                                            function estatus(event, num_cotizacion) {
                                                                event.preventDefault();
                                                                $("#num_cotizacion").val(num_cotizacion);
                                                                $("#estatus-modal").modal('show');
                                                            }

                                                            function estatus1(event, id_cliente) {
                                                                event.preventDefault();
                                                                $("#id_cliente").val(id_cliente);
                                                                $("#estatus-modal1").modal('show');
                                                            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
