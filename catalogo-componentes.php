<?php

if (!isset($_SESSION)) {
    session_start();
}

// if (($_SESSION['id_usuario'] == NULL)) {
//   header("Location: logout.php");
// }

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];
unset($_SESSION['id_componente']);

if (isset($_GET['comp'])) {
    $id_comp = $_GET['comp'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el componente con el id " . $id_comp . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE componentes SET activo = 0 WHERE id_componente =" . $id_comp;
    consulta($update);
    header("Location: catalogo-componentes.php");
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Componentes <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-componentes.php">
                                        Componentes
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable tabbable-custom boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                    <?php
                                      $sql = "SELECT * FROM componentes_grupos WHERE activo=1 ORDER BY grupo_componente";
                                      $query = consulta($sql);
                                      $num = mysql_num_rows($query);
                                      $a = 0;
                                      if ($num > 0) {
                                          while ($row = mysql_fetch_array($query)) {
                                              ?>
                                              <li <?php echo ($a == 0) ? 'class="active"' : ''; ?>>
                                                  <a href="#tab_<?php echo $a ?>" data-toggle="tab">
                                                      <?php echo $row['grupo_componente']; ?>
                                                  </a>
                                              </li>
                                              <?php
                                              $a++;
                                          }
                                      }
                                    ?>
                                    <li>
                                        <button onclick="window.location.href = 'alta-grupo-componente.php?id=0'"  title="Nuevo grupo" data-toggle="tooltip" data-placement="right" class="btn btn-info pull-right">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <?php
                                    $sql1 = "SELECT * FROM componentes_grupos WHERE activo=1 ORDER BY grupo_componente";
                                    $query1 = consulta($sql1);
                                    $num1 = mysql_num_rows($query1);
                                    $b = 0;
                                    if ($num1 > 0) {
                                        while ($row1 = mysql_fetch_array($query1)) {
                                            ?>
                                            <div <?php echo ($b == 0) ? 'class="tab-pane active"' : 'class="tab-pane"'; ?> id="tab_<?php echo $b; ?>">
                                                <div class="portlet box grey">
                                                    <div class="portlet-title">
                                                        <button onclick="window.location.href = 'alta-componentes.php?id=0&idgc=<?php echo $row1['id_grupo_componente']; ?>'"  title="Crear Nuevo Componente" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <?php
                                                          $sql3 = "SELECT * FROM componentes WHERE activo=1 AND id_componente_grupo=" . $row1['id_grupo_componente'] . " ORDER BY id_componente";
                                                          $query3 = consulta($sql3);
                                                          $num3 = mysql_num_rows($query3);

                                                          if ($num3 > 0) {
                                                        ?>
                                                        <form method="POST" id="compForm" name="compForm" action="./ajax/delComp.php">
                                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_<?php echo $b; ?>">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Nombre del Componente</th>
                                                                        <th>VA</th>
                                                                        <th>VB</th>
                                                                        <th>VC</th>
                                                                        <th>VD</th>
                                                                        <th>VE</th>
                                                                        <th>VF</th>
                                                                        <th>Precio</th>
                                                                        <!-- <th>Impuesto</th> -->
                                                                        <th>Eliminar</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                        $c = 0;
                                                                        while ($row3 = mysql_fetch_array($query3)) {
                                                                            ?>
                                                                            <tr>
                                                                                <td>
                                                                                  <a href="alta-componentes.php?id=<?php echo $row3['id_componente'] ?>&idgc=<?php echo $row1['id_grupo_componente']; ?>">
                                                                                      <?php echo $row3['nombre_componente']; ?>
                                                                                  </a>
                                                                                </td>
                                                                                <td>$ <?php echo number_format($row3['VA'], 2, '.', ','); ?></td>
                                                                                <td>$ <?php echo number_format($row3['VB'], 2, '.', ','); ?></td>
                                                                                <td>$ <?php echo number_format($row3['VC'], 2, '.', ','); ?></td>
                                                                                <td>$ <?php echo number_format($row3['VD'], 2, '.', ','); ?></td>
                                                                                <td>$ <?php echo number_format($row3['VE'], 2, '.', ','); ?></td>
                                                                                <td>$ <?php echo number_format($row3['VF'], 2, '.', ','); ?></td>
                                                                                <td>$ <?php echo number_format($row3['precio'], 2, '.', ','); ?></td>
                                                                                <!-- <td><php echo $row3['impuesto'] .'%'; ?></td> -->
                                                                                <td>
                                                                                    <div class="btn-group">
                                                                                        <a title="Eliminar Componente" data-toggle="tooltip" data-placement="right" class="btn btn-sm btn-danger pull-right" onclick="deshabilita(event, <?php echo $row3['id_componente']; ?>)">
                                                                                          <i class="fa fa-times"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                            $c++;
                                                                        } //end while
                                                                    ?>
                                                                </tbody>
                                                                <tfoot>
                                                                  <tr>
                                                                      <th>Nombre del Componente</th>
                                                                      <th>VA</th>
                                                                      <th>VB</th>
                                                                      <th>VC</th>
                                                                      <th>VD</th>
                                                                      <th>VE</th>
                                                                      <th>VF</th>
                                                                      <th>Precio</th>
                                                                      <!-- <th>Impuesto</th> -->
                                                                      <th>Eliminar</th>
                                                                  </tr>
                                                                </tfoot>
                                                            </table>
                                                        </form>
                                                        <?php
                                                          } else {
                                                            echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                                          } //end if
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $b++;
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script src="scripts/custom/form-samples.js"></script>
        <script type="text/javascript">
          jQuery(document).ready(function () {
              App.init(); // initlayout and core plugins
//            TableAdvanced.init();
              FormSamples.init();

              $("[data-toggle='tooltip']").tooltip();
          });
          $(document).ready(function () {
              for (i = 0; i <= 8; i++) {
                  var oTable = $('#sample_' + i).dataTable({
                      "aoColumnDefs": [
                          {"aTargets": [0]}
                      ],
                      "aaSorting": [[0, 'desc']],
                      "aLengthMenu": [
                          [5, 15, 20, -1],
                          [5, 15, 20, "All"] // change per page values here
                      ],
                      // set the initial value
                      "iDisplayLength": 10,
                  });

                  jQuery('#sample_' + i + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
                  jQuery('#sample_' + i + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
                  jQuery('#sample_' + i + '_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

                  $('#sample_' + i + '_column_toggler input[type="checkbox"]').change(function () {
                      /* Get the DataTables object again - this is not a recreation, just a get of the object */
                      var iCol = parseInt($(this).attr("data-column"));
                      var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
                      oTable.fnSetColumnVis(iCol, (bVis ? false : true));
                  });
              }
          });

          function deshabilita(event, id) {
              event.preventDefault();
              var respuesta = confirm('\u00BFDesea eliminar el componente?');
              if (respuesta) {
                  location.href = 'catalogo-componentes.php?comp=' + id;
              }
          }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
