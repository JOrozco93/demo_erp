<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}
require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la divisa con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE divisas SET activo = 0, id_log = " . $id_log . " WHERE id_divisa =" . $id_eliminado;
    consulta($update);
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>  
        <link href="plugins/data-tables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <link href="css/jquery-ui.min.css" rel="stylesheet">
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">                                                
                        <div class="col-lg-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Divisas
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-divisas.php">
                                        Divisas
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>                                
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->

                    <div class="box">                        
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="btn-group col-lg-12">
                                    <div class="input-group-btn">
                                        <div class="col-lg-11"></div>
                                        <div class="col-lg-1">
                                            <a href="alta-divisas.php?id=0" title="Agregar divisa" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                <i class="fa fa-plus"></i>
                                            </a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>Divisas</th>
                                            <th>Abreviatura Divisa</th>
                                            <th>Tipo Cambio</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    $sql = "SELECT * FROM divisas WHERE activo=1 ORDER BY id_divisa";
                                    $query = consulta($sql);
                                    $num = mysql_num_rows($query);
                                    if ($num > 0) {
                                        while ($row = mysql_fetch_array($query)) {
                                            ?> 
                                            <tr>
                                                <td>                                                              
                                                    <a href="alta-divisas.php?id=<?php echo $row['id_divisa'] ?>">
                                                        <?php echo $row['nombre']; ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <?php echo $row['abbrev']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['tipo_cambio']; ?>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a onclick="deshabilita(event, <?php echo $row['id_divisa']; ?>, '<?php echo ($row['abbrev'] . '' . $row['nombre']); ?>')">
                                                            <button  title="Eliminar divisas" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <tfoot>
                                        <tr>
                                            <th>Divisa</th>
                                            <th>Abreviatura Divisa</th>
                                            <th>Tipo Cambio</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ("includes/js.php"); ?>
        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>

        <script type="text/javascript">
                                                            jQuery(document).ready(function () {
                                                                App.init(); // initlayout and core plugins
                                                                TableAdvanced.init();
                                                            });
                                                            function ir(divisas) {
                                                                //                                                                    alert(moneda);
                                                                document.forms[0].divisas.value = divisas;
                                                                document.forms[0].submit();
                                                            }

                                                            function deshabilita(event, id, nombre) {
                                                                event.preventDefault();
                                                                var respuesta = confirm('\u00BFDesea Eliminar la Divisa "' + nombre + '"?');
                                                                if (respuesta) {
                                                                    location.href = 'catalogo-divisas.php?borrar=' + id;
                                                                }
                                                            }
        </script>
    </body>
    <?php include ("includes/footer.php"); ?>
</html>
