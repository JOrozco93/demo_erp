<?php

if (!isset($_SESSION)) {
    session_start();
}

// if (($_SESSION['id_usuario'] == NULL)) {
//     header("Location: logout.php");
// }

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

unset($_SESSION['id_kit']);

if (isset($_GET['herr'])) {
    $id_kit = $_GET['herr'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el kit con el id " . $id_kit . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE kit SET activo = 0 WHERE id_kit =" . $id_kit;
    consulta($update);
    header("Location: catalogo-herreria.php");
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>HS PUERTAS DE GARAJE</title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Kit Herrer&iacute;a <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-herreria.php">
                                        Kits
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable tabbable-custom boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pabe active">
                                        <div class="portlet box blue">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href = 'alta-kit.php?id=0'"  title="Crear Nuevo Kit Herrería" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                              <?php
                                                $sql1 = "SELECT * FROM kit WHERE activo=1 ORDER BY id_kit";
                                                $query1 = consulta($sql1);
                                                $num1 = mysql_num_rows($query1);
                                                $a = 0;
                                                if ($num1 > 0) {
                                              ?>
                                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre Kit Herrer&iacute;a</th>
                                                            <th>Proveedor</th>
                                                            <th>Componentes</th>
                                                            <th>VA</th>
                                                            <th>VB</th>
                                                            <th>VC</th>
                                                            <th>VD</th>
                                                            <th>VE</th>
                                                            <th>VF</th>
                                                            <th>Precio</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            while ($row1 = mysql_fetch_array($query1)) {
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <a data-placement="right" data-toggle="tooltip" title="Modificar" href="alta-kit.php?id=<?php echo $row1['id_kit'] ?>">
                                                                            <?php echo $row1['nombre_kit']; ?>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                      <?php
                                                                        $sql3 = "SELECT * FROM proveedores WHERE activo=1 AND id_proveedor = ".$row1['id_proveedor'];
                                                                        $rowp1 = mysql_fetch_array(consulta($sql3));
                                                                      ?>
                                                                        <?php echo $rowp1['nombre']; ?>
                                                                    </td>
                                                                    <td>
                                                                      <?php
                                                                        $num_KC = mysql_fetch_array(consulta("SELECT COUNT(*) as total FROM kit_componente WHERE activo =1 AND id_kit = ". $row1['id_kit']));
                                                                        echo $num_KC['total']
                                                                       ?>
                                                                    </td>
                                                                    <td>$ <?php echo number_format($row1['VA'], 2, '.', ','); ?></td>
                                                                    <td>$ <?php echo number_format($row1['VB'], 2, '.', ','); ?></td>
                                                                    <td>$ <?php echo number_format($row1['VC'], 2, '.', ','); ?></td>
                                                                    <td>$ <?php echo number_format($row1['VD'], 2, '.', ','); ?></td>
                                                                    <td>$ <?php echo number_format($row1['VE'], 2, '.', ','); ?></td>
                                                                    <td>$ <?php echo number_format($row1['VF'], 2, '.', ','); ?></td>
                                                                    <td>$ <?php echo number_format($row1['precio'], 2, '.', ','); ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a onclick="deshabilita(event, <?php echo $row1['id_kit']; ?>, '<?php echo ($row1['nombre_kit']); ?>')" title="Eliminar Kit Herrería" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                              <i class="fa fa-times"></i>
                                                                            </a>&nbsp;
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $a++;
                                                            }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Nombre Kit Herrer&iacute;a</th>
                                                            <th>Proveedor</th>
                                                            <th>Componentes</th>
                                                            <th>VA</th>
                                                            <th>VB</th>
                                                            <th>VC</th>
                                                            <th>VD</th>
                                                            <th>VE</th>
                                                            <th>VF</th>
                                                            <th>Precio</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                              <?php
                                            } else {
                                              echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                            }
                                              ?>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        <script>
          jQuery(document).ready(function () {
              App.init();
              TableAdvanced.init();
              $("[data-toggle='tooltip']").tooltip();
          });

          function  deshabilita(event, id_herr, nom_herr) {
              event.preventDefault();
              var respuesta = confirm('\u00BFDesea eliminar el Kit Herrería: "' + nom_herr + '"?');
              if (respuesta) {
                  location.href = 'catalogo-herreria.php?herr=' + id_herr;
              }
          }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
