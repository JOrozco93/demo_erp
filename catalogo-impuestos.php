<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];
unset($_SESSION['borrar']);
if (isset($_GET['borrar'])) {
    $borrar = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el kit con el id " . $borrar . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE impuestos SET activo = 0 WHERE id_impuesto =" . $borrar;
    consulta($update);
    header("Location: catalogo-impuestos.php");
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>HS PUERTAS DE GARAJE</title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Impuestos <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="catalogo-impuestos.php">
                                        Catalago impuestos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a>
                                        Listado de impuestos
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-11"></div>
                                            <div class="col-lg-1">
                                                <button onclick="window.location.href = 'alta-impuesto.php?id=0'"  title="Crear Nuevo impuesto" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <?php
                                    $sql1 = "SELECT * FROM impuestos WHERE activo=1 ORDER BY id_impuesto";
                                    $query1 = consulta($sql1);
                                    $num1 = mysql_num_rows($query1);
                                    $a = 0;
                                    if ($num1 > 0) {
                                        ?>
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>No. impuesto</th>
                                                    <th>Nombre del impuesto</th>         
                                                    <th>Porcentaje</th>
                                                    <th>Eliminar</th>                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($row1 = mysql_fetch_array($query1)) {
                                                    ?>
                                                    <tr>
                                                        <td>                                                           
                                                            <?php echo $row1['id_impuesto']; ?>                                                            
                                                        </td>
                                                        <td>             
                                                            <a data-placement="right" data-toggle="tooltip" title="Modificar" href="alta-impuesto.php?id=<?php echo $row1['id_impuesto'] ?>">
                                                                <?php echo $row1['nombre']; ?>
                                                            </a>
                                                        </td>  
                                                        <td>                                                           
                                                            <?php echo $row1['porcentaje']; ?>                                                            
                                                        </td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <a onclick="deshabilita(event, <?php echo $row1['id_impuesto']; ?>, '<?php echo ($row1['nombre']); ?>')" title="Eliminar Impuesto" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                    <i class="fa fa-times"></i>
                                                                </a>&nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $a++;
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No. impuesto</th>
                                                    <th>Nombre del impuesto</th>         
                                                    <th>Porcentaje</th>
                                                    <th>Eliminar</th> 
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <?php
                                    } else {
                                        echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        <script>
                                                            jQuery(document).ready(function () {
                                                                App.init(); // initlayout and core plugins
                                                                TableAdvanced.init();
                                                                $("[data-toggle='tooltip']").tooltip();
                                                            });

                                                            function  deshabilita(event, id, nombre) {
                                                                event.preventDefault();
                                                                var respuesta = confirm('\u00BFDesea eliminar el impuesto: "' + nombre + '"?');
                                                                if (respuesta) {
                                                                    location.href = 'catalogo-impuestos.php?borrar=' + id;
                                                                }
                                                            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
