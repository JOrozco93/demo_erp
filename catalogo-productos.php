<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}
require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El producto con el id " . $id_eliminado . " ha sido eliminado";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "DELETE FROM productos WHERE id = $id_eliminado";
    consulta($update);
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Productos <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-productos.php">
                                        Listado de Productos
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-11"></div>
                                            <div class="col-lg-1">
                                                <a href="alta-productos.php"  title="Crear Nuevo Usuario" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">

                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>
                                                        Descripcion
                                                    </th>
                                                    <th>
                                                        Tasa de IVA
                                                    </th>
                                                     <th>
                                                        Precio Unitario
                                                    </th>
                                                    <th>
                                                        Unidad
                                                    </th>
                                                    <th>
                                                        Acciones
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sql1 = "SELECT * FROM productos WHERE activo=1 ORDER BY nombre";
                                                $query1 = consulta($sql1);
                                                $num1 = mysql_num_rows($query1);
                                                $a = 0;
                                                if ($num1 > 0) {
                                                    while ($row1 = mysql_fetch_array($query1)) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                    <?php echo $row1['nombre'] ; ?>
                                                            </td>
                                                            <td>
                                                              <?php echo $row1['descripcion'] ; ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                      if ($row1['tasa_IVA'] == 1) {
                                                                          echo "Sin IVA";
                                                                        }
                                                                        else if ($row1['tasa_IVA'] == 2) {
                                                                          echo "16%";
                                                                        }
                                                                        else if ($row1['tasa_IVA'] == 3) {
                                                                          echo "11%";
                                                                        }
                                                                        else if ($row1['tasa_IVA'] == 4) {
                                                                          echo "0%";
                                                                        }
                                                                 ?>
                                                            </td>
                                                             <td>
                                                                <?php echo "$" . $row1['precio_Unitario'] . ".00"; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $row1['unidad'] ; ?>
                                                            </td>
                                                            <td>
                                                            <div class="btn-group">
                                                                    <a onclick="modifica(event, <?php echo $row1['id']; ?>)">
                                                                        <button  title="Modificar Producto" type="button" data-toggle="tooltip" data-placement="right" style="color: orange" class="btn btn-sm btn-default pull-right">
                                                                            <i class="fa fa-pencil"></i>
                                                                        </button>
                                                                    </a>&nbsp;
                                                                </div>
                                                                <div class="btn-group">
                                                                    <a onclick="deshabilita(event, <?php echo $row1['id']; ?>, '<?php echo $row1['nombre']; ?>')">
                                                                        <button  title="Eliminar Producto" type="button" data-toggle="tooltip" data-placement="right" style="color: red" class="btn btn-sm btn-default pull-right">
                                                                            <i class="fa fa-times"></i>
                                                                        </button>
                                                                    </a>&nbsp;
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $a++;
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                              <tr>
                                                  <th>Nombre</th>
                                                  <th>
                                                      Descripcion
                                                  </th>
                                                  <th>
                                                      Tasa de IVA
                                                  </th>
                                                   <th>
                                                      Precio Unitario
                                                  </th>
                                                  <th>
                                                      Unidad
                                                  </th>
                                                  <th>
                                                      Acciones
                                                  </th>
                                              </tr>
                                            </tfoot>
                                            <input type="hidden" id="usuarios" name="usuarios"/>
                                        </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        <script>
            jQuery(document).ready(function () {
                App.init(); // initlayout and core plugins
                TableAdvanced.init();
            });



            function  deshabilita(event, id_prod, nom_prod) {
                event.preventDefault();
                var respuesta = confirm('\u00BFDesea eliminar el Producto "' + nom_prod + '"?');
                if (respuesta) {
                    location.href = 'catalogo-productos.php?borrar=' + id_prod;
                }
            }

            function  modifica(event, id_prod) {
                event.preventDefault();
                    location.href = 'alta-productos-mod.php?modificar=' + id_prod;
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
