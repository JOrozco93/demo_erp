<?php

if (!isset($_SESSION)) {
    session_start();
}

$id_usuario = $_SESSION['id_usuario'];

// if (($_SESSION['id_usuario'] == NULL)) {
//     header("Location: logout.php");
// }

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

if (isset($_GET['borrar'])) {
    $id_modelo = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el modelo con el id " . $id_modelo . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE puerta_modelo SET activo = 0 WHERE id_pmodelo =" . $id_modelo;
    consulta($update);
}

if (isset($_GET['borrar2'])) {
    $id_medida = $_GET['borrar2'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la medida con el id " . $id_medida . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE puerta SET activo = 0 WHERE id_puerta =" . $id_medida;
    consulta($update);
}

if (isset($_GET['borrar3'])) {
    $id_textura = $_GET['borrar3'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la textura con el id " . $id_textura . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE puerta_textura SET activo = 0 WHERE id_ptextura =" . $id_textura;
    consulta($update);
}

if (isset($_GET['borrar4'])) {
    $id_puerta = $_GET['borrar4'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el componentes del puerta con el id " . $id_puerta . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE puerta_componente SET activo = 0 WHERE id_puerta =" . $id_puerta;
    consulta($update);
}

if (isset($_GET['borrar5'])) {
    $id_movimiento = $_GET['borrar5'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el movimiento con el id " . $id_movimiento . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE puerta_movimiento SET activo = 0 WHERE id_pmovimiento =" . $id_movimiento;
    consulta($update);
}

if (isset($_GET['borrar6'])) {
    $id_color = $_GET['borrar6'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el color con el id " . $id_color . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE puerta_color SET activo = 0 WHERE id_pcolor =" . $id_color;
    consulta($update);
}

if (isset($_GET['borrar7'])) {
    $id_tipo = $_GET['borrar7'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el tipo con el id " . $id_tipo . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE puerta_tipo SET activo = 0 WHERE id_ptipo =" . $id_tipo;
    consulta($update);
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Puertas</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-puertas.php">
                                        Puertas
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable tabbable-custom boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_0" data-toggle="tab">Puerta</a></li>
                                    <li><a href="#tab_1" data-toggle="tab">Modelo</a></li>
                                    <li><a href="#tab_2" data-toggle="tab">Textura</a></li>
                                    <li><a href="#tab_3" data-toggle="tab">Movimiento</a></li>
                                    <li><a href="#tab_4" data-toggle="tab">Color</a></li>
                                    <li><a href="#tab_5" data-toggle="tab">Tipo</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box red">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href='alta-puerta.php?id=0'" title="Crear Nuevo Ancho x Alto" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                              <?php
                                              $sql2 = "SELECT * FROM puerta AS p, puerta_tipo AS pt WHERE p.activo = 1 AND p.id_ptipo = pt.id_ptipo";
                                              $query2 = consulta($sql2);
                                              $num2 = mysql_num_rows($query2);
                                              if ($num2 > 0) {
                                              ?>
                                                <table id="sample_0" class="table table-striped table-bordered table-hover table-full-width">
                                                    <thead>
                                                        <tr>
                                                            <th>Metros</th>
                                                            <th>Pies</th>
                                                            <th>Tipo</th>
                                                            <th>VA</th>
                                                            <th>VB</th>
                                                            <th>VC</th>
                                                            <th>VD</th>
                                                            <th>VE</th>
                                                            <th>VF</th>
                                                            <th>Precio</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            while ($row2 = mysql_fetch_array($query2)) {
                                                                ?>
                                                                <tr>
                                                                    <td><a href="detalle-puerta.php?id=<?php echo $row2['id_puerta'] ?>"><?php echo $row2['m_ancho'] . ' x ' . $row2['m_alto']; ?></a></td>
                                                                    <td><?php echo $row2['f_ancho'] . '\''.$row2['in_ancho'] . '" x ' . $row2['f_alto']. '\''.$row2['in_alto'] . '"'; ?></td>
                                                                    <td><?php echo $row2['nombre_tipo']; ?></td>
                                                                    <td>$ <?php echo $row2['VA']; ?></td>
                                                                    <td>$ <?php echo $row2['VB']; ?></td>
                                                                    <td>$ <?php echo $row2['VC']; ?></td>
                                                                    <td>$ <?php echo $row2['VD']; ?></td>
                                                                    <td>$ <?php echo $row2['VE']; ?></td>
                                                                    <td>$ <?php echo $row2['VF']; ?></td>
                                                                    <td>$ <?php echo $row2['precio']; ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a onclick="deshabilita_medida(event, <?php echo $row2['id_puerta']; ?>, '<?php echo $row2['m_ancho'] . 'x' . $row2['m_alto']; ?>')">
                                                                                <button  title="Eliminar Puerta" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                                    <i class="fa fa-times"></i>
                                                                                </button>
                                                                            </a>&nbsp;
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Metros</th>
                                                            <th>Pies</th>
                                                            <th>Tipo</th>
                                                            <th>VA</th>
                                                            <th>VB</th>
                                                            <th>VC</th>
                                                            <th>VD</th>
                                                            <th>VE</th>
                                                            <th>VF</th>
                                                            <th>Precio</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                              <?php
                                              } else {
                                                  echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                              }
                                               ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function  deshabilita_medida(event, id, metros) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar la Medida en Metros "' + metros + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-puertas.php?borrar2=' + id;
                                            }
                                        }
                                    </script>

                                    <div class="tab-pane" id="tab_1">
                                        <div class="portlet box purple">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href = 'alta-puerta-modelo.php?id=0'" title="Crear Nuevo Modelo" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <?php
                                                // $sql1 = "SELECT * FROM puerta_modelo, puerta_tipo WHERE activo = 1 AND puerta_modelo.id_ptipo = puerta_tipo.id_ptipo ORDER BY id_pmodelo";
                                                $sql1 = "SELECT * FROM puerta_modelo AS pm, puerta_tipo AS pt WHERE pm.activo = 1 AND pm.id_ptipo = pt.id_ptipo ORDER BY pm.id_pmodelo";
                                                $query1 = consulta($sql1);
                                                $num1 = mysql_num_rows($query1);
                                                if ($num1 > 0) {
                                                ?>
                                                <table id="sample_1" class="table table-striped table-bordered table-hover table-full-width">
                                                    <thead>
                                                        <tr>
                                                            <th>Modelo</th>
                                                            <th>Descripción</th>
                                                            <th>Tipo</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            while ($row1 = mysql_fetch_array($query1)) {
                                                                ?>
                                                                <tr>
                                                                    <td><a href="alta-puerta-modelo.php?id=<?php echo $row1['id_pmodelo'] ?>"><?php echo $row1['nombre_modelo']; ?></a></td>
                                                                    <td><?php echo $row1['descripcion'] ?></td>
                                                                    <td><?php echo $row1['nombre_tipo'] ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a title="Eliminar Modelo" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right" onclick="deshabilita_modelo(event, <?php echo $row1['id_pmodelo']; ?>, '<?php echo ($row1['nombre_modelo']); ?>')">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Modelo</th>
                                                            <th>Descripción</th>
                                                            <th>Tipo</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <?php
                                                } else {
                                                      echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                                }
                                                 ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function  deshabilita_modelo(event, id, nombre_modelo) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar el Modelo "' + nombre_modelo + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-puertas.php?borrar=' + id;
                                            }
                                        }
                                    </script>

                                    <div class="tab-pane" id="tab_2">
                                        <div class="portlet box yellow">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">                                                
                                                    <button onclick="window.location.href = 'alta-puerta-textura.php?id=0'"  title="Crear Nueva Textura" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <?php
                                                $sql3 = "SELECT * FROM puerta_textura WHERE activo=1 ORDER BY id_ptextura";
                                                $query3 = consulta($sql3);
                                                $num3 = mysql_num_rows($query3);
                                                if ($num3 > 0) {
                                                ?>
                                                <table id="sample_2" class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Nombre de la Textura
                                                            </th>
                                                            <th>
                                                                Descripción
                                                            </th>
                                                            <th>
                                                                Eliminar
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            while ($row3 = mysql_fetch_array($query3)) {
                                                                ?>
                                                                <tr>
                                                                    <td><a href="alta-puerta-textura.php?id=<?php echo $row3['id_ptextura'] ?>"><?php echo $row3['nombre_textura']; ?></a></td>
                                                                    <td><?php echo $row3['descripcion']; ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a title="Eliminar Textura" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right" onclick="deshabilita_textura(event, <?php echo $row3['id_ptextura']; ?>, '<?php echo $row3['nombre_textura']; ?>')">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>&nbsp;
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>
                                                                Nombre de la Textura
                                                            </th>
                                                            <th>
                                                                Descripción
                                                            </th>
                                                            <th>
                                                                Eliminar
                                                            </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                              <?php
                                              } else {
                                                  echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                              }
                                              ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function  deshabilita_textura(event, id, nombre_textura) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar la Textura "' + nombre_textura + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-puertas.php?borrar3=' + id;
                                            }
                                        }
                                    </script>
                                    
                                    <div class="tab-pane" id="tab_3">
                                        <div class="portlet box blue">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href = 'alta-puerta-movimiento.php?id=0'"  title="Crear Nuevo Movimiento" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <?php
                                                $sql5 = "SELECT * FROM puerta_movimiento AS pm, puerta_tipo AS pt WHERE pm.activo = 1 AND pm.id_ptipo = pt.id_ptipo ORDER BY pm.id_pmovimiento";
                                                $query5 = consulta($sql5);
                                                $num5 = mysql_num_rows($query5);
                                                if ($num5 > 0) {
                                                ?>
                                                <table id="sample_3" class="table table-striped table-bordered table-hover table-full-width" id="sample_5">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre del Movimiento o Elevación</th>
                                                            <th>Tipo</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            while ($row5 = mysql_fetch_array($query5)) {
                                                                ?>
                                                                <tr>
                                                                    <td><a href="alta-puerta-movimiento.php?id=<?php echo $row5['id_pmovimiento'] ?>"><?php echo $row5['nombre_movimiento']; ?></a></td>
                                                                    <td><?php echo $row5['nombre_tipo'] ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a title="Eliminar Movimiento o Elevaci&oacute;n" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right" onclick="deshabilita_mov(event, <?php echo $row5['id_pmovimiento']; ?>, '<?php echo $row5['nombre_movimiento']; ?>')">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>&nbsp;
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Nombre del Movimiento o Elevación</th>
                                                            <th>Tipo</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <?php
                                                } else {
                                                    echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function  deshabilita_mov(event, id, nombre_movimiento) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar el Movimiento o Elevacion: "' + nombre_movimiento + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-puertas.php?borrar5=' + id;
                                            }
                                        }
                                    </script>

                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href = 'alta-puerta-color.php?id=0'" title="Crear Nuevo Color" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <?php
                                                $sql6 = "SELECT * FROM puerta_color WHERE activo=1 ORDER BY id_pcolor";
                                                $query6 = consulta($sql6);
                                                $num6 = mysql_num_rows($query6);
                                                if ($num6 > 0) {
                                                ?>
                                                <table id="sample_4" class="table table-striped table-bordered table-hover table-full-width" id="sample_6">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre del Color</th>
                                                            <th>Descripción</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            while ($row6 = mysql_fetch_array($query6)) {
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <a href="alta-puerta-color.php?id=<?php echo $row6['id_pcolor'] ?>">
                                                                            <?php echo $row6['nombre_color']; ?>
                                                                        </a>
                                                                    </td>
                                                                    <td><?php echo $row6['descripcion']; ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a title="Eliminar Color" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right" onclick="deshabilita_color(event, <?php echo $row6['id_pcolor']; ?>, '<?php echo $row6['nombre_color']; ?>')">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Nombre del Color</th>
                                                            <th>Descripción</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                              <?php
                                              } else {
                                                  echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                              }
                                              ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function  deshabilita_color(event, id, nombre_color) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar el Color: "' + nombre_color + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-puertas.php?borrar6=' + id;
                                            }
                                        }
                                    </script>

                                    <div class="tab-pane" id="tab_5">
                                        <div class="portlet box light-grey">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href = 'alta-puerta-tipo.php?id=0'"  title="Crear Nuevo Tipo" data-toggle="tooltip" data-placement="top" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <?php
                                                $sql7 = "SELECT * FROM puerta_tipo WHERE activo=1 ORDER BY id_ptipo";
                                                $query7 = consulta($sql7);
                                                $num7 = mysql_num_rows($query7);
                                                if ($num7 > 0) {
                                                ?>
                                                <table id="sample_5" class="table table-striped table-bordered table-hover table-full-width" id="sample_7">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Tipo
                                                            </th>
                                                            <th>
                                                                Eliminar
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            while ($row7 = mysql_fetch_array($query7)) {
                                                                ?>
                                                                <tr>
                                                                    <td><a href="alta-puerta-tipo.php?id=<?php echo $row7['id_ptipo'] ?>"><?php echo $row7['nombre_tipo']; ?></a></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a title="Eliminar Tipo" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right" onclick="deshabilita_tipo(event, <?php echo $row7['id_ptipo']; ?>, '<?php echo ($row7['nombre_tipo']); ?>')">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>&nbsp;
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>
                                                                Tipo
                                                            </th>
                                                            <th>
                                                                Eliminar
                                                            </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <?php
                                                } else {
                                                    echo "<h5 class='text-center'><em>No hay información</em></h5><p>&nbsp;</p>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function  deshabilita_tipo(event, id, nombre_tipo) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar el Tipo "' + nombre_tipo + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-puertas.php?borrar7=' + id;
                                            }
                                        }
                                    </script>
                                </div>
                            </div>
                            <!-- END CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script src="scripts/custom/form-samples.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                App.init();
                // TableAdvanced.init();

                for (i = 0; i <= 5; i++) {
                    $('#sample_' + i).dataTable({
                        "aaSorting": [[2, 'asc'], [0, 'asc']],
                         "aLengthMenu": [
                            [5, 15, 20, -1],
                            [5, 15, 20, "All"] // change per page values here
                        ],
                        // set the initial value
                        "iDisplayLength": 10,
                    });

                    $('#sample_' + i + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline");
                    $('#sample_' + i + '_wrapper .dataTables_length select').addClass("form-control input-small");
                    $('#sample_' + i + '_wrapper .dataTables_length select').select2();
                }

                $("[data-toggle='tooltip']").tooltip();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
