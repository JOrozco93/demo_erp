<?php

if (!isset($_SESSION)) {
    session_start();
}

// if (($_SESSION['id_usuario'] == NULL)) {
//     header("Location: logout.php");
// }

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar0'])) {
    $id_desventana = $_GET['borrar0'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la ventana con el id " . $id_desventana . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE ventana SET activo = 0 WHERE id_ventana =" . $id_desventana;
    consulta($update);
}

if (isset($_GET['borrar2'])) {
    $id_color = $_GET['borrar2'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el color con el id " . $id_color . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE ventana_color SET activo = 0 WHERE id_vcolor =" . $id_color;
    consulta($update);
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">                   
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Ventanas <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-ventanas.php">
                                        Ventanas
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable tabbable-custom boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_0" data-toggle="tab">Ventana</a></li>
                                    <li><a href="#tab_1" data-toggle="tab">Color</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box purple">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href = 'alta-ventanas.php?id=0'" title="Crear Nueva Ventana" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>   
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table id="sample_0" class="table table-striped table-bordered table-hover table-full-width">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre de la Ventana</th>
                                                            <th>Descripción</th>
                                                            <th>Tipo</th>
                                                            <th>VA</th>
                                                            <th>VB</th>
                                                            <th>VC</th>
                                                            <th>VD</th>
                                                            <th>VE</th>
                                                            <th>VF</th>
                                                            <th>Precio</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sql1 = "SELECT * FROM ventana AS v, puerta_tipo AS pt WHERE v.activo = 1 AND v.id_ptipo = pt.id_ptipo ORDER BY v.id_ventana";
                                                        $query1 = consulta($sql1);
                                                        $num1 = mysql_num_rows($query1);
                                                        $a = 0;
                                                        if ($num1 > 0) {
                                                            while ($row1 = mysql_fetch_array($query1)) {
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <a href="alta-ventanas.php?id=<?php echo $row1['id_ventana'] ?>">
                                                                            <?php echo $row1['nombre_ventana']; ?>
                                                                        </a>
                                                                    </td>
                                                                    <td><?php echo $row1['descripcion']; ?></td>
                                                                    <td><?php echo $row1['nombre_tipo']; ?></td>
                                                                    <td>$ <?php echo $row1['VA']; ?></td>
                                                                    <td>$ <?php echo $row1['VB']; ?></td>
                                                                    <td>$ <?php echo $row1['VC']; ?></td>
                                                                    <td>$ <?php echo $row1['VD']; ?></td>
                                                                    <td>$ <?php echo $row1['VE']; ?></td>
                                                                    <td>$ <?php echo $row1['VF']; ?></td>
                                                                    <td>$ <?php echo $row1['precio']; ?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a onclick="deshabilita_ventana(event, <?php echo $row1['id_ventana']; ?>, '<?php echo ($row1['nombre_ventana']); ?>')">
                                                                                <button  title="Eliminar Ventana" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                                    <i class="fa fa-times"></i>
                                                                                </button>
                                                                            </a>&nbsp;
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $a++;
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Nombre de la Ventana</th>
                                                            <th>Descripción</th>
                                                            <th>Tipo</th>
                                                            <th>VA</th>
                                                            <th>VB</th>
                                                            <th>VC</th>
                                                            <th>VD</th>
                                                            <th>VE</th>
                                                            <th>VF</th>
                                                            <th>Precio</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function deshabilita_ventana(event, id, nombre_ventana) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar la Ventana "' + nombre_ventana + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-ventanas.php?borrar0=' + id;
                                            }
                                        }
                                    </script>

                                    <div class="tab-pane" id="tab_1">
                                        <div class="portlet box green">
                                            <div class="row portlet-title">
                                                <div class="col-md-12">
                                                    <button onclick="window.location.href = 'alta-color.php?id=0'" title="Crear Nuevo Color" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>   
                                            </div>
                                            <div class="portlet-body">
                                                <table id="sample_1" class="table table-striped table-bordered table-hover table-full-width">
                                                    <thead>
                                                        <tr>
                                                            <th>Color</th>
                                                            <th>Descripci&oacute;n</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sql2 = "SELECT * FROM ventana_color WHERE activo=1 ORDER BY id_vcolor";
                                                        $query2 = consulta($sql2);
                                                        $num2 = mysql_num_rows($query2);

                                                        if ($num2 > 0) {
                                                            while ($row2 = mysql_fetch_array($query2)) {
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <a href="alta-color.php?id=<?php echo $row2['id_vcolor'] ?>">
                                                                            <?php echo $row2['nombre_color']; ?>
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <?php
                                                                        echo $row2['descripcion'];
                                                                        ?>
                                                                    </td>                                                                    
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a onclick="deshabilita_color(event, <?php echo $row2['id_vcolor']; ?>, '<?php echo ($row2['nombre_color']); ?>')">
                                                                                <button  title="Eliminar Color" data-toggle="tooltip" data-placement="right" style="color: blue" class="btn btn-sm btn-default pull-right">
                                                                                    <i class="fa fa-times"></i>
                                                                                </button>
                                                                            </a>&nbsp;
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Color</th>
                                                            <th>Descripci&oacute;n</th>
                                                            <th>Eliminar</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        function deshabilita_color(event, id, nombre_color) {
                                            event.preventDefault();
                                            var respuesta = confirm('\u00BFDesea Eliminar el Color "' + nombre_color + '"?');
                                            if (respuesta) {
                                                location.href = 'catalogo-ventanas.php?borrar2=' + id;
                                            }
                                        }
                                    </script>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <!-- END CONTAINER -->

        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script src="scripts/custom/form-samples.js"></script>
        
        <script>

            jQuery(document).ready(function () {
                App.init();
                
                for (i = 0; i <= 1; i++) {
                    $('#sample_' + i).dataTable({
                        "aaSorting": [[0, 'asc']],
                         "aLengthMenu": [
                            [5, 15, 20, -1],
                            [5, 15, 20, "All"]
                        ],
                        "iDisplayLength": 10,
                    });

                    $('#sample_' + i + '_wrapper .dataTables_filter input').addClass("form-control input-small input-inline");
                    $('#sample_' + i + '_wrapper .dataTables_length select').addClass("form-control input-small");
                    $('#sample_' + i + '_wrapper .dataTables_length select').select2();
                }

                $("[data-toggle='tooltip']").tooltip();
            });
        
        </script>
        <!-- END JAVASCRIPTS -->
    
    </body>
    <!-- END BODY -->
</html>