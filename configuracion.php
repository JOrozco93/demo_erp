<?php

header("Location: inicio.php");
exit;

session_start();

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_SESSION['notification'])) {
    $notification = $_SESSION['notification'];
    unset($_SESSION['notification']);
} else {
    $notification = NULL;
}

$stmt = $pdo->prepare("SELECT * FROM domicilios_fiscales");
$stmt->execute();

$domicilios_fiscales = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $name; ?></title>
    <?php include ("includes/css.php"); ?>
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
    <!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <?php include ("includes/header.php"); ?>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php include ("includes/menu.php"); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="theme-panel hidden-xs hidden-sm">
                    <div class="toggler-close"></div>
                </div>
                <!-- END STYLE CUSTOMIZER -->
                
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">Configuración</h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-cogs"></i>
                                <a href="inicio.php">
                                    Dashboard
                                </a>
                                <i class="fa fa-angle-right"></i>
                            </li>  
                            <li>
                                <a href="configuracion.php">
                                    Configuración
                                </a>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                
                <!-- BEGIN PAGE CONTENT-->
                <?php if ($notification != NULL): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($notification['status'] == 'success'): ?>
                                <div class="alert alert-success" role="alert"><?php echo $notification['message']; ?></div>
                            <?php endif; ?>
                            <?php if ($notification['status'] == 'error'): ?>
                                <div class="alert alert-danger" role="alert"><?php echo $notification['message']; ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Domicilios fiscales</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row" style="margin-bottom: 25px;">
                                    <div class="col-md-12">
                                        <a href="domicilio-fiscal-add.php" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Domicilio fiscal</a>
                                    </div>
                                </div>
                                <table id="table-domicilios-fiscales" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Razón Social</th>
                                            <th>RFC</th>
                                            <th>Calle</th>
                                            <th># Exterior</th>
                                            <th># Interior</th>
                                            <th>Colonia</th>
                                            <th>Referencia</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($domicilios_fiscales as $df): ?>
                                            <tr>
                                                <td><?php echo $df['razon_social']; ?></td>
                                                <td><?php echo $df['rfc']; ?></td>
                                                <td><?php echo $df['calle']; ?></td>
                                                <td><?php echo $df['no_exterior']; ?></td>
                                                <td><?php echo $df['no_interior']; ?></td>
                                                <td><?php echo $df['colonia']; ?></td>
                                                <td><?php echo $df['referencia']; ?></td>
                                                <td>
                                                    <a href="domicilio-fiscal-view.php?id=<?php echo $df['id_domicilio_fiscal']; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                                    <a href="domicilio-fiscal-edit.php?id=<?php echo $df['id_domicilio_fiscal']; ?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                                    <a href="domicilio-fiscal-delete.php?id=<?php echo $df['id_domicilio_fiscal']; ?>" class="btn btn-danger" onclick="return confirm('¿ Esta aseguro de eliminar el registro ?');"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Razón Social</th>
                                            <th>RFC</th>
                                            <th>Calle</th>
                                            <th># Exterior</th>
                                            <th># Interior</th>
                                            <th>Colonia</th>
                                            <th>Referencia</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>               
                <!-- END PAGE CONTENT-->

            </div>
        </div>
    </div>

    <?php include ("includes/footer.php"); ?>
    <?php include ("includes/js.php"); ?>

    <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="scripts/custom/table-advanced.js"></script>
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            // TableAdvanced.init();

            $('#table-domicilios-fiscales').dataTable({
                'aaSorting': [[0, 'asc']],
                'aLengthMenu': [
                    [5, 15, 20, -1],
                    [5, 15, 20, 'All']
                ],
                'iDisplayLength': 10,
            });
        });
    </script>

</body>
<!-- END BODY -->
</html>