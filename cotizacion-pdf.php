<?php

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == null)) {
        header("Location: logout.php");
        exit;
    }

    $id_usuario = $_SESSION['id_usuario'];

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    } else {
        header('Location: listado-cotizaciones.php');
        exit;
    }

    if (isset($id_usuario) && $id_usuario != 0) {    
        $sql = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
        $result = mysql_query($sql, $db_con);
        $editu = mysql_fetch_assoc($result);

        $sql = "SELECT * FROM cotizaciones WHERE cotizaciones.id_cotizacion = $id";
        $result = consulta($sql);

        if (mysql_num_rows($result)) {
            $cotizacion = mysql_fetch_array($result);

            $sql = "SELECT * FROM clientes WHERE id_cliente = {$cotizacion['id_cliente']}";
            $result = consulta($sql);
            $cliente = mysql_fetch_array($result);

            $sql = "SELECT * FROM domicilios WHERE id_domicilio = {$cliente['id_domicilio']}";
            $result = consulta($sql);
            $domicilio = mysql_fetch_array($result);

            $sql = "SELECT * FROM terminos_venta WHERE id_termino_venta = {$cotizacion['id_termino_venta']}";
            $result = consulta($sql);
            $termino_venta = mysql_fetch_array($result);

            $sql = "SELECT * FROM conceptos WHERE id_cotizacion = {$cotizacion['id_cotizacion']} ORDER BY id_concepto";
            $conceptos = consulta($sql);
        } else {
            header('Location: listado-cotizaciones.php');
            exit;   
        }
        
    }

    ob_start();

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Cotización</title>
        <style type="text/css">
            body {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
                color: #414141;
            }

            #contenedor {
                width: 816px;
                height: 1056px;
            }

            .logo {
                text-align: left;
                padding: 5px;
            }

            .tablaHeader {
                margin-bottom: 25px;
            }

            .tablaEmisor {
                text-align: right;
            }

            .cotizacion-numero {
                font-size: 14px;
                font-weight: bold;
                color: #f00;
            }

            .cotizacion-fecha {
                font-size: 14px;
                font-weight: bold;
                color: #666;
            }

            /* tabla cliente */

            .tablaCliente {
                border: 1px solid #b2b2b2;
            }

            .tablaCliente td, th {
                border: 1px solid #e4e4e4;   
            }

            .tablaCliente .fecha-th,
            .tablaCliente .cliente-th,
            .tablaCliente .direccion-th,
            .tablaCliente .telefono-th,
            .tablaCliente .email-th,
            .tablaCliente .nit-th, {
                width: 20%;
                padding: 4px 8px;
                font-weight: bold;
            }

            .tablaCliente .fecha-td,
            .tablaCliente .cliente-td,
            .tablaCliente .direccion-td,
            .tablaCliente .telefono-td,
            .tablaCliente .email-td,
            .tablaCliente .nit-td, {
                width: 80%;
                padding: 4px 8px;
            }

            /* tabla descripcion */

            .tablaDescripcion {
                border: 1px solid #b2b2b2;
            }

            .tablaDescripcion td, th {
                border: 1px solid #e4e4e4;   
            }

            .cotizacion-descripcion {
                padding: 4px 8px;
            }
            
            /* tabla conceptos */

            .tablaConceptos {
                border: 1px solid #b2b2b2;
            }

            .tablaConceptos th {
                padding: 4px 8px;
                vertical-align: top;
                background: #f00;
                color: #fff;
                border: 1px solid #b2b2b2;
            }

            .tablaConceptos td {
                padding: 4px 8px;
                vertical-align: top;
                border: 1px solid #e4e4e4;
            }

            .tablaConceptos .tipo-th {
                width: 10%;
            }

            .tablaConceptos .cantidad-th {
                width: 10%;
            }
            
            .tablaConceptos .descripcion-th {
                width: 50%;
            }
            
            .tablaConceptos .punitario-th {
                width: 15%;
            }
            
            .tablaConceptos .importe-th {
                width: 15%;
            }

            .tablaConceptos .tipo-td {
                width: 10%;
            }

            .tablaConceptos .cantidad-td {
                width: 10%;
            }
            
            .tablaConceptos .descripcion-td {
                width: 50%;
            }
            
            .tablaConceptos .punitario-td {
                text-align: right;
                width: 15%;
            }
            
            .tablaConceptos .importe-td {
                text-align: right;
                width: 15%;
            }

            .tabla {
                margin: 0;
                padding: 0;
            }

            .tabla th, td {
                padding: 4px 8px;
            }
        </style>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body>
        <div id="contenedor">
            <!--816 son 21.6 cm y 1054 son 27.9 cm para tamaño carta-->
            <div style="margin: 0; padding: 20px;">
                <!-- datos del emisor -->
                <table class="tablaHeader" width="100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50%" align="left">
                            <img src="img/logo_hs1.png" style="height: 85px;" alt="logo empresa" border="0" />
                        </td>
                        <td width="50%" align="right">
                            <table class="tablaEmisor" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td>
                                    <div class="cotizacion-numero"><?php echo $cotizacion['numero_cotizacion']; ?></div>
                                    <div class="cotizacion-fecha"><?php echo $cotizacion['fecha']; ?></div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>GRUPO EMPRESARIAL HS S.A DE C.V</td>
                                </tr>
                                <tr>
                                  <td>PROF.MA.VALDEZ N° 102 FRACC.</td>
                                </tr>
                                <tr>
                                  <td>LA VICTORIA,GUADALUPE,N.L., MEXICO</td>
                                </tr>
                                <tr>
                                  <td>RFC: HPG130304SK6 / Tel. +52 81 12341000</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <!-- datos del cliente -->
                <h4>Datos del cliente</h4>
                <table class="tablaCliente" width="100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="cliente-th">Cliente</td>
                        <td class="cliente-td"><?php echo $cliente['nombre_cliente']; ?></td>
                    </tr>
                    <tr>
                        <td class="direccion-th">Dirección</td>
                        <td class="direccion-td"><?php echo $domicilio['calle'] . ' ' . $domicilio['no_exterior'] . ' ' . $domicilio['no_interior'] . ', ' . $domicilio['colonia'] . ', ' . $domicilio['localidad'] . ' ' . $domicilio['municipio'] . ', ' . $domicilio['estado'] . ', ' . $domicilio['codigo_postal']; ?></td>
                    </tr>
                    <tr>
                        <td class="telefono-th">Telefono</td>
                        <td class="telefono-td"><?php echo $cliente['telefono_contacto']; ?></td>
                    </tr>
                    <tr>
                        <td class="email-th">Email</td>
                        <td class="email-td"><?php echo $cliente['email_contacto']; ?></td>
                    </tr>
                    <tr>
                        <td class="nit-th">No. Registro Fiscal</td>
                        <td class="nit-td"><?php echo $cliente['rfc']; ?></td>
                    </tr>
                </table>

                <!-- descripción -->
                <div style="margin-bottom: 5px;">
                    <h4>Descripción</h4>
                    <table class="tablaDescripcion" width="100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="cotizacion-descripcion"><?php echo $cotizacion['descripcion']; ?></td>
                        </tr>
                    </table>
                </div>

                <!-- conceptos -->
                <table class="tablaConceptos" width="100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <th class="tipo-th">Tipo</th>
                        <th class="cantidad-th">Cantidad</th>
                        <th class="descripcion-th">Descripción</th>
                        <th class="punitario-th">Precio unitario</th>
                        <th class="importe-th">importe</th>
                    </tr>
                        <?php while ($row = mysql_fetch_assoc($conceptos)) { ?>
                            <tr>
                                <?php 
                                    if (isset($row['id_puerta'])) { # puerta
                                        $sql2 = "SELECT * FROM puerta_tipo WHERE activo = 1 AND id_ptipo = ". $row['id_ptipo'];
                                        $ptipo = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM puerta WHERE activo = 1 AND id_puerta = ". $row['id_puerta'];
                                        $puerta = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM puerta_modelo WHERE activo = 1 AND id_pmodelo = ". $row['id_pmodelo'];
                                        $pmodelo = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM puerta_color WHERE activo = 1 AND id_pcolor = ". $row['id_pcolor'];
                                        $pcolor = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM puerta_movimiento WHERE activo = 1 AND id_pmovimiento = ". $row['id_pmovimiento'];
                                        $pmovimiento = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM puerta_textura WHERE activo = 1 AND id_ptextura = ". $row['id_ptextura'];
                                        $ptextura = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM puerta_sello WHERE activo = 1 AND id_psello = ". $row['id_psello'];
                                        $psello = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM puerta_motor WHERE activo = 1 AND id_pmotor = ". $row['id_pmotor'];
                                        $pmotor = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM ventana WHERE id_ventana = {$row['id_ventana']} AND activo = 1";
                                        $pventana = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM ventana_color WHERE id_vcolor = {$row['id_vcolor']} AND activo = 1";
                                        $pvcolor = mysql_fetch_array(consulta($sql2));
                                        $nventanas = $row['nventanas'];
                                ?>
                                <td class="tipo-td">Puerta</td>
                                <td class="cantidad-td"><?php echo $row['cantidad']; ?></td>
                                <td class="descripcion-td">
                                    <strong>Tipo:</strong> <?php echo $ptipo['nombre_tipo']; ?> |
                                    <strong>Medidas:</strong>

                                    <?php if ($row['m_ancho'] == 0 && $row['m_alto'] == 0 && $row['f_ancho'] == 0 && $row['in_ancho'] == 0 && $row['f_alto'] == 0 && $row['in_alto'] == 0) { ?>
                                      <?php echo $puerta['m_ancho'] . 'm' . 'x' . $puerta['m_alto'] . 'm'; ?>
                                    <?php } ?>

                                    <?php if ($row['m_ancho'] != 0 || $row['m_alto'] != 0) { ?>
                                      <?php echo $row['m_ancho'] . 'm' . 'x' . $row['m_alto'] . 'm'; ?>
                                    <?php } ?>

                                    <?php if ($row['f_ancho'] != 0 || $row['in_ancho'] != 0 || $row['f_alto'] != 0 || $row['in_alto'] != 0) { ?>
                                      <?php echo $row['f_ancho'] . '\'' . $row['in_ancho'] . '"' . 'x' . $row['f_alto'] . '\'' . $row['in_alto'] . "\""; ?>
                                    <?php } ?>

                                    |

                                    <strong>Modelo:</strong> <?php echo $pmodelo['nombre_modelo']; ?> |
                                    <strong>Color:</strong> <?php echo $pcolor['nombre_color']; ?> |
                                    <strong>Movimiento:</strong> <?php echo $pmovimiento['nombre_movimiento']; ?> |
                                    <strong>Textura:</strong> <?php echo $ptextura['nombre_textura']; ?> |
                                    <strong>Sello:</strong> <?php echo $psello['nombre_sello']; ?> |
                                    <strong>Motor:</strong> <?php echo $pmotor['nombre_motor']; ?> |
                                    <strong>Ventana:</strong> <?php echo $pventana['nombre_ventana'] ? $pventana['nombre_ventana'] : 'Sin Ventana'; ?> |
                                    <strong>Color ventana:</strong> <?php echo $pvcolor['nombre_color'] ? $pvcolor['nombre_color'] : 'Sin Color'; ?> |

                                    <?php if ($nventanas > 0): ?>
                                    <strong># ventanas:</strong> <?php echo $nventanas ?>
                                    <?php endif; ?>
                                </td>
                                <td class="punitario-td"><?php echo number_format($row['precio_unitario'], 2, '.', ','); ?></td>
                                <td class="importe-td"><?php echo number_format($row['importe'], 2, '.', ','); ?></td>
                                <?php
                                    } elseif (isset($row['id_componente'])) { # Componente
                                        $sql2 = "SELECT * FROM componentes_grupos WHERE activo=1 AND id_grupo_componente = ". $row['id_grupo_componente'];
                                        $gcomp = mysql_fetch_array(consulta($sql2));
                                        $sql2 = "SELECT * FROM componentes WHERE activo=1 AND id_componente = ". $row['id_componente'];
                                        $ccomp = mysql_fetch_array(consulta($sql2));
                                ?>
                                <td class="tipo-td">Componente</td>
                                <td class="cantidad-td"><?php echo $row['cantidad']; ?></td>
                                <td class="desripcion-td">
                                    <strong>Grupo:</strong> <?php echo $gcomp['grupo_componente']; ?> |
                                    <strong>Componente:</strong> <?php echo htmlentities($ccomp['nombre_componente']); ?>
                                </td>
                                <td class="punitario-td"><?php echo number_format($row['precio_unitario'], 2, '.', ','); ?></td>
                                <td class="importe-td"><?php echo number_format($row['importe'], 2, '.', ','); ?></td>
                                <?php
                                    } elseif (isset($row['id_kit'])) { # Kit
                                    $sql2 = "SELECT * FROM kit WHERE activo = 1 AND id_kit = ". $row['id_kit'];
                                    $kkit = mysql_fetch_array(consulta($sql2));
                                ?>
                                <td class="tipo-td">Kit</td>
                                <td class="cantidad-td"><?php echo $row['cantidad']; ?></td>
                                <td class="descripcion-td">
                                    <strong>Kit:</strong> <?php echo $kkit['nombre_kit']; ?>
                                </td>
                                <td class="punitario-td"><?php echo number_format($row['precio_unitario'], 2, '.', ','); ?></td>
                                <td class="importe-td"><?php echo number_format($row['importe'], 2, '.', ','); ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </table>

                <br>

                <table class="tabla" width="100%;" cellpadding="0" cellspacing="0" align="left" valign="top">
                    <tr>
                        <td width="85%">
                            <h4> Términos de venta.</h4><br>
                            <p><?php echo nl2br($termino_venta['descripcion']); ?></p>
                        </td>
                        <td width="15%" style="background: #f2f2f2; border-top: 2px solid #b2b2b2;">
                            <table class="tabla" width="100%;" cellpadding="0" cellspacing="0" align="right" valign="top">
                                <tr>
                                    <td align="right">
                                        <strong>Subtotal</strong>
                                        <div><?php echo number_format($cotizacion['subtotal'], 2, '.', ','); ?></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <strong>Impuestos</strong>
                                        <div><?php echo number_format($cotizacion['impuestos'], 2, '.', ','); ?></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <strong>Total</strong>
                                        <div><?php echo number_format($cotizacion['total'], 2, '.', ','); ?></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <hr>

                <p>* Precio en pesos mexicanos, cotización valida  por 15 dias, valor neto.<p>
            </div>
        </div>
    </body>
</html>

<?php

    $filename = $cotizacion['numero_cotizacion'] . '.pdf';

    $html = ob_get_clean(); 
 
    require_once('util/MPDF53/mpdf.php');

    $mpdf = new mPDF('utf-8','LETTER', 0, '', 6, 6, 2, 2, 2, 2, 'L'); 

    $mpdf->SetDisplayMode('fullpage');

    $mpdf->mirrorMargins=1;
    $mpdf->ignore_invalid_utf8 = true;
    $mpdf->list_indent_first_level = 0;

    $mpdf->SetHTMLFooter("Página {PAGENO} de {nb}","O");
    $mpdf->SetHTMLFooter("Página {PAGENO} de {nb}","E"); 
    
    $mpdf->WriteHTML($html);
        
    $mpdf->Output($filename, 'I');

?>