-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 02, 2015 at 06:21 PM
-- Server version: 5.5.42
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hefesto`
--

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_usuario` int(11) DEFAULT NULL,
  `tp_cliente` int(11) DEFAULT NULL,
  `tipo_costo` varchar(25) DEFAULT NULL,
  `motivo_costo` text,
  `aceptado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id_usuario`, `tp_cliente`, `tipo_costo`, `motivo_costo`, `aceptado`) VALUES
(22, 1, 'va', NULL, 1),
(24, 2, 'va', NULL, 0),
(31, 1, 'vf', NULL, 1),
(32, 1, 've', NULL, 1),
(33, 2, 'vd', NULL, 2),
(34, 1, 'vc', NULL, 1),
(35, 2, 'vc', NULL, 1),
(36, 1, 'vg', NULL, 1),
(37, 1, 'va', 'Pruebas basica', 1),
(38, 2, 'vd', 'Compras promedio', 1),
(39, 2, 'vi', 'Compro a buen precio', 1),
(40, 1, 'va', 'Cliente final', 1),
(41, 2, 'va', 'Final ', 1),
(42, 2, 'vc', 'Compra ', 1),
(43, 2, 'vf', 'Autorizado', 1),
(45, 2, 'vc', 'Buen precio ', 1),
(46, 1, 'va', 'Cliente final', 1),
(47, 1, 'vd', 'Monterrey ', 1),
(48, 1, 'vc', 'Cliente Final ', 1),
(49, 1, 'va', 'Cliente Final. ', 1),
(50, 2, 'va', 'Cliente Final ', 1),
(51, 2, 'va', 'Cliente Final ', 0),
(52, 2, 'vb', 'Cliente con un flujo de 15 puertas al mes', 1),
(53, 2, 'vg', NULL, 1),
(54, 2, 'vf', 'ikiok', 0),
(55, 2, 'vb', 'Cliente Final ', 1),
(56, 1, 'va', 'Cliente Final ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `componentes`
--

CREATE TABLE IF NOT EXISTS `componentes` (
  `id_componente` int(11) NOT NULL,
  `id_descomp` int(11) DEFAULT NULL,
  `nom_comp` varchar(255) DEFAULT NULL,
  `tasa_cambio` float(18,2) DEFAULT NULL,
  `iva` float(18,2) DEFAULT NULL,
  `vfusa` float(18,2) DEFAULT NULL,
  `va` float(18,2) DEFAULT NULL,
  `vb` float(18,2) DEFAULT NULL,
  `vc` float(18,2) DEFAULT NULL,
  `vd` float(18,2) DEFAULT NULL,
  `ve` float(18,2) DEFAULT NULL,
  `vf` float(18,2) DEFAULT NULL,
  `vg` float(18,2) DEFAULT NULL,
  `vh` float(18,2) DEFAULT NULL,
  `vi` float(18,2) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `componentes`
--

INSERT INTO `componentes` (`id_componente`, `id_descomp`, `nom_comp`, `tasa_cambio`, `iva`, `vfusa`, `va`, `vb`, `vc`, `vd`, `ve`, `vf`, `vg`, `vh`, `vi`, `activo`, `id_log`) VALUES
(1, 1, 'Panel 50', 15.00, 1.16, 2.20, 95.70, 63.93, 53.32, 46.32, 42.11, 38.28, 34.83, 30.24, 26.30, 1, 422),
(2, 2, 'Flecha 2.8', NULL, NULL, NULL, 19.20, 12.00, 10.44, 9.08, 8.25, 7.50, 6.52, 5.67, 4.93, 1, NULL),
(3, 2, '70', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(4, 1, '70', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(5, 1, 'Panel 70', 15.00, 1.16, 2.10, 91.35, 61.02, 50.90, 44.21, 40.19, 36.54, 33.25, 28.87, 25.10, 1, NULL),
(6, 10, 'Motor HS ONE', 15.00, 1.16, 120.00, 5220.00, 3486.96, 2908.58, 2526.48, 2296.80, 2088.00, 1900.08, 1649.52, 1434.46, 1, NULL),
(7, 9, 'Sello Perimetral', 15.00, 1.16, 0.00, 144.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, NULL),
(8, 9, 'Sello Regido', 15.00, 1.16, 0.00, 140.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, NULL),
(9, 9, 'Sello Flexible', 15.00, 1.16, 0.00, 140.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
  `id_usuario` int(11) DEFAULT NULL,
  `nom_contacto` varchar(255) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `ext` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactos`
--

INSERT INTO `contactos` (`id_usuario`, `nom_contacto`, `tel`, `ext`, `email`) VALUES
(24, 'Jose Gutierrez', NULL, NULL, 'jgutierrez@teknik.mx'),
(31, 'Luis ', NULL, NULL, 'guillermoortizloera@gmail.com'),
(32, 'Martha Mesa', '1 772 344 0385', NULL, 'martha@hsportones.com'),
(33, 'julio', '34 45 56 6789', NULL, 'ramirez.ffghjkk'),
(34, 'luis ', '893475934bh', 'oi3wuwoei ', 'luis23hotmail.com'),
(35, 'Flavio Ortiz', '77-23440385', '115', 'flavio@hspuertasdegaraje.com'),
(36, 'Memo Ortiz', '81-12341000', '111', 'memo@hspue.com'),
(37, 'Rogelio Gutierrez', NULL, NULL, 'jgutierrez@teknik.mx'),
(38, 'Luis Garcia', '772-3440385_', NULL, 'luis@garcia.com'),
(39, 'MEMO', NULL, NULL, 'guillermoortizloera@gmail.com'),
(40, 'Martha Mesa', '772-3468987_', NULL, 'martha@live.com'),
(41, 'memo ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(42, 'Panfilo ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(43, 'Petra ', NULL, NULL, 'guillermoortizloera@gmail.com'),
(45, 'Tito ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(46, 'Carolina Perez', '123-45678901', NULL, 'carolina@perez.com'),
(47, 'FIDENCIO ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(48, 'Filiberto Rupertino ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(49, 'Guillermo Ortiz ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(50, 'Rosario Gomez ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(51, 'Pancho Lopez ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(52, 'Humberto', NULL, NULL, 'humberto@prueba.com'),
(53, 'fRANCO ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(54, 'sfsew', NULL, NULL, 'rwewer@kasmdk.com'),
(55, 'Guillermo Ortiz ', NULL, NULL, 'gortiz@hspuertasdegaraje.com'),
(56, 'Herbert Cardenas', NULL, NULL, 'gortiz@hspuertasdegaraje.com');

-- --------------------------------------------------------

--
-- Table structure for table `cotcomp_totales`
--

CREATE TABLE IF NOT EXISTS `cotcomp_totales` (
  `id_cotizacion` int(11) DEFAULT NULL,
  `total_general` float(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cotherr_totales`
--

CREATE TABLE IF NOT EXISTS `cotherr_totales` (
  `id_cotizacion` int(11) DEFAULT NULL,
  `total_general` float(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cotizacion`
--

CREATE TABLE IF NOT EXISTS `cotizacion` (
  `id_cotizacion` int(11) NOT NULL,
  `num_cotizacion` varchar(255) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `autorizacion` int(11) DEFAULT NULL,
  `vta` int(11) DEFAULT NULL,
  `autorizacion_pag` tinyint(1) DEFAULT '0',
  `archivo` varchar(255) DEFAULT NULL,
  `fpago` varchar(255) DEFAULT NULL,
  `tp_cotizacion` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `id_vendedor` int(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cotizacion`
--

INSERT INTO `cotizacion` (`id_cotizacion`, `num_cotizacion`, `id_usuario`, `autorizacion`, `vta`, `autorizacion_pag`, `archivo`, `fpago`, `tp_cotizacion`, `fecha`, `id_vendedor`, `activo`, `id_log`) VALUES
(1, '201510HSA81BR3WIM', 31, 1, 1, 1, '../archivos/1-Teknik.png', 'vf', 1, '2015-10-07', 2, 1, 371),
(2, '201510HSN5LOX7Y2H', 40, 1, 1, 1, '../archivos/2-Teknik.png', 'va', 1, '2015-10-07', 1, 1, 372),
(3, '201510HSBPOG9RVWH', 31, 1, NULL, 0, NULL, 'vf', 1, '2015-10-07', 1, 1, 373),
(4, '201510HSLTVRWYO6Z', 31, 1, NULL, 0, NULL, 'vf', 1, '2015-10-07', 1, 1, 374),
(5, '201510HSEMFRSIVYQ', 48, 1, 1, 1, '../archivos/5-Logo_1.pdf', 'vc', 1, '2015-10-08', 26, 1, 376),
(6, '201510HSL41KV073P', 48, 1, 1, 0, NULL, 'vc', 1, '2015-10-08', 26, 1, 377),
(7, '201510HSTPDI1BMVZ', 48, 1, NULL, 0, NULL, 'vc', 1, '2015-10-07', 28, 1, 378),
(8, '201510HS5PS1NEG3J', 49, 1, 1, 1, '../archivos/8-Logo_1.pdf', 'va', 1, '2015-10-08', 26, 1, 381),
(9, '201510HSBON5MSYKC', 49, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 28, 1, 382),
(10, '201510HSK8N2PXEZR', 49, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 28, 1, 383),
(11, '201510HSDG5R69ICV', 49, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 28, 1, 384),
(12, '201510HSZTSML9HE2', 37, 1, 0, 1, '../archivos/12-Logo Black (528x323).jpg', 'va', 1, '2015-10-07', 1, 1, 385),
(13, '201510HS03L6JV8TE', 32, 1, 0, 1, '../archivos/13-1.jpg', 've', 1, '2015-10-07', 1, 1, 386),
(14, '201510HSY0HQRIBKZ', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 387),
(15, '201510HSE0XZ6LIOB', 37, 1, 0, 1, '../archivos/15-TipsParaClientes.jpeg', 'va', 1, '2015-10-07', 1, 1, 388),
(16, '201510HS3NS51IDHV', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 389),
(17, '201510HS5DV7ZJC8M', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 390),
(18, '201510HSQPISO2YK5', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 391),
(19, '201510HS0L5YPE27O', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 392),
(20, '201510HSVXW54KG6H', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 393),
(21, '201510HSZCGOWDMRV', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 394),
(22, '201510HS6A7VM9C1B', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 395),
(23, '201510HSXOZ1K4DFY', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 396),
(24, '201510HSWHGCO26RD', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 397),
(25, '201510HSRXYP6WLOC', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 398),
(26, '201510HSJL36SAUVG', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 399),
(27, '201510HS9D4QSTE68', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 400),
(28, '201510HSRH8UXGK3D', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 401),
(29, '201510HS6AYOW2I0V', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 402),
(30, '201510HSGP9R07KXA', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 403),
(31, '201510HSUYIQPHET5', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 404),
(32, '201510HSWDJG2NSFH', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 405),
(33, '201510HSE87ZUT3WY', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 406),
(34, '201510HSAPMB71Z2K', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 407),
(35, '201510HS9ZG3R2YJI', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 408),
(36, '201510HSB7CSUGVDL', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 409),
(37, '201510HSB3VOUML80', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 410),
(38, '201510HST2NYE5RBC', 37, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 411),
(39, '201510HSP5Z12IJQD', 47, 1, 1, 1, '../archivos/39-Logo_Vertical.jpeg', 'vd', 1, '2015-10-13', 26, 1, 412),
(40, '201510HSHYU30LO71', 50, 0, NULL, 0, NULL, 'va', 1, '2015-10-07', 27, 1, 414),
(41, '201510HS0UJ568IV7', 52, NULL, NULL, 0, NULL, 'vb', 1, '2015-10-07', 1, 1, 417),
(42, '201510HSWZ8AVLGU1', 52, 1, NULL, 0, NULL, 'vb', 1, '2015-10-07', 1, 1, 423),
(43, '201510HS5IFJT0BCL', 41, NULL, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 424),
(44, '201510HS1GXJOA8HR', 41, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 425),
(45, '201510HSAT08U7NKG', 41, 1, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 426),
(46, '201510HSWZYMJ7S0P', 41, NULL, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 427),
(47, '201510HSVT278USAY', 41, NULL, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 428),
(48, '201510HSVAG4QILD0', 41, NULL, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 429),
(49, '201510HSHT80EXFAU', 41, NULL, NULL, 0, NULL, 'va', 1, '2015-10-07', 1, 1, 430),
(50, '201510HSGK8WCPHB4', 34, 1, NULL, 0, NULL, 'vc', 1, '2015-10-07', 1, 1, 431),
(51, '201510HSNFL2DUI4P', 31, 1, NULL, 0, NULL, 'vf', 1, '2015-10-07', 1, 1, 432),
(52, '201511HS07K5TVUG3', 53, 1, 0, 0, NULL, 'vg', 1, '2015-11-09', 25, 1, 435),
(53, '201511HSX1QY3MLUS', 31, 1, NULL, 0, NULL, 'vf', 1, '2015-11-30', 1, 1, 440),
(54, '201512HSQ26REAV9J', 55, 0, NULL, 0, NULL, 'vb', 1, '2015-12-01', 27, 1, 441);

-- --------------------------------------------------------

--
-- Table structure for table `cotizacion_comp`
--

CREATE TABLE IF NOT EXISTS `cotizacion_comp` (
  `id_cotizacion` int(11) DEFAULT NULL,
  `id_descomp` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cotizacion_herreria`
--

CREATE TABLE IF NOT EXISTS `cotizacion_herreria` (
  `id_cotizacion` int(11) DEFAULT NULL,
  `id_herreria` int(11) DEFAULT NULL,
  `cantidad` float(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cotizacion_puerta`
--

CREATE TABLE IF NOT EXISTS `cotizacion_puerta` (
  `id_cot` int(11) NOT NULL,
  `id_cotizacion` int(11) DEFAULT NULL,
  `tipo_puerta` int(11) DEFAULT NULL,
  `opt_medida` int(11) DEFAULT NULL,
  `cantidad` int(255) DEFAULT NULL,
  `pta_modelo` int(11) DEFAULT NULL,
  `med1` float(18,2) DEFAULT NULL,
  `med2` float(18,2) DEFAULT NULL,
  `md` int(11) DEFAULT NULL,
  `pta_medida` int(11) DEFAULT NULL,
  `pta_textura` int(11) DEFAULT NULL,
  `pta_mov` int(11) DEFAULT NULL,
  `pta_color` int(11) DEFAULT NULL,
  `vta_modelo` int(11) DEFAULT NULL,
  `vta_color` int(11) DEFAULT NULL,
  `id_sello` int(11) DEFAULT NULL,
  `id_motor` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cotizacion_puerta`
--

INSERT INTO `cotizacion_puerta` (`id_cot`, `id_cotizacion`, `tipo_puerta`, `opt_medida`, `cantidad`, `pta_modelo`, `med1`, `med2`, `md`, `pta_medida`, `pta_textura`, `pta_mov`, `pta_color`, `vta_modelo`, `vta_color`, `id_sello`, `id_motor`) VALUES
(1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 3, 2, 0, 6),
(2, 2, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 19, 2, 7, 6),
(3, 3, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 1, 2, 7, 6),
(4, 4, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 0, 0, 0, 0),
(5, 5, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 0, 0, 0, 0),
(6, 6, 1, 1, 1, 9, NULL, NULL, NULL, 9, 1, 1, 1, 0, 0, 0, 0),
(7, 7, 1, 1, 2, 8, NULL, NULL, NULL, 8, 2, 1, 1, 4, 1, 7, 6),
(8, 8, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 7, 6),
(9, 9, 1, 1, 1, 1, NULL, NULL, NULL, 1, 2, 1, 1, 3, 1, 7, 6),
(10, 10, 1, 1, 1, 1, NULL, NULL, NULL, 1, 2, 1, 1, 3, 1, 7, 6),
(11, 11, 1, 1, 1, 20, NULL, NULL, NULL, 20, 2, 1, 1, 0, 0, 7, 6),
(12, 12, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 0, 0, 0, 0),
(13, 12, 1, 1, 1, 3, NULL, NULL, NULL, 3, 2, 3, 2, 1, 1, 7, 6),
(14, 12, 1, 1, 1, 1, NULL, NULL, NULL, 1, 2, 6, 4, 10, 3, 7, 6),
(15, 13, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 3, 1, 0, 0, 0, 6),
(16, 13, 1, 1, 2, 3, NULL, NULL, NULL, 3, 2, 3, 2, 1, 1, 7, 6),
(17, 14, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 7, 0),
(18, 14, 1, 1, 2, 9, NULL, NULL, NULL, 9, 2, 3, 4, 7, 2, 8, 6),
(19, 15, 1, 1, 4, 1, NULL, NULL, NULL, 1, 1, 3, 2, 3, 2, 7, 6),
(20, 16, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(21, 17, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(22, 18, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(23, 19, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(24, 20, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(25, 21, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(26, 22, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(27, 23, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(28, 24, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(29, 25, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(30, 26, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(31, 27, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(32, 28, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(33, 29, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(34, 30, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(35, 31, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(36, 32, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(37, 33, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(38, 34, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(39, 35, 1, 1, 2, 3, NULL, NULL, NULL, 3, 1, 3, 2, 1, 1, 8, 6),
(40, 36, 1, 1, 1, 3, NULL, NULL, NULL, 3, 2, 3, 2, 1, 2, 7, 6),
(41, 37, 1, 1, 1, 3, NULL, NULL, NULL, 3, 2, 3, 2, 1, 2, 7, 6),
(42, 38, 1, 1, 1, 4, NULL, NULL, NULL, 4, 1, 3, 2, 1, 1, 7, 6),
(43, 38, 1, 1, 1, 13, NULL, NULL, NULL, 13, 2, 3, 2, 9, 2, 8, 6),
(44, 39, 1, 1, 1, 20, NULL, NULL, NULL, 20, 2, 1, 1, 0, 0, 0, 0),
(45, 39, 1, 1, 1, 1, NULL, NULL, NULL, 1, 2, 1, 1, 0, 0, 0, 0),
(46, 40, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 0, 0, 0, 0),
(47, 40, 1, 1, 1, 20, NULL, NULL, NULL, 20, 1, 1, 1, 0, 0, 0, 0),
(48, 41, 1, 1, 10, 5, NULL, NULL, NULL, 5, 1, 1, 1, 8, 1, 0, 0),
(49, 42, 1, 1, 12, 20, NULL, NULL, NULL, 20, 1, 6, 1, 13, 1, 7, 0),
(50, 43, 1, 1, 1, 1, NULL, NULL, NULL, 1, 2, 1, 1, 5, 1, 7, 6),
(51, 43, 1, 1, 1, 16, NULL, NULL, NULL, 16, 1, 3, 2, 1, 2, 7, 6),
(52, 43, 1, 1, 1, 12, NULL, NULL, NULL, 12, 1, 1, 1, 16, 1, 7, 6),
(53, 44, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 12, 1, 7, 6),
(54, 45, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 12, 1, 7, 6),
(55, 46, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 12, 1, 7, 6),
(56, 47, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 12, 1, 7, 6),
(57, 48, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 12, 1, 7, 6),
(58, 49, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0, 7, 6),
(59, 50, 1, 1, 9, 14, NULL, NULL, NULL, 14, 1, 3, 1, 17, 1, 7, 6),
(60, 51, 1, 1, 2, 17, NULL, NULL, NULL, 17, 2, 8, 1, 16, 1, 0, 0),
(61, 52, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 0, 0, 0, 0),
(62, 53, 2, 1, 8, 26, NULL, NULL, NULL, 26, 1, 10, 2, 0, 0, 7, 6),
(63, 54, 1, 1, 1, 1, NULL, NULL, NULL, 1, 2, 1, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cotpta_totales`
--

CREATE TABLE IF NOT EXISTS `cotpta_totales` (
  `id_cot` int(11) NOT NULL,
  `id_cotizacion` int(11) DEFAULT NULL,
  `total_ptas` float(38,2) DEFAULT NULL,
  `total_vtas` float(18,2) DEFAULT NULL,
  `total_sellos` float(18,2) DEFAULT NULL,
  `total_motores` float(18,2) DEFAULT NULL,
  `total_general` float(18,2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cotpta_totales`
--

INSERT INTO `cotpta_totales` (`id_cot`, `id_cotizacion`, `total_ptas`, `total_vtas`, `total_sellos`, `total_motores`, `total_general`) VALUES
(1, 1, 6310.00, 3480.00, 0.00, 2088.00, 11878.00),
(2, 2, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(3, 3, 6310.00, 3480.00, 0.00, 2088.00, 11878.00),
(4, 4, 6310.00, 0.00, 0.00, 0.00, 6310.00),
(5, 5, 8790.00, 0.00, 0.00, 0.00, 8790.00),
(6, 6, 13573.00, 0.00, 0.00, 0.00, 13573.00),
(7, 7, 23752.00, NULL, 0.00, 2908.00, 26660.00),
(8, 8, 15776.00, NULL, 1152.00, 5220.00, 22148.00),
(9, 9, 15776.00, NULL, 1152.00, 5220.00, 22148.00),
(10, 10, 15776.00, NULL, 1152.00, 5220.00, 22148.00),
(11, 11, 34800.00, 0.00, 3456.00, 5220.00, 43476.00),
(12, 12, 15776.00, 0.00, 0.00, 0.00, 15776.00),
(13, 12, 16530.00, 1.90, 1152.00, 5220.00, 22903.90),
(14, 12, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(15, 13, 6941.00, 0.00, 0.00, 2296.00, 9237.00),
(16, 13, 14546.00, 12.00, 0.00, 2296.00, 16854.00),
(17, 14, 15776.00, NULL, 1152.00, 0.00, 16928.00),
(18, 14, 48720.00, 0.00, 840.00, 5220.00, 54780.00),
(19, 15, 63104.00, 34800.00, 4608.00, 5220.00, 107732.00),
(20, 16, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(21, 17, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(22, 18, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(23, 19, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(24, 20, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(25, 21, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(26, 22, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(27, 23, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(28, 24, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(29, 25, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(30, 26, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(31, 27, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(32, 28, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(33, 29, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(34, 30, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(35, 31, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(36, 32, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(37, 33, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(38, 34, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(39, 35, 33060.00, 23.00, 560.00, 5220.00, 38863.00),
(40, 36, 16530.00, 12.00, 1152.00, 5220.00, 22914.00),
(41, 37, 16530.00, 12.00, 1152.00, 5220.00, 22914.00),
(42, 38, 18705.00, 1231.00, 1152.00, 5220.00, 26308.00),
(43, 38, 31320.00, 12345678.00, 560.00, 5220.00, 12382778.00),
(44, 39, 16843.00, 0.00, 0.00, 0.00, 16843.00),
(45, 39, 7635.00, 0.00, 0.00, 0.00, 7635.00),
(46, 40, 15776.00, 0.00, 0.00, 0.00, 15776.00),
(47, 40, 34800.00, 0.00, 0.00, 0.00, 34800.00),
(48, 41, 122040.00, NULL, 0.00, 0.00, 122040.00),
(49, 42, 278952.00, NULL, 0.00, 0.00, 278952.00),
(50, 43, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(51, 43, 37410.00, 0.00, 2304.00, 5220.00, 44934.00),
(52, 43, 27405.00, 0.00, 2304.00, 5220.00, 34929.00),
(53, 44, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(54, 45, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(55, 46, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(56, 47, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(57, 48, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(58, 49, 15776.00, 8700.00, 1152.00, 5220.00, 30848.00),
(59, 50, 146151.00, 0.00, 0.00, 2908.00, 149059.00),
(60, 51, 24360.00, NULL, 0.00, 0.00, 24360.00),
(61, 52, 5742.00, 0.00, 0.00, 0.00, 5742.00),
(62, 53, 81568.00, 0.00, 0.00, 2088.00, 83656.00),
(63, 54, 10538.00, 0.00, 0.00, 0.00, 10538.00);

-- --------------------------------------------------------

--
-- Table structure for table `des_componentes`
--

CREATE TABLE IF NOT EXISTS `des_componentes` (
  `id_descomp` int(11) NOT NULL,
  `componente` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `des_componentes`
--

INSERT INTO `des_componentes` (`id_descomp`, `componente`, `activo`, `id_log`) VALUES
(1, 'Paneles', 1, NULL),
(2, 'Flechas', 1, NULL),
(3, 'Guias', 1, NULL),
(4, 'Herrajes', 1, NULL),
(5, 'Tapas', 1, NULL),
(6, 'Plasticos', 1, NULL),
(7, 'Tornilleria', 1, NULL),
(8, 'Resortes', 1, NULL),
(9, 'Sellos', 1, NULL),
(10, 'Motores', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `divisas`
--

CREATE TABLE IF NOT EXISTS `divisas` (
  `id_divisas` int(11) NOT NULL,
  `abbrev` varchar(50) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `cambio` double DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `id_log` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `divisas`
--

INSERT INTO `divisas` (`id_divisas`, `abbrev`, `descripcion`, `cambio`, `activo`, `id_log`) VALUES
(1, 'MXN', 'PESOS', 1, 1, 332),
(2, 'USD', 'DOLARES', 15.31, 1, 349),
(3, 'EUR', 'EUROS', 16.34, 1, 0),
(4, 'JPY', 'Yen japonés', 0.14, 0, 389),
(5, 'RP', 'Rupia', 0.2, 0, 148),
(6, 'CAD', 'Dolar Canadiense', 0.093, 1, 363),
(7, 'ARS', 'Peso Argentino', 11.21, 0, 396),
(8, 'pc', 'paola', 1, 1, 420);

-- --------------------------------------------------------

--
-- Table structure for table `domicilio`
--

CREATE TABLE IF NOT EXISTS `domicilio` (
  `id_domicilio` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `calle` varchar(80) DEFAULT NULL,
  `no_exterior` varchar(20) DEFAULT NULL,
  `no_interior` varchar(20) DEFAULT NULL,
  `colonia` varchar(80) DEFAULT NULL,
  `referencia` varchar(160) DEFAULT NULL,
  `municipio` varchar(45) DEFAULT NULL,
  `localidad` varchar(75) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `pais` varchar(35) DEFAULT NULL,
  `cp` int(35) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `domicilio`
--

INSERT INTO `domicilio` (`id_domicilio`, `id_usuario`, `calle`, `no_exterior`, `no_interior`, `colonia`, `referencia`, `municipio`, `localidad`, `estado`, `pais`, `cp`) VALUES
(1, 1, 'juan igancio ramon', '112', NULL, 'centro', 'matamoros y zaragoza', 'monterrey', NULL, 'nuevo leon', 'México', 64000),
(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 17, 'Camino Al Milagro', '558', '1', 'parque industrial milimex', NULL, 'APODACA', 'Apodaca', 'NUEVO LEON', 'Mexico ', NULL),
(9, 18, 'Milagro ', '456', '45', 'Huerto ', NULL, NULL, 'Apodaca', 'NUEVO LEON', 'Mexico ', 34567),
(10, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mexico', NULL),
(14, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mexico', NULL),
(15, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 31, 'Milagro ', '2 ', NULL, 'Milemax ', NULL, 'Apodaca', 'Apodaca ', 'Nuevo Leon ', 'Mexico ', 87490),
(17, 32, 'Bayshore Boulevard', '123', NULL, 'Lake Charles', NULL, 'Saint Lucie West', 'Port Saint Lucie', 'FL', 'United States', 34986),
(18, 33, '145', '31-12', NULL, 'Paramo', NULL, 'grosso', 'monterrey', 'Nuevo Leon', 'Camerun', 0),
(19, 34, 'L ', '4', '3', 'JHF', 'KDSKJF', 'KJYNFK', 'GPE ', 'NUEVO LEON', 'mexico ', 98435),
(20, 35, 'nn', '106', '106', 'nn', 'bb', 'nl', 'conutrs', 'NL', 'Mexico', 66884),
(21, 36, 'qq', '102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 40, NULL, NULL, NULL, NULL, NULL, NULL, 'Port Saint Lucie', 'FL', 'United States', 34986),
(26, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `herreria`
--

CREATE TABLE IF NOT EXISTS `herreria` (
  `id_herreria` int(11) NOT NULL,
  `nom_herreria` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `proveedor` varchar(255) DEFAULT NULL,
  `costo` float(18,2) DEFAULT NULL,
  `tasa_cambio` float(18,2) DEFAULT NULL,
  `iva` float(18,2) DEFAULT NULL,
  `vfusa` float(18,2) DEFAULT NULL,
  `va` float(18,2) DEFAULT NULL,
  `vb` float(18,2) DEFAULT NULL,
  `vc` float(18,2) DEFAULT NULL,
  `vd` float(18,2) DEFAULT NULL,
  `ve` float(18,2) DEFAULT NULL,
  `vf` float(18,2) DEFAULT NULL,
  `vg` float(18,2) DEFAULT NULL,
  `vh` float(18,2) DEFAULT NULL,
  `vi` float(18,2) DEFAULT NULL,
  `id_log` int(11) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pais`
--

CREATE TABLE IF NOT EXISTS `pais` (
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pais`
--

INSERT INTO `pais` (`id_pais`, `nombre`) VALUES
(1, 'Afganistán'),
(2, 'Albania'),
(3, 'Alemania'),
(4, 'Andorra'),
(5, 'Angola'),
(6, 'Antigua y Barbuda'),
(7, 'Arabia Saudita'),
(8, 'Argelia'),
(9, 'Argentina'),
(10, 'Armenia'),
(11, 'Australia'),
(12, 'Austria'),
(13, 'Azerbaiyán'),
(14, 'Bahamas'),
(15, 'Bangladesh'),
(16, 'Barbados'),
(17, 'Baréin'),
(18, 'Bélgica'),
(19, 'Belice'),
(20, 'Benín'),
(21, 'Bielorrusia'),
(22, 'Birmania'),
(23, 'Bolivia'),
(24, 'Bosnia-Herzegovina'),
(25, 'Botsuana'),
(26, 'Brasil'),
(27, 'Brunéi'),
(28, 'Bulgaria'),
(29, 'Burkina Faso'),
(30, 'Burundi'),
(31, 'Bután'),
(32, 'Cabo Verde'),
(33, 'Camboya'),
(34, 'Camerún'),
(35, 'Canadá'),
(36, 'Chad'),
(37, 'Chile'),
(38, 'China'),
(39, 'Chipre'),
(40, 'Ciudad del Vaticano'),
(41, 'Colombia'),
(42, 'Comoras'),
(43, 'Corea del Norte'),
(44, 'Corea del Sur'),
(45, 'Costa de Marfil'),
(46, 'Costa Rica'),
(47, 'Croacia'),
(48, 'Cuba'),
(49, 'Dinamarca'),
(50, 'Dominica'),
(51, 'Ecuador'),
(52, 'Egipto'),
(53, 'El Salvador'),
(54, 'Emiratos Árabes Unidos'),
(55, 'Eritrea'),
(56, 'Eslovaquia'),
(57, 'Eslovenia'),
(58, 'España'),
(59, 'Estados Unidos'),
(60, 'Estonia'),
(61, 'Etiopia'),
(62, 'Filipinas'),
(63, 'Finlandia'),
(64, 'Fiyi'),
(65, 'Francia'),
(66, 'Gabón'),
(67, 'Gambia'),
(68, 'Georgia'),
(69, 'Ghana'),
(70, 'Granada'),
(71, 'Grecia'),
(72, 'Guatemala'),
(73, 'Guyana'),
(74, 'Guinea'),
(75, 'Guinea ecuatorial'),
(76, 'Guinea-Bisáu'),
(77, 'Haití'),
(78, 'Holanda'),
(79, 'Honduras'),
(80, 'Hungría'),
(81, 'India'),
(82, 'Indonesia'),
(83, 'Irak'),
(84, 'Irán'),
(85, 'Irlanda'),
(86, 'Islandia'),
(87, 'Islas Marshall'),
(88, 'Islas Salomón'),
(89, 'Israel'),
(90, 'Italia'),
(91, 'Jamaica'),
(92, 'Japón'),
(93, 'Jordania'),
(94, 'Kazajistán'),
(95, 'Kenia'),
(96, 'Kirguistán'),
(97, 'Kiribati'),
(98, 'Kuwait'),
(99, 'Laos'),
(100, 'Lesoto'),
(101, 'Letonia'),
(102, 'Libano'),
(103, 'Liberia'),
(104, 'Libia'),
(105, 'Liechtenstein'),
(106, 'Lituania'),
(107, 'Luxemburgo'),
(108, 'Madagascar'),
(109, 'Malasia'),
(110, 'Malaui'),
(111, 'Maldivas'),
(112, 'Malí'),
(113, 'Malta'),
(114, 'Marruecos'),
(115, 'Mauricio'),
(116, 'Mauritania'),
(117, 'México'),
(118, 'Micronesia'),
(119, 'Moldavia'),
(120, 'Mónaco'),
(121, 'Mongolia'),
(122, 'Montenegro'),
(123, 'Mozambique'),
(124, 'Namibia'),
(125, 'Nauru'),
(126, 'Nepal'),
(127, 'Nicaragua'),
(128, 'Níger'),
(129, 'Nigeria'),
(130, 'Noruega'),
(131, 'Nueva Zelanda'),
(132, 'Omán'),
(133, 'Pakistán'),
(134, 'Palaos'),
(135, 'Panamá'),
(136, 'Papua Nueva Guinea'),
(137, 'Paraguay'),
(138, 'Perú'),
(139, 'Polonia'),
(140, 'Portugal'),
(141, 'Qatar'),
(142, 'Reino Unido'),
(143, 'República Centroafricana'),
(144, 'República Checa'),
(145, 'República de Macedonia'),
(146, 'República del Congo'),
(147, 'República Democratica del Congo'),
(148, 'República Dominicana'),
(149, 'República Sudafricana'),
(150, 'Ruanda'),
(151, 'Rumania'),
(152, 'Rusia'),
(153, 'Samoa'),
(154, 'San Cristóbal y Nieves'),
(155, 'San Marino'),
(156, 'San Vicente y las Granadinas'),
(157, 'Santa Lucía'),
(158, 'Santo Tomé y Príncipe'),
(159, 'Senegal'),
(160, 'Serbia'),
(161, 'Seychelles'),
(162, 'Sierra Leona'),
(163, 'Singapur'),
(164, 'Siria'),
(165, 'Somalia'),
(166, 'Sri Lanka'),
(167, 'Suazilandia'),
(168, 'Sudán'),
(169, 'Sudán del Sur'),
(170, 'Suecia'),
(171, 'Suiza'),
(172, 'Surinam'),
(173, 'Tailandia'),
(174, 'Tanzania'),
(175, 'Tayikistán'),
(176, 'Timor Oriental'),
(177, 'Togo'),
(178, 'Tonga'),
(179, 'Trinidad y Tobago'),
(180, 'Túnez'),
(181, 'Turkmenistán'),
(182, 'Turquía'),
(183, 'Tuvalu'),
(184, 'Ucrania'),
(185, 'Uganda'),
(186, 'Uruguay'),
(187, 'Uzbekistán'),
(188, 'Vanuatu'),
(189, 'Venezuela'),
(190, 'Vietnam'),
(191, 'Yemen'),
(192, 'Yibuti'),
(193, 'Zambia'),
(194, 'Zimbabue');

-- --------------------------------------------------------

--
-- Table structure for table `puerta_color`
--

CREATE TABLE IF NOT EXISTS `puerta_color` (
  `id_color` int(11) NOT NULL,
  `nom_color` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `descripcion` text,
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_color`
--

INSERT INTO `puerta_color` (`id_color`, `nom_color`, `activo`, `descripcion`, `id_log`) VALUES
(1, 'Blanco', 1, 'Color<br><ul><li>&nbsp;Blanco</li></ul>', 22),
(2, 'Almendra', 1, 'Color<br><ul><li>Almendra</li></ul>', NULL),
(3, 'Cafe', 0, 'Color<br><ul><li>Cafe</li></ul>', NULL),
(4, 'Café', 1, 'Color<br><ul><li>Café</li></ul>', NULL),
(5, 'azul', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_componentes`
--

CREATE TABLE IF NOT EXISTS `puerta_componentes` (
  `id_puerta_comp` int(11) NOT NULL,
  `id_medida` int(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_componentes`
--

INSERT INTO `puerta_componentes` (`id_puerta_comp`, `id_medida`, `activo`, `id_log`) VALUES
(1, 1, 0, NULL),
(2, 1, 0, 0),
(3, 1, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_des_comp`
--

CREATE TABLE IF NOT EXISTS `puerta_des_comp` (
  `id_puerta_comp` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad` float(18,2) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_des_comp`
--

INSERT INTO `puerta_des_comp` (`id_puerta_comp`, `id_componente`, `cantidad`, `activo`) VALUES
(1, 1, 2.00, 0),
(1, 2, 1.00, 1),
(2, 1, 2.00, 0),
(NULL, 1, 2.00, 0),
(2, 1, 2.00, 0),
(3, 1, 2.00, 0),
(3, 2, 1.00, 1),
(3, 4, 2.00, 1),
(3, 1, 34.00, 0),
(3, 1, 123.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_medida`
--

CREATE TABLE IF NOT EXISTS `puerta_medida` (
  `id_medida` int(11) NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `metros_ancho` float(18,2) DEFAULT NULL,
  `metros_alto` float(18,2) DEFAULT NULL,
  `pies_ancho` float(18,2) DEFAULT NULL,
  `pies_alto` float(18,2) DEFAULT NULL,
  `tasa_cambio` float(18,2) DEFAULT NULL,
  `iva` float(18,2) DEFAULT NULL,
  `vfusa` float(18,2) DEFAULT NULL,
  `va` float(18,2) DEFAULT NULL,
  `vb` float(18,2) DEFAULT NULL,
  `vc` float(18,2) DEFAULT NULL,
  `vd` float(18,2) DEFAULT NULL,
  `ve` float(18,2) DEFAULT NULL,
  `vf` float(18,2) NOT NULL,
  `vg` float(18,2) DEFAULT NULL,
  `vh` float(18,2) DEFAULT NULL,
  `vi` float(18,2) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_medida`
--

INSERT INTO `puerta_medida` (`id_medida`, `id_tipo`, `metros_ancho`, `metros_alto`, `pies_ancho`, `pies_alto`, `tasa_cambio`, `iva`, `vfusa`, `va`, `vb`, `vc`, `vd`, `ve`, `vf`, `vg`, `vh`, `vi`, `activo`, `id_log`) VALUES
(1, 1, 2.44, 2.10, 8.00, 7.00, 16.00, 1.16, 340.00, 15776.00, 10538.37, 8790.39, 7635.58, 6941.44, 6310.40, 5742.46, 4985.22, 4335.24, 1, 317),
(3, 1, 2.44, 2.44, 8.00, 8.00, 15.00, 1.16, 380.00, 16530.00, 11042.04, 9210.52, 8000.52, 7273.20, 6612.00, 6016.92, 5223.48, 4542.44, 1, NULL),
(4, 1, 2.44, 2.70, 8.00, 7.00, 15.00, 1.16, 430.00, 18705.00, 12494.94, 10422.43, 9053.22, 8230.20, 7482.00, 6808.62, 5910.78, 5140.13, 1, NULL),
(5, 1, 3.05, 2.10, 10.00, 7.00, 15.00, 1.16, 420.00, 18270.00, 12204.36, 10180.04, 8842.68, 8038.80, 7308.00, 6650.28, 5773.32, 5020.60, 1, NULL),
(6, 1, 3.05, 2.40, 10.00, 8.00, 15.00, 1.16, 480.00, 20880.00, 13947.84, 11634.34, 10105.92, 9187.20, 8352.00, 7600.32, 6598.08, 5737.82, 1, NULL),
(7, 1, 3.05, 2.70, 10.00, 9.00, 15.00, 1.16, 520.00, 22620.00, 15110.16, 12603.86, 10948.08, 9952.80, 9048.00, 8233.68, 7147.92, 6215.98, 1, NULL),
(8, 1, 3.67, 2.10, 12.00, 7.00, 15.00, 1.16, 490.00, 21315.00, 14238.42, 11876.72, 10316.46, 9378.60, 8526.00, 7758.66, 6735.54, 5857.36, 1, NULL),
(9, 1, 3.67, 2.40, 12.00, 8.00, 15.00, 1.16, 560.00, 24360.00, 16272.48, 13573.39, 11790.24, 10718.40, 9744.00, 8867.04, 7697.76, 6694.13, 1, NULL),
(10, 1, 3.67, 2.70, 12.00, 9.00, 15.00, 1.16, 640.00, 27840.00, 18597.12, 15512.45, 13474.56, 12249.60, 11136.00, 10133.76, 8797.44, 7650.43, 1, NULL),
(11, 1, 4.27, 2.10, 14.00, 7.00, 15.00, 1.16, 580.00, 25230.00, 16853.64, 14058.16, 12211.32, 11101.20, 10092.00, 9183.72, 7972.68, 6933.20, 1, NULL),
(12, 1, 4.27, 2.40, 14.00, 8.00, 15.00, 1.16, 630.00, 27405.00, 18306.54, 15270.07, 13264.02, 12058.20, 10962.00, 9975.42, 8659.98, 7530.89, 1, NULL),
(13, 1, 4.27, 2.70, 14.00, 9.00, 15.00, 1.16, 720.00, 31320.00, 20921.76, 17451.50, 15158.88, 13780.80, 12528.00, 11400.48, 9897.12, 8606.74, 1, NULL),
(14, 1, 4.88, 2.10, 16.00, 7.00, 15.00, 1.16, 670.00, 29145.00, 19468.86, 16239.59, 14106.18, 12823.80, 11658.00, 10608.78, 9209.82, 8009.05, 1, NULL),
(15, 1, 4.88, 2.44, 16.00, 8.00, 15.00, 1.16, 770.00, 33495.00, 22374.66, 18663.41, 16211.58, 14737.80, 13398.00, 12192.18, 10584.42, 9204.43, 1, NULL),
(16, 1, 4.88, 2.70, 16.00, 9.00, 15.00, 1.16, 860.00, 37410.00, 24989.88, 20844.85, 18106.44, 16460.40, 14964.00, 13617.24, 11821.56, 10280.27, 1, NULL),
(17, 1, 5.49, 2.10, 18.00, 7.00, 15.00, 1.16, 700.00, 30450.00, 20340.60, 16966.74, 14737.80, 13398.00, 12180.00, 11083.80, 9622.20, 8367.66, 1, NULL),
(18, 1, 5.49, 2.40, 18.00, 8.00, 15.00, 1.16, 845.00, 36757.50, 24554.01, 20481.28, 17790.63, 16173.30, 14703.00, 13379.73, 11615.37, 10100.96, 1, NULL),
(19, 1, 5.49, 2.70, 18.00, 9.00, 15.00, 1.16, 1000.00, 43500.00, 29058.00, 24238.20, 21054.00, 19140.00, 17400.00, 15834.00, 13746.00, 11953.80, 1, NULL),
(20, 1, 6.10, 2.10, 20.00, 7.00, 15.00, 1.16, 800.00, 34800.00, 23246.40, 19390.56, 16843.20, 15312.00, 13920.00, 12667.20, 10996.80, 9563.04, 1, NULL),
(21, 1, 6.10, 2.40, 20.00, 8.00, 15.00, 1.16, 930.00, 40455.00, 27023.94, 22541.53, 19580.22, 17800.20, 16182.00, 14725.62, 12783.78, 11117.03, 1, NULL),
(22, 1, 6.10, 2.70, 20.00, 9.00, 15.00, 1.16, 1100.00, 47850.00, 31963.80, 26662.02, 23159.40, 21054.00, 19140.00, 17417.40, 15120.60, 13149.18, 1, NULL),
(23, 2, 2.44, 2.44, 8.00, 8.00, 15.00, 1.16, 403.00, 17530.50, 11710.37, 9767.99, 8484.76, 7713.42, 7012.20, 6381.10, 5539.64, 4817.38, 1, NULL),
(24, 2, 2.44, 2.74, 8.00, 9.00, 15.00, 1.16, 486.00, 21141.00, 14122.19, 11779.77, 10232.24, 9302.04, 8456.40, 7695.32, 6680.56, 5809.55, 1, NULL),
(25, 2, 2.44, 3.05, 8.00, 10.00, 15.00, 1.16, 513.00, 22315.50, 14906.75, 12434.20, 10800.70, 9818.82, 8926.20, 8122.84, 7051.70, 6132.30, 1, NULL),
(26, 2, 2.44, 3.35, 8.00, 11.00, 15.00, 1.16, 586.00, 25491.00, 17027.99, 14203.59, 12337.64, 11216.04, 10196.40, 9278.72, 8055.16, 7004.93, 1, NULL),
(27, 2, 2.44, 3.66, 8.00, 12.00, 15.00, 1.16, 659.00, 28666.50, 19149.22, 15972.97, 13874.59, 12613.26, 11466.60, 10434.61, 9058.61, 7877.55, 1, NULL),
(28, 2, 2.44, 3.96, 8.00, 13.00, 15.00, 1.16, 724.00, 31494.00, 21037.99, 17548.46, 15243.10, 13857.36, 12597.60, 11463.82, 9952.10, 8654.55, 1, NULL),
(29, 2, 2.44, 4.27, 8.00, 14.00, 15.00, 1.16, 789.00, 34321.50, 22926.76, 19123.94, 16611.61, 15101.46, 13728.60, 12493.03, 10845.59, 9431.55, 1, NULL),
(30, 2, 2.44, 4.57, 8.00, 15.00, 15.00, 1.16, 869.00, 37801.50, 25251.40, 21063.00, 18295.93, 16632.66, 15120.60, 13759.75, 11945.27, 10387.85, 1, NULL),
(31, 2, 2.44, 4.88, 8.00, 16.00, 15.00, 1.16, 949.00, 41281.50, 27576.04, 23002.05, 19980.25, 18163.86, 16512.60, 15026.47, 13044.95, 11344.16, 1, NULL),
(32, 2, 2.44, 5.18, 8.00, 17.00, 15.00, 1.16, 1039.00, 45196.50, 30191.26, 25183.49, 21875.11, 19886.46, 18078.60, 16451.53, 14282.09, 12420.00, 1, NULL),
(33, 2, 2.44, 5.49, 8.00, 18.00, 15.00, 1.16, 1128.00, 49068.00, 32777.42, 27340.69, 23748.91, 21589.92, 19627.20, 17860.75, 15505.49, 13483.89, 1, NULL),
(34, 2, 2.44, 6.10, 8.00, 20.00, 15.00, 1.16, 1330.00, 57855.00, 38647.14, 32236.81, 28001.82, 25456.20, 23142.00, 21059.22, 18282.18, 15898.55, 1, NULL),
(35, 2, 2.74, 2.44, 9.00, 8.00, 15.00, 1.16, 448.00, 19488.00, 13017.98, 10858.71, 9432.19, 8574.72, 7795.20, 7093.63, 6158.21, 5355.30, 1, NULL),
(36, 2, 2.74, 2.74, 9.00, 9.00, 15.00, 1.16, 529.00, 23011.50, 15371.68, 12822.01, 11137.57, 10125.06, 9204.60, 8376.19, 7271.63, 6323.56, 1, NULL),
(37, 2, 2.74, 3.05, 9.00, 10.00, 15.00, 1.16, 559.00, 24316.50, 16243.42, 13549.15, 11769.19, 10699.26, 9726.60, 8851.21, 7684.01, 6682.17, 1, NULL),
(38, 2, 2.74, 3.35, 9.00, 11.00, 15.00, 1.16, 638.00, 27753.00, 18539.00, 15463.97, 13432.45, 12211.32, 11101.20, 10102.09, 8769.95, 7626.52, 1, NULL),
(39, 2, 2.74, 3.66, 9.00, 12.00, 15.00, 1.16, 717.00, 31189.50, 20834.59, 17378.79, 15095.72, 13723.38, 12475.80, 11352.98, 9855.88, 8570.87, 1, NULL),
(40, 2, 2.74, 3.96, 9.00, 13.00, 15.00, 1.16, 797.00, 34669.50, 23159.23, 19317.85, 16780.04, 15254.58, 13867.80, 12619.70, 10955.56, 9527.18, 1, NULL),
(41, 2, 2.74, 4.27, 9.00, 14.00, 15.00, 1.16, 876.00, 38106.00, 25454.81, 21232.66, 18443.30, 16766.64, 15242.40, 13870.58, 12041.50, 10471.53, 1, NULL),
(42, 2, 2.74, 4.57, 9.00, 15.00, 15.00, 1.16, 951.00, 41368.50, 27634.16, 23050.53, 20022.35, 18202.14, 16547.40, 15058.13, 13072.45, 11368.06, 1, NULL),
(43, 2, 2.74, 4.88, 9.00, 16.00, 15.00, 1.16, 1024.00, 44544.00, 29755.39, 24819.92, 21559.30, 19599.36, 17817.60, 16214.02, 14075.90, 12240.69, 1, NULL),
(44, 2, 2.74, 5.18, 9.00, 17.00, 15.00, 1.16, 1109.00, 48241.50, 32225.32, 26880.16, 23348.89, 21226.26, 19296.60, 17559.91, 15244.31, 13256.76, 1, NULL),
(45, 2, 2.74, 5.49, 9.00, 18.00, 15.00, 1.16, 1194.00, 51939.00, 34695.25, 28940.41, 25138.48, 22853.16, 20775.60, 18905.80, 16412.72, 14272.84, 1, NULL),
(46, 2, 2.44, 6.10, 9.00, 20.00, 15.00, 1.16, 1340.00, 58290.00, 38937.72, 32479.19, 28212.36, 25647.60, 23316.00, 21217.56, 18419.64, 16018.09, 1, NULL),
(47, 2, 3.05, 2.44, 10.00, 8.00, 15.00, 1.16, 482.00, 20967.00, 14005.96, 11682.81, 10148.03, 9225.48, 8386.80, 7631.99, 6625.57, 5761.73, 1, NULL),
(48, 2, 3.05, 2.74, 10.00, 9.00, 15.00, 1.16, 577.00, 25099.50, 16766.47, 13985.44, 12148.16, 11043.78, 10039.80, 9136.22, 7931.44, 6897.34, 1, NULL),
(49, 2, 3.05, 3.05, 10.00, 10.00, 15.00, 1.16, 606.00, 26361.00, 17609.15, 14688.35, 12758.72, 11598.84, 10544.40, 9595.40, 8330.08, 7244.00, 1, NULL),
(50, 2, 3.05, 3.05, 10.00, 10.00, 15.00, 1.16, 606.00, 26361.00, 17609.15, 14688.35, 12758.72, 11598.84, 10544.40, 9595.40, 8330.08, 7244.00, 1, NULL),
(51, 2, 3.05, 3.05, 10.00, 11.00, 15.00, 1.16, 677.00, 29449.50, 19672.27, 16409.26, 14253.56, 12957.78, 11779.80, 10719.62, 9306.04, 8092.72, 1, NULL),
(52, 2, 3.05, 3.66, 10.00, 12.00, 15.00, 1.16, 748.00, 32538.00, 21735.38, 18130.17, 15748.39, 14316.72, 13015.20, 11843.83, 10282.01, 8941.44, 1, NULL),
(53, 2, 3.05, 3.96, 10.00, 13.00, 15.00, 1.16, 850.00, 36975.00, 24699.30, 20602.47, 17895.90, 16269.00, 14790.00, 13458.90, 11684.10, 10160.73, 1, NULL),
(54, 2, 3.05, 4.27, 10.00, 14.00, 15.00, 1.16, 953.00, 41455.50, 27692.27, 23099.00, 20064.46, 18240.42, 16582.20, 15089.80, 13099.94, 11391.97, 1, NULL),
(55, 2, 3.05, 4.57, 10.00, 15.00, 15.00, 1.16, 1029.00, 44761.50, 29900.68, 24941.11, 21664.57, 19695.06, 17904.60, 16293.19, 14144.63, 12300.46, 1, NULL),
(56, 2, 3.05, 4.88, 10.00, 16.00, 15.00, 1.16, 1104.00, 48024.00, 32080.03, 26758.97, 23243.62, 21130.56, 19209.60, 17480.74, 15175.58, 13197.00, 1, NULL),
(57, 2, 3.05, 5.18, 10.00, 17.00, 15.00, 1.16, 1177.00, 51199.50, 34201.27, 28528.36, 24780.56, 22527.78, 20479.80, 18636.62, 16179.04, 14069.62, 1, NULL),
(58, 2, 3.05, 5.18, 10.00, 17.00, 15.00, 1.16, 1177.00, 51199.50, 34201.27, 28528.36, 24780.56, 22527.78, 20479.80, 18636.62, 16179.04, 14069.62, 1, NULL),
(59, 2, 3.05, 5.49, 10.00, 18.00, 15.00, 1.16, 1249.00, 54331.50, 36293.44, 30273.51, 26296.45, 23905.86, 21732.60, 19776.67, 17168.75, 14930.30, 1, NULL),
(60, 2, 3.05, 6.10, 10.00, 20.00, 15.00, 1.16, 1488.00, 64728.00, 43238.30, 36066.44, 31328.35, 28480.32, 25891.20, 23560.99, 20454.05, 17787.25, 1, NULL),
(61, 2, 3.66, 2.44, 12.00, 8.00, 15.00, 1.16, 559.00, 24316.50, 16243.42, 13549.15, 11769.19, 10699.26, 9726.60, 8851.21, 7684.01, 6682.17, 1, NULL),
(62, 2, 3.66, 2.74, 12.00, 9.00, 15.00, 1.16, 670.00, 29145.00, 19468.86, 16239.59, 14106.18, 12823.80, 11658.00, 10608.78, 9209.82, 8009.05, 1, NULL),
(63, 2, 3.66, 3.05, 12.00, 10.00, 15.00, 1.16, 705.00, 30667.50, 20485.89, 17087.93, 14843.07, 13493.70, 12267.00, 11162.97, 9690.93, 8427.43, 1, NULL),
(64, 2, 3.66, 3.35, 12.00, 11.00, 15.00, 1.16, 785.00, 34147.50, 22810.53, 19026.99, 16527.39, 15024.90, 13659.00, 12429.69, 10790.61, 9383.73, 1, NULL),
(65, 2, 3.66, 3.96, 12.00, 13.00, 15.00, 1.16, 967.00, 42064.50, 28099.09, 23438.34, 20359.22, 18508.38, 16825.80, 15311.48, 13292.38, 11559.32, 1, NULL),
(66, 2, 3.66, 4.27, 12.00, 14.00, 15.00, 1.16, 1070.00, 46545.00, 31092.06, 25934.87, 22527.78, 20479.80, 18618.00, 16942.38, 14708.22, 12790.57, 1, NULL),
(67, 2, 3.66, 4.57, 12.00, 15.00, 15.00, 1.16, 1171.00, 50938.50, 34026.92, 28382.93, 24654.23, 22412.94, 20375.40, 18541.61, 16096.57, 13997.90, 1, NULL),
(68, 2, 3.66, 4.88, 12.00, 16.00, 15.00, 1.16, 1273.00, 55375.50, 36990.83, 30855.23, 26801.74, 24365.22, 22150.20, 20156.68, 17498.66, 15217.19, 1, NULL),
(69, 2, 3.66, 5.18, 12.00, 17.00, 15.00, 1.16, 1384.00, 60204.00, 40216.27, 33545.67, 29138.74, 26489.76, 24081.60, 21914.26, 19024.46, 16544.06, 1, NULL),
(70, 2, 3.66, 5.49, 12.00, 18.00, 15.00, 1.16, 1495.00, 65044.00, 43441.71, 36236.11, 31475.73, 28614.30, 26013.00, 23671.83, 20550.27, 17870.93, 1, NULL),
(71, 2, 3.66, 6.10, 12.00, 20.00, 15.00, 1.16, 1748.00, 76038.00, 50793.38, 42368.37, 36802.39, 33456.72, 30415.20, 27677.83, 24028.01, 20895.24, 1, NULL),
(72, 2, 4.27, 2.44, 14.00, 8.00, 15.00, 1.16, 692.00, 30102.00, 20108.14, 16772.83, 14569.37, 13244.88, 12040.80, 10957.13, 9512.23, 8272.03, 1, NULL),
(73, 2, 4.27, 2.74, 14.00, 9.00, 15.00, 1.16, 812.00, 35322.00, 23595.10, 19681.42, 17095.85, 15541.68, 14128.80, 12857.21, 11161.75, 9706.49, 1, NULL),
(74, 2, 4.27, 3.05, 14.00, 10.00, 15.00, 1.16, 869.00, 37801.50, 25251.40, 21063.00, 18295.93, 16632.66, 15120.60, 13759.75, 11945.27, 10387.85, 1, NULL),
(75, 2, 4.27, 3.35, 14.00, 11.00, 15.00, 1.16, 1025.00, 44587.50, 29784.45, 24844.15, 21580.35, 19618.50, 17835.00, 16229.85, 14089.65, 12252.65, 1, NULL),
(76, 2, 4.27, 3.66, 14.00, 12.00, 15.00, 1.16, 1049.00, 45631.50, 30481.84, 25425.87, 22085.65, 20077.86, 18252.60, 16609.87, 14419.55, 12539.54, 1, NULL),
(77, 2, 4.27, 3.96, 14.00, 13.00, 15.00, 1.16, 1251.00, 54418.50, 36351.56, 30321.99, 26338.55, 23944.14, 21767.40, 19808.33, 17196.25, 14954.20, 1, NULL),
(78, 2, 4.27, 4.27, 14.00, 14.00, 15.00, 1.16, 1310.00, 56985.00, 38065.98, 31752.04, 27580.74, 25073.40, 22794.00, 20742.54, 18007.26, 15659.48, 1, NULL),
(79, 2, 4.27, 4.57, 14.00, 15.00, 15.00, 1.16, 1514.00, 65859.00, 43993.81, 36696.63, 31875.76, 28977.96, 26343.60, 23972.68, 20811.44, 18098.05, 1, NULL),
(80, 2, 4.27, 4.88, 14.00, 16.00, 15.00, 1.16, 1555.00, 67642.50, 45185.19, 37690.40, 32738.97, 29762.70, 27057.00, 24621.87, 21375.03, 18588.16, 1, NULL),
(81, 2, 4.27, 5.18, 14.00, 17.00, 15.00, 1.16, 1710.00, 74385.00, 49689.18, 41447.32, 36002.34, 32729.40, 29754.00, 27076.14, 23505.66, 20441.00, 1, NULL),
(82, 2, 4.27, 5.49, 14.00, 18.00, 15.00, 1.16, 1858.00, 80823.00, 53989.76, 45034.58, 39118.33, 35562.12, 32329.20, 29419.57, 25540.07, 22210.16, 1, NULL),
(83, 2, 4.88, 2.44, 16.00, 8.00, 15.00, 1.16, 792.00, 34452.00, 23013.94, 19196.65, 16674.77, 15158.88, 13780.80, 12540.53, 10886.83, 9467.41, 1, NULL),
(84, 2, 4.88, 2.74, 16.00, 9.00, 15.00, 1.16, 933.00, 40585.50, 27111.11, 22614.24, 19643.38, 17857.62, 16234.20, 14773.12, 12825.02, 11152.90, 1, NULL),
(85, 2, 4.88, 3.05, 16.00, 10.00, 15.00, 1.16, 1002.00, 43587.00, 29116.12, 24286.68, 21096.11, 19178.28, 17434.80, 15865.67, 13773.49, 11977.71, 1, NULL),
(86, 2, 4.88, 3.35, 16.00, 11.00, 15.00, 1.16, 1180.00, 51330.00, 34288.44, 28601.08, 24843.72, 22585.20, 20532.00, 18684.12, 16220.28, 14105.48, 1, NULL),
(87, 2, 4.88, 3.66, 16.00, 12.00, 15.00, 1.16, 1252.00, 54462.00, 36380.62, 30346.23, 26359.61, 23963.28, 21784.80, 19824.17, 17209.99, 14966.16, 1, NULL),
(88, 2, 4.88, 3.96, 16.00, 13.00, 15.00, 1.16, 1452.00, 63162.00, 42192.22, 35193.87, 30570.41, 27791.28, 25264.80, 22990.97, 19959.19, 17356.92, 1, NULL),
(89, 2, 4.88, 4.27, 16.00, 14.00, 15.00, 1.16, 1584.00, 68904.00, 46027.87, 38393.31, 33349.54, 30317.76, 27561.60, 25081.06, 21773.66, 18934.82, 1, NULL),
(90, 2, 4.88, 4.57, 16.00, 15.00, 15.00, 1.16, 1819.00, 79126.50, 52856.50, 44089.29, 38297.23, 34815.66, 31650.60, 28802.05, 25003.97, 21743.96, 1, NULL),
(91, 2, 4.88, 4.88, 16.00, 16.00, 15.00, 1.16, 1947.00, 84694.50, 56575.93, 47191.78, 40992.14, 37265.58, 33877.80, 30828.80, 26763.46, 23274.05, 1, NULL),
(92, 2, 4.88, 5.18, 16.00, 17.00, 15.00, 1.16, 2302.00, 100137.00, 66891.52, 55796.34, 48466.31, 44060.28, 40054.80, 36449.87, 31643.29, 27517.65, 1, NULL),
(93, 2, 4.88, 5.49, 16.00, 18.00, 15.00, 1.16, 2784.00, 121104.00, 80897.47, 67479.15, 58614.34, 53285.76, 48441.60, 44081.86, 38268.86, 33279.38, 1, NULL),
(94, 2, 4.88, 6.10, 16.00, 20.00, 15.00, 1.16, 3035.00, 132022.50, 88191.03, 73562.94, 63898.89, 58089.90, 52809.00, 48056.19, 41719.11, 36279.78, 1, NULL),
(95, 2, 5.18, 2.44, 17.00, 8.00, 15.00, 1.16, 850.00, 36975.00, 24699.30, 20602.47, 17895.90, 16269.00, 14790.00, 13458.90, 11684.10, 10160.73, 1, NULL),
(96, 2, 5.18, 2.74, 17.00, 9.00, 15.00, 1.16, 1001.00, 43543.50, 29087.06, 24262.44, 21075.05, 19159.14, 17417.40, 15849.83, 13759.75, 11965.75, 1, NULL),
(97, 2, 5.18, 3.05, 17.00, 10.00, 15.00, 1.16, 1082.00, 47067.00, 31440.76, 26225.73, 22780.43, 20709.48, 18826.80, 17132.39, 14873.17, 12934.01, 1, NULL),
(98, 2, 5.18, 3.35, 17.00, 11.00, 15.00, 1.16, 1265.00, 55027.50, 36758.37, 30661.32, 26633.31, 24212.10, 22011.00, 20030.01, 17388.69, 15121.56, 1, NULL),
(99, 2, 5.18, 3.66, 17.00, 12.00, 15.00, 1.16, 1342.00, 58377.00, 38995.84, 32527.66, 28254.47, 25685.88, 23350.80, 21249.23, 18447.13, 16042.00, 1, NULL),
(100, 2, 5.18, 3.96, 17.00, 13.00, 15.00, 1.16, 1616.00, 70296.00, 46957.73, 39168.93, 34023.26, 30930.24, 28118.40, 25587.74, 22213.54, 19317.34, 1, NULL),
(101, 2, 5.18, 4.27, 17.00, 14.00, 15.00, 1.16, 1738.00, 75603.00, 50502.80, 42125.99, 36591.85, 33265.32, 30241.20, 27519.49, 23890.55, 20775.70, 1, NULL),
(102, 2, 5.18, 4.57, 17.00, 15.00, 15.00, 1.16, 1941.00, 84433.50, 56401.58, 47046.35, 40865.81, 37150.74, 33773.40, 30733.79, 26680.99, 23202.33, 1, NULL),
(103, 2, 5.18, 4.88, 17.00, 16.00, 15.00, 1.16, 2066.00, 89871.00, 60033.83, 50076.12, 43497.56, 39543.24, 35948.40, 32713.04, 28399.24, 24696.55, 1, NULL),
(104, 2, 5.18, 5.18, 17.00, 17.00, 15.00, 1.16, 2458.00, 106923.00, 71424.56, 59577.50, 51750.73, 47046.12, 42769.20, 38919.97, 33787.67, 29382.44, 1, NULL),
(105, 2, 5.18, 5.49, 17.00, 18.00, 15.00, 1.16, 2940.00, 127890.00, 85430.52, 71260.31, 61898.76, 56271.60, 51156.00, 46551.96, 40413.24, 35144.17, 1, NULL),
(106, 2, 5.18, 6.10, 17.00, 20.00, 15.00, 1.16, 3190.00, 138765.00, 92695.02, 77319.86, 67162.26, 61056.60, 55506.00, 50510.46, 43849.74, 38132.62, 1, NULL),
(107, 2, 5.49, 2.44, 18.00, 8.00, 15.00, 1.16, 909.00, 39541.50, 26413.72, 22032.52, 19138.09, 17398.26, 15816.60, 14393.11, 12495.11, 10866.00, 1, NULL),
(108, 2, 5.49, 2.74, 18.00, 9.00, 15.00, 1.16, 1070.00, 46545.00, 31092.06, 25934.87, 22527.78, 20479.80, 18618.00, 16942.38, 14708.22, 12790.57, 1, NULL),
(109, 2, 5.49, 3.05, 18.00, 10.00, 15.00, 1.16, 1162.00, 50547.00, 33765.40, 28164.79, 24464.75, 22240.68, 20218.80, 18399.11, 15972.85, 13890.32, 1, NULL),
(110, 2, 5.49, 3.35, 18.00, 11.00, 15.00, 1.16, 1351.00, 58768.50, 39257.36, 32745.81, 28443.95, 25858.14, 23507.40, 21391.73, 18570.85, 16149.58, 1, NULL),
(111, 2, 5.49, 3.66, 18.00, 12.00, 15.00, 1.16, 1432.00, 62292.00, 41611.06, 34709.10, 30149.33, 27408.48, 24916.80, 22674.29, 19684.27, 17117.84, 1, NULL),
(112, 2, 5.49, 3.96, 18.00, 13.00, 15.00, 1.16, 1779.00, 77386.50, 51694.18, 43119.76, 37455.07, 34050.06, 30954.60, 28168.69, 24454.13, 21265.81, 1, NULL),
(113, 2, 5.49, 4.27, 18.00, 14.00, 15.00, 1.16, 1892.00, 82302.00, 54977.74, 45858.67, 39834.17, 36212.88, 32920.80, 29957.93, 26007.43, 22616.59, 1, NULL),
(114, 2, 5.49, 4.57, 18.00, 15.00, 15.00, 1.16, 2064.00, 89784.00, 59975.71, 50027.64, 43455.46, 39504.96, 35913.60, 32681.38, 28371.74, 24672.64, 1, NULL),
(115, 2, 5.49, 4.88, 18.00, 16.00, 15.00, 1.16, 2185.00, 95047.50, 63491.73, 52960.47, 46002.99, 41820.90, 38019.00, 34597.29, 30035.01, 26119.05, 1, NULL),
(116, 2, 5.49, 5.18, 18.00, 17.00, 15.00, 1.16, 2613.00, 113665.50, 75928.55, 63334.42, 55014.10, 50012.82, 45466.20, 41374.24, 35918.30, 31235.28, 1, NULL),
(117, 2, 5.49, 5.49, 18.00, 18.00, 15.00, 1.16, 3095.00, 134632.50, 89934.51, 75017.23, 65162.13, 59238.30, 53853.00, 49006.23, 42543.87, 36997.01, 1, NULL),
(118, 2, 5.49, 6.10, 18.00, 20.00, 15.00, 1.16, 3346.00, 145551.00, 97228.07, 81101.02, 70446.68, 64042.44, 58220.40, 52980.56, 45994.12, 39997.41, 1, NULL),
(119, 2, 6.09, 2.44, 20.00, 8.00, 15.00, 1.16, 1068.00, 46458.00, 31033.94, 25886.40, 22485.67, 20441.52, 18583.20, 16910.71, 14680.73, 12766.66, 1, NULL),
(120, 2, 6.09, 2.44, 20.00, 9.00, 15.00, 1.16, 1216.00, 52896.00, 35334.53, 29473.65, 25601.66, 23274.24, 21158.40, 19254.14, 16715.14, 14535.82, 1, NULL),
(121, 2, 6.09, 3.05, 20.00, 10.00, 15.00, 1.16, 1331.00, 57898.50, 38676.20, 32261.04, 28022.87, 25475.34, 23159.40, 21075.05, 18295.93, 15910.51, 1, NULL),
(122, 2, 6.09, 3.35, 20.00, 11.00, 15.00, 1.16, 1674.00, 72819.00, 48643.09, 40574.75, 35244.40, 32040.36, 29127.60, 26506.12, 23010.80, 20010.66, 1, NULL),
(123, 2, 6.09, 3.66, 20.00, 12.00, 15.00, 1.16, 1776.00, 77256.00, 51607.01, 43047.04, 37391.90, 33992.64, 30902.40, 28121.18, 24412.90, 21229.95, 1, NULL),
(124, 2, 6.09, 3.96, 20.00, 13.00, 15.00, 1.16, 2008.00, 87348.00, 58348.46, 48670.31, 42276.43, 38433.12, 34939.20, 31794.67, 27601.97, 24003.23, 1, NULL),
(125, 2, 6.09, 4.27, 20.00, 14.00, 15.00, 1.16, 2066.00, 89871.00, 60033.83, 50076.12, 43497.56, 39543.24, 35948.40, 32713.04, 28399.24, 24696.55, 1, NULL),
(126, 2, 6.09, 4.57, 20.00, 15.00, 15.00, 1.16, 2256.00, 98136.00, 65554.85, 54681.38, 47497.82, 43179.84, 39254.40, 35721.50, 31010.98, 26967.77, 1, NULL),
(127, 2, 6.09, 4.88, 20.00, 16.00, 15.00, 1.16, 2484.00, 108054.00, 72180.07, 60207.69, 52298.14, 47543.76, 43221.60, 39331.66, 34145.06, 29693.24, 1, NULL),
(128, 2, 6.09, 5.18, 20.00, 17.00, 15.00, 1.16, 2768.00, 120408.00, 80432.54, 67091.34, 58277.47, 52979.52, 48163.20, 43828.51, 38048.93, 33088.12, 1, NULL),
(129, 2, 6.09, 5.49, 20.00, 18.00, 15.00, 1.16, 3250.00, 141375.00, 94438.50, 78774.15, 68425.50, 62205.00, 56550.00, 51460.50, 44674.50, 38849.85, 1, NULL),
(130, 2, 6.09, 6.10, 20.00, 20.00, 15.00, 1.16, 3501.00, 152293.50, 101732.06, 84857.94, 73710.05, 67009.14, 60917.40, 55434.83, 48124.75, 41850.25, 1, NULL),
(131, 1, 9.10, 9.10, 27.30, 27.30, 17.00, 16.00, 123.00, 83640.00, 55871.52, 46604.21, 40481.76, 36801.60, 33456.00, 30444.96, 26430.24, 22984.27, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_modelo`
--

CREATE TABLE IF NOT EXISTS `puerta_modelo` (
  `id_modelo` int(11) NOT NULL,
  `nom_modelo` varchar(255) DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `descripcion` text,
  `activo` tinyint(4) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_modelo`
--

INSERT INTO `puerta_modelo` (`id_modelo`, `nom_modelo`, `id_tipo`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Classica', 1, '<ul><li>Modelo<br></li><ul><li>Residencial: <br></li><ul><li>&nbsp;Classica</li></ul></ul></ul>', 1, 2),
(2, 'vhj', 2, 'fgbhhnyny', 0, NULL),
(3, 'Adonnia', 1, '<ul><li>Modelo</li><ul><li>Residencial</li><ul><li>Adonnia<br></li></ul></ul></ul>', 1, NULL),
(4, 'Elegantti', 1, NULL, 1, NULL),
(5, 'Finesse', 1, NULL, 1, NULL),
(6, 'Allure', 1, NULL, 1, NULL),
(7, 'Finne', 1, NULL, 1, NULL),
(8, 'Modderni', 1, NULL, 1, NULL),
(9, 'Belissa', 1, NULL, 1, NULL),
(10, 'Exquesitte', 1, NULL, 1, NULL),
(11, 'HS1024', 2, NULL, 1, 419),
(12, 'HS1527', 2, NULL, 1, NULL),
(13, 'HS2527', 2, NULL, 1, 153),
(14, 'HS2027', 2, NULL, 1, NULL),
(15, 'HS3027', 2, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_mov`
--

CREATE TABLE IF NOT EXISTS `puerta_mov` (
  `id_mov` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `nom_mov` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_mov`
--

INSERT INTO `puerta_mov` (`id_mov`, `id_tipo`, `nom_mov`, `activo`, `id_log`) VALUES
(1, 1, 'Seccional', 1, 18),
(2, 2, 'Seccional', 0, NULL),
(3, 1, 'Abatible', 1, NULL),
(4, 1, 'Estandar', 0, NULL),
(5, 2, 'Estandar', 1, NULL),
(6, 1, 'Corrediza', 1, NULL),
(7, 1, 'Maroma', 1, NULL),
(8, 1, 'Enrrollable', 1, NULL),
(9, 1, 'Deslizable Lateral', 1, NULL),
(10, 2, 'Inclinada', 1, NULL),
(11, 2, 'Inclinada Suave', 1, NULL),
(12, 2, 'Vertical', 1, NULL),
(13, 2, 'Riel Inclinado', 1, NULL),
(14, 2, 'Techo Bajo', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_textura`
--

CREATE TABLE IF NOT EXISTS `puerta_textura` (
  `id_textura` int(11) NOT NULL,
  `nom_textura` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `proveedor` varchar(255) DEFAULT NULL,
  `costo` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_textura`
--

INSERT INTO `puerta_textura` (`id_textura`, `nom_textura`, `tipo`, `proveedor`, `costo`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Lisa', 'Nacional', 'prdsMEX', '100 x mts', 'Texturas disponibles<br><ul><li>Lisa<br></li></ul>', 1, 93),
(2, 'Madera', 'Importado', 'pvdImportado', '10 x mts (dollar)', 'prov x<br>', 1, 96);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_tipo`
--

CREATE TABLE IF NOT EXISTS `puerta_tipo` (
  `id_tipo` int(11) NOT NULL,
  `nom_tipo` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_tipo`
--

INSERT INTO `puerta_tipo` (`id_tipo`, `nom_tipo`, `activo`, `id_log`) VALUES
(1, 'Residencial', 1, 29),
(2, 'Industrial', 1, NULL),
(3, 'Pedo', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tp_usuario`
--

CREATE TABLE IF NOT EXISTS `tp_usuario` (
  `id_tpusuario` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tp_usuario`
--

INSERT INTO `tp_usuario` (`id_tpusuario`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Administrador', 1, NULL),
(2, 'Director', 1, NULL),
(3, 'Director Produccion', 1, NULL),
(4, 'Direcctor Ventas', 1, NULL),
(5, 'Cliente', 1, NULL),
(6, 'Vendedor', 1, NULL),
(7, 'Produccion', 1, NULL),
(8, 'Despacho', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_divisas` int(11) DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `nom_usuario` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `nom_cliente` varchar(255) DEFAULT NULL,
  `ap_paterno` varchar(30) DEFAULT NULL,
  `ap_materno` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `tel1` varchar(50) DEFAULT NULL,
  `tel2` varchar(50) DEFAULT NULL,
  `ext` varchar(50) DEFAULT NULL,
  `rfc` varchar(255) DEFAULT NULL,
  `curp` varchar(255) DEFAULT NULL,
  `razon_social` varchar(30) DEFAULT NULL,
  `nss` varchar(50) DEFAULT NULL,
  `num_empleado` varchar(50) DEFAULT NULL,
  `id_tpusuario` int(11) DEFAULT NULL,
  `activo` int(11) DEFAULT '1',
  `accesos` int(11) DEFAULT NULL,
  `id_log` int(11) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `id_divisas`, `id_pais`, `nom_usuario`, `password`, `nombre`, `nom_cliente`, `ap_paterno`, `ap_materno`, `email`, `tel1`, `tel2`, `ext`, `rfc`, `curp`, `razon_social`, `nss`, `num_empleado`, `id_tpusuario`, `activo`, `accesos`, `id_log`, `foto`) VALUES
(1, 1, 117, 'admintk', 'Teknik', 'Teknik', NULL, 'Mx', NULL, 'webmaster@teknik.mx', '81-19498714', '81-19314009', '0', 'GURR910918', 'GURR910918', NULL, '1234567890-000', '123456', 1, 1, NULL, 437, 'img/avatar.png'),
(22, 1, 117, NULL, NULL, NULL, 'Inn teknik', NULL, NULL, 'jgutierrez@teknik.mx', '19314009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 123, 'img/avatar.png'),
(24, 1, 117, NULL, NULL, NULL, 'inn teknik1', NULL, NULL, 'jgutierrez@teknik.mx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 125, 'img/avatar.png'),
(25, 1, 117, 'HC115', 'Hc0115', 'Herbert', NULL, 'Cardenas', NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, '123123123', NULL, 1, 1, NULL, 136, 'img/avatar.png'),
(26, 1, 117, 'PC112', 'Pc0113', 'Paola', NULL, 'Cardenas', NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1234', 4, 1, NULL, 137, 'img/avatar.png'),
(27, 1, 117, 'GO112', 'Go0112', 'Guillermo ', NULL, 'Ortiz', NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1234', 3, 1, NULL, 138, 'img/avatar.png'),
(28, 1, 117, 'Ventas1MC', 'Mc0101', 'Mario ', NULL, 'Cortez ', NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1234', 6, 1, NULL, 139, 'img/avatar.png'),
(29, 1, 117, 'Ventas3AG', 'Ag0103', 'Ana', NULL, 'Gómez', NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123', 6, 1, NULL, 433, 'img/avatar.png'),
(30, 1, 117, 'SC2LV', 'Lv0106', 'Luis ', NULL, ' Villarreal', NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12123', 6, 1, NULL, 141, 'img/avatar.png'),
(31, 1, 117, NULL, NULL, 'Diego ', 'Diego ', 'Sanchez ', 'V. ', 'gortiz@hspuertasdegaraje.com', '83464849', NULL, NULL, 'FJHDH12KJFHD2', NULL, 'Sanchez SA de CV', NULL, NULL, 5, 1, NULL, 262, 'img/avatar.png'),
(32, 2, 59, NULL, NULL, 'Luis', 'Luis Garcia', 'Garcia', NULL, 'bbb@bbb.com', '1 772 344 0385', NULL, NULL, '3245677', NULL, 'Luis Garcia', NULL, NULL, 5, 1, NULL, 316, 'img/avatar.png'),
(33, 2, 59, NULL, NULL, 'Luis', 'Luis Jaramillo', 'Jaramillo', 'gomez', 'xxx)45566hhgv', '+14182217900', NULL, '115#€>¶<~|<%^**%#', NULL, NULL, 'gosrta asc', NULL, NULL, 5, 0, NULL, 319, 'img/avatar.png'),
(34, 1, 117, NULL, NULL, 'KARLA ', 'Karla Sanchez ', 'SANCHEZ ', 'GOMEZ ', 'karla1%hotmail.com', '8110900356', NULL, '08237928', 'LSKDJFLKSDFJ7123', NULL, 'KARLA SA DE CV ', NULL, NULL, 5, 1, NULL, 320, 'img/avatar.png'),
(35, 1, NULL, NULL, NULL, 'Flavio', 'Flavio Ortiz', 'Ortiz', NULL, 'flavio@hspuertasdegaraje.com', '81-12341000', NULL, '115', 'hjgsadgjfh', NULL, 'HS', NULL, NULL, 5, 0, NULL, 322, 'img/avatar.png'),
(36, 1, 117, NULL, NULL, 'Memo', 'Memo ortiz', 'Ortiz', 'Loera', 'memo@hspuertasdegaraje.com', '81-12341000', NULL, '113', '78435785', NULL, 'HS', NULL, NULL, 5, 0, NULL, 323, 'img/avatar.png'),
(37, 1, 117, NULL, NULL, NULL, 'Rogelio Teknik', NULL, NULL, 'rogelio.18.91@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 330, 'img/avatar.png'),
(38, 1, 117, NULL, NULL, 'Luis', 'Luis Garcia', 'Garcia', 'nn', 'luis@garcia.com', '772-3440385_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 331, 'img/avatar.png'),
(39, 1, 117, NULL, NULL, 'Memo', 'Memo', 'Ortiz ', 'Loera ', 'gortiz@hspuertasdegaraje.com', '555-5017719_', NULL, '113', 'KJFHEFB', NULL, 'MEMO SA DE CV', NULL, NULL, 5, 0, NULL, 340, 'img/avatar.png'),
(40, 1, 117, NULL, NULL, 'Martha', 'Martha Mesa', 'Mesa', NULL, 'martha@live.com', '772-3488679_', NULL, NULL, '1234', NULL, 'mm', NULL, NULL, 5, 1, NULL, 343, 'img/avatar.png'),
(41, 1, 117, NULL, NULL, 'Memo 2 ', 'Memo 2 ', 'Ortiz 2 ', NULL, 'gortiz@hspuertasdegaraje.com', '555-5017719_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 346, 'img/avatar.png'),
(42, 1, 117, NULL, NULL, 'Panfilo ', 'Panfilo ', 'Gutierrez ', NULL, 'gortiz@hspuertasdegaraje.com', '555-5017719_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 351, 'img/avatar.png'),
(43, 1, 117, NULL, NULL, NULL, 'Petra ', NULL, NULL, 'guillermoortizloera@gmail.com', '811-2341000_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 352, 'img/avatar.png'),
(44, NULL, 117, 'PCM113', 'Pcm0113', 'Paola', NULL, 'Cardenas', 'Mesa', 'paola@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 356, 'img/avatar.png'),
(45, 1, 117, NULL, NULL, NULL, 'Tito ', NULL, NULL, 'gortiz@hspuertasdegaraje.com', '555-5017719_', NULL, '113', NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 357, 'img/avatar.png'),
(46, 1, 117, NULL, NULL, NULL, 'Carolina Perez', NULL, NULL, 'carolina@perez.com', '123-45678901', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 361, 'img/avatar.png'),
(47, 1, 117, NULL, NULL, 'FIDENCIO ', 'Fidencio Martinez ', 'MARTINEZ ', NULL, 'gortiz@hspuertasdegaraje.com', '811-2341000_', NULL, NULL, 'WKHGFKJ', NULL, 'Fidencio ', NULL, NULL, 5, 1, NULL, 364, 'img/avatar.png'),
(48, 1, NULL, NULL, NULL, NULL, 'Filiberto Rupertino ', NULL, NULL, 'herbertcardenas@gmail.com', '811-2341000_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, 375, 'img/avatar.png'),
(49, 1, 117, NULL, NULL, NULL, 'Inocencio Guadaluope', NULL, NULL, 'herbertcardenas@gmail.com', '811-2341000_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 380, 'img/avatar.png'),
(50, NULL, 117, NULL, NULL, NULL, 'Rosario Gomez ', NULL, NULL, 'gortiz@hspuertasdegaraje.com', '811-2341000_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 413, 'img/avatar.png'),
(51, NULL, 117, NULL, NULL, NULL, 'Pancho Lopez ', NULL, NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 415, 'img/avatar.png'),
(52, 1, 117, NULL, NULL, 'Humberto', 'Puertas automaticas del Norte', 'Moreno', NULL, 'humberto@prueba.com', '123-45678901', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 416, 'img/avatar.png'),
(53, 1, NULL, NULL, NULL, 'Franco ', 'Franco Gtz. ', 'Gtz', NULL, 'gortiz@hspuertasdegaraje.com', '811-2341000_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 434, 'img/avatar.png'),
(54, 1, 117, NULL, NULL, NULL, 'iuytiouyo8uyu', NULL, NULL, 'rwewer@kasmdk.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 436, 'img/avatar.png'),
(55, 1, 117, NULL, NULL, NULL, 'Guillermo Ortiz ', NULL, NULL, 'gortiz@hspuertasdegaraje.com', '811-2341000_', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 438, 'img/avatar.png'),
(56, 1, 117, NULL, NULL, NULL, 'Hebert Cardenas ', NULL, NULL, 'gortiz@hspuertasdegaraje.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, NULL, 439, 'img/avatar.png');

-- --------------------------------------------------------

--
-- Table structure for table `usuario_log`
--

CREATE TABLE IF NOT EXISTS `usuario_log` (
  `id_log` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `evento` text,
  `ip_address` text,
  `fecha` date DEFAULT NULL,
  `texto_evento` text
) ENGINE=InnoDB AUTO_INCREMENT=442 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario_log`
--

INSERT INTO `usuario_log` (`id_log`, `id_usuario`, `evento`, `ip_address`, `fecha`, `texto_evento`) VALUES
(1, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id: 1  creo un usuario con el id: 5 '),
(2, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el modelo con el id1 '),
(3, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un modelo con el id3 '),
(4, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico la medida con el id1 '),
(5, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo una medida con el id2 '),
(6, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo una medida con el id3 '),
(7, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo una textura con el id2 '),
(8, 1, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id 1 elimino el componente con el id 1 para la id 3'),
(9, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico los componentes con el id3 '),
(10, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un movimiento con el id2 '),
(11, 0, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id  elimino el movimiento con el id 2 '),
(12, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el movimeinto con el id1 '),
(13, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el movimeinto con el id1 '),
(14, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el movimeinto con el id1 '),
(15, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el movimeinto con el id1 '),
(16, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el movimeinto con el id1 '),
(17, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el movimeinto con el id1 '),
(18, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el movimeinto con el id1 '),
(19, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un movimiento con el id3 '),
(20, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el color con el id1 '),
(21, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el color con el id1 '),
(22, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el color con el id1 '),
(23, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un color con el id5 '),
(24, 0, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id  elimino el color con el id 5 '),
(25, 0, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id  elimino el color con el id 5 '),
(26, 0, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id  elimino el color con el id 5 '),
(27, 0, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id  elimino el color con el id 5 '),
(28, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el tipo con el id1 '),
(29, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el tipo con el id1 '),
(30, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un tipo con el id3 '),
(31, 0, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id  elimino el tipo con el id 3 '),
(32, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un modelo con el id2 '),
(33, 0, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id  elimino el modelo con el id 2 '),
(34, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el modelo con el id1 '),
(35, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el modelo con el id1 '),
(36, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico la medida con el id1 '),
(37, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico la medida con el id1 '),
(38, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el color con el id2 '),
(39, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el color con el id2 '),
(40, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el color con el id2 '),
(41, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un color con el id3 '),
(42, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el tipo con el id1 '),
(43, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un tipo con el id2 '),
(44, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico el componente con el id1 '),
(45, 1, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id 1 elimino el componente con el id 4 '),
(46, 1, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id 1 elimino el componente con el id 4 '),
(47, 1, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id 1 elimino el componente con el id 4 '),
(48, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un componente con el id5 '),
(49, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un componente con el id12 '),
(50, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un componente con el id6 '),
(51, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un componente con el id7 '),
(52, 1, 'Eliminar', '::1', '2015-06-23', 'El usuario con el id 1 elimino al usuario con el id 5 '),
(53, 1, 'Alta', '::1', '2015-06-23', 'El usuario con el id 1  creo un cliente con el id6 '),
(54, 1, 'Modificar', '::1', '2015-06-23', 'El usuario con el id 1  modifico al cliente con el id 6 '),
(55, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 1 '),
(56, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 2 '),
(57, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 3 '),
(58, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 4 '),
(59, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 5 '),
(60, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 6 '),
(61, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 7 '),
(62, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 8 '),
(63, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 9 '),
(64, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 10 '),
(65, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 11 '),
(66, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 12 '),
(67, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 13 '),
(68, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 14 '),
(69, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 15 '),
(70, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 16 '),
(71, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 17 '),
(72, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 18 '),
(73, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 19 '),
(74, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 20 '),
(75, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 21 '),
(76, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 22 '),
(77, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 23 '),
(78, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo una cotizacion con el id: 24 '),
(79, 3, 'Alta', '::1', '2015-06-24', 'El usuario con el id: 3  creo un componente con el id: 13 '),
(80, 0, 'Alta', '::1', '2015-07-12', 'El usuario con el id   creo un cliente con el id7 '),
(81, 0, 'Alta', '::1', '2015-07-13', 'El usuario con el id   creo un cliente con el id8 '),
(82, 0, 'Alta', '::1', '2015-07-13', 'El usuario con el id   creo un cliente con el id9 '),
(83, 3, 'Modificar', '::1', '2015-07-13', 'El usuario con el id 3  modifico al cliente con el id 9 '),
(84, 3, 'Modificar', '::1', '2015-07-13', 'El usuario con el id 3  modifico al cliente con el id 9 '),
(85, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 0 '),
(86, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 8 '),
(87, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 9 '),
(88, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 10 '),
(89, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 11 '),
(90, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 12 '),
(91, 3, 'Modificar', '::1', '2015-07-13', 'El usuario con el id 3  modifico la textura con el id2 '),
(92, 3, 'Modificar', '::1', '2015-07-13', 'El usuario con el id 3  modifico la textura con el id2 '),
(93, 3, 'Modificar', '::1', '2015-07-13', 'El usuario con el id 3  modifico la textura con el id1 '),
(94, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 13 '),
(95, 3, 'Modificar', '::1', '2015-07-13', 'El usuario con el id 3  modifico al cliente con el id 9 '),
(96, 3, 'Modificar', '::1', '2015-07-13', 'El usuario con el id 3  modifico la textura con el id2 '),
(97, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 14 '),
(98, 3, 'Alta', '::1', '2015-07-13', 'El usuario con el id: 3  creo una cotizacion con el id: 15 '),
(99, 3, 'Modificar', '::1', '2015-07-20', 'El usuario con el id 3  modifico al cliente con el id 6 '),
(100, 3, 'Alta', '::1', '2015-07-20', 'El usuario con el id: 3  creo una cotizacion con el id: 16 '),
(101, 3, 'Alta', '::1', '2015-07-20', 'El usuario con el id: 3  creo una cotizacion con el id: 17 '),
(102, 3, 'Alta', '::1', '2015-07-20', 'El usuario con el id 3  creo un cliente con el id10 '),
(103, 3, 'Modificar', '::1', '2015-07-20', 'El usuario con el id 3  modifico al cliente con el id 10 '),
(104, 3, 'Eliminar', '::1', '2015-07-20', 'El usuario con el id 3 elimino al cliente con el id 10 '),
(105, 3, 'Modificar', '::1', '2015-07-20', 'El usuario con el id 3  modifico el modelo con el id1 '),
(106, 3, 'Eliminar', '189.210.167.14', '2015-07-28', 'El usuario con el id 3 elimino al cliente con el id 10 '),
(107, 3, 'Alta', '189.210.167.14', '2015-07-28', 'El usuario con el id: 3  creo un usuario con el id: 11 '),
(108, 3, 'Alta', '189.210.167.14', '2015-07-28', 'El usuario con el id: 3  creo un usuario con el id: 12 '),
(109, 3, 'Alta', '189.210.167.14', '2015-07-28', 'El usuario con el id: 3  creo un usuario con el id: 13 '),
(110, 3, 'Alta', '189.210.167.14', '2015-07-28', 'El usuario con el id: 3  creo un usuario con el id: 14 '),
(111, 3, 'Alta', '189.210.167.14', '2015-07-28', 'El usuario con el id: 3  creo un usuario con el id: 15 '),
(112, 3, 'Alta', '189.210.167.14', '2015-07-28', 'El usuario con el id: 3  creo un usuario con el id: 16 '),
(113, 13, 'Eliminar', '201.159.109.205', '2015-08-05', 'El usuario con el id 13 elimino al cliente con el id 9 '),
(114, 13, 'Alta', '201.159.109.205', '2015-08-05', 'El usuario con el id 13  creo un cliente con el id17 '),
(115, 13, 'Alta', '201.159.109.205', '2015-08-05', 'El usuario con el id 13  creo un cliente con el id18 '),
(116, 3, 'Alta', '189.254.162.20', '2015-08-05', 'El usuario con el id 3  creo un cliente con el id19 '),
(117, 3, 'Alta', '189.211.237.230', '2015-08-05', 'El usuario con el id 3  creo un cliente con el id20 '),
(118, 3, 'Eliminar', '189.211.237.230', '2015-08-07', 'El usuario con el id 3 elimino al usuario con el id 19 '),
(119, 3, 'Eliminar', '189.211.237.230', '2015-08-07', 'El usuario con el id 3 elimino al usuario con el id 20 '),
(120, 13, 'Alta', '201.159.109.205', '2015-08-07', 'El usuario con el id 13  creo un cliente con el id21 '),
(121, 3, 'Alta', '::1', '2015-08-18', 'El usuario con el id 3  creo un cliente con el id22 '),
(122, 3, 'Modificar', '::1', '2015-08-18', 'El usuario con el id 3  modifico al cliente con el id 22 '),
(123, 3, 'Modificar', '::1', '2015-08-18', 'El usuario con el id 3  modifico al cliente con el id 22 '),
(124, 3, 'Alta', '::1', '2015-08-18', 'El usuario con el id 3  creo un cliente con el id23 '),
(125, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id 1  creo un cliente con el id24 '),
(126, 1, 'Eliminar', '::1', '2015-08-18', 'El usuario con el id 1 elimino al cliente con el id 22 '),
(127, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id: 1  creo una cotizacion con el id: 1 '),
(128, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id: 1  creo una cotizacion con el id: 2 '),
(129, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id: 1  creo una cotizacion con el id: 3 '),
(130, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id: 1  creo una cotizacion con el id: 4 '),
(131, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id: 1  creo una cotizacion con el id: 5 '),
(132, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id: 1  creo una cotizacion con el id: 6 '),
(133, 1, 'Alta', '::1', '2015-08-18', 'El usuario con el id: 1  creo una cotizacion con el id: 7 '),
(134, 1, 'Alta', '::1', '2015-08-19', 'El usuario con el id: 1  creo una cotizacion con el id: 8 '),
(135, 1, 'Alta', '::1', '2015-08-19', 'El usuario con el id: 1  creo una cotizacion con el id: 9 '),
(136, 1, 'Alta', '189.211.237.230', '2015-08-19', 'El usuario con el id: 1  creo un usuario con el id: 25 '),
(137, 1, 'Alta', '189.211.237.230', '2015-08-19', 'El usuario con el id: 1  creo un usuario con el id: 26 '),
(138, 1, 'Alta', '189.211.237.230', '2015-08-19', 'El usuario con el id: 1  creo un usuario con el id: 27 '),
(139, 1, 'Alta', '189.211.237.230', '2015-08-19', 'El usuario con el id: 1  creo un usuario con el id: 28 '),
(140, 1, 'Alta', '189.211.237.230', '2015-08-19', 'El usuario con el id: 1  creo un usuario con el id: 29 '),
(141, 1, 'Alta', '189.211.237.230', '2015-08-19', 'El usuario con el id: 1  creo un usuario con el id: 30 '),
(142, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id4 '),
(143, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id5 '),
(144, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id6 '),
(145, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id7 '),
(146, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id8 '),
(147, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id9 '),
(148, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id10 '),
(149, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id11 '),
(150, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id12 '),
(151, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id13 '),
(152, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id14 '),
(153, 1, 'Modificar', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  modifico el modelo con el id13 '),
(154, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo un modelo con el id15 '),
(155, 1, 'Alta', '189.254.162.16', '2015-08-20', 'El usuario con el id 1  creo una medida con el id4 '),
(156, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id5 '),
(157, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id6 '),
(158, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id7 '),
(159, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id8 '),
(160, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id9 '),
(161, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id10 '),
(162, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id11 '),
(163, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id12 '),
(164, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id13 '),
(165, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id14 '),
(166, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id15 '),
(167, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id16 '),
(168, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id17 '),
(169, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id18 '),
(170, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id19 '),
(171, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id20 '),
(172, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id21 '),
(173, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id22 '),
(174, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id23 '),
(175, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id24 '),
(176, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id25 '),
(177, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id26 '),
(178, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id27 '),
(179, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id28 '),
(180, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id29 '),
(181, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id30 '),
(182, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id31 '),
(183, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id32 '),
(184, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id33 '),
(185, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id34 '),
(186, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id35 '),
(187, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id36 '),
(188, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id37 '),
(189, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id38 '),
(190, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id39 '),
(191, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id40 '),
(192, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id41 '),
(193, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id42 '),
(194, 1, 'Alta', '201.144.162.9', '2015-08-20', 'El usuario con el id 1  creo una medida con el id43 '),
(195, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id44 '),
(196, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id45 '),
(197, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id46 '),
(198, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id47 '),
(199, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id48 '),
(200, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id49 '),
(201, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id50 '),
(202, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id51 '),
(203, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id52 '),
(204, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id53 '),
(205, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id54 '),
(206, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id55 '),
(207, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id56 '),
(208, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id57 '),
(209, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id58 '),
(210, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id59 '),
(211, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id60 '),
(212, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id61 '),
(213, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id62 '),
(214, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id63 '),
(215, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id64 '),
(216, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id65 '),
(217, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id66 '),
(218, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id67 '),
(219, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id68 '),
(220, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id69 '),
(221, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id70 '),
(222, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id71 '),
(223, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id72 '),
(224, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id73 '),
(225, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id74 '),
(226, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id75 '),
(227, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id76 '),
(228, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id77 '),
(229, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id78 '),
(230, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id79 '),
(231, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id80 '),
(232, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id81 '),
(233, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id82 '),
(234, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id83 '),
(235, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id84 '),
(236, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo una medida con el id85 '),
(237, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id4 '),
(238, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id5 '),
(239, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id6 '),
(240, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id7 '),
(241, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id8 '),
(242, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id9 '),
(243, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id10 '),
(244, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id11 '),
(245, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id12 '),
(246, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id13 '),
(247, 1, 'Alta', '131.178.200.65', '2015-08-20', 'El usuario con el id 1  creo un movimiento con el id14 '),
(248, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id86 '),
(249, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id87 '),
(250, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id88 '),
(251, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id89 '),
(252, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id90 '),
(253, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id91 '),
(254, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id92 '),
(255, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id93 '),
(256, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id94 '),
(257, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id95 '),
(258, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id96 '),
(259, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id97 '),
(260, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id98 '),
(261, 27, 'Eliminar', '187.138.0.48', '2015-08-21', 'El usuario con el id 27 elimino al cliente con el id 22 '),
(262, 27, 'Alta', '187.138.0.48', '2015-08-21', 'El usuario con el id 27  creo un cliente con el id31 '),
(263, 27, 'Alta', '187.138.0.48', '2015-08-21', 'El usuario con el id: 27  creo una cotizacion con el id: 10 '),
(264, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id99 '),
(265, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id100 '),
(266, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id101 '),
(267, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id102 '),
(268, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id103 '),
(269, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id104 '),
(270, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id105 '),
(271, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id106 '),
(272, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id107 '),
(273, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id108 '),
(274, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id109 '),
(275, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id110 '),
(276, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id111 '),
(277, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id112 '),
(278, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id113 '),
(279, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id114 '),
(280, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id115 '),
(281, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id116 '),
(282, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id117 '),
(283, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id118 '),
(284, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id119 '),
(285, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id120 '),
(286, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id121 '),
(287, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id122 '),
(288, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id123 '),
(289, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id124 '),
(290, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id125 '),
(291, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id126 '),
(292, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id127 '),
(293, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id128 '),
(294, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id129 '),
(295, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo una medida con el id130 '),
(296, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id3 '),
(297, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id4 '),
(298, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id5 '),
(299, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id6 '),
(300, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id7 '),
(301, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id8 '),
(302, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id9 '),
(303, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id10 '),
(304, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id11 '),
(305, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id12 '),
(306, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id13 '),
(307, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id14 '),
(308, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id15 '),
(309, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id16 '),
(310, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id17 '),
(311, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id18 '),
(312, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id19 '),
(313, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id20 '),
(314, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id21 '),
(315, 1, 'Alta', '131.178.200.65', '2015-08-21', 'El usuario con el id 1  creo un modelo con el id22 '),
(316, 25, 'Alta', '201.159.124.138', '2015-08-22', 'El usuario con el id 25  creo un cliente con el id32 '),
(317, 25, 'Modificar', '201.159.124.138', '2015-08-22', 'El usuario con el id 25  modifico la medida con el id1 '),
(318, 25, 'Alta', '201.159.124.138', '2015-08-22', 'El usuario con el id: 25  creo una cotizacion con el id: 11 '),
(319, 25, 'Alta', '201.159.109.205', '2015-08-24', 'El usuario con el id 25  creo un cliente con el id33 '),
(320, 28, 'Alta', '201.159.109.205', '2015-09-17', 'El usuario con el id 28  creo un cliente con el id34 '),
(321, 1, 'Alta', '201.127.224.57', '2015-09-22', 'El usuario con el id: 1  creo una cotizacion con el id: 12 '),
(322, 25, 'Alta', '201.159.109.205', '2015-09-22', 'El usuario con el id 25  creo un cliente con el id35 '),
(323, 25, 'Alta', '201.159.109.205', '2015-09-22', 'El usuario con el id 25  creo un cliente con el id36 '),
(324, 25, 'Alta', '201.159.109.205', '2015-09-22', 'El usuario con el id 25  creo una medida con el id131 '),
(325, 25, 'Alta', '201.127.224.57', '2015-09-22', 'El usuario con el id: 25  creo una cotizacion con el id: 13 '),
(326, 25, 'Alta', '201.127.224.57', '2015-09-22', 'El usuario con el id: 25  creo una cotizacion con el id: 14 '),
(327, 1, 'Eliminar', '187.188.134.14', '2015-09-24', 'El usuario con el id 1 elimino al cliente con el id 24 '),
(328, 1, 'Eliminar', '187.188.134.14', '2015-09-24', 'El usuario con el id 1 elimino al cliente con el id 35 '),
(329, 1, 'Eliminar', '187.188.134.14', '2015-09-24', 'El usuario con el id 1 elimino al cliente con el id 33 '),
(330, 1, 'Alta', '187.188.134.14', '2015-09-24', 'El usuario con el id 1  creo un cliente con el id37 '),
(331, 25, 'Alta', '201.159.124.138', '2015-09-24', 'El usuario con el id 25  creo un cliente con el id38 '),
(332, 25, 'Alta', '201.159.124.138', '2015-09-24', 'El usuario con el id: 25  creo una cotizacion con el id: 15 '),
(333, 1, 'Alta', '187.188.134.14', '2015-09-24', 'El usuario con el id: 1  creo una cotizacion con el id: 16 '),
(334, 1, 'Alta', '187.188.134.14', '2015-09-24', 'El usuario con el id: 1  creo una cotizacion con el id: 17 '),
(335, 1, 'Alta', '187.188.134.14', '2015-09-24', 'El usuario con el id: 1  creo una cotizacion con el id: 18 '),
(336, 1, 'Alta', '187.188.134.14', '2015-09-24', 'El usuario con el id: 1  creo una cotizacion con el id: 19 '),
(337, 1, 'Alta', '187.188.134.14', '2015-09-24', 'El usuario con el id: 1  creo una cotizacion con el id: 20 '),
(338, 25, 'Alta', '201.159.124.138', '2015-09-24', 'El usuario con el id: 25  creo una cotizacion con el id: 21 '),
(339, 25, 'Alta', '201.159.124.138', '2015-09-24', 'El usuario con el id: 25  creo una cotizacion con el id: 22 '),
(340, 28, 'Alta', '187.138.16.28', '2015-09-24', 'El usuario con el id 28  creo un cliente con el id39 '),
(341, 28, 'Alta', '187.138.16.28', '2015-09-24', 'El usuario con el id: 28  creo una cotizacion con el id: 23 '),
(342, 1, 'Alta', '187.188.134.14', '2015-09-24', 'El usuario con el id: 1  creo una cotizacion con el id: 24 '),
(343, 25, 'Alta', '201.159.124.138', '2015-09-25', 'El usuario con el id 25  creo un cliente con el id40 '),
(344, 28, 'Eliminar', '187.138.16.28', '2015-09-25', 'El usuario con el id 28 elimino al cliente con el id 36 '),
(345, 28, 'Eliminar', '187.138.16.28', '2015-09-25', 'El usuario con el id 28 elimino al cliente con el id 39 '),
(346, 28, 'Alta', '187.138.16.28', '2015-09-25', 'El usuario con el id 28  creo un cliente con el id41 '),
(347, 25, 'Alta', '201.159.124.138', '2015-09-25', 'El usuario con el id: 25  creo una cotizacion con el id: 25 '),
(348, 25, 'Alta', '201.159.124.138', '2015-09-25', 'El usuario con el id: 25  creo una cotizacion con el id: 26 '),
(349, 1, 'Alta', '187.188.134.14', '2015-09-25', 'El usuario con el id: 1  creo una cotizacion con el id: 27 '),
(350, 1, 'Alta', '187.188.134.14', '2015-09-25', 'El usuario con el id: 1  creo una cotizacion con el id: 28 '),
(351, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id 28  creo un cliente con el id42 '),
(352, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id 28  creo un cliente con el id43 '),
(353, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id: 28  creo una cotizacion con el id: 29 '),
(354, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id: 28  creo una cotizacion con el id: 30 '),
(355, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id: 28  creo una cotizacion con el id: 31 '),
(356, 25, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id: 25  creo un usuario con el id: 44 '),
(357, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id 28  creo un cliente con el id45 '),
(358, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id: 28  creo una cotizacion con el id: 32 '),
(359, 28, 'Alta', '201.159.109.205', '2015-09-25', 'El usuario con el id: 28  creo una cotizacion con el id: 33 '),
(360, 1, 'Modificar', '201.127.202.104', '2015-10-06', 'El usuario con el id: 1  modifico al usuario con el id: 1 '),
(361, 25, 'Alta', '216.218.54.16', '2015-10-06', 'El usuario con el id 25  creo un cliente con el id46 '),
(362, 25, 'Alta', '216.218.54.16', '2015-10-06', 'El usuario con el id: 25  creo una cotizacion con el id: 34 '),
(363, 1, 'Alta', '187.188.134.14', '2015-10-06', 'El usuario con el id: 1  creo una cotizacion con el id: 35 '),
(364, 28, 'Alta', '201.159.109.205', '2015-10-06', 'El usuario con el id 28  creo un cliente con el id47 '),
(365, 28, 'Alta', '201.159.109.205', '2015-10-06', 'El usuario con el id: 28  creo una cotizacion con el id: 36 '),
(366, 28, 'Alta', '201.159.109.205', '2015-10-06', 'El usuario con el id: 28  creo una cotizacion con el id: 37 '),
(367, 28, 'Alta', '201.159.109.205', '2015-10-06', 'El usuario con el id: 28  creo una cotizacion con el id: 38 '),
(368, 28, 'Alta', '201.159.109.205', '2015-10-06', 'El usuario con el id: 28  creo una cotizacion con el id: 39 '),
(369, 28, 'Alta', '201.159.109.205', '2015-10-06', 'El usuario con el id: 28  creo una cotizacion con el id: 40 '),
(370, 28, 'Alta', '201.159.109.205', '2015-10-06', 'El usuario con el id: 28  creo una cotizacion con el id: 41 '),
(371, 1, 'Alta', '201.127.202.104', '2015-10-07', 'El usuario con el id: 1  creo una cotizacion con el id: 1 '),
(372, 1, 'Alta', '201.127.202.104', '2015-10-07', 'El usuario con el id: 1  creo una cotizacion con el id: 2 '),
(373, 1, 'Alta', '201.127.202.104', '2015-10-07', 'El usuario con el id: 1  creo una cotizacion con el id: 3 '),
(374, 1, 'Alta', '201.127.202.104', '2015-10-07', 'El usuario con el id: 1  creo una cotizacion con el id: 4 '),
(375, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id 28  creo un cliente con el id48 '),
(376, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id: 28  creo una cotizacion con el id: 5 '),
(377, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id: 28  creo una cotizacion con el id: 6 '),
(378, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id: 28  creo una cotizacion con el id: 7 '),
(379, 28, 'Eliminar', '201.159.109.205', '2015-10-08', 'El usuario con el id 28 elimino al cliente con el id 48 '),
(380, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id 28  creo un cliente con el id49 '),
(381, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id: 28  creo una cotizacion con el id: 8 '),
(382, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id: 28  creo una cotizacion con el id: 9 '),
(383, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id: 28  creo una cotizacion con el id: 10 '),
(384, 28, 'Alta', '201.159.109.205', '2015-10-08', 'El usuario con el id: 28  creo una cotizacion con el id: 11 '),
(385, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 12 '),
(386, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 13 '),
(387, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 14 '),
(388, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 15 '),
(389, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 16 '),
(390, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 17 '),
(391, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 18 '),
(392, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 19 '),
(393, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 20 '),
(394, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 21 '),
(395, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 22 '),
(396, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 23 '),
(397, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 24 '),
(398, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 25 '),
(399, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 26 '),
(400, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 27 '),
(401, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 28 '),
(402, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 29 '),
(403, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 30 '),
(404, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 31 '),
(405, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 32 '),
(406, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 33 '),
(407, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 34 '),
(408, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 35 '),
(409, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 36 '),
(410, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 37 '),
(411, 1, 'Alta', '189.211.237.230', '2015-10-13', 'El usuario con el id: 1  creo una cotizacion con el id: 38 '),
(412, 28, 'Alta', '201.159.109.205', '2015-10-13', 'El usuario con el id: 28  creo una cotizacion con el id: 39 '),
(413, 27, 'Alta', '201.159.109.205', '2015-10-21', 'El usuario con el id 27  creo un cliente con el id50 '),
(414, 27, 'Alta', '201.159.109.205', '2015-10-21', 'El usuario con el id: 27  creo una cotizacion con el id: 40 '),
(415, 27, 'Alta', '201.159.109.205', '2015-10-22', 'El usuario con el id 27  creo un cliente con el id51 '),
(416, 28, 'Alta', '189.211.237.230', '2015-10-22', 'El usuario con el id 28  creo un cliente con el id52 '),
(417, 28, 'Alta', '189.211.237.230', '2015-10-22', 'El usuario con el id: 28  creo una cotizacion con el id: 41 '),
(418, 25, 'Modificar', '216.218.54.16', '2015-10-22', 'El usuario con el id 25  modifico el modelo con el id11 '),
(419, 25, 'Modificar', '216.218.54.16', '2015-10-22', 'El usuario con el id 25  modifico el modelo con el id11 '),
(420, 26, 'Alta', '70.50.205.182', '2015-10-22', 'El usuario con el id 26  creo la moneda paola '),
(421, 25, 'Modificar', '216.218.54.16', '2015-10-22', 'El usuario con el id 25  modifico el componente con el id1 '),
(422, 25, 'Modificar', '216.218.54.16', '2015-10-22', 'El usuario con el id 25  modifico el componente con el id1 '),
(423, 28, 'Alta', '189.211.237.230', '2015-10-22', 'El usuario con el id: 28  creo una cotizacion con el id: 42 '),
(424, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 43 '),
(425, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 44 '),
(426, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 45 '),
(427, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 46 '),
(428, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 47 '),
(429, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 48 '),
(430, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 49 '),
(431, 1, 'Alta', '187.162.161.12', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 50 '),
(432, 1, 'Alta', '189.211.237.230', '2015-10-22', 'El usuario con el id: 1  creo una cotizacion con el id: 51 '),
(433, 1, 'Modificar', '189.211.237.230', '2015-10-27', 'El usuario con el id: 1  modifico al usuario con el id: 29 '),
(434, 27, 'Alta', '201.159.109.205', '2015-11-09', 'El usuario con el id 27  creo un cliente con el id53 '),
(435, 27, 'Alta', '201.159.109.205', '2015-11-09', 'El usuario con el id: 27  creo una cotizacion con el id: 52 '),
(436, 1, 'Alta', '189.211.237.230', '2015-11-19', 'El usuario con el id 1  creo un cliente con el id54 '),
(437, 1, 'Modificar', '189.211.237.230', '2015-11-24', 'El usuario con el id: 1  modifico al usuario con el id: 1 '),
(438, 27, 'Alta', '201.159.109.205', '2015-11-24', 'El usuario con el id 27  creo un cliente con el id55 '),
(439, 27, 'Alta', '201.159.109.205', '2015-11-24', 'El usuario con el id 27  creo un cliente con el id56 '),
(440, 1, 'Alta', '189.211.237.230', '2015-11-30', 'El usuario con el id: 1  creo una cotizacion con el id: 53 '),
(441, 27, 'Alta', '201.159.109.205', '2015-12-01', 'El usuario con el id: 27  creo una cotizacion con el id: 54 ');

-- --------------------------------------------------------

--
-- Table structure for table `ventana_color`
--

CREATE TABLE IF NOT EXISTS `ventana_color` (
  `id_color` int(11) NOT NULL,
  `nom_color` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `descripcion` text,
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ventana_color`
--

INSERT INTO `ventana_color` (`id_color`, `nom_color`, `activo`, `descripcion`, `id_log`) VALUES
(1, 'Blanco', 1, 'Color <br><blockquote>-Blanco<br></blockquote>', NULL),
(2, 'Azul', 1, 'Test', 40),
(3, 'Negro', 1, 'Color Azul<br>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ventana_medida`
--

CREATE TABLE IF NOT EXISTS `ventana_medida` (
  `id_medida` int(11) NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `id_medidas` int(11) DEFAULT NULL,
  `cantidad` int(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ventana_medida`
--

INSERT INTO `ventana_medida` (`id_medida`, `id_tipo`, `id_medidas`, `cantidad`, `activo`, `id_log`) VALUES
(1, 1, 1, 5, 1, 37);

-- --------------------------------------------------------

--
-- Table structure for table `ventana_modelo`
--

CREATE TABLE IF NOT EXISTS `ventana_modelo` (
  `id_modelo` int(11) NOT NULL,
  `nom_modelo` varchar(255) DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `proveedor` varchar(255) DEFAULT NULL,
  `costo` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ventana_modelo`
--

INSERT INTO `ventana_modelo` (`id_modelo`, `nom_modelo`, `id_tipo`, `tipo`, `proveedor`, `costo`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Asthon', 1, 'Importado', 'Prov', '0.0', 'Modelo (Solo residencial).', 1, 105),
(2, 'Asthonn', 1, NULL, NULL, NULL, 'Modelo (Solo residencial).', 0, NULL),
(3, 'Cascade', 1, 'Importado', 'Temporal', '$0.0', NULL, 1, NULL),
(4, 'Cathedral', 1, 'Importado', 'Agregar', '$0.0', NULL, 1, NULL),
(5, 'Delicato', 1, 'Importado', 'Agregar', '$0.0', NULL, 1, NULL),
(6, 'Divin', 1, 'Importado', 'Agregar', '$0.0', NULL, 1, NULL),
(7, 'Fascino', 1, 'Importado', 'Agregar', '$0.0', NULL, 1, NULL),
(8, 'Fort', 1, 'Importado', 'Agregar', '$0.0', NULL, 1, NULL),
(9, 'Madison', 1, 'Importado', 'Agregar', '$0.0', NULL, 1, NULL),
(10, 'Muse', 1, 'Importado', 'Agregar', '$0.0', NULL, 1, NULL),
(11, 'Ruston', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(12, 'Sicura', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(13, 'Stockbridge', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(14, 'Stockford', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(15, 'Stockton', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(16, 'Waterton', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(17, 'Williamburg (03)', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(18, 'Williamburg (04)', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(19, 'Williamburg (05)', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(20, 'Williambiurg (06)', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(21, 'Williamburg (07)', 1, 'Importado', NULL, NULL, NULL, 1, NULL),
(22, 'Williamburg (08)', 1, 'Importado', NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ventana_tipo`
--

CREATE TABLE IF NOT EXISTS `ventana_tipo` (
  `id_tipo` int(11) NOT NULL,
  `nom_tipo` varchar(255) DEFAULT NULL,
  `tasa_cambio` float(18,2) DEFAULT NULL,
  `iva` float(18,2) DEFAULT NULL,
  `vfusa` float(18,2) DEFAULT NULL,
  `va` float(18,2) DEFAULT NULL,
  `vb` float(18,2) DEFAULT NULL,
  `vc` float(18,2) DEFAULT NULL,
  `vd` float(18,2) DEFAULT NULL,
  `ve` float(18,2) DEFAULT NULL,
  `vf` float(18,2) DEFAULT NULL,
  `vg` float(18,2) DEFAULT NULL,
  `vh` float(18,2) DEFAULT NULL,
  `vi` float(18,2) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ventana_tipo`
--

INSERT INTO `ventana_tipo` (`id_tipo`, `nom_tipo`, `tasa_cambio`, `iva`, `vfusa`, `va`, `vb`, `vc`, `vd`, `ve`, `vf`, `vg`, `vh`, `vi`, `activo`, `id_log`) VALUES
(1, 'Residencial', 15.00, 1.16, 40.00, 1740.00, 1162.32, 969.53, 842.16, 765.60, 696.00, 633.36, 549.84, 478.15, 1, 42),
(2, 'Industrial', 15.00, 1.16, 60.00, 2610.00, 1743.48, 1454.29, 1263.24, 1148.40, 1044.00, 950.04, 824.76, 717.23, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `componentes`
--
ALTER TABLE `componentes`
  ADD PRIMARY KEY (`id_componente`);

--
-- Indexes for table `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- Indexes for table `cotizacion_puerta`
--
ALTER TABLE `cotizacion_puerta`
  ADD PRIMARY KEY (`id_cot`);

--
-- Indexes for table `cotpta_totales`
--
ALTER TABLE `cotpta_totales`
  ADD PRIMARY KEY (`id_cot`);

--
-- Indexes for table `des_componentes`
--
ALTER TABLE `des_componentes`
  ADD PRIMARY KEY (`id_descomp`);

--
-- Indexes for table `divisas`
--
ALTER TABLE `divisas`
  ADD PRIMARY KEY (`id_divisas`);

--
-- Indexes for table `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`id_domicilio`);

--
-- Indexes for table `herreria`
--
ALTER TABLE `herreria`
  ADD PRIMARY KEY (`id_herreria`);

--
-- Indexes for table `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indexes for table `puerta_color`
--
ALTER TABLE `puerta_color`
  ADD PRIMARY KEY (`id_color`);

--
-- Indexes for table `puerta_componentes`
--
ALTER TABLE `puerta_componentes`
  ADD PRIMARY KEY (`id_puerta_comp`);

--
-- Indexes for table `puerta_medida`
--
ALTER TABLE `puerta_medida`
  ADD PRIMARY KEY (`id_medida`);

--
-- Indexes for table `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  ADD PRIMARY KEY (`id_modelo`);

--
-- Indexes for table `puerta_mov`
--
ALTER TABLE `puerta_mov`
  ADD PRIMARY KEY (`id_mov`);

--
-- Indexes for table `puerta_textura`
--
ALTER TABLE `puerta_textura`
  ADD PRIMARY KEY (`id_textura`);

--
-- Indexes for table `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indexes for table `tp_usuario`
--
ALTER TABLE `tp_usuario`
  ADD PRIMARY KEY (`id_tpusuario`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indexes for table `usuario_log`
--
ALTER TABLE `usuario_log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `ventana_color`
--
ALTER TABLE `ventana_color`
  ADD PRIMARY KEY (`id_color`);

--
-- Indexes for table `ventana_medida`
--
ALTER TABLE `ventana_medida`
  ADD PRIMARY KEY (`id_medida`);

--
-- Indexes for table `ventana_modelo`
--
ALTER TABLE `ventana_modelo`
  ADD PRIMARY KEY (`id_modelo`);

--
-- Indexes for table `ventana_tipo`
--
ALTER TABLE `ventana_tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `componentes`
--
ALTER TABLE `componentes`
  MODIFY `id_componente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `cotizacion_puerta`
--
ALTER TABLE `cotizacion_puerta`
  MODIFY `id_cot` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `cotpta_totales`
--
ALTER TABLE `cotpta_totales`
  MODIFY `id_cot` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `des_componentes`
--
ALTER TABLE `des_componentes`
  MODIFY `id_descomp` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `divisas`
--
ALTER TABLE `divisas`
  MODIFY `id_divisas` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `id_domicilio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `herreria`
--
ALTER TABLE `herreria`
  MODIFY `id_herreria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT for table `puerta_color`
--
ALTER TABLE `puerta_color`
  MODIFY `id_color` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `puerta_componentes`
--
ALTER TABLE `puerta_componentes`
  MODIFY `id_puerta_comp` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `puerta_medida`
--
ALTER TABLE `puerta_medida`
  MODIFY `id_medida` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT for table `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  MODIFY `id_modelo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `puerta_mov`
--
ALTER TABLE `puerta_mov`
  MODIFY `id_mov` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `puerta_textura`
--
ALTER TABLE `puerta_textura`
  MODIFY `id_textura` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tp_usuario`
--
ALTER TABLE `tp_usuario`
  MODIFY `id_tpusuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `usuario_log`
--
ALTER TABLE `usuario_log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=442;
--
-- AUTO_INCREMENT for table `ventana_color`
--
ALTER TABLE `ventana_color`
  MODIFY `id_color` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ventana_medida`
--
ALTER TABLE `ventana_medida`
  MODIFY `id_medida` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ventana_modelo`
--
ALTER TABLE `ventana_modelo`
  MODIFY `id_modelo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ventana_tipo`
--
ALTER TABLE `ventana_tipo`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
