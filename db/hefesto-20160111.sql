-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-01-2016 a las 22:12:09
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `hefesto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
`id_cliente` int(11) NOT NULL,
  `tipo` int(11) DEFAULT '1',
  `activo` tinyint(1) DEFAULT '1',
  `aprobado` tinyint(1) DEFAULT '0',
  `nombre_cliente` varchar(256) DEFAULT NULL,
  `divisa_cliente` int(11) DEFAULT NULL,
  `tipo_costo` varchar(5) DEFAULT NULL,
  `motivo_costo` text,
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `razon_social` varchar(256) DEFAULT NULL,
  `rfc` varchar(20) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `nombre_autorizado` varchar(256) DEFAULT NULL,
  `email_autorizado` varchar(128) DEFAULT NULL,
  `telefono_autorizado` varchar(64) DEFAULT NULL,
  `ext_autorizado` varchar(32) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `tipo`, `activo`, `aprobado`, `nombre_cliente`, `divisa_cliente`, `tipo_costo`, `motivo_costo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `razon_social`, `rfc`, `id_domicilio`, `nombre_autorizado`, `email_autorizado`, `telefono_autorizado`, `ext_autorizado`, `id_usuario_alta`, `fecha_alta`, `id_log`) VALUES
(1, 1, 1, 0, 'Sams', 2, '2', 'Este es un cliente de prueba', 'Marco Martinez', 'marco@test.com', '324452342332', '12344', 'SSM121212M3A', 'Sams de Mexico, SA d', 6, 'Pedro Palma', 'pedro@test.com', '24346345432', '2342', 1, '2016-01-11 08:28:57', 37),
(2, 2, 1, 0, 'asddddddddddddd', 2, '1', NULL, 'fdfffffffffffffffff', 'ssssssssssssss', NULL, NULL, 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 9, NULL, NULL, NULL, NULL, 1, '2016-01-11 08:44:14', 38),
(3, 1, 0, 0, 'Celular Express', 2, '1', 'asdfsfdaafad', 'asdf', 'ssssssssssssss', NULL, NULL, 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 10, NULL, NULL, NULL, NULL, 1, '2016-01-11 08:44:52', 39),
(4, 1, 1, 0, 'Celular Express', 2, '1', 'Sin motivo aparente', 'asdf', 'ssssssssssssss', '233333', '123', 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 11, 'dkljlfsdñ', 'dldkldakldf', '22222', '3232', 1, '2016-01-11 09:06:20', 48),
(5, 2, 1, 0, 'Paquito Lopez', 2, '2', 'Precio de siempre', 'Paco Lopez Lopez', 'paco@lopez.com ', NULL, NULL, 'TEst', NULL, 12, NULL, NULL, NULL, NULL, 1, '2016-01-11 11:30:38', 49);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes`
--

CREATE TABLE IF NOT EXISTS `componentes` (
`id_componente` int(11) NOT NULL,
  `id_componente_grupo` int(11) DEFAULT NULL,
  `nombre_componente` varchar(128) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes_grupos`
--

CREATE TABLE IF NOT EXISTS `componentes_grupos` (
`id_grupo_componente` int(11) NOT NULL,
  `grupo_componente` varchar(128) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divisas`
--

CREATE TABLE IF NOT EXISTS `divisas` (
`id_divisa` int(11) NOT NULL,
  `abbrev` varchar(5) DEFAULT NULL,
  `nombre` varchar(128) DEFAULT NULL,
  `tipo_cambio` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `divisas`
--

INSERT INTO `divisas` (`id_divisa`, `abbrev`, `nombre`, `tipo_cambio`, `activo`, `id_log`) VALUES
(1, 'USD', 'Dolar Americano', 17.35, 1, 0),
(2, 'MXN', 'Peso Mexicano', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilios`
--

CREATE TABLE IF NOT EXISTS `domicilios` (
`id_domicilio` int(11) NOT NULL,
  `calle` varchar(256) DEFAULT NULL,
  `no_exterior` varchar(32) DEFAULT NULL,
  `no_interior` varchar(32) DEFAULT NULL,
  `colonia` varchar(256) DEFAULT NULL,
  `localidad` varchar(256) DEFAULT NULL,
  `referencia` text,
  `municipio` varchar(256) DEFAULT NULL,
  `estado` varchar(64) DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `codigo_postal` varchar(32) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `domicilios`
--

INSERT INTO `domicilios` (`id_domicilio`, `calle`, `no_exterior`, `no_interior`, `colonia`, `localidad`, `referencia`, `municipio`, `estado`, `id_pais`, `codigo_postal`, `fecha_alta`) VALUES
(1, 'Francisco Javier Mina', '312', 'N/A', 'Valles de Huinalá', 'Bodega industrial', 'Monterrey', 'Apodaca', 'Nuevo León', 117, '66600', '2016-01-07 23:11:00'),
(2, 'Test', NULL, '221', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-07 23:11:53'),
(3, '33333333333', '22', '45555555555', 'Las pruebas', 'salkdjf', 'Casa sola', 'klsdf', 'alksdf', 17, '3452', '2016-01-07 23:22:25'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-07 23:23:10'),
(5, NULL, 'ext', 'int', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 00:29:56'),
(6, 'Av. de la Palma', '234', '21', 'Palmira', 'Edificio blanco', 'Ciudad de México', 'Alvaro Obregón', 'Distrito Federal', 117, '03630', '2016-01-11 08:28:57'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 08:42:12'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 08:42:45'),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 08:44:14'),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 08:44:52'),
(11, 'asdfasdfasd', 'askdlfja', 'lasl', 'aslkfkdas', 'aslkdlk', NULL, 'lkasjafñlkajsdf', 'askflksadjfdsal', 113, '77777', '2016-01-11 09:06:20'),
(12, 'Callejón de los Milagros', '123', NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 11:30:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kit`
--

CREATE TABLE IF NOT EXISTS `kit` (
`id_kit` int(11) NOT NULL,
  `nombre` int(11) DEFAULT NULL,
  `tipo` varchar(16) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE IF NOT EXISTS `log` (
`id_log` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `evento` varchar(32) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `descripcion` text,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`id_log`, `id_usuario`, `evento`, `ip`, `descripcion`, `fecha`) VALUES
(1, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 11:01:29'),
(2, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 11:02:01'),
(3, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 2 ', '2016-01-07 03:50:47'),
(4, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 3 ', '2016-01-07 03:51:33'),
(5, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 2 ', '2016-01-07 03:51:38'),
(6, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 03:52:09'),
(7, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 03:52:34'),
(8, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 03:53:44'),
(9, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 4 ', '2016-01-07 03:54:24'),
(10, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 03:59:34'),
(11, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 5 ', '2016-01-07 04:00:13'),
(12, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 04:16:38'),
(13, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 04:17:27'),
(14, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 04:33:11'),
(15, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 05:13:43'),
(16, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 06:57:09'),
(17, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 06:57:17'),
(18, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 6 ', '2016-01-07 07:36:05'),
(19, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 07:36:39'),
(20, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 07:37:03'),
(21, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 07:39:33'),
(22, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 07:39:48'),
(23, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:11:00'),
(24, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:11:53'),
(25, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al proveedor con el id 1 ', '2016-01-07 23:21:58'),
(26, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:22:26'),
(27, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:23:11'),
(28, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-07 23:43:00'),
(29, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-07 23:43:56'),
(30, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-07 23:45:09'),
(31, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 00:27:57'),
(32, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 00:28:26'),
(33, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 4 ', '2016-01-08 00:28:41'),
(34, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 1 ', '2016-01-08 00:29:31'),
(35, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 00:29:56'),
(36, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 5 ', '2016-01-08 00:31:09'),
(37, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 1 ', '2016-01-11 08:28:57'),
(38, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 2 ', '2016-01-11 08:44:14'),
(39, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 3 ', '2016-01-11 08:44:52'),
(40, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 4 ', '2016-01-11 09:06:20'),
(41, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:08:18'),
(42, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 3 ', '2016-01-11 09:08:31'),
(43, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 3 ', '2016-01-11 09:09:03'),
(44, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:09:16'),
(45, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:24:54'),
(46, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:25:46'),
(47, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:26:11'),
(48, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:27:26'),
(49, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 5 ', '2016-01-11 11:30:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multiplicadores`
--

CREATE TABLE IF NOT EXISTS `multiplicadores` (
`id_multiplicador` int(11) NOT NULL,
  `nombre_multiplicador` varchar(16) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `multiplicadores`
--

INSERT INTO `multiplicadores` (`id_multiplicador`, `nombre_multiplicador`, `valor`, `activo`, `id_log`) VALUES
(1, 'VA', 2.5, 1, 0),
(2, 'VB', 1.67, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE IF NOT EXISTS `pais` (
`id_pais` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=195 ;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `nombre`) VALUES
(1, 'Afganistán'),
(2, 'Albania'),
(3, 'Alemania'),
(4, 'Andorra'),
(5, 'Angola'),
(6, 'Antigua y Barbuda'),
(7, 'Arabia Saudita'),
(8, 'Argelia'),
(9, 'Argentina'),
(10, 'Armenia'),
(11, 'Australia'),
(12, 'Austria'),
(13, 'Azerbaiyán'),
(14, 'Bahamas'),
(15, 'Bangladesh'),
(16, 'Barbados'),
(17, 'Baréin'),
(18, 'Bélgica'),
(19, 'Belice'),
(20, 'Benín'),
(21, 'Bielorrusia'),
(22, 'Birmania'),
(23, 'Bolivia'),
(24, 'Bosnia-Herzegovina'),
(25, 'Botsuana'),
(26, 'Brasil'),
(27, 'Brunéi'),
(28, 'Bulgaria'),
(29, 'Burkina Faso'),
(30, 'Burundi'),
(31, 'Bután'),
(32, 'Cabo Verde'),
(33, 'Camboya'),
(34, 'Camerún'),
(35, 'Canadá'),
(36, 'Chad'),
(37, 'Chile'),
(38, 'China'),
(39, 'Chipre'),
(40, 'Ciudad del Vaticano'),
(41, 'Colombia'),
(42, 'Comoras'),
(43, 'Corea del Norte'),
(44, 'Corea del Sur'),
(45, 'Costa de Marfil'),
(46, 'Costa Rica'),
(47, 'Croacia'),
(48, 'Cuba'),
(49, 'Dinamarca'),
(50, 'Dominica'),
(51, 'Ecuador'),
(52, 'Egipto'),
(53, 'El Salvador'),
(54, 'Emiratos Árabes Unidos'),
(55, 'Eritrea'),
(56, 'Eslovaquia'),
(57, 'Eslovenia'),
(58, 'España'),
(59, 'Estados Unidos'),
(60, 'Estonia'),
(61, 'Etiopia'),
(62, 'Filipinas'),
(63, 'Finlandia'),
(64, 'Fiyi'),
(65, 'Francia'),
(66, 'Gabón'),
(67, 'Gambia'),
(68, 'Georgia'),
(69, 'Ghana'),
(70, 'Granada'),
(71, 'Grecia'),
(72, 'Guatemala'),
(73, 'Guyana'),
(74, 'Guinea'),
(75, 'Guinea ecuatorial'),
(76, 'Guinea-Bisáu'),
(77, 'Haití'),
(78, 'Holanda'),
(79, 'Honduras'),
(80, 'Hungría'),
(81, 'India'),
(82, 'Indonesia'),
(83, 'Irak'),
(84, 'Irán'),
(85, 'Irlanda'),
(86, 'Islandia'),
(87, 'Islas Marshall'),
(88, 'Islas Salomón'),
(89, 'Israel'),
(90, 'Italia'),
(91, 'Jamaica'),
(92, 'Japón'),
(93, 'Jordania'),
(94, 'Kazajistán'),
(95, 'Kenia'),
(96, 'Kirguistán'),
(97, 'Kiribati'),
(98, 'Kuwait'),
(99, 'Laos'),
(100, 'Lesoto'),
(101, 'Letonia'),
(102, 'Libano'),
(103, 'Liberia'),
(104, 'Libia'),
(105, 'Liechtenstein'),
(106, 'Lituania'),
(107, 'Luxemburgo'),
(108, 'Madagascar'),
(109, 'Malasia'),
(110, 'Malaui'),
(111, 'Maldivas'),
(112, 'Malí'),
(113, 'Malta'),
(114, 'Marruecos'),
(115, 'Mauricio'),
(116, 'Mauritania'),
(117, 'México'),
(118, 'Micronesia'),
(119, 'Moldavia'),
(120, 'Mónaco'),
(121, 'Mongolia'),
(122, 'Montenegro'),
(123, 'Mozambique'),
(124, 'Namibia'),
(125, 'Nauru'),
(126, 'Nepal'),
(127, 'Nicaragua'),
(128, 'Níger'),
(129, 'Nigeria'),
(130, 'Noruega'),
(131, 'Nueva Zelanda'),
(132, 'Omán'),
(133, 'Pakistán'),
(134, 'Palaos'),
(135, 'Panamá'),
(136, 'Papua Nueva Guinea'),
(137, 'Paraguay'),
(138, 'Perú'),
(139, 'Polonia'),
(140, 'Portugal'),
(141, 'Qatar'),
(142, 'Reino Unido'),
(143, 'República Centroafricana'),
(144, 'República Checa'),
(145, 'República de Macedonia'),
(146, 'República del Congo'),
(147, 'República Democratica del Congo'),
(148, 'República Dominicana'),
(149, 'República Sudafricana'),
(150, 'Ruanda'),
(151, 'Rumania'),
(152, 'Rusia'),
(153, 'Samoa'),
(154, 'San Cristóbal y Nieves'),
(155, 'San Marino'),
(156, 'San Vicente y las Granadinas'),
(157, 'Santa Lucía'),
(158, 'Santo Tomé y Príncipe'),
(159, 'Senegal'),
(160, 'Serbia'),
(161, 'Seychelles'),
(162, 'Sierra Leona'),
(163, 'Singapur'),
(164, 'Siria'),
(165, 'Somalia'),
(166, 'Sri Lanka'),
(167, 'Suazilandia'),
(168, 'Sudán'),
(169, 'Sudán del Sur'),
(170, 'Suecia'),
(171, 'Suiza'),
(172, 'Surinam'),
(173, 'Tailandia'),
(174, 'Tanzania'),
(175, 'Tayikistán'),
(176, 'Timor Oriental'),
(177, 'Togo'),
(178, 'Tonga'),
(179, 'Trinidad y Tobago'),
(180, 'Túnez'),
(181, 'Turkmenistán'),
(182, 'Turquía'),
(183, 'Tuvalu'),
(184, 'Ucrania'),
(185, 'Uganda'),
(186, 'Uruguay'),
(187, 'Uzbekistán'),
(188, 'Vanuatu'),
(189, 'Venezuela'),
(190, 'Vietnam'),
(191, 'Yemen'),
(192, 'Yibuti'),
(193, 'Zambia'),
(194, 'Zimbawe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
`id_permiso` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permiso`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Administrador', 1, NULL),
(2, 'Director', 1, NULL),
(3, 'Director Produccion', 1, NULL),
(4, 'Direcctor Ventas', 1, NULL),
(5, 'Cliente', 1, NULL),
(6, 'Vendedor', 1, NULL),
(7, 'Produccion', 1, NULL),
(8, 'Despacho', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
`id_proveedor` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `tipo` varchar(16) DEFAULT NULL,
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `nombre`, `tipo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `id_domicilio`, `id_usuario_alta`, `fecha_alta`, `activo`, `id_log`) VALUES
(1, 'HS puertas de garaje', 'Nacional', 'Guillermo Ortiz', 'test@hs.com', '8112341000', '112', 1, 1, '2016-01-07 23:11:00', 1, 34),
(2, 'test', 'Nacional', '0', NULL, NULL, NULL, 2, 1, '2016-01-07 23:11:53', 1, 24),
(3, 'Pruebas de desarrollo', 'Nacional', 'jñlksdfaj', 'esteesuncorreoelectonicomuylargoparaprobarlabd@testingdecorreo.com', '342332423', '232', 3, 1, '2016-01-07 23:22:26', 1, 32),
(4, 'Juan Javier Jaime Gabriel', 'Nacional', NULL, NULL, NULL, NULL, 4, 1, '2016-01-07 23:23:10', 1, 33),
(5, 'test', 'Nacional', NULL, NULL, NULL, NULL, 5, 1, '2016-01-08 00:29:56', 1, 36);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta`
--

CREATE TABLE IF NOT EXISTS `puerta` (
`id_puerta` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `m_ancho` double DEFAULT NULL,
  `m_alto` double DEFAULT NULL,
  `f_ancho` double DEFAULT NULL,
  `in_ancho` double DEFAULT NULL,
  `f_alto` double DEFAULT NULL,
  `in_alto` double DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `puerta`
--

INSERT INTO `puerta` (`id_puerta`, `id_ptipo`, `m_ancho`, `m_alto`, `f_ancho`, `in_ancho`, `f_alto`, `in_alto`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 1, 3.05, 2.1, 10, 0, 7, 0, 420, 1.15, 1, 0),
(2, 1, 3.05, 2.4, 10, 0, 8, 0, 480, 1.15, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_color`
--

CREATE TABLE IF NOT EXISTS `puerta_color` (
`id_pcolor` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `default` smallint(6) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `puerta_color`
--

INSERT INTO `puerta_color` (`id_pcolor`, `nombre_color`, `descripcion`, `default`, `activo`, `id_log`) VALUES
(1, 'Blanco', NULL, 2, 1, 0),
(2, 'Almendra', NULL, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_componente`
--

CREATE TABLE IF NOT EXISTS `puerta_componente` (
  `id_puerta` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_modelo`
--

CREATE TABLE IF NOT EXISTS `puerta_modelo` (
`id_pmodelo` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_modelo` text,
  `descripcion` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_movimiento`
--

CREATE TABLE IF NOT EXISTS `puerta_movimiento` (
`id_pmovimiento` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_movimiento` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_textura`
--

CREATE TABLE IF NOT EXISTS `puerta_textura` (
`id_ptextura` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_textura` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `puerta_textura`
--

INSERT INTO `puerta_textura` (`id_ptextura`, `id_proveedor`, `nombre_textura`, `descripcion`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Madera', 'Textura por defecto', 0, NULL, 1, 1, 0),
(2, NULL, 'Lisa', NULL, 30, 1.15, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_tipo`
--

CREATE TABLE IF NOT EXISTS `puerta_tipo` (
`id_ptipo` int(11) NOT NULL,
  `nombre_tipo` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `puerta_tipo`
--

INSERT INTO `puerta_tipo` (`id_ptipo`, `nombre_tipo`, `default`, `activo`, `id_log`) VALUES
(1, 'Residencial', 1, 1, 0),
(2, 'Industrial', 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_ventana`
--

CREATE TABLE IF NOT EXISTS `puerta_ventana` (
  `id_puerta` int(11) DEFAULT NULL,
  `id_ventana` int(11) DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id_usuario` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `pwd` varchar(64) DEFAULT NULL,
  `id_divisa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `nombre_usuario` varchar(256) DEFAULT NULL,
  `email_usuario` varchar(128) DEFAULT NULL,
  `telefono_usuario` varchar(32) DEFAULT NULL,
  `ext_usuario` varchar(16) DEFAULT NULL,
  `num_empleado` varchar(32) DEFAULT NULL,
  `permiso_usuario` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `accesos` int(11) DEFAULT '0',
  `id_log` int(11) DEFAULT '0',
  `ultimo_acceso` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `username`, `pwd`, `id_divisa`, `id_cliente`, `nombre_usuario`, `email_usuario`, `telefono_usuario`, `ext_usuario`, `num_empleado`, `permiso_usuario`, `fecha_alta`, `activo`, `accesos`, `id_log`, `ultimo_acceso`) VALUES
(1, 'admintk', 'Teknik', 2, NULL, 'Teknik', 'webmaster@teknik.mx', '(81) 1931 4009', NULL, NULL, 1, '2016-01-06 06:40:29', 1, 9, 22, '2016-01-11 21:08:31'),
(2, 'pp', 'perez', 1, NULL, 'Jose', 'pp@perez.com', NULL, NULL, NULL, 1, '2016-01-07 03:50:47', 0, 0, 5, '2016-01-07 03:51:38'),
(3, 'pp', 'perez', 1, NULL, 'Jose', 'pp@perez.com', '3233', '32', '3232', 8, '2016-01-07 03:51:33', 1, 1, 17, '2016-01-07 06:57:18'),
(4, 'lopez', 'juan', 2, NULL, 'Juan Lopez', 'juan@lopez.co', '345', '22', '222', 5, '2016-01-07 03:54:23', 1, 0, 13, '2016-01-07 04:17:27'),
(5, 'asdf', 'asdf', 1, NULL, 'asdfasd', 'asdf@askdf.com', NULL, NULL, '21221', 5, '2016-01-07 04:00:13', 1, 0, 21, '2016-01-07 07:39:33'),
(6, 'asdf', 'asfd', 1, NULL, 'asdf', 'test@prueba.com', '222221111', NULL, '2222', 8, '2016-01-07 07:36:05', 1, 0, 20, '2016-01-07 07:37:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventana`
--

CREATE TABLE IF NOT EXISTS `ventana` (
`id_ventana` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_ventana` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `ventana`
--

INSERT INTO `ventana` (`id_ventana`, `id_ptipo`, `id_proveedor`, `nombre_ventana`, `descripcion`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 1, NULL, 'Ashton', NULL, 30, 15, 1, 0),
(2, 1, NULL, 'Cascade', NULL, 50, 15, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventana_color`
--

CREATE TABLE IF NOT EXISTS `ventana_color` (
`id_vcolor` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `componentes`
--
ALTER TABLE `componentes`
 ADD PRIMARY KEY (`id_componente`);

--
-- Indices de la tabla `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
 ADD PRIMARY KEY (`id_grupo_componente`);

--
-- Indices de la tabla `divisas`
--
ALTER TABLE `divisas`
 ADD PRIMARY KEY (`id_divisa`);

--
-- Indices de la tabla `domicilios`
--
ALTER TABLE `domicilios`
 ADD PRIMARY KEY (`id_domicilio`);

--
-- Indices de la tabla `kit`
--
ALTER TABLE `kit`
 ADD PRIMARY KEY (`id_kit`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`id_log`);

--
-- Indices de la tabla `multiplicadores`
--
ALTER TABLE `multiplicadores`
 ADD PRIMARY KEY (`id_multiplicador`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
 ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
 ADD PRIMARY KEY (`id_permiso`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
 ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `puerta`
--
ALTER TABLE `puerta`
 ADD PRIMARY KEY (`id_puerta`);

--
-- Indices de la tabla `puerta_color`
--
ALTER TABLE `puerta_color`
 ADD PRIMARY KEY (`id_pcolor`);

--
-- Indices de la tabla `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
 ADD PRIMARY KEY (`id_pmodelo`);

--
-- Indices de la tabla `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
 ADD PRIMARY KEY (`id_pmovimiento`);

--
-- Indices de la tabla `puerta_textura`
--
ALTER TABLE `puerta_textura`
 ADD PRIMARY KEY (`id_ptextura`);

--
-- Indices de la tabla `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
 ADD PRIMARY KEY (`id_ptipo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `ventana`
--
ALTER TABLE `ventana`
 ADD PRIMARY KEY (`id_ventana`);

--
-- Indices de la tabla `ventana_color`
--
ALTER TABLE `ventana_color`
 ADD PRIMARY KEY (`id_vcolor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `componentes`
--
ALTER TABLE `componentes`
MODIFY `id_componente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
MODIFY `id_grupo_componente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `divisas`
--
ALTER TABLE `divisas`
MODIFY `id_divisa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `domicilios`
--
ALTER TABLE `domicilios`
MODIFY `id_domicilio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `kit`
--
ALTER TABLE `kit`
MODIFY `id_kit` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `multiplicadores`
--
ALTER TABLE `multiplicadores`
MODIFY `id_multiplicador` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `puerta`
--
ALTER TABLE `puerta`
MODIFY `id_puerta` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_color`
--
ALTER TABLE `puerta_color`
MODIFY `id_pcolor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
MODIFY `id_pmodelo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
MODIFY `id_pmovimiento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `puerta_textura`
--
ALTER TABLE `puerta_textura`
MODIFY `id_ptextura` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
MODIFY `id_ptipo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `ventana`
--
ALTER TABLE `ventana`
MODIFY `id_ventana` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventana_color`
--
ALTER TABLE `ventana_color`
MODIFY `id_vcolor` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
