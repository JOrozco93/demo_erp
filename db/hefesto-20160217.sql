-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generaciÃ³n: 18-02-2016 a las 01:54:13
-- VersiÃ³n del servidor: 10.1.9-MariaDB
-- VersiÃ³n de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hefesto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `tipo` int(11) DEFAULT '1',
  `activo` tinyint(1) DEFAULT '1',
  `aprobado` tinyint(1) DEFAULT '0',
  `motivo_aprobado` text,
  `nombre_cliente` varchar(256) DEFAULT NULL,
  `divisa_cliente` int(11) DEFAULT NULL,
  `tipo_costo` varchar(5) DEFAULT NULL,
  `motivo_costo` text,
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `razon_social` varchar(256) DEFAULT NULL,
  `rfc` varchar(20) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `nombre_autorizado` varchar(256) DEFAULT NULL,
  `email_autorizado` varchar(128) DEFAULT NULL,
  `telefono_autorizado` varchar(64) DEFAULT NULL,
  `ext_autorizado` varchar(32) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `tipo`, `activo`, `aprobado`, `motivo_aprobado`, `nombre_cliente`, `divisa_cliente`, `tipo_costo`, `motivo_costo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `razon_social`, `rfc`, `id_domicilio`, `nombre_autorizado`, `email_autorizado`, `telefono_autorizado`, `ext_autorizado`, `id_usuario_alta`, `fecha_alta`, `id_log`) VALUES
(1, 1, 1, 2, NULL, 'Sams', 2, '2', 'Este es un cliente de prueba', 'Marco Martinez', 'marco@test.com', '324452342332', '12344', 'SSM121212M3A', 'Sams de Mexico, SA d', 6, 'Pedro Palma', 'pedro@test.com', '24346345432', '2342', 1, '2016-01-11 08:28:57', 0),
(2, 2, 1, 0, NULL, 'Instalaciones modernas', 2, '2', 'jlÃ±ka sjfÃ±lkjsaÃ± fasldkjf lasdkf', 'fdfffffffffffffffff', 'ssssssssssssss', '32465635345655', NULL, 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 9, NULL, NULL, NULL, 'sss', 1, '2016-01-11 08:44:14', 0),
(3, 1, 1, 1, NULL, 'Celular Express', 2, '1', 'asdfsfdaafad', 'asdf', 'ssssssssssssss', '8987988', NULL, 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 10, 'Pedro Perez', 'perez@perez.com', '3221332', NULL, 1, '2016-01-11 08:44:52', 0),
(4, 1, 0, 0, NULL, 'Vista Hermosa', 2, '1', 'Sin motivo aparente', 'asdf', 'ssssssssssssss', '233333', '123', 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 11, 'dkljlfsdÃ±', 'dldkldakldf', '22222', '3232', 1, '2016-01-11 09:06:20', 48),
(5, 2, 1, 2, NULL, 'Paquito Lopez', 2, '2', 'Precio de siempre', 'Paco Lopez Lopez', 'paco@lopez.com ', NULL, NULL, 'TEst', NULL, 12, 'Juan JosÃ© LÃ³pez LÃ³pez', 'pepe@lopez.com', '324546', NULL, 1, '2016-01-11 11:30:38', 0),
(6, 2, 1, 1, 'Test', 'Bodegas de frutas del centro', 2, '2', 'Cliente con 200 bodegas en el paÃ­s', 'Francisco Solera', 'fran@bodeguitas.com', '323322332', NULL, 'Bodegas del centro, SA de CV', 'AAA213212BBB', 13, 'Pablo Pardo', 'pardo@test.com', '2343523', '222', 1, '2016-01-12 06:07:59', 0),
(7, 1, 1, 1, 'Esta bueno, pero lo estarÃ© supervisando.', 'Juan Perez', 2, '2', 'Dale chance', 'Juan Perez', 'juanito@hotmail.com', NULL, NULL, NULL, NULL, 14, NULL, NULL, NULL, NULL, 1, '2016-01-31 18:51:45', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes`
--

CREATE TABLE `componentes` (
  `id_componente` int(11) NOT NULL,
  `id_componente_grupo` int(11) DEFAULT NULL,
  `nombre_componente` varchar(128) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componentes`
--

INSERT INTO `componentes` (`id_componente`, `id_componente_grupo`, `nombre_componente`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 1, 'Nuevo', 60, 10, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes_grupos`
--

CREATE TABLE `componentes_grupos` (
  `id_grupo_componente` int(11) NOT NULL,
  `grupo_componente` varchar(128) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componentes_grupos`
--

INSERT INTO `componentes_grupos` (`id_grupo_componente`, `grupo_componente`, `activo`, `id_log`) VALUES
(1, 'Nuevo componente', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conceptos`
--

CREATE TABLE `conceptos` (
  `id_concepto` int(11) NOT NULL,
  `nombre_concepto` varchar(128) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `id_puerta` int(11) DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `id_pmodelo` int(11) DEFAULT NULL,
  `id_pcolor` int(11) DEFAULT NULL,
  `id_pmovimiento` int(11) DEFAULT NULL,
  `id_ptextura` int(11) DEFAULT NULL,
  `id_psello` int(11) DEFAULT NULL,
  `id_pmotor` int(11) DEFAULT NULL,
  `id_ventana` int(11) DEFAULT NULL,
  `id_vcolor` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `id_grupo_componente` int(11) DEFAULT NULL,
  `id_kit` int(11) DEFAULT NULL,
  `id_cotizacion` int(11) DEFAULT NULL,
  `costo_concepto` double DEFAULT NULL,
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `conceptos`
--

INSERT INTO `conceptos` (`id_concepto`, `nombre_concepto`, `cantidad`, `id_puerta`, `id_ptipo`, `id_pmodelo`, `id_pcolor`, `id_pmovimiento`, `id_ptextura`, `id_psello`, `id_pmotor`, `id_ventana`, `id_vcolor`, `id_componente`, `id_grupo_componente`, `id_kit`, `id_cotizacion`, `costo_concepto`, `id_log`) VALUES
(1, NULL, 1, 1, 1, 1, 1, 1, 1, 2, 2, NULL, NULL, NULL, NULL, NULL, 1, 2819, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizaciones`
--

CREATE TABLE `cotizaciones` (
  `id_cotizacion` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `numero_cotizacion` varchar(64) DEFAULT NULL,
  `descripcion` text,
  `autorizada` tinyint(1) DEFAULT '0',
  `id_usuario_autoriza` int(11) DEFAULT NULL,
  `autorizada_fecha` datetime DEFAULT NULL,
  `autorizada_rechazo` text,
  `id_usuario_autoriza_rechazo` int(11) DEFAULT NULL,
  `autorizada_rechazo_fecha` datetime DEFAULT NULL,
  `pagada` tinyint(1) DEFAULT '0',
  `url_pago` varchar(500) DEFAULT NULL,
  `id_usuario_pagada` int(11) DEFAULT NULL,
  `pagada_fecha` datetime DEFAULT NULL,
  `pagada_rechazo` text,
  `id_usuario_pagada_rechazo` int(11) DEFAULT NULL,
  `pagada_rechazo_fecha` datetime DEFAULT NULL,
  `pagada_comprobante` text,
  `precio_venta` int(11) DEFAULT NULL,
  `id_divisa` int(11) DEFAULT NULL,
  `tipo_cambio` double DEFAULT NULL,
  `produccion` int(11) DEFAULT '0',
  `produccion_fecha` datetime DEFAULT NULL,
  `despacho` int(11) DEFAULT '0',
  `tipo_despacho` int(11) DEFAULT '0',
  `num_guia` varchar(25) DEFAULT NULL,
  `impuestos` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `id_vendedor` int(11) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cotizaciones`
--

INSERT INTO `cotizaciones` (`id_cotizacion`, `id_cliente`, `numero_cotizacion`, `descripcion`, `autorizada`, `id_usuario_autoriza`, `autorizada_fecha`, `autorizada_rechazo`, `id_usuario_autoriza_rechazo`, `autorizada_rechazo_fecha`, `pagada`, `url_pago`, `id_usuario_pagada`, `pagada_fecha`, `pagada_rechazo`, `id_usuario_pagada_rechazo`, `pagada_rechazo_fecha`, `pagada_comprobante`, `precio_venta`, `id_divisa`, `tipo_cambio`, `produccion`, `produccion_fecha`, `despacho`, `tipo_despacho`, `num_guia`, `impuestos`, `total`, `id_vendedor`, `fecha`, `activo`, `id_log`) VALUES
(1, 6, NULL, 'sadfsdfdsfsdf', 1, 1, '2016-02-18 01:48:47', NULL, NULL, NULL, 1, 'http://localhost/hs/comprobantes/cotizacion_1/comprobante1.docx', 1, '2016-02-18 01:49:34', NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, '2016-02-18 01:49:39', 1, 1, '', NULL, 5413.889499999999, 1, '2016-02-18 00:47:45', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divisas`
--

CREATE TABLE `divisas` (
  `id_divisa` int(11) NOT NULL,
  `abbrev` varchar(5) DEFAULT NULL,
  `nombre` varchar(128) DEFAULT NULL,
  `tipo_cambio` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `divisas`
--

INSERT INTO `divisas` (`id_divisa`, `abbrev`, `nombre`, `tipo_cambio`, `activo`, `id_log`) VALUES
(1, 'USD', 'Dolar Americano', 17.35, 1, 0),
(2, 'MXN', 'Peso Mexicano', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilios`
--

CREATE TABLE `domicilios` (
  `id_domicilio` int(11) NOT NULL,
  `calle` varchar(256) DEFAULT NULL,
  `no_exterior` varchar(32) DEFAULT NULL,
  `no_interior` varchar(32) DEFAULT NULL,
  `colonia` varchar(256) DEFAULT NULL,
  `localidad` varchar(256) DEFAULT NULL,
  `referencia` text,
  `municipio` varchar(256) DEFAULT NULL,
  `estado` varchar(64) DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `codigo_postal` varchar(32) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `domicilios`
--

INSERT INTO `domicilios` (`id_domicilio`, `calle`, `no_exterior`, `no_interior`, `colonia`, `localidad`, `referencia`, `municipio`, `estado`, `id_pais`, `codigo_postal`, `fecha_alta`) VALUES
(1, 'Francisco Javier Mina', '312', 'N/A', 'Valles de HuinalÃ¡', 'Bodega industrial', 'Monterrey', 'Apodaca', 'Nuevo LeÃ³n', 117, '66600', '2016-01-07 23:11:00'),
(2, 'Test', NULL, '221', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-07 23:11:53'),
(3, '33333333333', '22', '45555555555', 'Las pruebas', 'salkdjf', 'Casa sola', 'klsdf', 'alksdf', 17, '3452', '2016-01-07 23:22:25'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-07 23:23:10'),
(5, NULL, 'ext', 'int', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 00:29:56'),
(6, 'Av. de la Palma', '234', '21', 'Palmira', 'Edificio blanco', 'Ciudad de MÃ©xico', 'Alvaro ObregÃ³n', 'Distrito Federal', 117, '03630', '2016-01-11 08:28:57'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 08:42:12'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 08:42:45'),
(9, NULL, 'asdfsda', NULL, NULL, NULL, NULL, NULL, 'asdf', 102, NULL, '2016-01-11 08:44:14'),
(10, 'Sin calle conocida', '2145', NULL, 'Primavera', 'Monterrey', NULL, 'Monterrey', NULL, 117, NULL, '2016-01-11 08:44:52'),
(11, 'asdfasdfasd', 'askdlfja', 'lasl', 'aslkfkdas', 'aslkdlk', NULL, 'lkasjafÃ±lkajsdf', 'askflksadjfdsal', 113, '77777', '2016-01-11 09:06:20'),
(12, 'CallejÃ³n de los Milagros', '123', NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 11:30:38'),
(13, 'Prueba', '2345', NULL, 'Ningun', NULL, NULL, 'Monterrey', 'Nuevo LeÃ³n', 117, '2456654', '2016-01-12 06:07:59'),
(14, 'jkashdfilhsdaf lu', '2323', '34', 'jdf Ã±asdfj kasdh', NULL, NULL, NULL, NULL, 117, NULL, '2016-01-31 18:51:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id_impuesto` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `porcentaje` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`id_impuesto`, `nombre`, `porcentaje`) VALUES
(1, 'Mexico', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kit`
--

CREATE TABLE `kit` (
  `id_kit` int(11) NOT NULL,
  `nombre_kit` text,
  `tipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `kit`
--

INSERT INTO `kit` (`id_kit`, `nombre_kit`, `tipo`, `id_proveedor`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 'Kit', NULL, 1, 50, 10, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kit_componente`
--

CREATE TABLE `kit_componente` (
  `id_kit_componente` int(11) NOT NULL,
  `id_kit` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad_kit_componente` int(11) DEFAULT '1',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `evento` varchar(32) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `descripcion` text,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`id_log`, `id_usuario`, `evento`, `ip`, `descripcion`, `fecha`) VALUES
(1, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 11:01:29'),
(2, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 11:02:01'),
(3, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 2 ', '2016-01-07 03:50:47'),
(4, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 3 ', '2016-01-07 03:51:33'),
(5, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 2 ', '2016-01-07 03:51:38'),
(6, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 03:52:09'),
(7, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 03:52:34'),
(8, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 03:53:44'),
(9, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 4 ', '2016-01-07 03:54:24'),
(10, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 03:59:34'),
(11, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 5 ', '2016-01-07 04:00:13'),
(12, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 04:16:38'),
(13, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 04:17:27'),
(14, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 04:33:11'),
(15, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 05:13:43'),
(16, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 06:57:09'),
(17, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 06:57:17'),
(18, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 6 ', '2016-01-07 07:36:05'),
(19, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 07:36:39'),
(20, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 07:37:03'),
(21, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 07:39:33'),
(22, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 07:39:48'),
(23, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:11:00'),
(24, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:11:53'),
(25, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al proveedor con el id 1 ', '2016-01-07 23:21:58'),
(26, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:22:26'),
(27, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-07 23:23:11'),
(28, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-07 23:43:00'),
(29, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-07 23:43:56'),
(30, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-07 23:45:09'),
(31, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 00:27:57'),
(32, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 00:28:26'),
(33, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 4 ', '2016-01-08 00:28:41'),
(34, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 1 ', '2016-01-08 00:29:31'),
(35, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 00:29:56'),
(36, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 5 ', '2016-01-08 00:31:09'),
(37, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 1 ', '2016-01-11 08:28:57'),
(38, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 2 ', '2016-01-11 08:44:14'),
(39, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 3 ', '2016-01-11 08:44:52'),
(40, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 4 ', '2016-01-11 09:06:20'),
(41, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:08:18'),
(42, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 3 ', '2016-01-11 09:08:31'),
(43, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 3 ', '2016-01-11 09:09:03'),
(44, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:09:16'),
(45, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:24:54'),
(46, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:25:46'),
(47, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:26:11'),
(48, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 09:27:26'),
(49, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 5 ', '2016-01-11 11:30:38'),
(50, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 6 ', '2016-01-12 06:08:00'),
(51, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 06:17:15'),
(52, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 06:20:55'),
(53, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 06:21:08'),
(54, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 6 ', '2016-01-12 16:22:51'),
(55, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 16:26:43'),
(56, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 16:26:53'),
(57, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 16:28:40'),
(58, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 16:31:09'),
(59, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 4 ', '2016-01-12 18:51:29'),
(60, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 4 ', '2016-01-12 18:52:01'),
(61, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 6 ', '2016-01-12 18:58:42'),
(62, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 18:59:18'),
(63, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 18:59:36'),
(64, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-13 14:30:09'),
(65, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-13 14:30:42'),
(66, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un color con el id: 0 ', '2016-01-13 14:31:32'),
(67, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 01:51:39'),
(68, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 01:51:52'),
(69, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 01:52:02'),
(70, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un color con el id: 0 ', '2016-01-18 01:52:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multiplicadores`
--

CREATE TABLE `multiplicadores` (
  `id_multiplicador` int(11) NOT NULL,
  `nombre_multiplicador` varchar(16) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `multiplicadores`
--

INSERT INTO `multiplicadores` (`id_multiplicador`, `nombre_multiplicador`, `valor`, `activo`, `id_log`) VALUES
(1, 'VA', 2.5, 1, 0),
(2, 'VB', 1.67, 1, 0),
(3, 'VC', 1.393, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `nombre`) VALUES
(1, 'AfganistÃ¡n'),
(2, 'Albania'),
(3, 'Alemania'),
(4, 'Andorra'),
(5, 'Angola'),
(6, 'Antigua y Barbuda'),
(7, 'Arabia Saudita'),
(8, 'Argelia'),
(9, 'Argentina'),
(10, 'Armenia'),
(11, 'Australia'),
(12, 'Austria'),
(13, 'AzerbaiyÃ¡n'),
(14, 'Bahamas'),
(15, 'Bangladesh'),
(16, 'Barbados'),
(17, 'BarÃ©in'),
(18, 'BÃ©lgica'),
(19, 'Belice'),
(20, 'BenÃ­n'),
(21, 'Bielorrusia'),
(22, 'Birmania'),
(23, 'Bolivia'),
(24, 'Bosnia-Herzegovina'),
(25, 'Botsuana'),
(26, 'Brasil'),
(27, 'BrunÃ©i'),
(28, 'Bulgaria'),
(29, 'Burkina Faso'),
(30, 'Burundi'),
(31, 'ButÃ¡n'),
(32, 'Cabo Verde'),
(33, 'Camboya'),
(34, 'CamerÃºn'),
(35, 'CanadÃ¡'),
(36, 'Chad'),
(37, 'Chile'),
(38, 'China'),
(39, 'Chipre'),
(40, 'Ciudad del Vaticano'),
(41, 'Colombia'),
(42, 'Comoras'),
(43, 'Corea del Norte'),
(44, 'Corea del Sur'),
(45, 'Costa de Marfil'),
(46, 'Costa Rica'),
(47, 'Croacia'),
(48, 'Cuba'),
(49, 'Dinamarca'),
(50, 'Dominica'),
(51, 'Ecuador'),
(52, 'Egipto'),
(53, 'El Salvador'),
(54, 'Emiratos Ãrabes Unidos'),
(55, 'Eritrea'),
(56, 'Eslovaquia'),
(57, 'Eslovenia'),
(58, 'EspaÃ±a'),
(59, 'Estados Unidos'),
(60, 'Estonia'),
(61, 'Etiopia'),
(62, 'Filipinas'),
(63, 'Finlandia'),
(64, 'Fiyi'),
(65, 'Francia'),
(66, 'GabÃ³n'),
(67, 'Gambia'),
(68, 'Georgia'),
(69, 'Ghana'),
(70, 'Granada'),
(71, 'Grecia'),
(72, 'Guatemala'),
(73, 'Guyana'),
(74, 'Guinea'),
(75, 'Guinea ecuatorial'),
(76, 'Guinea-BisÃ¡u'),
(77, 'HaitÃ­'),
(78, 'Holanda'),
(79, 'Honduras'),
(80, 'HungrÃ­a'),
(81, 'India'),
(82, 'Indonesia'),
(83, 'Irak'),
(84, 'IrÃ¡n'),
(85, 'Irlanda'),
(86, 'Islandia'),
(87, 'Islas Marshall'),
(88, 'Islas SalomÃ³n'),
(89, 'Israel'),
(90, 'Italia'),
(91, 'Jamaica'),
(92, 'JapÃ³n'),
(93, 'Jordania'),
(94, 'KazajistÃ¡n'),
(95, 'Kenia'),
(96, 'KirguistÃ¡n'),
(97, 'Kiribati'),
(98, 'Kuwait'),
(99, 'Laos'),
(100, 'Lesoto'),
(101, 'Letonia'),
(102, 'Libano'),
(103, 'Liberia'),
(104, 'Libia'),
(105, 'Liechtenstein'),
(106, 'Lituania'),
(107, 'Luxemburgo'),
(108, 'Madagascar'),
(109, 'Malasia'),
(110, 'Malaui'),
(111, 'Maldivas'),
(112, 'MalÃ­'),
(113, 'Malta'),
(114, 'Marruecos'),
(115, 'Mauricio'),
(116, 'Mauritania'),
(117, 'MÃ©xico'),
(118, 'Micronesia'),
(119, 'Moldavia'),
(120, 'MÃ³naco'),
(121, 'Mongolia'),
(122, 'Montenegro'),
(123, 'Mozambique'),
(124, 'Namibia'),
(125, 'Nauru'),
(126, 'Nepal'),
(127, 'Nicaragua'),
(128, 'NÃ­ger'),
(129, 'Nigeria'),
(130, 'Noruega'),
(131, 'Nueva Zelanda'),
(132, 'OmÃ¡n'),
(133, 'PakistÃ¡n'),
(134, 'Palaos'),
(135, 'PanamÃ¡'),
(136, 'Papua Nueva Guinea'),
(137, 'Paraguay'),
(138, 'PerÃº'),
(139, 'Polonia'),
(140, 'Portugal'),
(141, 'Qatar'),
(142, 'Reino Unido'),
(143, 'RepÃºblica Centroafricana'),
(144, 'RepÃºblica Checa'),
(145, 'RepÃºblica de Macedonia'),
(146, 'RepÃºblica del Congo'),
(147, 'RepÃºblica Democratica del Congo'),
(148, 'RepÃºblica Dominicana'),
(149, 'RepÃºblica Sudafricana'),
(150, 'Ruanda'),
(151, 'Rumania'),
(152, 'Rusia'),
(153, 'Samoa'),
(154, 'San CristÃ³bal y Nieves'),
(155, 'San Marino'),
(156, 'San Vicente y las Granadinas'),
(157, 'Santa LucÃ­a'),
(158, 'Santo TomÃ© y PrÃ­ncipe'),
(159, 'Senegal'),
(160, 'Serbia'),
(161, 'Seychelles'),
(162, 'Sierra Leona'),
(163, 'Singapur'),
(164, 'Siria'),
(165, 'Somalia'),
(166, 'Sri Lanka'),
(167, 'Suazilandia'),
(168, 'SudÃ¡n'),
(169, 'SudÃ¡n del Sur'),
(170, 'Suecia'),
(171, 'Suiza'),
(172, 'Surinam'),
(173, 'Tailandia'),
(174, 'Tanzania'),
(175, 'TayikistÃ¡n'),
(176, 'Timor Oriental'),
(177, 'Togo'),
(178, 'Tonga'),
(179, 'Trinidad y Tobago'),
(180, 'TÃºnez'),
(181, 'TurkmenistÃ¡n'),
(182, 'TurquÃ­a'),
(183, 'Tuvalu'),
(184, 'Ucrania'),
(185, 'Uganda'),
(186, 'Uruguay'),
(187, 'UzbekistÃ¡n'),
(188, 'Vanuatu'),
(189, 'Venezuela'),
(190, 'Vietnam'),
(191, 'Yemen'),
(192, 'Yibuti'),
(193, 'Zambia'),
(194, 'Zimbawe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_permiso` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permiso`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Administrador', 1, NULL),
(2, 'Director', 1, NULL),
(3, 'Director Produccion', 1, NULL),
(4, 'Direcctor Ventas', 1, NULL),
(5, 'Cliente', 1, NULL),
(6, 'Vendedor', 1, NULL),
(7, 'Produccion', 1, NULL),
(8, 'Despacho', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `tipo` varchar(16) DEFAULT NULL,
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `nombre`, `tipo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `id_domicilio`, `id_usuario_alta`, `fecha_alta`, `activo`, `id_log`) VALUES
(1, 'HS puertas de garaje', 'Nacional', 'Guillermo Ortiz', 'test@hs.com', '8112341000', '112', 1, 1, '2016-01-07 23:11:00', 1, 34),
(2, 'test', 'Nacional', '0', NULL, NULL, NULL, 2, 1, '2016-01-07 23:11:53', 1, 24),
(3, 'Pruebas de desarrollo', 'Nacional', 'jÃ±lksdfaj', 'esteesuncorreoelectonicomuylargoparaprobarlabd@testingdecorreo.com', '342332423', '232', 3, 1, '2016-01-07 23:22:26', 1, 32),
(4, 'Juan Javier Jaime Gabriel', 'Nacional', NULL, NULL, NULL, NULL, 4, 1, '2016-01-07 23:23:10', 1, 33),
(5, 'test', 'Nacional', NULL, NULL, NULL, NULL, 5, 1, '2016-01-08 00:29:56', 1, 36);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta`
--

CREATE TABLE `puerta` (
  `id_puerta` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `m_ancho` double DEFAULT NULL,
  `m_alto` double DEFAULT NULL,
  `f_ancho` double DEFAULT NULL,
  `in_ancho` double DEFAULT NULL,
  `f_alto` double DEFAULT NULL,
  `in_alto` double DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta`
--

INSERT INTO `puerta` (`id_puerta`, `id_ptipo`, `m_ancho`, `m_alto`, `f_ancho`, `in_ancho`, `f_alto`, `in_alto`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 1, 3.05, 2.1, 10, 2, 7, 2, 420, 16, 1, 0),
(2, 1, 3.05, 2.5, 10, 3, 8, 3, 481, 16, 1, 0),
(3, 2, 2.44, 2.44, 8, 8, 0, 0, 403, 16, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_color`
--

CREATE TABLE `puerta_color` (
  `id_pcolor` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `default` smallint(6) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_color`
--

INSERT INTO `puerta_color` (`id_pcolor`, `nombre_color`, `descripcion`, `default`, `activo`, `id_log`) VALUES
(1, 'Blanco', NULL, 2, 1, 0),
(2, 'Almendra', NULL, 2, 1, 0),
(3, 'CafÃ©', 'Color cafÃ©', 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_componente`
--

CREATE TABLE `puerta_componente` (
  `id_puerta` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_componente`
--

INSERT INTO `puerta_componente` (`id_puerta`, `id_componente`, `cantidad`, `activo`) VALUES
(NULL, NULL, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_modelo`
--

CREATE TABLE `puerta_modelo` (
  `id_pmodelo` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_modelo` text,
  `descripcion` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_modelo`
--

INSERT INTO `puerta_modelo` (`id_pmodelo`, `id_ptipo`, `nombre_modelo`, `descripcion`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Classica', 'Puerta default', 2, 1, 0),
(2, NULL, 'Adonnia', 'Sencilla y agradable', 2, 1, 0),
(3, 2, 'Elegantti', NULL, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_motor`
--

CREATE TABLE `puerta_motor` (
  `id_pmotor` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_motor` text,
  `descripcion` text,
  `precio` int(11) DEFAULT NULL,
  `impuesto` int(11) DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_motor`
--

INSERT INTO `puerta_motor` (`id_pmotor`, `id_proveedor`, `nombre_motor`, `descripcion`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Sin Motor', NULL, 0, 0, 1, 1, 0),
(2, NULL, 'HS ONE', NULL, 2296, 15, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_movimiento`
--

CREATE TABLE `puerta_movimiento` (
  `id_pmovimiento` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_movimiento` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_movimiento`
--

INSERT INTO `puerta_movimiento` (`id_pmovimiento`, `id_ptipo`, `nombre_movimiento`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Seccional', 2, 1, 0),
(2, NULL, 'Abatible', 2, 1, 0),
(3, NULL, 'Corrediza', 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_sello`
--

CREATE TABLE `puerta_sello` (
  `id_psello` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_sello` text,
  `tipo` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_sello`
--

INSERT INTO `puerta_sello` (`id_psello`, `id_proveedor`, `nombre_sello`, `tipo`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, 0, 'Sin Sello', 0, 0, 0, 1, 1, 0),
(2, NULL, 'Perimetral', 1, 10, 15, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_textura`
--

CREATE TABLE `puerta_textura` (
  `id_ptextura` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_textura` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_textura`
--

INSERT INTO `puerta_textura` (`id_ptextura`, `id_proveedor`, `nombre_textura`, `descripcion`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Madera', 'Textura por defecto', 0, NULL, 1, 1, 0),
(2, NULL, 'Lisa', NULL, 30, 1.15, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_tipo`
--

CREATE TABLE `puerta_tipo` (
  `id_ptipo` int(11) NOT NULL,
  `nombre_tipo` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_tipo`
--

INSERT INTO `puerta_tipo` (`id_ptipo`, `nombre_tipo`, `default`, `activo`, `id_log`) VALUES
(1, 'Residencial', 1, 1, 0),
(2, 'Industrial', 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_ventana`
--

CREATE TABLE `puerta_ventana` (
  `id_puerta` int(11) DEFAULT NULL,
  `id_ventana` int(11) DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `pwd` varchar(64) DEFAULT NULL,
  `id_divisa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `nombre_usuario` varchar(256) DEFAULT NULL,
  `email_usuario` varchar(128) DEFAULT NULL,
  `telefono_usuario` varchar(32) DEFAULT NULL,
  `ext_usuario` varchar(16) DEFAULT NULL,
  `num_empleado` varchar(32) DEFAULT NULL,
  `permiso_usuario` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `accesos` int(11) DEFAULT '0',
  `id_log` int(11) DEFAULT '0',
  `ultimo_acceso` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `username`, `pwd`, `id_divisa`, `id_cliente`, `nombre_usuario`, `email_usuario`, `telefono_usuario`, `ext_usuario`, `num_empleado`, `permiso_usuario`, `fecha_alta`, `activo`, `accesos`, `id_log`, `ultimo_acceso`) VALUES
(1, 'admintk', 'Teknik', 2, 1, 'Teknik', 'webmaster@teknik.mx', '(81) 1931 4009', NULL, NULL, 1, '2016-01-06 06:40:29', 1, 20, 22, '2016-01-11 21:08:31'),
(2, 'pp', 'perez', 1, NULL, 'Jose', 'pp@perez.com', NULL, NULL, NULL, 1, '2016-01-07 03:50:47', 0, 0, 5, '2016-01-07 03:51:38'),
(3, 'pp', 'perez', 1, NULL, 'Jose', 'pp@perez.com', '3233', '32', '3232', 8, '2016-01-07 03:51:33', 1, 1, 17, '2016-01-07 06:57:18'),
(4, 'lopez', 'juan', 2, NULL, 'Juan Lopez', 'juan@lopez.co', '345', '22', '222', 5, '2016-01-07 03:54:23', 1, 0, 13, '2016-01-07 04:17:27'),
(5, 'asdf', 'asdf', 1, NULL, 'asdfasd', 'asdf@askdf.com', NULL, NULL, '21221', 5, '2016-01-07 04:00:13', 1, 0, 21, '2016-01-07 07:39:33'),
(6, 'asdf', 'asfd', 1, NULL, 'asdf', 'test@prueba.com', '222221111', NULL, '2222', 8, '2016-01-07 07:36:05', 1, 0, 20, '2016-01-07 07:37:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventana`
--

CREATE TABLE `ventana` (
  `id_ventana` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_ventana` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventana`
--

INSERT INTO `ventana` (`id_ventana`, `id_ptipo`, `id_proveedor`, `nombre_ventana`, `descripcion`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 1, NULL, 'Ashton', NULL, 30, 15, 1, 0),
(2, 1, NULL, 'Cascade', NULL, 50, 15, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventana_color`
--

CREATE TABLE `ventana_color` (
  `id_vcolor` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventana_color`
--

INSERT INTO `ventana_color` (`id_vcolor`, `nombre_color`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Blanco', '-', 1, 0);

--
-- Ãndices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `componentes`
--
ALTER TABLE `componentes`
  ADD PRIMARY KEY (`id_componente`);

--
-- Indices de la tabla `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
  ADD PRIMARY KEY (`id_grupo_componente`);

--
-- Indices de la tabla `conceptos`
--
ALTER TABLE `conceptos`
  ADD PRIMARY KEY (`id_concepto`);

--
-- Indices de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- Indices de la tabla `divisas`
--
ALTER TABLE `divisas`
  ADD PRIMARY KEY (`id_divisa`);

--
-- Indices de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  ADD PRIMARY KEY (`id_domicilio`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id_impuesto`);

--
-- Indices de la tabla `kit`
--
ALTER TABLE `kit`
  ADD PRIMARY KEY (`id_kit`);

--
-- Indices de la tabla `kit_componente`
--
ALTER TABLE `kit_componente`
  ADD PRIMARY KEY (`id_kit_componente`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indices de la tabla `multiplicadores`
--
ALTER TABLE `multiplicadores`
  ADD PRIMARY KEY (`id_multiplicador`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `puerta`
--
ALTER TABLE `puerta`
  ADD PRIMARY KEY (`id_puerta`);

--
-- Indices de la tabla `puerta_color`
--
ALTER TABLE `puerta_color`
  ADD PRIMARY KEY (`id_pcolor`);

--
-- Indices de la tabla `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  ADD PRIMARY KEY (`id_pmodelo`);

--
-- Indices de la tabla `puerta_motor`
--
ALTER TABLE `puerta_motor`
  ADD PRIMARY KEY (`id_pmotor`);

--
-- Indices de la tabla `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
  ADD PRIMARY KEY (`id_pmovimiento`);

--
-- Indices de la tabla `puerta_sello`
--
ALTER TABLE `puerta_sello`
  ADD PRIMARY KEY (`id_psello`);

--
-- Indices de la tabla `puerta_textura`
--
ALTER TABLE `puerta_textura`
  ADD PRIMARY KEY (`id_ptextura`);

--
-- Indices de la tabla `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  ADD PRIMARY KEY (`id_ptipo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `ventana`
--
ALTER TABLE `ventana`
  ADD PRIMARY KEY (`id_ventana`);

--
-- Indices de la tabla `ventana_color`
--
ALTER TABLE `ventana_color`
  ADD PRIMARY KEY (`id_vcolor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `componentes`
--
ALTER TABLE `componentes`
  MODIFY `id_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
  MODIFY `id_grupo_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `conceptos`
--
ALTER TABLE `conceptos`
  MODIFY `id_concepto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `divisas`
--
ALTER TABLE `divisas`
  MODIFY `id_divisa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  MODIFY `id_domicilio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id_impuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `kit`
--
ALTER TABLE `kit`
  MODIFY `id_kit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `kit_componente`
--
ALTER TABLE `kit_componente`
  MODIFY `id_kit_componente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT de la tabla `multiplicadores`
--
ALTER TABLE `multiplicadores`
  MODIFY `id_multiplicador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `puerta`
--
ALTER TABLE `puerta`
  MODIFY `id_puerta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `puerta_color`
--
ALTER TABLE `puerta_color`
  MODIFY `id_pcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  MODIFY `id_pmodelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `puerta_motor`
--
ALTER TABLE `puerta_motor`
  MODIFY `id_pmotor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
  MODIFY `id_pmovimiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `puerta_sello`
--
ALTER TABLE `puerta_sello`
  MODIFY `id_psello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_textura`
--
ALTER TABLE `puerta_textura`
  MODIFY `id_ptextura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  MODIFY `id_ptipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `ventana`
--
ALTER TABLE `ventana`
  MODIFY `id_ventana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventana_color`
--
ALTER TABLE `ventana_color`
  MODIFY `id_vcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
