-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-05-2016 a las 22:13:25
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hefesto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `tipo` int(11) DEFAULT '1',
  `activo` tinyint(1) DEFAULT '1',
  `aprobado` tinyint(1) DEFAULT '0',
  `motivo_aprobado` text,
  `nombre_cliente` varchar(256) DEFAULT NULL,
  `divisa_cliente` int(11) DEFAULT NULL,
  `tipo_costo` varchar(5) DEFAULT NULL,
  `motivo_costo` text,
  `depositos` double NOT NULL DEFAULT '0',
  `saldo` double NOT NULL DEFAULT '0',
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `razon_social` varchar(256) DEFAULT NULL,
  `rfc` varchar(20) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `nombre_autorizado` varchar(256) DEFAULT NULL,
  `email_autorizado` varchar(128) DEFAULT NULL,
  `telefono_autorizado` varchar(64) DEFAULT NULL,
  `ext_autorizado` varchar(32) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `tipo`, `activo`, `aprobado`, `motivo_aprobado`, `nombre_cliente`, `divisa_cliente`, `tipo_costo`, `motivo_costo`, `depositos`, `saldo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `razon_social`, `rfc`, `id_domicilio`, `nombre_autorizado`, `email_autorizado`, `telefono_autorizado`, `ext_autorizado`, `id_usuario_alta`, `fecha_alta`, `id_log`) VALUES
(1, 2, 1, 1, 'Es un buen cliente', 'Sams', 2, '2', 'Este es un cliente de prueba', 0, 0, 'Marco Martinez', 'marco@test.com', '324452342332', '12344', 'SSM121212M3A', 'Sams de Mexico, SA d', 6, 'Pedro Palma', 'pedro@test.com', '24346345432', '2342', 1, '2016-01-11 14:28:57', 0),
(2, 2, 0, 1, NULL, 'Instalaciones modernas', 2, '2', 'jlñka sjfñlkjsañ fasldkjf lasdkf', 0, 0, 'fdfffffffffffffffff', 'ssssssssssssss', '32465635345655', NULL, 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 9, 'asdf', NULL, NULL, 'sss', 1, '2016-01-11 14:44:14', 0),
(3, 2, 1, 1, NULL, 'Celular Express', 2, '1', 'asdfsfdaafad', 0, 1820.98, 'asdf', 'ssssssssssssss', '8987988', NULL, 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 10, 'Pedro Perez', 'perez@perez.com', '3221332', NULL, 1, '2016-01-11 14:44:52', 0),
(4, 1, 0, 0, NULL, 'Vista Hermosa', 2, '1', 'Sin motivo aparente', 0, 0, 'asdf', 'ssssssssssssss', '233333', '123', 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 11, 'dkljlfsdñ', 'dldkldakldf', '22222', '3232', 1, '2016-01-11 15:06:20', 48),
(5, 2, 0, 2, NULL, 'Paquito Lopez', 2, '2', 'Precio de siempre', 0, 0, 'Paco Lopez Lopez', 'paco@lopez.com ', NULL, NULL, 'TEst', NULL, 12, 'Juan José López López', 'pepe@lopez.com', '324546', NULL, 1, '2016-01-11 17:30:38', 0),
(6, 2, 1, 1, NULL, 'Bodegas de frutas del centro', 2, '2', 'Cliente con 200 bodegas en el país', 0, 785497.99, 'Francisco Solera', 'fran@bodeguitas.com', '323322332', NULL, 'Bodegas del centro, SA de CV', 'AAA213212BBB', 13, 'Pablo Pardo', 'pardo@test.com', '2343523', '222', 1, '2016-01-12 12:07:59', 0),
(7, 2, 0, 1, NULL, 'Juan Perez', 2, '2', 'Dale chance', 0, 0, 'Juan Perez', 'juanito@hotmail.com', NULL, NULL, NULL, NULL, 14, NULL, NULL, NULL, NULL, 1, '2016-02-01 00:51:45', 0),
(8, 2, 1, 1, NULL, 'Jacobo Altamirano', 2, '1', NULL, 0, 209.5, 'Jacobo Altamirano', 'test@example.com', NULL, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, 7, '2016-02-27 10:59:47', 0),
(9, 2, 1, 1, NULL, 'Sitemas de Victoria', 2, '1', 'Ninguno por ahora', 0, 1449.9, 'Miguel Angel Amaro Hernández', 'miguel@hs.com', '8341265342', NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, 1, '2016-04-08 21:37:29', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes`
--

CREATE TABLE `componentes` (
  `id_componente` int(11) NOT NULL,
  `id_componente_grupo` int(11) DEFAULT NULL,
  `nombre_componente` varchar(128) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componentes`
--

INSERT INTO `componentes` (`id_componente`, `id_componente_grupo`, `nombre_componente`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(2, 2, 'Flecha 2.8', 7.5, 0, 1, NULL),
(3, 2, 'Flecha 3.4', 8.8, 0, 1, NULL),
(4, 3, 'Plano 3/8', 10.8, 0, 1, NULL),
(5, 3, 'Dinamico 1-10', 120.99, 0, 1, NULL),
(6, 4, 'Guia 7''', 47.74, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componentes_grupos`
--

CREATE TABLE `componentes_grupos` (
  `id_grupo_componente` int(11) NOT NULL,
  `grupo_componente` varchar(128) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componentes_grupos`
--

INSERT INTO `componentes_grupos` (`id_grupo_componente`, `grupo_componente`, `activo`, `id_log`) VALUES
(2, 'Flechas', 1, 2),
(3, 'Tornillos', 1, 0),
(4, 'Guias', 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conceptos`
--

CREATE TABLE `conceptos` (
  `id_concepto` int(11) NOT NULL,
  `nombre_concepto` varchar(128) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `id_puerta` int(11) DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `id_pmodelo` int(11) DEFAULT NULL,
  `id_pcolor` int(11) DEFAULT NULL,
  `id_pmovimiento` int(11) DEFAULT NULL,
  `id_ptextura` int(11) DEFAULT NULL,
  `id_psello` int(11) DEFAULT NULL,
  `id_pmotor` int(11) DEFAULT NULL,
  `id_ventana` int(11) DEFAULT NULL,
  `id_vcolor` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `id_grupo_componente` int(11) DEFAULT NULL,
  `id_kit` int(11) DEFAULT NULL,
  `id_cotizacion` int(11) DEFAULT NULL,
  `costo_concepto` double DEFAULT NULL,
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `conceptos`
--

INSERT INTO `conceptos` (`id_concepto`, `nombre_concepto`, `cantidad`, `id_puerta`, `id_ptipo`, `id_pmodelo`, `id_pcolor`, `id_pmovimiento`, `id_ptextura`, `id_psello`, `id_pmotor`, `id_ventana`, `id_vcolor`, `id_componente`, `id_grupo_componente`, `id_kit`, `id_cotizacion`, `costo_concepto`, `id_log`) VALUES
(1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 432, 1),
(2, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 7.5, 2),
(3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1800, 3),
(4, NULL, 2, 4, 2, 4, 1, 2, 1, 1, 1, 1, 1, NULL, NULL, NULL, 9, 1356, 4),
(5, NULL, 4, 3, 1, 2, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 10, 1700, 5),
(6, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 11, 432, 6),
(7, NULL, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 13, 425, 7),
(8, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 14, 432, 8),
(9, NULL, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 15, 421, 9),
(10, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 17, 432, 10),
(11, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 18, 432, 11),
(12, NULL, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 19, 842, 12),
(13, NULL, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 19, 864, 13),
(14, NULL, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 19, 864, 14),
(15, NULL, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 19, 842, 15),
(16, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 20, 432, 16),
(17, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 20, 432, 17),
(18, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 21, 432, 18),
(19, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 22, 432, 19),
(20, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 23, 432, 20),
(21, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 23, 432, 21),
(22, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 24, 432, 22),
(23, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 26, 432, 23),
(24, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 27, 432, 24),
(25, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 28, 432, 25),
(26, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 29, 432, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizaciones`
--

CREATE TABLE `cotizaciones` (
  `id_cotizacion` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `numero_cotizacion` varchar(64) DEFAULT NULL,
  `descripcion` text,
  `autorizada` tinyint(1) DEFAULT '0',
  `id_usuario_autoriza` int(11) DEFAULT NULL,
  `autorizada_fecha` datetime DEFAULT NULL,
  `autorizada_rechazo` text,
  `id_usuario_autoriza_rechazo` int(11) DEFAULT NULL,
  `autorizada_rechazo_fecha` datetime DEFAULT NULL,
  `pagada` tinyint(4) DEFAULT '0',
  `pagoconsaldo` double NOT NULL DEFAULT '0',
  `url_pago` varchar(500) DEFAULT NULL,
  `id_usuario_pendiente` int(11) DEFAULT NULL,
  `pendiente_fecha` datetime DEFAULT NULL,
  `pendiente_motivo` varchar(255) DEFAULT NULL,
  `id_usuario_pagada` int(11) DEFAULT NULL,
  `pagada_fecha` datetime DEFAULT NULL,
  `aprobada_motivo` varchar(255) DEFAULT NULL,
  `id_usuario_pagada_rechazo` int(11) DEFAULT NULL,
  `pagada_rechazo_fecha` datetime DEFAULT NULL,
  `rechazada_motivo` varchar(255) DEFAULT NULL,
  `pagada_rechazo` text,
  `pagada_comprobante` text,
  `precio_venta` int(11) DEFAULT NULL,
  `id_divisa` int(11) DEFAULT NULL,
  `tipo_cambio` double DEFAULT NULL,
  `id_impuesto` int(11) NOT NULL,
  `produccion` int(11) DEFAULT '0',
  `produccion_fecha` datetime DEFAULT NULL,
  `despacho` int(11) DEFAULT '0',
  `tipo_despacho` int(11) DEFAULT '0',
  `despacho_fecha` datetime DEFAULT NULL,
  `num_guia` varchar(25) DEFAULT NULL,
  `impuestos` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `id_vendedor` int(11) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `comentarios_cotizacion` text,
  `activo` tinyint(1) DEFAULT '0',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cotizaciones`
--

INSERT INTO `cotizaciones` (`id_cotizacion`, `id_cliente`, `numero_cotizacion`, `descripcion`, `autorizada`, `id_usuario_autoriza`, `autorizada_fecha`, `autorizada_rechazo`, `id_usuario_autoriza_rechazo`, `autorizada_rechazo_fecha`, `pagada`, `pagoconsaldo`, `url_pago`, `id_usuario_pendiente`, `pendiente_fecha`, `pendiente_motivo`, `id_usuario_pagada`, `pagada_fecha`, `aprobada_motivo`, `id_usuario_pagada_rechazo`, `pagada_rechazo_fecha`, `rechazada_motivo`, `pagada_rechazo`, `pagada_comprobante`, `precio_venta`, `id_divisa`, `tipo_cambio`, `id_impuesto`, `produccion`, `produccion_fecha`, `despacho`, `tipo_despacho`, `despacho_fecha`, `num_guia`, `impuestos`, `total`, `id_vendedor`, `fecha`, `comentarios_cotizacion`, `activo`, `id_log`) VALUES
(1, 6, '201604HSYAFMNU917', 'Descripción', 1, NULL, NULL, NULL, NULL, NULL, 2, 4300, 'comprobantes/cotizacion_1/comprobante1.JPG', 1, '2016-04-29 19:13:08', 'pendiente', 1, '2016-04-29 19:18:09', 'Aprobada', 1, '2016-04-29 19:16:43', 'rechazada', NULL, NULL, NULL, 2, NULL, 1, 1, '2016-04-29 19:18:59', 1, 1, '2016-04-29 19:19:57', '123456789', NULL, 4312.38, 1, '2016-04-29 16:04:44', 'Listo', 1, NULL),
(4, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-04-29 17:30:01', NULL, 0, NULL),
(5, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-04-29 17:30:15', NULL, 0, NULL),
(6, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-04-29 17:30:20', NULL, 0, NULL),
(7, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-04-29 17:57:04', NULL, 0, NULL),
(8, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-04-29 17:58:15', NULL, 0, NULL),
(9, 6, '201604HSKU7L9DSG0', 'Descripción', 1, NULL, NULL, NULL, NULL, NULL, 2, 2611.11, '', NULL, NULL, NULL, 1, '2016-04-29 20:32:52', '', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 1, '2016-04-29 20:42:02', 1, 1, '2016-04-29 20:42:28', '11245782116', NULL, 2611.11, 1, '2016-04-29 18:15:33', '', 1, NULL),
(10, 6, '201604HSK5LGXQJIR', 'Descripción III', 1, NULL, NULL, NULL, NULL, NULL, 2, 3273.52, '', NULL, NULL, NULL, 1, '2016-04-29 20:47:29', '', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 1, '2016-04-30 00:36:52', 0, 1, NULL, NULL, NULL, 3273.52, 1, '2016-04-29 18:45:28', '', 1, NULL),
(11, 6, '201604HSPDF78JEUT', 'Descripción IV', 1, NULL, NULL, NULL, NULL, NULL, 2, 831.86, '', 1, '2016-04-30 00:17:21', '', 1, '2016-04-30 00:38:27', '', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, NULL, 831.86, 1, '2016-04-29 18:48:35', '', 1, NULL),
(12, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-04-29 22:27:35', NULL, 0, NULL),
(13, 8, '201604HSXNKH7RJST', 'Descripción', 1, NULL, NULL, NULL, NULL, NULL, 2, 1232.5, '', 1, '2016-05-02 18:49:02', '', 1, '2016-05-02 20:10:40', '', 1, '2016-05-02 19:47:28', '', NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 2, NULL, NULL, NULL, 1232.5, 1, '2016-04-29 22:37:13', '', 1, NULL),
(14, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 16:22:30', NULL, 0, NULL),
(15, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 16:41:25', NULL, 0, NULL),
(16, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 17:15:59', NULL, 0, NULL),
(17, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 17:17:30', NULL, 0, NULL),
(18, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 17:25:28', NULL, 0, NULL),
(19, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 17:28:03', NULL, 0, NULL),
(20, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 17:43:33', NULL, 0, NULL),
(21, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 17:48:04', NULL, 0, NULL),
(22, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 18:00:58', NULL, 0, NULL),
(23, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 18:02:29', NULL, 0, NULL),
(24, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:21:06', NULL, 0, NULL),
(25, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:28:43', NULL, 0, NULL),
(26, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:28:48', NULL, 0, NULL),
(27, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:37:01', NULL, 0, NULL),
(28, 6, '201605HSD2PYGZRFJ', 'Descripción', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, NULL, 831.86, 1, '2016-05-03 19:40:52', NULL, 0, NULL),
(29, 6, '201605HSKST2M0LVO', 'Descripción', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, NULL, 831.95, 1, '2016-05-03 19:43:32', NULL, 1, NULL),
(30, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:44:32', NULL, 0, NULL),
(31, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:44:57', NULL, 0, NULL),
(32, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:45:16', NULL, 0, NULL),
(33, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:46:35', NULL, 0, NULL),
(34, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:48:32', NULL, 0, NULL),
(36, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:50:08', NULL, 0, NULL),
(37, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, '2016-05-03 19:50:33', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depositos`
--

CREATE TABLE `depositos` (
  `id_deposito` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `monto_deposito` text,
  `fecha_deposito` text,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT '1',
  `id_log` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `depositos`
--

INSERT INTO `depositos` (`id_deposito`, `id_cliente`, `monto_deposito`, `fecha_deposito`, `fecha`, `id_usuario_alta`, `activo`, `id_log`) VALUES
(1, 7, '2034', '4342332-4-32', '2016-03-27 01:03:33', 1, 0, 0),
(2, 5, '4034', '16-02-01', '2016-03-27 01:03:19', 1, 1, 0),
(3, 6, '200', '14-02-02', '2016-03-27 01:02:54', 1, 1, 0),
(4, 6, '10000', '16-04-04', '2016-03-27 01:03:07', 1, 1, 0),
(5, 8, '1000', '2016-03-15', '2016-03-17 06:15:40', 1, 1, 0),
(6, 1, '12500.50', '2016-04-11', '2016-04-29 20:50:12', 1, 1, 9),
(7, 1, '12500.50', '2016-04-04', '2016-04-06 21:45:46', 1, 1, 0),
(8, 1, '120', '2016-04-08', '2016-04-29 21:52:59', 1, 1, 16),
(9, 6, '250', '2016-04-01', '2016-04-29 21:49:29', 1, 1, 14),
(10, 6, '89.90', '2016-04-02', '2016-04-29 20:51:11', 1, 1, 0),
(11, 9, '1000', '2016-04-29', '2016-04-29 21:25:46', NULL, 1, 0),
(12, 9, '250', '2016-04-29', '2016-04-29 21:45:06', NULL, 1, 0),
(13, 9, '499', '2016-04-29', '2016-04-29 21:47:47', NULL, 1, 0),
(14, 9, '199.90', '2016-04-29', '2016-04-29 21:49:29', NULL, 1, 0),
(15, 8, '3500.95', '2016-04-29', '2016-04-29 21:50:42', NULL, 1, 0),
(16, 8, '180.25', '2016-04-29', '2016-04-29 21:52:59', NULL, 1, 0),
(17, 3, '1820.98', '2016-04-29', '2016-04-29 21:54:30', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divisas`
--

CREATE TABLE `divisas` (
  `id_divisa` int(11) NOT NULL,
  `abbrev` varchar(5) DEFAULT NULL,
  `nombre` varchar(128) DEFAULT NULL,
  `tipo_cambio` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `divisas`
--

INSERT INTO `divisas` (`id_divisa`, `abbrev`, `nombre`, `tipo_cambio`, `activo`, `id_log`) VALUES
(1, 'USD', 'Dolar Americano', 17.41, 1, 0),
(2, 'MXN', 'Peso Mexicano', 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilios`
--

CREATE TABLE `domicilios` (
  `id_domicilio` int(11) NOT NULL,
  `calle` varchar(256) DEFAULT NULL,
  `no_exterior` varchar(32) DEFAULT NULL,
  `no_interior` varchar(32) DEFAULT NULL,
  `colonia` varchar(256) DEFAULT NULL,
  `localidad` varchar(256) DEFAULT NULL,
  `referencia` text,
  `municipio` varchar(256) DEFAULT NULL,
  `estado` varchar(64) DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `codigo_postal` varchar(32) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `domicilios`
--

INSERT INTO `domicilios` (`id_domicilio`, `calle`, `no_exterior`, `no_interior`, `colonia`, `localidad`, `referencia`, `municipio`, `estado`, `id_pais`, `codigo_postal`, `fecha_alta`) VALUES
(1, 'Francisco Javier Mina', '312', 'N/A', 'Valles de Huinalá', 'Bodega industrial', 'Monterrey', 'Apodaca', 'Nuevo León', 117, '66600', '2016-01-08 05:11:00'),
(2, 'Test', NULL, '221', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 05:11:53'),
(3, '33333333333', '22', '45555555555', 'Las pruebas', 'salkdjf', 'Casa sola', 'klsdf', 'alksdf', 17, '3452', '2016-01-08 05:22:25'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 05:23:10'),
(5, NULL, 'ext', 'int', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 06:29:56'),
(6, 'Av. de la Palma', '234', '21', 'Palmira', 'Edificio blanco', 'Ciudad de México', 'Alvaro Obregón', 'Distrito Federal', 117, '03630', '2016-01-11 14:28:57'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 14:42:12'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 14:42:45'),
(9, NULL, 'asdfsda', NULL, NULL, NULL, NULL, NULL, 'asdf', 102, NULL, '2016-01-11 14:44:14'),
(10, 'Sin calle conocida', '2145', NULL, 'Primavera', 'Monterrey', NULL, 'Monterrey', NULL, 117, NULL, '2016-01-11 14:44:52'),
(11, 'asdfasdfasd', 'askdlfja', 'lasl', 'aslkfkdas', 'aslkdlk', NULL, 'lkasjafñlkajsdf', 'askflksadjfdsal', 113, '77777', '2016-01-11 15:06:20'),
(12, 'Callejón de los Milagros', '123', NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 17:30:38'),
(13, 'Prueba', '2345', NULL, 'Ningun', NULL, NULL, 'Monterrey', 'Nuevo León', 117, '2456654', '2016-01-12 12:07:59'),
(14, 'jkashdfilhsdaf lu', '2323', '34', 'jdf ñasdfj kasdh', NULL, NULL, NULL, NULL, 117, NULL, '2016-02-01 00:51:44'),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-02-27 10:59:47'),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-04-08 21:37:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id_impuesto` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `porcentaje` double DEFAULT NULL,
  `id_log` int(11) DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`id_impuesto`, `nombre`, `porcentaje`, `id_log`, `activo`) VALUES
(1, 'Mexico', 16, 0, 1),
(3, 'prueba 1', 34, NULL, 0),
(4, 'Mexico', 16, 0, 0),
(5, 'USA', 12.4, 0, 1),
(6, 'Canada', 11, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencias`
--

CREATE TABLE `incidencias` (
  `id_incidencia` int(11) NOT NULL,
  `id_cotizacion` int(11) DEFAULT NULL,
  `autorizada` tinyint(11) DEFAULT '0',
  `autorizada_rechazo` text,
  `motivo` varchar(200) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `id_usuario_resolvio` int(11) DEFAULT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `id_log` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `incidencias`
--

INSERT INTO `incidencias` (`id_incidencia`, `id_cotizacion`, `autorizada`, `autorizada_rechazo`, `motivo`, `descripcion`, `fecha`, `id_usuario_alta`, `id_usuario_resolvio`, `activo`, `id_log`) VALUES
(1, 11, 0, NULL, NULL, 'Sin descripicón por el momento', '2016-04-29 19:35:40', 1, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kit`
--

CREATE TABLE `kit` (
  `id_kit` int(11) NOT NULL,
  `nombre_kit` text,
  `tipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `kit`
--

INSERT INTO `kit` (`id_kit`, `nombre_kit`, `tipo`, `id_proveedor`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 'Kit One', NULL, 3, 1800, 0, 1, 0),
(2, 'Kit 225', NULL, 3, 1200, 0, 1, 0),
(3, 'Kit 212', NULL, 3, 999.9, 0, 0, 0),
(4, '', NULL, 0, 0, 0, 0, 0),
(5, '', NULL, 0, 0, 0, 0, 0),
(6, '', NULL, 0, 0, 0, 0, 0),
(7, '', NULL, 0, 0, 0, 0, 0),
(8, '', NULL, 0, 0, 0, 0, 0),
(9, 'Kit X2', NULL, 1, 7.5, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kit_componente`
--

CREATE TABLE `kit_componente` (
  `id_kit_componente` int(11) NOT NULL,
  `id_kit` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad_kit_componente` int(11) DEFAULT '1',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `kit_componente`
--

INSERT INTO `kit_componente` (`id_kit_componente`, `id_kit`, `id_componente`, `cantidad_kit_componente`, `activo`, `id_log`) VALUES
(1, 1, 2, 1, 0, 0),
(2, 1, 3, 1, 0, 0),
(3, 1, 5, 2, 0, 0),
(4, 3, 2, 2, 0, 0),
(5, 9, 2, 1, 1, 5),
(6, 9, 3, 1, 1, 6),
(7, 9, 4, 2, 1, 7),
(8, 9, 5, 4, 1, 8),
(9, 2, 2, 2, 1, 9),
(10, 2, 3, 4, 1, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `evento` varchar(32) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `descripcion` text,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`id_log`, `id_usuario`, `evento`, `ip`, `descripcion`, `fecha`) VALUES
(1, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 17:01:29'),
(2, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 17:02:01'),
(3, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 2 ', '2016-01-07 09:50:47'),
(4, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 3 ', '2016-01-07 09:51:33'),
(5, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 2 ', '2016-01-07 09:51:38'),
(6, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 09:52:09'),
(7, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 09:52:34'),
(8, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 09:53:44'),
(9, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 4 ', '2016-01-07 09:54:24'),
(10, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 09:59:34'),
(11, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 5 ', '2016-01-07 10:00:13'),
(12, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 10:16:38'),
(13, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 10:17:27'),
(14, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 10:33:11'),
(15, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 11:13:43'),
(16, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 12:57:09'),
(17, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 12:57:17'),
(18, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 6 ', '2016-01-07 13:36:05'),
(19, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 13:36:39'),
(20, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 13:37:03'),
(21, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 13:39:33'),
(22, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 13:39:48'),
(23, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 05:11:00'),
(24, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 05:11:53'),
(25, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al proveedor con el id 1 ', '2016-01-08 05:21:58'),
(26, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 05:22:26'),
(27, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 05:23:11'),
(28, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 05:43:00'),
(29, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 05:43:56'),
(30, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 05:45:09'),
(31, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 06:27:57'),
(32, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 06:28:26'),
(33, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 4 ', '2016-01-08 06:28:41'),
(34, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 1 ', '2016-01-08 06:29:31'),
(35, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 06:29:56'),
(36, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 5 ', '2016-01-08 06:31:09'),
(37, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 1 ', '2016-01-11 14:28:57'),
(38, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 2 ', '2016-01-11 14:44:14'),
(39, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 3 ', '2016-01-11 14:44:52'),
(40, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 4 ', '2016-01-11 15:06:20'),
(41, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 15:08:18'),
(42, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 3 ', '2016-01-11 15:08:31'),
(43, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 3 ', '2016-01-11 15:09:03'),
(44, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 15:09:16'),
(45, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 15:24:54'),
(46, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 15:25:46'),
(47, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 15:26:11'),
(48, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 15:27:26'),
(49, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 5 ', '2016-01-11 17:30:38'),
(50, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 6 ', '2016-01-12 12:08:00'),
(51, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 12:17:15'),
(52, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 12:20:55'),
(53, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 12:21:08'),
(54, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 6 ', '2016-01-12 22:22:51'),
(55, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 22:26:43'),
(56, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 22:26:53'),
(57, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 22:28:40'),
(58, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-12 22:31:09'),
(59, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 4 ', '2016-01-13 00:51:29'),
(60, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 4 ', '2016-01-13 00:52:01'),
(61, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 6 ', '2016-01-13 00:58:42'),
(62, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 00:59:18'),
(63, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 00:59:36'),
(64, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-13 20:30:09'),
(65, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-13 20:30:42'),
(66, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un color con el id: 0 ', '2016-01-13 20:31:32'),
(67, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 07:51:39'),
(68, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 07:51:52'),
(69, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 07:52:02'),
(70, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un color con el id: 0 ', '2016-01-18 07:52:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multiplicadores`
--

CREATE TABLE `multiplicadores` (
  `id_multiplicador` int(11) NOT NULL,
  `nombre_multiplicador` varchar(16) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `multiplicadores`
--

INSERT INTO `multiplicadores` (`id_multiplicador`, `nombre_multiplicador`, `valor`, `activo`, `id_log`) VALUES
(1, 'VA', 2.5, 1, 0),
(2, 'VB', 1.66, 1, 0),
(3, 'VC', 1.392, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `nombre`) VALUES
(1, 'Afganistán'),
(2, 'Albania'),
(3, 'Alemania'),
(4, 'Andorra'),
(5, 'Angola'),
(6, 'Antigua y Barbuda'),
(7, 'Arabia Saudita'),
(8, 'Argelia'),
(9, 'Argentina'),
(10, 'Armenia'),
(11, 'Australia'),
(12, 'Austria'),
(13, 'Azerbaiyán'),
(14, 'Bahamas'),
(15, 'Bangladesh'),
(16, 'Barbados'),
(17, 'Baréin'),
(18, 'Bélgica'),
(19, 'Belice'),
(20, 'Benín'),
(21, 'Bielorrusia'),
(22, 'Birmania'),
(23, 'Bolivia'),
(24, 'Bosnia-Herzegovina'),
(25, 'Botsuana'),
(26, 'Brasil'),
(27, 'Brunéi'),
(28, 'Bulgaria'),
(29, 'Burkina Faso'),
(30, 'Burundi'),
(31, 'Bután'),
(32, 'Cabo Verde'),
(33, 'Camboya'),
(34, 'Camerún'),
(35, 'Canadá'),
(36, 'Chad'),
(37, 'Chile'),
(38, 'China'),
(39, 'Chipre'),
(40, 'Ciudad del Vaticano'),
(41, 'Colombia'),
(42, 'Comoras'),
(43, 'Corea del Norte'),
(44, 'Corea del Sur'),
(45, 'Costa de Marfil'),
(46, 'Costa Rica'),
(47, 'Croacia'),
(48, 'Cuba'),
(49, 'Dinamarca'),
(50, 'Dominica'),
(51, 'Ecuador'),
(52, 'Egipto'),
(53, 'El Salvador'),
(54, 'Emiratos Árabes Unidos'),
(55, 'Eritrea'),
(56, 'Eslovaquia'),
(57, 'Eslovenia'),
(58, 'España'),
(59, 'Estados Unidos'),
(60, 'Estonia'),
(61, 'Etiopia'),
(62, 'Filipinas'),
(63, 'Finlandia'),
(64, 'Fiyi'),
(65, 'Francia'),
(66, 'Gabón'),
(67, 'Gambia'),
(68, 'Georgia'),
(69, 'Ghana'),
(70, 'Granada'),
(71, 'Grecia'),
(72, 'Guatemala'),
(73, 'Guyana'),
(74, 'Guinea'),
(75, 'Guinea ecuatorial'),
(76, 'Guinea-Bisáu'),
(77, 'Haití'),
(78, 'Holanda'),
(79, 'Honduras'),
(80, 'Hungría'),
(81, 'India'),
(82, 'Indonesia'),
(83, 'Irak'),
(84, 'Irán'),
(85, 'Irlanda'),
(86, 'Islandia'),
(87, 'Islas Marshall'),
(88, 'Islas Salomón'),
(89, 'Israel'),
(90, 'Italia'),
(91, 'Jamaica'),
(92, 'Japón'),
(93, 'Jordania'),
(94, 'Kazajistán'),
(95, 'Kenia'),
(96, 'Kirguistán'),
(97, 'Kiribati'),
(98, 'Kuwait'),
(99, 'Laos'),
(100, 'Lesoto'),
(101, 'Letonia'),
(102, 'Libano'),
(103, 'Liberia'),
(104, 'Libia'),
(105, 'Liechtenstein'),
(106, 'Lituania'),
(107, 'Luxemburgo'),
(108, 'Madagascar'),
(109, 'Malasia'),
(110, 'Malaui'),
(111, 'Maldivas'),
(112, 'Malí'),
(113, 'Malta'),
(114, 'Marruecos'),
(115, 'Mauricio'),
(116, 'Mauritania'),
(117, 'México'),
(118, 'Micronesia'),
(119, 'Moldavia'),
(120, 'Mónaco'),
(121, 'Mongolia'),
(122, 'Montenegro'),
(123, 'Mozambique'),
(124, 'Namibia'),
(125, 'Nauru'),
(126, 'Nepal'),
(127, 'Nicaragua'),
(128, 'Níger'),
(129, 'Nigeria'),
(130, 'Noruega'),
(131, 'Nueva Zelanda'),
(132, 'Omán'),
(133, 'Pakistán'),
(134, 'Palaos'),
(135, 'Panamá'),
(136, 'Papua Nueva Guinea'),
(137, 'Paraguay'),
(138, 'Perú'),
(139, 'Polonia'),
(140, 'Portugal'),
(141, 'Qatar'),
(142, 'Reino Unido'),
(143, 'República Centroafricana'),
(144, 'República Checa'),
(145, 'República de Macedonia'),
(146, 'República del Congo'),
(147, 'República Democratica del Congo'),
(148, 'República Dominicana'),
(149, 'República Sudafricana'),
(150, 'Ruanda'),
(151, 'Rumania'),
(152, 'Rusia'),
(153, 'Samoa'),
(154, 'San Cristóbal y Nieves'),
(155, 'San Marino'),
(156, 'San Vicente y las Granadinas'),
(157, 'Santa Lucía'),
(158, 'Santo Tomé y Príncipe'),
(159, 'Senegal'),
(160, 'Serbia'),
(161, 'Seychelles'),
(162, 'Sierra Leona'),
(163, 'Singapur'),
(164, 'Siria'),
(165, 'Somalia'),
(166, 'Sri Lanka'),
(167, 'Suazilandia'),
(168, 'Sudán'),
(169, 'Sudán del Sur'),
(170, 'Suecia'),
(171, 'Suiza'),
(172, 'Surinam'),
(173, 'Tailandia'),
(174, 'Tanzania'),
(175, 'Tayikistán'),
(176, 'Timor Oriental'),
(177, 'Togo'),
(178, 'Tonga'),
(179, 'Trinidad y Tobago'),
(180, 'Túnez'),
(181, 'Turkmenistán'),
(182, 'Turquía'),
(183, 'Tuvalu'),
(184, 'Ucrania'),
(185, 'Uganda'),
(186, 'Uruguay'),
(187, 'Uzbekistán'),
(188, 'Vanuatu'),
(189, 'Venezuela'),
(190, 'Vietnam'),
(191, 'Yemen'),
(192, 'Yibuti'),
(193, 'Zambia'),
(194, 'Zimbawe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_permiso` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permiso`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Administrador', 1, NULL),
(2, 'Director', 1, NULL),
(3, 'Director Produccion', 1, NULL),
(4, 'Direcctor Ventas', 1, NULL),
(5, 'Cliente', 1, NULL),
(6, 'Vendedor', 1, NULL),
(7, 'Produccion', 1, NULL),
(8, 'Despacho', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `tipo` varchar(16) DEFAULT NULL,
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `nombre`, `tipo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `id_domicilio`, `id_usuario_alta`, `fecha_alta`, `activo`, `id_log`) VALUES
(1, 'HS puertas de garaje', 'Nacional', 'Guillermo Ortiz', 'test@hs.com', '8112341000', '112', 1, 1, '2016-01-08 05:11:00', 1, 34),
(2, 'test', 'Nacional', '0', NULL, NULL, NULL, 2, 1, '2016-01-08 05:11:53', 1, 24),
(3, 'Pruebas de desarrollo', 'Nacional', 'jñlksdfaj', 'esteesuncorreoelectonicomuylargoparaprobarlabd@testingdecorreo.com', '342332423', '232', 3, 1, '2016-01-08 05:22:26', 1, 32),
(4, 'Juan Javier Jaime Gabriel', 'Nacional', NULL, NULL, NULL, NULL, 4, 1, '2016-01-08 05:23:10', 1, 33),
(5, 'test', 'Nacional', NULL, NULL, NULL, NULL, 5, 1, '2016-01-08 06:29:56', 1, 36);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta`
--

CREATE TABLE `puerta` (
  `id_puerta` int(11) NOT NULL,
  `m_ancho` double DEFAULT NULL,
  `m_alto` double DEFAULT NULL,
  `f_ancho` double DEFAULT NULL,
  `in_ancho` double DEFAULT NULL,
  `f_alto` double DEFAULT NULL,
  `in_alto` double DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta`
--

INSERT INTO `puerta` (`id_puerta`, `m_ancho`, `m_alto`, `f_ancho`, `in_ancho`, `f_alto`, `in_alto`, `id_ptipo`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 3.05, 2.1, 10, 2, 7, 4, 1, 432, 0, 1, 0),
(2, 3.05, 2.4, 10, 2, 8, 1, 1, 421, 0, 1, 0),
(3, 2.42, 2.42, 8, 4, 7, 2, 1, 425, 0, 1, 0),
(4, 2.44, 2.1, 8, 7, 4, 2, 2, 678, 0, 1, 0),
(5, 2, 2, 2, 2, 2, 2, 1, 100, 0, 1, 0),
(6, 1, 1, 1, 1, 1, 1, 2, 1200, 0, 1, 0),
(7, 1, 1, 1, 1, 1, 1, 1, 1350, 0, 1, 0),
(8, 1, 1, 1, 1, 1, 1, 2, 2500, 0, 1, 0),
(9, 2, 2, 2, 2, 2, 2, 2, 1800, 0, 1, 0),
(10, 3, 4, 3, 2, 3, 2, 1, 1280, 0, 1, 0),
(11, 11, 11, 11, 11, 111, 111, 1, 10000, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_color`
--

CREATE TABLE `puerta_color` (
  `id_pcolor` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `default` smallint(6) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_color`
--

INSERT INTO `puerta_color` (`id_pcolor`, `nombre_color`, `descripcion`, `default`, `activo`, `id_log`) VALUES
(1, 'Blanco', NULL, 1, 1, 0),
(2, 'Almendra', NULL, 2, 1, 0),
(3, 'Café', 'Color café', 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_componente`
--

CREATE TABLE `puerta_componente` (
  `id` int(11) NOT NULL,
  `id_puerta` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_componente`
--

INSERT INTO `puerta_componente` (`id`, `id_puerta`, `id_componente`, `cantidad`, `activo`, `id_log`) VALUES
(4, 1, 2, 10, 1, 0),
(12, 2, 2, 8, 1, 0),
(13, 2, 3, 2, 1, 0),
(14, 3, 2, 6, 1, 0),
(15, 1, 4, 8, 1, 0),
(18, 3, 6, 8, 1, 0),
(19, 11, 2, 4, 1, 0),
(20, 11, 5, 2, 1, 0),
(24, 11, 6, 8, 1, 0),
(27, 1, 5, 2, 1, 0),
(28, 1, 6, 2, 1, 0),
(29, 1, 3, 12, 1, 0),
(30, 4, 2, 2, 1, 0),
(31, 4, 3, 4, 1, 0),
(32, 4, 4, 8, 1, 0),
(33, 2, 6, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_modelo`
--

CREATE TABLE `puerta_modelo` (
  `id_pmodelo` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_modelo` text,
  `descripcion` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_modelo`
--

INSERT INTO `puerta_modelo` (`id_pmodelo`, `id_ptipo`, `nombre_modelo`, `descripcion`, `default`, `activo`, `id_log`) VALUES
(1, 1, 'Classica', 'Puerta default', 1, 1, 0),
(2, 1, 'Finesse', 'Sencilla y agradable', 1, 1, 0),
(3, 1, 'Elegantti', NULL, 2, 1, 0),
(4, 2, 'BF10', NULL, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_motor`
--

CREATE TABLE `puerta_motor` (
  `id_pmotor` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_motor` text,
  `descripcion` text,
  `precio` int(11) DEFAULT NULL,
  `impuesto` int(11) DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_motor`
--

INSERT INTO `puerta_motor` (`id_pmotor`, `id_proveedor`, `nombre_motor`, `descripcion`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Sin Motor', NULL, 0, 0, 1, 1, 0),
(2, NULL, 'HS ONE', NULL, 2296, 15, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_movimiento`
--

CREATE TABLE `puerta_movimiento` (
  `id_pmovimiento` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_movimiento` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_movimiento`
--

INSERT INTO `puerta_movimiento` (`id_pmovimiento`, `id_ptipo`, `nombre_movimiento`, `default`, `activo`, `id_log`) VALUES
(1, 1, 'Estandar', 1, 1, 0),
(2, 1, 'Abatible', 2, 1, 0),
(3, 2, 'Corrediza', 2, 1, 0),
(4, 1, 'Syndicate', 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_sello`
--

CREATE TABLE `puerta_sello` (
  `id_psello` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_sello` text,
  `tipo` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_sello`
--

INSERT INTO `puerta_sello` (`id_psello`, `id_proveedor`, `nombre_sello`, `tipo`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, 0, 'Sin Sello', 0, 0, 0, 1, 1, 0),
(2, NULL, 'Perimetral', 1, 10, 0, 2, 1, 0),
(3, NULL, 'Flexible', 1, 0, 0, 2, 1, 0),
(4, NULL, 'Rigido', 1, 0, 0, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_textura`
--

CREATE TABLE `puerta_textura` (
  `id_ptextura` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_textura` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_textura`
--

INSERT INTO `puerta_textura` (`id_ptextura`, `id_proveedor`, `nombre_textura`, `descripcion`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Madera', 'Textura por defecto', 0, NULL, 1, 1, 0),
(2, NULL, 'Lisa', NULL, 30, 1.15, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_tipo`
--

CREATE TABLE `puerta_tipo` (
  `id_ptipo` int(11) NOT NULL,
  `nombre_tipo` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puerta_tipo`
--

INSERT INTO `puerta_tipo` (`id_ptipo`, `nombre_tipo`, `default`, `activo`, `id_log`) VALUES
(1, 'Residencial', 1, 1, 0),
(2, 'Industrial', 2, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puerta_ventana`
--

CREATE TABLE `puerta_ventana` (
  `id_puerta` int(11) DEFAULT NULL,
  `id_ventana` int(11) DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `pwd` varchar(64) DEFAULT NULL,
  `id_divisa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `nombre_usuario` varchar(256) DEFAULT NULL,
  `email_usuario` varchar(128) DEFAULT NULL,
  `telefono_usuario` varchar(32) DEFAULT NULL,
  `ext_usuario` varchar(16) DEFAULT NULL,
  `num_empleado` varchar(32) DEFAULT NULL,
  `permiso_usuario` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `accesos` int(11) DEFAULT '0',
  `id_log` int(11) DEFAULT '0',
  `ultimo_acceso` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `username`, `pwd`, `id_divisa`, `id_cliente`, `nombre_usuario`, `email_usuario`, `telefono_usuario`, `ext_usuario`, `num_empleado`, `permiso_usuario`, `fecha_alta`, `activo`, `accesos`, `id_log`, `ultimo_acceso`) VALUES
(1, 'admintk', 'Teknik', 2, 1, 'Teknik', 'webmaster@teknik.mx', '(81) 1931 4009', NULL, NULL, 1, '2016-01-06 12:40:29', 1, 63, 22, '2016-01-12 03:08:31'),
(2, 'pp', 'perez', 1, NULL, 'Jose', 'pp@perez.com', NULL, NULL, NULL, 1, '2016-01-07 09:50:47', 0, 0, 5, '2016-01-07 09:51:38'),
(3, 'pp', 'perez', 1, NULL, 'Jose', 'pp@perez.com', '3233', '32', '3232', 8, '2016-01-07 09:51:33', 1, 2, 17, '2016-01-07 12:57:18'),
(4, 'lopez', 'juan', 2, NULL, 'Juan Lopez', 'juan@lopez.co', '345', '22', '222', 5, '2016-01-07 09:54:23', 1, 0, 13, '2016-01-07 10:17:27'),
(5, 'asdf', 'asdf', 1, NULL, 'asdfasd', 'asdf@askdf.com', NULL, NULL, '21221', 5, '2016-01-07 10:00:13', 1, 0, 21, '2016-01-07 13:39:33'),
(6, 'asdf', 'asfd', 1, NULL, 'asdf', 'test@prueba.com', '222221111', NULL, '2222', 8, '2016-01-07 13:36:05', 1, 0, 20, '2016-01-07 13:37:03'),
(7, 'ventas', 'ventas', 1, NULL, 'Ventas', 'ventas@test.com', NULL, NULL, '23425235', 6, '2016-02-27 10:54:01', 1, 1, 7, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventana`
--

CREATE TABLE `ventana` (
  `id_ventana` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_ventana` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventana`
--

INSERT INTO `ventana` (`id_ventana`, `id_ptipo`, `id_proveedor`, `nombre_ventana`, `descripcion`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 0, NULL, 'Sin Ventana', '', 0, 0, 1, 0),
(2, 1, NULL, 'Cascade', 'Cascade Window', 99.9, 0, 1, 0),
(3, 2, NULL, 'Big Daddy', 'Big Daddy Window', 1200, 0, 1, 0),
(4, 1, NULL, 'False Window', 'False Window', 1.2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventana_color`
--

CREATE TABLE `ventana_color` (
  `id_vcolor` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventana_color`
--

INSERT INTO `ventana_color` (`id_vcolor`, `nombre_color`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Blanco', '-', 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `componentes`
--
ALTER TABLE `componentes`
  ADD PRIMARY KEY (`id_componente`);

--
-- Indices de la tabla `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
  ADD PRIMARY KEY (`id_grupo_componente`);

--
-- Indices de la tabla `conceptos`
--
ALTER TABLE `conceptos`
  ADD PRIMARY KEY (`id_concepto`);

--
-- Indices de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- Indices de la tabla `depositos`
--
ALTER TABLE `depositos`
  ADD PRIMARY KEY (`id_deposito`);

--
-- Indices de la tabla `divisas`
--
ALTER TABLE `divisas`
  ADD PRIMARY KEY (`id_divisa`);

--
-- Indices de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  ADD PRIMARY KEY (`id_domicilio`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id_impuesto`);

--
-- Indices de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  ADD PRIMARY KEY (`id_incidencia`);

--
-- Indices de la tabla `kit`
--
ALTER TABLE `kit`
  ADD PRIMARY KEY (`id_kit`);

--
-- Indices de la tabla `kit_componente`
--
ALTER TABLE `kit_componente`
  ADD PRIMARY KEY (`id_kit_componente`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indices de la tabla `multiplicadores`
--
ALTER TABLE `multiplicadores`
  ADD PRIMARY KEY (`id_multiplicador`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `puerta`
--
ALTER TABLE `puerta`
  ADD PRIMARY KEY (`id_puerta`);

--
-- Indices de la tabla `puerta_color`
--
ALTER TABLE `puerta_color`
  ADD PRIMARY KEY (`id_pcolor`);

--
-- Indices de la tabla `puerta_componente`
--
ALTER TABLE `puerta_componente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  ADD PRIMARY KEY (`id_pmodelo`);

--
-- Indices de la tabla `puerta_motor`
--
ALTER TABLE `puerta_motor`
  ADD PRIMARY KEY (`id_pmotor`);

--
-- Indices de la tabla `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
  ADD PRIMARY KEY (`id_pmovimiento`);

--
-- Indices de la tabla `puerta_sello`
--
ALTER TABLE `puerta_sello`
  ADD PRIMARY KEY (`id_psello`);

--
-- Indices de la tabla `puerta_textura`
--
ALTER TABLE `puerta_textura`
  ADD PRIMARY KEY (`id_ptextura`);

--
-- Indices de la tabla `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  ADD PRIMARY KEY (`id_ptipo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `ventana`
--
ALTER TABLE `ventana`
  ADD PRIMARY KEY (`id_ventana`);

--
-- Indices de la tabla `ventana_color`
--
ALTER TABLE `ventana_color`
  ADD PRIMARY KEY (`id_vcolor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `componentes`
--
ALTER TABLE `componentes`
  MODIFY `id_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
  MODIFY `id_grupo_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `conceptos`
--
ALTER TABLE `conceptos`
  MODIFY `id_concepto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `depositos`
--
ALTER TABLE `depositos`
  MODIFY `id_deposito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `divisas`
--
ALTER TABLE `divisas`
  MODIFY `id_divisa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  MODIFY `id_domicilio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id_impuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `incidencias`
--
ALTER TABLE `incidencias`
  MODIFY `id_incidencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `kit`
--
ALTER TABLE `kit`
  MODIFY `id_kit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `kit_componente`
--
ALTER TABLE `kit_componente`
  MODIFY `id_kit_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT de la tabla `multiplicadores`
--
ALTER TABLE `multiplicadores`
  MODIFY `id_multiplicador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `puerta`
--
ALTER TABLE `puerta`
  MODIFY `id_puerta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `puerta_color`
--
ALTER TABLE `puerta_color`
  MODIFY `id_pcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `puerta_componente`
--
ALTER TABLE `puerta_componente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  MODIFY `id_pmodelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `puerta_motor`
--
ALTER TABLE `puerta_motor`
  MODIFY `id_pmotor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
  MODIFY `id_pmovimiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `puerta_sello`
--
ALTER TABLE `puerta_sello`
  MODIFY `id_psello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `puerta_textura`
--
ALTER TABLE `puerta_textura`
  MODIFY `id_ptextura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  MODIFY `id_ptipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `ventana`
--
ALTER TABLE `ventana`
  MODIFY `id_ventana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `ventana_color`
--
ALTER TABLE `ventana_color`
  MODIFY `id_vcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
