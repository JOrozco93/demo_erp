<?php

	set_time_limit(0);
	
	session_start();

	if (isset($_SESSION['id_usuario']) && $_SESSION['id_usuario'] != "") {
    	$id_usuario = $_SESSION['id_usuario'];
    } else {
    	header("Location: logout.php");
    	exit();
    }

    if (isset($_GET['id_cotizacion']) && $_GET['id_cotizacion']) {
    	$id_cotizacion = $_GET['id_cotizacion'];
    } else {
    	header("Location: inicio.php");
    	exit();
    }
   
    require_once("config.php");
	require_once("includes/funciones_BD.php");

	// get the xml path from db
	$stmt = $pdo->prepare("SELECT * FROM cotizaciones WHERE id_cotizacion = :id_cotizacion");

	$stmt->bindParam(':id_cotizacion', $id_cotizacion);

	$result = $stmt->execute();

	if ($result) {
		$cotizacion = $stmt->fetch(PDO::FETCH_ASSOC);

		$file = $cotizacion['factura_pdf_url'];

		if (is_file($file)) {
			header('Content-Disposition: attachment; filename=' . urlencode(basename($file)));
		    header('Content-Type: application/octet-stream');
		    header("Content-Type: application/force-download");
		    header('Content-Description: File Transfer');
		    
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    
		    // ob_clean();
		    // flush();
		    
		    readfile($file);

		    exit();
		} else {
			header("Location: listado-ventas.php");
			exit();
		}
	} else {
		header("Location: listado-ventas.php");
		exit();
	}

?>



