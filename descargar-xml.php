<?php
	
	session_start();

	if (isset($_SESSION['id_usuario']) && $_SESSION['id_usuario'] != "") {
    	$id_usuario = $_SESSION['id_usuario'];
    } else {
    	header("Location: logout.php");
    	exit();
    }

    if (isset($_GET['id_cotizacion']) && $_GET['id_cotizacion']) {
    	$id_cotizacion = $_GET['id_cotizacion'];
    } else {
    	header("Location: inicio.php");
    	exit();
    }
   
    require_once("config.php");
	require_once("includes/funciones_BD.php");

	// get the xml path from db
	$stmt = $pdo->prepare("SELECT * FROM cotizaciones WHERE id_cotizacion = :id_cotizacion");

	$stmt->bindParam(':id_cotizacion', $id_cotizacion);

	$result = $stmt->execute();

	if ($result) {
		$cotizacion = $stmt->fetch(PDO::FETCH_ASSOC);

		$file = $cotizacion['factura_xml_url'];

		if (is_file($file)) {
			// set IE read from page only, no read from cache
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-cache, must-revalidate");
			header("Pragma: no-cache");

			header("Content-type: text/xml");
			header("Accept-Ranges: bytes");
			header("Content-Length: " + filesize($file));
			header("Content-Disposition: attachment; filename=" . basename($file) . "\n");

			readfile($file);
			exit();
		} else {
			header("Location: listado-ventas.php");
			exit();
		}
	} else {
		header("Location: listado-ventas.php");
		exit();
	}

?>



