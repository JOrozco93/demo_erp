<?php

if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql = 'SELECT * FROM clientes AS c, domicilios AS d WHERE c.id_cliente ='.$id.' AND c.id_domicilio = d.id_domicilio';
        $edit = mysql_fetch_array(consulta($sql));
    }     
    $pais = datoRapido("SELECT nombre AS dato FROM pais WHERE id_pais= ".$edit['id_pais']);   
    $divisa = datoRapido("SELECT CONCAT(abbrev,' - ',nombre) AS dato FROM divisas WHERE id_divisa= ".$edit['divisa_cliente']);
}

$cotizaciones = getCotizacionesByIdCliente($id);
$depositos = getDepositosByIdCliente($id);
$incidencias = getIncidenciasByIdCliente($id);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $name; ?></title>
    <?php include ("includes/css.php"); ?>
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <?php include ("includes/header.php"); ?>
    <div class="clearfix"></div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php include ("includes/menu.php"); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->

                <div class="theme-panel hidden-xs hidden-sm">
                    <div class="toggler-close"></div>
                </div>
                <!-- END STYLE CUSTOMIZER -->
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">Clientes</h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="listado-clientes.php">Clientes</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="listado-clientes.php">Listado de Clientes</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Detalle de Cliente</a>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT--> 
                <?php if ($edit['aprobado'] == 2): ?>
                    <div class="alert alert-danger" role="alert"><?php echo $edit['motivo_aprobado']; ?></div>
                <?php endif; ?>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="portlet box blue">
                            <div class="portlet-title"></div>
                            <div class="portlet-body form">
                                <br/>
                                <div class="row">

                                    <div class="col-sm-12 text-right">  
                                        <a  href="alta-clientes.php?tipo=cliente&id=<?php echo $edit['id_cliente']; ?>" title="Editar cliente" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <?php if ($edit['aprobado'] == '1') { ?>
                                            <a  href="alta-cotizacion.php?id_cliente=<?php echo $edit['id_cliente']; ?>" title="Crear Nueva Cotizacion" data-toggle="tooltip" data-placement="left" style="color: blue" class="btn btn-default">
                                                <i class="fa fa-file-text"></i>
                                            </a>
                                        <?php } ?>
                                        <!--
                                        <?php if ($edit['aprobado'] == '0') { ?>
                                            <a  onclick="estatus(event,'<?php echo $edit['id_cliente']; ?>');" title="Aprobar al Cliente" data-toggle="tooltip" data-placement="left" class="btn btn-warning">
                                                <i class="fa fa-check-square-o"></i>
                                            </a>
                                        <?php } ?>
                                        -->
                                        <?php if ($_SESSION['permiso_usuario'] == 1) { ?>
                                            <a onclick="deshabilita(event, '<?php echo $edit['id_cliente']; ?>', '<?php echo $edit['nombre_cliente']; ?>');" title="Eliminar Cliente" data-toggle="tooltip" data-placement="right" class="btn btn-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-12">  
                                        <h2 style="font-size:1.6em;"> &nbsp; <?php echo $edit['nombre_cliente'];?></h2>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-12 well">                                                                              
                                            <p>
                                                <i class="fa fa-user"></i> &nbsp; Contacto: <?php echo $edit['nombre_contacto']; ?><br/>
                                                <i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit['telefono_contacto']; ?><?php if($edit['ext_contacto']!='') { ?> ext <?php echo $edit['ext_contacto']; } ?><br/>   
                                                <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit['email_contacto']; ?><br/>
                                                <i class="fa fa-calendar"></i> Fecha: <?php echo date_format(date_create($edit['fecha_alta']), 'Y-m-d'); ?><br/>
                                            </p>
                                            <p><i class="fa fa-users"></i> &nbsp; Tipo: <?php echo ($edit['tipo']==1)?'Prospecto':'Cliente'; ?></p> 
                                            <p>&nbsp;</p>
                                            <?php if($edit['nombre_autorizado'] || $edit['telefono_autorizado'] || $edit['ext_autorizado'] || $edit['email_autorizado']){ ?>
                                            <h4>Personal autorizado para hacer pedidos</h4>                                    
                                            <p>
                                                <i class="fa fa-user"></i> &nbsp; Contacto: <?php echo $edit['nombre_autorizado']; ?><br/>
                                                <i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit['telefono_autorizado']; ?> <?php if($edit['ext_autorizado']!=''){ ?> ext <?php echo $edit['ext_autorizado']; } ?> <br/>   
                                                <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit['email_autorizado']; ?>
                                            </p>
                                            <?php } ?>
                                        </div><!-- /.col-->     
                                        <div class="col-sm-12 ">
                                            <h4><i class="fa fa-map-pin"></i> &nbsp;<?php if($edit['calle'] || $edit['no_exterior'] || $edit['no_interior']) { echo $edit['calle']." ".$edit['no_exterior']." ".$edit['no_interior']; } ?></h4> 
                                            <?php if($edit['colonia']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['colonia']; ?></p>  <?php } ?>  
                                            <?php if($edit['referencia']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['referencia']; ?></p><?php } ?>  
                                            <?php if($edit['localidad'] || $edit['municipio'] || $edit['estado']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['localidad']." ".$edit['municipio']." ".$edit['estado']; ?></p><?php } ?>  
                                            <p> &nbsp; &nbsp; &nbsp; <?php echo $pais; ?> <?php if($edit['codigo_postal']){ echo "CP ".$edit['codigo_postal']; } ?></p>                            
                                        </div><!-- /.col-->                                  
                                    </div>
                                </div>
                                <br/>            
                                <div class="form-actions fluid">
                                    <div class="col-lg-6">
                                        <a class="btn btn-warning" href="listado-clientes.php"><i class="fa fa-arrow-circle-left"></i> Regresar a la lista</a>
                                    </div>
                                    <div class="col-lg-6">                                        
                                        <a href="alta-clientes.php?tipo=cliente&id=<?php echo $edit['id_cliente']; ?>" class="btn btn-info pull-right"><i class="fa fa-pencil" ></i> Modificar</a>                                         
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Detalles del cliente -->

                    <!-- tables -->
                    <div class="col-md-8">
                        <div class="portlet box blue">
                            <div class="portlet-title"></div>
                            <div class="portlet-body">
                                <h4><i class="fa fa-list-alt" style="color: #ffb848;"></i> Cotizaciones</h4>
                                <hr>
                                <table id="table-cotizaciones" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No. Cotización</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($cotizaciones as $cotizacion): ?>
                                            <?php if ($cotizacion['autorizada'] == 0 && $cotizacion['pagada'] != 2 && $cotizacion['produccion'] == 0 && $cotizacion['despacho'] == 0): ?>
                                                <tr>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $cotizacion['id_cotizacion']; ?>"><?php echo $cotizacion['numero_cotizacion']; ?></a></td>
                                                    <td><?php echo $cotizacion['fecha']; ?></td>
                                                    <td><?php echo $cotizacion['descripcion']; ?></td>
                                                    <td><?php echo number_format($cotizacion['total'], 2, '.', ','); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No. Cotización</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Total</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="portlet box blue">
                            <div class="portlet-title"></div>
                            <div class="portlet-body">
                                <h4><i class="fa fa-tasks" style="color: #d9534f;"></i> Pedidos en proceso</h4>
                                <hr>
                                <table id="table-pedidos-proceso" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Estatus</th>
                                            <th>No. Cotización</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($cotizaciones as $cotizacion): ?>
                                            <!-- ventas -->
                                            <?php if ($cotizacion['autorizada'] == 1 && $cotizacion['pagada'] != 2 && $cotizacion['produccion'] == 0 && $cotizacion['despacho'] == 0): ?>
                                                <tr>
                                                    <td><i class="fa fa-shopping-cart" style="color: #28b779;"></i></td>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $cotizacion['id_cotizacion']; ?>"><?php echo $cotizacion['numero_cotizacion']; ?></a></td>
                                                    <td><?php echo $cotizacion['fecha']; ?></td>
                                                    <td><?php echo $cotizacion['descripcion']; ?></td>
                                                    <td><?php echo number_format($cotizacion['total'], 2, '.', ','); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <!-- ventas -->

                                            <!-- producción -->
                                            <?php if ($cotizacion['autorizada'] == 1 && $cotizacion['pagada'] == 2 && $cotizacion['produccion'] == 0 && $cotizacion['despacho'] == 0): ?>
                                                <tr>
                                                    <td><i class="fa fa-cog" style="color: #852b99;"></i></td>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $cotizacion['id_cotizacion']; ?>"><?php echo $cotizacion['numero_cotizacion']; ?></a></td>
                                                    <td><?php echo $cotizacion['fecha']; ?></td>
                                                    <td><?php echo $cotizacion['descripcion']; ?></td>
                                                    <td><?php echo number_format($cotizacion['total'], 2, '.', ','); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <!-- producción -->

                                            <!-- despacho -->
                                            <?php if ($cotizacion['autorizada'] == 1 && $cotizacion['pagada'] == 2 && $cotizacion['produccion'] == 1 && $cotizacion['despacho'] == 0): ?>
                                                <tr>
                                                    <td><i class="fa fa-truck" style="color: #27a9e3;"></i></td>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $cotizacion['id_cotizacion']; ?>"><?php echo $cotizacion['numero_cotizacion']; ?></a></td>
                                                    <td><?php echo $cotizacion['fecha']; ?></td>
                                                    <td><?php echo $cotizacion['descripcion']; ?></td>
                                                    <td><?php echo number_format($cotizacion['total'], 2, '.', ','); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <!-- despacho -->
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Estatus</th>
                                            <th>No. Cotización</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Total</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="portlet box blue">
                            <div class="portlet-title"></div>
                            <div class="portlet-body">
                                <h4><i class="fa fa-check" style="color: #28b779;"></i> Finalizadas</h4>
                                <hr>
                                <table id="table-finalizadas" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No. Cotización</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($cotizaciones as $cotizacion): ?>
                                            <?php if ($cotizacion['autorizada'] == 1 && $cotizacion['pagada'] == 2 && $cotizacion['produccion'] == 1 && $cotizacion['despacho'] == 1): ?>
                                                <tr>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $cotizacion['id_cotizacion']; ?>"><?php echo $cotizacion['numero_cotizacion']; ?></a></td>
                                                    <td><?php echo $cotizacion['fecha']; ?></td>
                                                    <td><?php echo $cotizacion['descripcion']; ?></td>
                                                    <td><?php echo number_format($cotizacion['total'], 2, '.', ','); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No. Cotización</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                            <th>Total</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <!--
                        <div class="portlet box blue">
                            <div class="portlet-title">
                            </div>
                            <div class="portlet-body">
                                <h4><i class="fa fa-usd"></i> Depositos</h4>
                                <hr>
                                <table id="table-depositos" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Monto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($depositos as $deposito): ?>
                                            <tr>
                                                <td><?php echo $deposito['fecha']; ?></td>
                                                <td><?php echo number_format($deposito['monto_deposito'], 2, '.', ','); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Monto</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        -->

                        <div class="portlet box blue">
                            <div class="portlet-title">
                            </div>
                            <div class="portlet-body">
                                <h4><i class="fa fa-comment"></i> Incidencias</h4>
                                <hr>
                                <table id="table-incidencias" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No. Cotizacion</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($incidencias as $incidencia): ?>
                                            <tr>
                                                <td><a href="detalle-incidencia.php?id=<?php echo $incidencia['id_incidencia']; ?>"><?php echo $incidencia['numero_cotizacion']; ?></a></td>
                                                <td><?php echo $incidencia['fecha']; ?></td>
                                                <td><?php echo $incidencia['descripcion']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No. Cotizacion</th>
                                            <th>Fecha</th>
                                            <th>Descripción</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>                        
                    </div>
                </div>
                <!-- tablas -->

            </div><!--  page-content  -->
        </div><!--  page-content-wrapper  -->
    </div><!-- page-container -->

    <?php include ("includes/footer.php"); ?>
    <?php include ("includes/js.php"); ?>
    
    <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="scripts/custom/table-advanced.js"></script>
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
    
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            // TableAdvanced.init();
            
            $('#table-cotizaciones').dataTable({
                'aaSorting': [[1, 'desc']],
                'aLengthMenu': [
                    [5, 15, 20, -1],
                    [5, 15, 20, 'All']
                ],
                'iDisplayLength': 10,
            });

            $('#table-pedidos-proceso').dataTable({
                'aaSorting': [[2, 'desc']],
                'aLengthMenu': [
                    [5, 15, 20, -1],
                    [5, 15, 20, 'All']
                ],
                'iDisplayLength': 10,
            });

            $('#table-finalizadas').dataTable({
                'aaSorting': [[1, 'desc']],
                'aLengthMenu': [
                    [5, 15, 20, -1],
                    [5, 15, 20, 'All']
                ],
                'iDisplayLength': 10,
            });

            $('#table-depositos').dataTable({
                'aaSorting': [[0, 'desc']],
                'aLengthMenu': [
                    [5, 15, 20, -1],
                    [5, 15, 20, 'All']
                ],
                'iDisplayLength': 10,
            });

            $('#table-incidencias').dataTable({
                'aaSorting': [[1, 'desc']],
                'aLengthMenu': [
                    [5, 15, 20, -1],
                    [5, 15, 20, 'All']
                ],
                'iDisplayLength': 10,
            });

            $('.dataTables_filter input').addClass("form-control input-small input-inline");
            $('.dataTables_length select').addClass("form-control input-small");
            $('.dataTables_length select').select2();

            $("[data-toggle='tooltip']").tooltip();
        });

        function  deshabilita(event, id, nombre) {
            event.preventDefault();
            var respuesta = confirm('\u00BFDesea Eliminar al Cliente "' + nombre + '"?');
            if (respuesta) {
                location.href = 'listado-clientes.php?borrar=' + id;
            }
        }
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>