<?php

if (!isset($_SESSION)) {
    session_start();
}

// if (($_SESSION['id_usuario'] == NULL)) {
//     header("Location: logout.php");
// }

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    header('Location: listado-cotizaciones.php');
    exit;
}

if (isset($id_usuario) && $id_usuario != 0) {    
    $sql = "SELECT * FROM usuarios WHERE id_usuario = $id_usuario";
    $result = mysql_query($sql, $db_con);
    $editu = mysql_fetch_assoc($result);

    $sql = "
        SELECT
            cotizaciones.id_cotizacion AS id_cotizacion,
            cotizaciones.id_cliente AS id_cliente,
            clientes.nombre_cliente AS nombre_cliente,
            divisas.nombre AS nombre_divisa,
            impuestos.nombre AS nombre_impuesto,
            cotizaciones.tipo_despacho AS tipo_despacho,
            cotizaciones.descripcion AS descripcion,
            terminos_venta.descripcion AS termino_venta,
            cotizaciones.subtotal AS subtotal,
            cotizaciones.impuestos AS impuestos,
            cotizaciones.total AS total
        FROM
            cotizaciones,
            clientes,
            divisas,
            impuestos,
            terminos_venta
        WHERE cotizaciones.id_cliente = clientes.id_cliente AND cotizaciones.id_divisa = divisas.id_divisa AND cotizaciones.id_impuesto = impuestos.id_impuesto AND cotizaciones.id_termino_venta = terminos_venta.id_termino_venta AND cotizaciones.id_cotizacion = $id";
    
    $result = consulta($sql);

    if (mysql_num_rows($result)) {
        $cotizacion = mysql_fetch_array($result);

        $sql = "SELECT * FROM conceptos WHERE id_cotizacion = $id ORDER BY id_concepto";
        $conceptos = consulta($sql);
    } else {
        header('Location: listado-cotizaciones.php');
        exit;   
    }
    
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $name; ?></title>
    <?php include ("includes/css.php"); ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <?php include ("includes/header.php"); ?>
    <div class="clearfix">
    </div>
    
    <!-- page-container -->
    <div class="page-container">
        <?php include ("includes/menu.php"); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="theme-panel hidden-xs hidden-sm">
                    <div class="toggler-close">
                    </div>
                </div>
                <!-- END STYLE CUSTOMIZER -->
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            Cotización
                        </h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-list-alt"></i>
                                <a href="listado-cotizaciones.php">
                                    Cotizaciones
                                </a>
                                <i class="fa fa-angle-right"></i>
                            </li>

                            <li>
                                <a href="#">
                                    Alta de cotización
                                </a>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <form id="formPrincipal" name="formPrincipal" class="form-horizontal">
                    <input type="hidden" id="id_cotizacion" name="id_cotizacion" value="<?php echo $cotizacion['id_cotizacion']; ?>">
                    <input type="hidden" id="id_cliente" name="id_cliente" value="<?php echo $cotizacion['id_cliente']; ?>">
                    
                    <div class="portlet box blue">
                        <!-- portlet-title -->
                        <div class="portlet-title">
                            <div class="row" style="margin-bottom: 5px;">
                                <div class="col-md-12">
                                    <a href="cotizacion-pdf.php?id=<?php echo $cotizacion['id_cotizacion']; ?>" target="_blank" class="btn btn-success pull-right"><i class="fa fa-download"></i> Descargar PDF</a>
                                </div>
                            </div>
                        </div>
                        <!-- portlet-title -->

                        <!-- portlet-body -->
                        <div class="portlet-body" style="padding: 25px;">
                            <div class="">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <div style="margin-bottom: 10px;">
                                                <strong>Clientes</strong>
                                                <p><?php echo $cotizacion['nombre_cliente']; ?></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <div style="margin-bottom: 10px;">
                                                <strong>Divisa</strong>
                                                <p><?php echo $cotizacion['nombre_divisa']; ?></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <div style="margin-bottom: 10px;">
                                                <strong>Impuesto</strong>
                                                <p><?php echo $cotizacion['nombre_impuesto']; ?></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <div style="margin-bottom: 10px;">
                                                <strong>Despacho</strong>
                                                <?php if ($cotizacion['tipo_despacho'] == 1): ?>
                                                    <p>Envío a cliente</p>
                                                <?php endif; ?>
                                                <?php if ($cotizacion['tipo_despacho'] == 2): ?>
                                                    <p>Recolección en oficina</p>
                                                <?php endif; ?>
                                                <?php if ($cotizacion['tipo_despacho'] == 3): ?>
                                                    <p>Instalación HS</p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div style="margin-bottom: 10px;">
                                                <strong>Descripción de la cotización</strong>
                                                <p><?php echo $cotizacion['descripcion']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="direccion-cliente">
                                                <?php
                                                    $sql = 'SELECT * FROM clientes, domicilios WHERE clientes.id_cliente =' . $cotizacion['id_cliente'] . ' AND clientes.id_domicilio = domicilios.id_domicilio';
                                                    $edit = mysql_fetch_array(consulta($sql));
                                                    $pais = datoRapido("SELECT nombre AS dato FROM pais WHERE id_pais= ".$edit['id_pais']);
                                                ?>
                                                <div class="col-md-12" style="background-color:#eee;">
        <div class="col-md-4 "> 
            <h4>Datos Cliente</h4>                                                                              
            <p>
                <i class="fa fa-user"></i> &nbsp; Contacto: <?php echo $edit['nombre_contacto']; ?><br/>
                <i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit['telefono_contacto']; ?> <?php if($edit['ext_contacto']!=''){ ?> ext <?php echo $edit['ext_contacto']; } ?> <br/>   
                <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit['email_contacto']; ?>
            </p>
            <p><i class="fa fa-users"></i> &nbsp; Tipo: <?php echo ($edit['tipo']==1)?'Prospecto':'Cliente'; ?></p> 
            <?php if($edit['nombre_autorizado'] || $edit['telefono_autorizado'] || $edit['ext_autorizado'] || $edit['email_autorizado']){ ?>
            </div>
            <div class="col-md-4 ">  
            <h4>Personal autorizado para hacer pedidos</h4>                                    
            <p>
                <i class="fa fa-user"></i> &nbsp; Contacto: <?php echo $edit['nombre_autorizado']; ?><br/>
                <i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit['telefono_autorizado']; ?> <?php if($edit['ext_autorizado']!=''){ ?> ext <?php echo $edit['ext_autorizado']; } ?> <br/>   
                <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit['email_autorizado']; ?>
            </p>
            <?php } ?>
            </div>
            <div class="col-md-4 ">  
            <h4><i class="fa fa-map-pin"></i> &nbsp;<?php if($edit['calle'] || $edit['no_exterior'] || $edit['no_interior']) { echo $edit['calle']." ".$edit['no_exterior']." ".$edit['no_interior']; } ?></h4> 
            <?php if($edit['colonia']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['colonia']; ?></p>  <?php } ?>  
            <?php if($edit['referencia']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['referencia']; ?></p><?php } ?>  
            <?php if($edit['localidad'] || $edit['municipio'] || $edit['estado']){ ?><p> &nbsp; &nbsp; &nbsp; <?php echo $edit['localidad']." ".$edit['municipio']." ".$edit['estado']; ?></p><?php } ?>  
            <p> &nbsp; &nbsp; &nbsp; <?php echo $pais; ?> <?php if($edit['codigo_postal']){ echo "CP ".$edit['codigo_postal']; } ?></p>
        </div>
    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="well">
                                            <h4>Elementos cotizados</h4>
                                            <?php if (count($conceptos)): ?>
                                                <div class="table-responsive">
                                                <table id="table-concepts" class='table table-striped table-bordered'>
                                                    <tr>
                                                        <th style="width: 10%;">Nombre</th>
                                                        <th style="width: 10%;">Cantidad</th>
                                                        <th style="width: 60%;">Descripción</th>
                                                        <th style="width: 10%;">Precio unitario</th>
                                                        <th style="width: 10%;">Importe</th>
                                                    </tr>
                                                    <?php while ($row1 = mysql_fetch_array($conceptos)) { ?>
                                                        <tr>
                                                            <?php 
                                                                if (isset($row1['id_puerta'])) {
                                                                    $sql2 = "SELECT * FROM puerta_tipo WHERE activo = 1 AND id_ptipo = ". $row1['id_ptipo'];
                                                                    $ptipo = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM puerta WHERE activo = 1 AND id_puerta = ". $row1['id_puerta'];
                                                                    $puerta = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM puerta_modelo WHERE activo = 1 AND id_pmodelo = ". $row1['id_pmodelo'];
                                                                    $pmodelo = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM puerta_color WHERE activo = 1 AND id_pcolor = ". $row1['id_pcolor'];
                                                                    $pcolor = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM puerta_movimiento WHERE activo = 1 AND id_pmovimiento = ". $row1['id_pmovimiento'];
                                                                    $pmovimiento = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM puerta_textura WHERE activo = 1 AND id_ptextura = ". $row1['id_ptextura'];
                                                                    $ptextura = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM puerta_sello WHERE activo = 1 AND id_psello = ". $row1['id_psello'];
                                                                    $psello = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM puerta_motor WHERE activo = 1 AND id_pmotor = ". $row1['id_pmotor'];
                                                                    $pmotor = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM ventana WHERE id_ventana = {$row1['id_ventana']} AND activo = 1";
                                                                    $pventana = mysql_fetch_array(consulta($sql2));
                                                                    $sql2 = "SELECT * FROM ventana_color WHERE id_vcolor = {$row1['id_vcolor']} AND activo = 1";
                                                                    $pvcolor = mysql_fetch_array(consulta($sql2));
                                                                    $nventanas = $row1['nventanas'];
                                                            ?>
                                                            <td>Puerta</td>
                                                            <td><?php echo $row1['cantidad']; ?></td>
                                                            <td>
                                                                <strong>Tipo:</strong> <?php echo $ptipo['nombre_tipo']; ?> |
                                                                <strong>Medidas:</strong>

                                                                <?php if ($row1['m_ancho'] == 0 && $row1['m_alto'] == 0 && $row1['f_ancho'] == 0 && $row1['in_ancho'] == 0 && $row1['f_alto'] == 0 && $row1['in_alto'] == 0) { ?>
                                                                    <?php echo $puerta['m_ancho'] . 'm' . 'x' . $puerta['m_alto'] . 'm'; ?>
                                                                <?php } ?>

                                                                <?php if ($row1['m_ancho'] != 0 || $row1['m_alto'] != 0) { ?>
                                                                    <?php echo $row1['m_ancho'] . 'm' . 'x' . $row1['m_alto'] . 'm'; ?>
                                                                <?php } ?>

                                                                <?php if ($row1['f_ancho'] != 0 || $row1['in_ancho'] != 0 || $row1['f_alto'] != 0 || $row1['in_alto'] != 0) { ?>
                                                                    <?php echo $row1['f_ancho'] . '\'' . $row1['in_ancho'] . '"' . 'x' . $row1['f_alto'] . '\'' . $row1['in_alto'] . "\""; ?>
                                                                <?php } ?>

                                                                |

                                                                <strong>Modelo:</strong> <?php echo $pmodelo['nombre_modelo']; ?> |
                                                                <strong>Color:</strong> <?php echo $pcolor['nombre_color']; ?> |
                                                                <strong>Movimiento:</strong> <?php echo $pmovimiento['nombre_movimiento']; ?> |
                                                                <strong>Textura:</strong> <?php echo $ptextura['nombre_textura']; ?> |
                                                                <strong>Sello:</strong> <?php echo $psello['nombre_sello']; ?> |
                                                                <strong>Motor:</strong> <?php echo $pmotor['nombre_motor']; ?> |
                                                                <strong>Ventana:</strong> <?php echo $pventana['nombre_ventana'] ? $pventana['nombre_ventana'] : 'Sin Ventana'; ?> |
                                                                <strong>Color ventana:</strong> <?php echo $pvcolor['nombre_color'] ? $pvcolor['nombre_color'] : 'Sin Color'; ?> |

                                                                <?php if ($nventanas > 0): ?>
                                                                  <strong># ventanas:</strong> <?php echo $nventanas ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo number_format($row1['precio_unitario'], 2, '.', ','); ?>
                                                            </td>
                                                            <td>
                                                                <?php echo number_format($row1['importe'], 2, '.', ','); ?>
                                                            </td>
                                                            <?php
                                                            } elseif (isset($row1['id_componente'])) { # Componente
                                                              $sql2 = "SELECT * FROM componentes_grupos WHERE activo=1 AND id_grupo_componente = ". $row1['id_grupo_componente'];
                                                              $gcomp = mysql_fetch_array(consulta($sql2));
                                                              $sql2 = "SELECT * FROM componentes WHERE activo=1 AND id_componente = ". $row1['id_componente'];
                                                              $ccomp = mysql_fetch_array(consulta($sql2));
                                                            ?>
                                                            <td>Componente</td>
                                                            <td><?php echo $row1['cantidad']; ?></td>
                                                            <td>
                                                                <strong>Grupo:</strong> <?php echo $gcomp['grupo_componente']; ?> |
                                                                <strong>Componente:</strong> <?php echo htmlentities($ccomp['nombre_componente']); ?>
                                                            </td>
                                                            <td>
                                                                <?php echo number_format($row1['precio_unitario'], 2, '.', ','); ?>
                                                            </td>
                                                            <td>
                                                                <?php echo number_format($row1['importe'], 2, '.', ','); ?>
                                                            </td>
                                                            <?php
                                                            } elseif (isset($row1['id_kit'])) { # Kit
                                                              $sql2 = "SELECT * FROM kit WHERE activo=1 AND id_kit = ". $row1['id_kit'];
                                                              $kkit = mysql_fetch_array(consulta($sql2));
                                                            ?>
                                                            <td>Kit</td>
                                                            <td><?php echo $row1['cantidad']; ?></td>
                                                            <td>
                                                                <strong>Kit:</strong> <?php echo $kkit['nombre_kit']; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo number_format($row1['precio_unitario'], 2, '.', ','); ?>
                                                            </td>
                                                            <td>
                                                                <?php echo number_format($row1['importe'], 2, '.', ','); ?>
                                                            </td>
                                                            <?php
                                                            } elseif (isset($row1['id_ventana'])) { # Ventana
                                                              $sql2 = "SELECT * FROM ventana WHERE activo=1 AND id_ventana = ". $row1['id_ventana'];
                                                              $vventana = mysql_fetch_array(consulta($sql2));
                                                              $sql2 = "SELECT * FROM ventana_color WHERE activo=1 AND id_vcolor = ". $row1['id_vcolor'];
                                                              $vcolor = mysql_fetch_array(consulta($sql2));
                                                            ?>
                                                              <td>Ventana</td>
                                                              <td></td>
                                                              <td>
                                                                <strong>Modelo:</strong> <?php echo $vventana['nombre_ventana']; ?> |
                                                                <strong>Color:</strong> <?php echo $vcolor['nombre_color']; ?>
                                                              </td>
                                                              <td>
                                                                <?php echo $row1['precio_unitario']; ?>
                                                              </td>
                                                              <td>
                                                                <?php echo $row1['importe']; ?>
                                                              </td>
                                                            <?php
                                                            }
                                                            ?>
                                                </tr>
                                              <?php
                                              } // end while
                                              ?>
                                              </table>
                                                </div>
                                            <?php else: ?>
                                                <div id="concepts-section">
                                                    <p>No se han agregado conceptos a esta cotización</p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-12 col-md-8">
                                        <h4> Términos de venta.</h4>
                                        <p style="width: 500px;"><?php echo nl2br($cotizacion['termino_venta']); ?></p>
                                        <!--
                                        <ul>
                                            <li>Todos los modelos con textura de madera en BN.</li>
                                            <li>Para ver las especificaciones técnicas de cada puerta, consultar <a href="http://hspuertasdegaraje.com" title="HS Puertas de Garaje" target="_blank">http://hspuertasdegaraje.com</a></li>
                                            <li>No incluye envío.</li>
                                            <li>Puertas completas, paneles, herrajes calibre 14 y resorte acorde al tamaño de la puerta.</li>
                                            <li>Modelo con cuadros, con cuadro de ajuste.</li>
                                            <li>Puertas fabricadas a la medida con tiempo de envió a 10 días hábiles.</li>
                                            <li>Precios en MXN</li>
                                            <li>IVA incluido.</li>
                                            <li>Elegantti y exquisite comienzan en precio VE.</li>
                                            <li>Pago de contado.</li>
                                            <li>Validez de 15 días hábiles.</li>
                                            <li>Una vez realizado el pedido, no hay cambios o devoluciones.</li>
                                        </ul>
                                        -->
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="row" style="margin-bottom: 4px;">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input type="hidden" id="subtotaluno">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 4px;">
                                            <div class="col-sm-4">
                                                <label class="control-label">Subtotal</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <label class="form-control"><?php echo number_format($cotizacion['subtotal'], 2, '.', ','); ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 4px;">
                                            <div class="col-sm-4">
                                                <label class="control-label">Impuestos</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <label class="form-control"><?php echo number_format($cotizacion['impuestos'], 2, '.', ','); ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="control-label">Total</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <label class="form-control"><?php echo number_format($cotizacion['total'], 2, '.', ','); ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="listado-cotizaciones.php" class="btn btn-default"><i class="fa fa-arrow-left"></i> Regresar</a>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- portlet-body -->

                    </div>
                </form>
            
            </div>
        </div>
    </div>
    <div class='notifications bottom-right'></div>

    <?php include ("modal/imagen.php"); ?>
    <?php include ("includes/footer.php"); ?>
    <?php include ("includes/js.php"); ?>

    <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
    <script src="scripts/custom/table-advanced.js"></script>
    <script src="plugins/select2/select2.min.js" type="text/javascript"></script>
    <script src="plugins/numeral.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            App.init();
            TableAdvanced.init();
        });
    
    </script>

</body>
</html>
