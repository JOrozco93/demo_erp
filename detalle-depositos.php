<?php
if (!isset($_SESSION)) {
    session_start();
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM depositos WHERE id_deposito =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));

        $sql_usu2 = 'SELECT * FROM depositos WHERE id_cliente =' . $id;
        $edit_usu2 = mysql_fetch_array(consulta($sql_usu2));

        $sql_usu1 = 'SELECT * FROM clientes WHERE id_cliente =' . $id;
        $edit_usu1 = mysql_fetch_array(consulta($sql_usu1));
    }
}

$sql = "SELECT * FROM depositos WHERE activo=1 AND id_cliente = $id ORDER BY id_deposito";
$query = consulta($sql);
$num = mysql_num_rows($query);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Deposito
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-usd"></i>
                                    <a href="listado-depositos.php">
                                        Listado de Depositos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>                                
                                <li>

                                    Datos del deposito

                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->                   
                    <div class="portlet box blue">
                        <div class="portlet-title"> 
                            <div class="btn-group col-lg-12">
                                <div class="input-group-btn">
                                    <div class="col-lg-11"></div>
                                    <div class="col-lg-1">
                                        <a href="alta-depositos.php?id=<?php echo $id; ?>" title="Crear Nuevo Deposito" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                            <i class="fa fa-plus"></i>
                                        </a>   
                                    </div>
                                </div>
                            </div>                           
                        </div>
                        <div class="portlet-body form">
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">  
                                        <h2>Cliente: <?php echo $edit_usu1['nombre_cliente']; ?></h2>  
                                    </div>
                                </div>
                            </div>                            
                            <div class="portlet-body" style="padding: 10px;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th style="width: 80%;">Monto depositado</th>
                                            <th style="width: 20%;">Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($num > 0) {
                                            while ($row = mysql_fetch_array($query)) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row['monto_deposito']; ?></td>
                                                    <td><?php echo $row['fecha']; ?></td>                                                    
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Monto depositado</th>
                                            <th>Fecha</th>
                                        </tr>
                                    </tfoot>
                                </table>                                      
                            </div>
                            <br/>            
                            <div class="form-actions fluid">
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <a class="btn btn-warning" href="listado-depositos.php"><i class="fa fa-arrow-circle-left"></i> Regresar a la Lista</a>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
            <script>
                jQuery(document).ready(function () {
                    App.init(); // initlayout and core plugins
                    TableAdvanced.init();
                });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>