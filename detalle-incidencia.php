<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la incidencia con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE incidencias SET activo = 0 WHERE id_incidencia =" . $id_eliminado;
    consulta($update);
}

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM incidencias WHERE id_incidencia =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));

        $sql_usu1 = 'SELECT * FROM cotizaciones WHERE id_cotizacion =' . $edit_usu['id_cotizacion'];
        $edit_usu1 = mysql_fetch_array(consulta($sql_usu1));

        $sql_usu12 = 'SELECT id_cotizacion FROM cotizaciones WHERE id_cotizacion =' . $id;
        $edit_usu2 = mysql_fetch_array(consulta($sql_usu12));
    }
}

$sql = "SELECT * FROM incidencias WHERE activo = 1 AND id_incidencia  = $id  ORDER BY id_incidencia";
$query = consulta($sql);
$num = mysql_num_rows($query);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->

                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Incidencia
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="inicio.php">
                                        Lista de Incidencias
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a>
                                        Datos de la Incidencia
                                    </a>                                  
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->                   
                    <div class="portlet box blue">
                        <div class="portlet-title">                            
                        </div>
                        <div class="portlet-body form">
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">  
                                        <h2>Numero de Cotizacion: <?php echo $edit_usu1['numero_cotizacion']; ?></h2>  
                                    </div>
                                </div>
                            </div>                            
                            <div class="portlet-body" style="padding: 10px;">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th style="width: 90%">Descripción</th>
                                            <th style="width: 10%">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($num > 0) { ?>
                                            <?php while ($row = mysql_fetch_array($query)) { ?>
                                                <tr>
                                                    <td><?php echo $row['descripcion']; ?></td>
                                                    <td>
                                                        <div class="btn-group">                                                            
                                                            <div class="btn-group">                                                            
                                                                <a href="alta-incidencia.php?id=<?php echo $row['id_incidencia']; ?>" title="Mostrar Incidencia" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Descripción</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </tfoot>
                                </table>                                      
                            </div>
                            <br/>            
                            <div class="form-actions fluid">
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <a class="btn btn-warning" href="listado-incidencias.php"><i class="fa fa-arrow-circle-left"></i> Regresar a la Lista</a>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("modal/status-incidencia.php"); ?>
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>
        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
            jQuery(document).ready(function () {
                App.init(); // initlayout and core plugins
                TableAdvanced.init();
            });

            function estatus(event, id_incidencia, tipo) {
                event.preventDefault();
                
                $("#id").val(id_incidencia);
                $("#tipo").val(tipo);

                $("#modal_status_incidencia").modal('show');
            }

            function  deshabilita(event, id, descripcion) {
                event.preventDefault();
                var respuesta = confirm('\u00BFDesea eliminar la "' + descripcion + '"?');
                if (respuesta) {
                    location.href = 'listado-incidencias.php?borrar=' + id;
                }
            }

            function validar_vacios() {
                document.forms[0].btn.value = "guardar";
                document.forms[0].submit();
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>