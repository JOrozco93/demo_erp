<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql = 'SELECT * FROM proveedores AS p, domicilios AS d WHERE p.id_proveedor ='.$id.' AND p.id_domicilio = d.id_domicilio';
        $edit = mysql_fetch_array(consulta($sql));
    }     
     $pais = datoRapido("SELECT nombre AS dato FROM pais WHERE id_pais= ".$edit['id_pais']);                                                 
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Proveedor
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-proveedores.php">
                                        Proveedores
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del Proveedor
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->                   
                    <div class="portlet box blue col-sm-11">
                        <div class="portlet-title">                            
                        </div>
                        <div class="portlet-body form">
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-11 col-sm-offset-1">  
                                        <h2><?php echo $edit['nombre'];  ?>  </h2>  
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4 col-sm-offset-1 well">                                                                              
                                        <p><i class="fa fa-user"></i> &nbsp; Contacto: <?php echo $edit['nombre_contacto']; ?><br/><i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit['telefono_contacto']; ?> <?php if($edit['ext_contacto']!=''){ ?> ext <?php echo $edit['ext_contacto']; } ?> <br/>   <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit['email_contacto']; ?></p>
                                        <p><i class="fa fa-globe"></i> &nbsp; Tipo: <?php echo $edit['tipo']; ?></p> 
                                    </div><!-- /.col-->     
                                     <div class="col-sm-5 col-sm-offset-1    well">
                                        <h4><i class="fa fa-map-pin"></i> &nbsp;<?php echo $edit['calle']." ".$edit['no_exterior']." ".$edit['no_interior']; ?></h4> 
                                        <p> &nbsp; &nbsp; &nbsp; <?php echo $edit['colonia']; ?></p>    
                                        <p> &nbsp; &nbsp; &nbsp; <?php echo $edit['referencia']; ?></p>
                                        <p> &nbsp; &nbsp; &nbsp; <?php echo $edit['localidad']." ".$edit['municipio']." ".$edit['estado']; ?></p>
                                        <p> &nbsp; &nbsp; &nbsp; <?php echo $pais." &nbsp; &nbsp; CP ".$edit['codigo_postal']; ?></p>                                  
                                    </div><!-- /.col-->                                  
                                </div>
                            </div>
                            <br/>            
                                <div class="form-actions fluid">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <a class="btn btn-warning" href="catalogo-proveedores.php"><i class="fa fa-arrow-circle-left"></i> Regresar a la Lista</a>
                                        </div>
                                        <div class="col-lg-6">                                        
                                            <a href="alta-proveedores.php?id=<?php echo $edit['id_proveedor']; ?>" class="btn btn-info pull-right"><i class="fa fa-pencil" ></i> Modificar</a>                                         </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script>
                                            jQuery(document).ready(function () {
                                                App.init(); // initlayout and core plugins
                                                TableAdvanced.init();
                                            });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>