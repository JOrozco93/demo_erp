<?php
    
    if (!isset($_SESSION)) {
        session_start();
    }

    /*
    if (($_SESSION['id_usuario'] == NULL)) {
        header("Location: logout.php");
    }
    */

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    $id_usuario = $_SESSION['id_usuario'];

    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        if ($id != '') {
            $sql_usu = 'SELECT * FROM puerta WHERE id_puerta =' . $id;
            $edit_usu = mysql_fetch_array(consulta($sql_usu));
        }
    }

    $componentsForDoors = getComponentsForDoors($id);
    $componentsByDoor = getComponentsByDoor($id);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
    
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>
        
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close"></div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Puerta
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-puertas.php">
                                        Puertas
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos de la puerta
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <div class="row">

                        <div class="col-xs-12 col-sm-4">
                            <div class="well">
                                <div class="row" style="margin-bottom: 25px;">
                                    <div class="col-xs-6">  
                                        <label>Ancho (m)</label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <strong><?php echo ($id != '') ? $edit_usu['m_ancho'] : ''; ?></strong>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">  
                                        <label>Alto (m)</label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <strong><?php echo ($id != '') ? $edit_usu['m_alto'] : ''; ?></strong>
                                        </div>
                                    </div>   
                                </div>

                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-xs-6">  
                                        <label>Ancho (f)</label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <strong><?php echo ($id != '') ? $edit_usu['f_ancho'] : ''; ?></strong>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">  
                                        <label>Ancho (p)</label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <strong><?php echo ($id != '') ? $edit_usu['in_ancho'] : ''; ?></strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 25px;">
                                    <div class="col-xs-6">  
                                        <label>Alto (f)</label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <strong><?php echo ($id != '') ? $edit_usu['f_alto'] : ''; ?></strong>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">  
                                        <label>Alto (p)</label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <strong><?php echo ($id != '') ? $edit_usu['in_alto'] : ''; ?></strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-xs-12">  
                                        <label><strong>Tipo</strong></label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <label>
                                                <?php
                                                    $sql1 = "SELECT * FROM puerta_tipo WHERE activo = 1";
                                                    $query1 = consulta($sql1);
                                                    $num1 = mysql_num_rows($query1);
                                                    if ($num1 > 0) {
                                                        while ($row1 = mysql_fetch_array($query1)) {
                                                            if ($id != '') {
                                                                if ($row1['id_ptipo'] == $edit_usu['id_ptipo']) {
                                                                    echo $row1['nombre_tipo'];
                                                                }
                                                            }
                                                        }
                                                    }
                                                ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 25px;">
                                    <div class="col-xs-12">  
                                        <label><strong>Precio</strong></label>
                                        <div style="border-bottom: 1px solid #e2e2e2;">
                                            <label><?php echo ($id != '') ? $edit_usu['precio'] : ''; ?></label>
                                        </div>
                                    </div> 
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <a href="catalogo-puertas.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <a href="alta-puerta.php?id=<?php echo ($id != '') ? $id : 0; ?>" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i> Editar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-8">
                            <div class="page-header" style="margin: 0 0 10px 0;">
                                <h1 style="margin: 0 0 0 0;">Componentes</h1>
                            </div>

                            <div class="row" style="margin-bottom: 5px;">
                                <div class="col-sm-12">
                                    <button type="button" id="addComponenteModal-btn" data-id-puerta="<?php echo ($id != '') ? $id : 0; ?>" class="btn btn-primary pull-right">
                                        <i class="fa fa-plus"></i> Componete
                                    </button>
                                </div>
                            </div>

                            <?php if (count($componentsByDoor)): ?>
                                <div class=".table-responsive">
                                    <table id="table-componentes" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="width: 60%;">Componente</th>
                                                <th style="width: 20%;">Cantidad</th>
                                                <th style="width: 20%;">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($componentsByDoor as $component): ?>
                                                <tr>
                                                    <td><?php echo $component['componente']; ?></td>
                                                    <td><?php echo $component['cantidad']; ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" title="Editar Componente" data-id="<?php echo $component['id']; ?>" class="btn btn-warning btn-edit-componente"><i class="fa fa-pencil-square-o"></i></button>
                                                            <button type="button" title="Eliminar Componente" data-id="<?php echo $component['id']; ?>" class="btn btn-danger btn-delete-componente"><i class="fa fa-trash-o"></i></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Componente</th>
                                                <th>Cantidad</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <?php else: ?>
                                <div class="alert alert-warning" role="alert">No se han agregado componetes</div>
                            <?php endif; ?>
                        </div>
                    
                    </div>

                    <!-- addComponenteModal -->
                    <div id="addComponenteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 id="myModalLabel" class="modal-title">Agregar componete</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="addComponenteForm" method="post" action="alta/puerta-componente.php" class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <input type="hidden" id="id" name="id" value="0" class="form-control">
                                                <input type="hidden" id="id_puerta" name="id_puerta" value="0" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_componente" class="col-sm-2 control-label">Componente</label>
                                            <div class="col-sm-10">
                                                <select id="id_componente" name="id_componente" class="form-control">
                                                    <option value="">Seleccione un componente ...</div>
                                                    <?php foreach ($componentsForDoors as $component): ?>
                                                        <option value="<?php echo $component['id_componente']; ?>"><?php echo $component['nombre_componente']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Cantidad</label>
                                            <div class="col-sm-10">
                                                <input type="number" id="cantidad" name="cantidad" class="form-control">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="btn-cerrar" class="btn btn-default">Cerrar</button>
                                    <button type="button" id="btn-guardar" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- addComponenteModal -->

                    <!-- updateComponenteModal -->
                    <div id="updateComponenteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 id="myModalLabel" class="modal-title">Editar componete</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="updateComponenteForm" method="post" action="alta/update-puerta-componente.php" class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <input type="hidden" id="id" name="id" value="0" class="form-control">
                                                <input type="hidden" id="id_puerta" name="id_puerta" value="0" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nombre_componente" class="col-sm-2 control-label">Componente</label>
                                            <div class="col-sm-10">
                                                <label id="nombre_componente" class="control-label" style="font-weight: bold;"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Cantidad</label>
                                            <div class="col-sm-10">
                                                <input type="number" id="cantidad" name="cantidad" class="form-control">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="btn-cerrar" class="btn btn-default">Cerrar</button>
                                    <button type="button" id="btn-actualizar" class="btn btn-primary">Actualizar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- updateComponenteModal -->

                    <!-- deleteComponenteModal -->
                    <div id="deleteComponenteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 id="myModalLabel" class="modal-title">Eliminar componete</h4>
                                </div>
                                <div class="modal-body">
                                    <p>¿ Esta seguro que desea eliminar el componente ?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="btn-cerrar" class="btn btn-default">Cerrar</button>
                                    <button type="button" id="btn-aceptar" class="btn btn-primary">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- deleteComponenteModal -->

                </div>
                <!-- page-content -->

            </div>
            <!-- page-content-wrapper -->

        </div>
        <!-- END CONTAINER -->

        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>
        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        <script src="scripts/custom/form-samples.js"></script>
        
        <script type="text/javascript">
            
            jQuery(document).ready(function () {
                
                App.init();
                TableAdvanced.init();

                $("[data-toggle='tooltip']").tooltip();

                // addComponenteModal
                $('#addComponenteModal-btn').click(function() {
                    var id = 0;
                    var id_puerta = $(this).data('id-puerta');

                    $('#addComponenteForm input[id=id]').val(id);
                    $('#addComponenteForm input[id=id_puerta]').val(id_puerta);
                    $('#addComponenteForm select[id=id_componente] option').eq(0).prop('selected', true);
                    $('#addComponenteForm input[id=cantidad]').val(0);

                    $('#addComponenteModal').modal('show');
                });

                $('#addComponenteModal button[id=btn-cerrar]').click(function() {
                    $('#addComponenteModal').modal('hide'); 
                });

                $('#addComponenteModal button[id=btn-guardar]').click(function() {
                    var componente = $('#addComponenteForm select[id=id_componente]').val();
                    var cantidad = $('#addComponenteForm input[id=cantidad]').val();

                    if (componente == '') {
                        return false;
                    }

                    if (cantidad == '' || cantidad <= 0) {
                        return false;
                    }

                    $('#addComponenteForm').submit();
                });

                // editComponenteModal
                $('.btn-edit-componente').click(function() {
                    var id = $(this).data('id');

                    $.ajax({
                        url: 'ajax/getDoorComponentById.php?id=' + id,
                        type: 'get',
                        dataType: 'json',
                        success: function (response) {
                            var id = typeof(response.id) !== 'undefined' ? response.id : 0;
                            var id_puerta = typeof(response.id_puerta) !== 'undefined' ? response.id_puerta : 0;
                            var nombre_componente = typeof(response.nombre_componente) !== 'undefined' ? response.nombre_componente : 0;
                            var cantidad = typeof(response.cantidad) !== 'undefined' ? response.cantidad : 0;

                            $('#updateComponenteForm input[id=id]').val(id);
                            $('#updateComponenteForm input[id=id_puerta]').val(id_puerta);
                            $('#updateComponenteForm label[id=nombre_componente]').text(nombre_componente);
                            $('#updateComponenteForm input[id=cantidad]').val(cantidad);

                            $('#updateComponenteModal').modal('show');
                        },
                        error: function (xhr, status) {
                            console.log('Hubo un error durante la petición');
                        },
                        async: false
                    });
                });

                $('#updateComponenteModal button[id=btn-cerrar]').click(function() {
                    $('#updateComponenteModal').modal('hide'); 
                });

                $('#updateComponenteModal button[id=btn-actualizar]').click(function() {
                    var cantidad = $('#updateComponenteForm input[id=cantidad]').val();

                    if (cantidad == '' || cantidad <= 0) {
                        return false;
                    }

                    $('#updateComponenteForm').submit();
                });

                // deleteComponenteModal
                $('.btn-delete-componente').click(function() {
                    var id = $(this).data('id');

                    $('#deleteComponenteModal').data('id', id).modal('show');
                });

                $('#deleteComponenteModal button[id=btn-cerrar]').click(function() {
                    $('#deleteComponenteModal').modal('hide'); 
                });

                $('#deleteComponenteModal button[id=btn-aceptar]').click(function(e) {
                    var id = $('#deleteComponenteModal').data('id');

                    $.ajax({
                        url: 'ajax/deleteDoorComponentById.php?id=' + id,
                        type: 'get',
                        dataType: 'json',
                        success: function (response) {
                            var result = typeof(response.result) !== 'undefined' ? response.result : 0;

                            if (result) {
                                location.reload(true);
                            }
                        },
                        error: function (xhr, status) {
                            console.log('Hubo un error durante la petición');
                        },
                        async: false
                    });                    
                });
            });
        </script>

    </body>
    <!-- END BODY -->

</html>