<?php
if (!isset($_SESSION)) {
    session_start();
}

//if (($_SESSION['id_usuario'] == NULL)) {
//    header("Location: logout.php");
//}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    if ($id != '') {
        $sql_usu = 'SELECT * FROM usuarios WHERE id_usuario =' . $id;
        $edit_usu = mysql_fetch_array(consulta($sql_usu));
    }

    if($edit_usu['permiso_usuario']==5 && $edit_usu['id_cliente'] != NULL){
        $sql_usu = 'SELECT nombre_cliente AS nombre FROM clientes WHERE id_cliente =' . $edit_usu['id_cliente'];
        $cliente = mysql_fetch_array(consulta($sql_usu));
        $edit_usu['nombre_usuario'] = $cliente['nombre'];
    }

     
     $permiso = datoRapido("SELECT descripcion AS dato FROM permisos WHERE activo=1 AND id_permiso = ".$edit_usu['permiso_usuario']." ORDER BY id_permiso");

     if($edit_usu['id_divisa'] != NULL){
        $qdivisa = "SELECT * FROM divisas WHERE activo=1 AND id_divisa = ".$edit_usu['id_divisa']." ORDER BY id_divisa";
        $adivisa = consulta($qdivisa);
        $rdivisa = mysql_fetch_array($adivisa);
        $divisa = $rdivisa['abbrev']." - ".$rdivisa['nombre'];
     }                                               
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler-close">
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Usuario
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-cogs"></i>
                                    <a href="inicio.php">
                                        Catalogos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="catalogo-usuarios.php">
                                        Usuarios
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Datos del Usuario
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->                   
                    <div class="portlet box blue col-sm-8">
                        <div class="portlet-title">                            
                        </div>
                        <div class="portlet-body form">
                            <br/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-11 col-sm-offset-1">  
                                        <h2><?php echo $edit_usu['nombre_usuario'];  ?>  </h2>  
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-5 col-sm-offset-1 well">
                                        <h4><?php echo $permiso; ?></h4> 
                                        <p><i class="fa fa-users"></i> &nbsp; # Empleado: <?php echo $edit_usu['num_empleado']; ?><br/>    <i class="fa fa-dollar"></i> &nbsp; Divisa: <?php echo $divisa; ?></p>
                                        <p><i class="fa fa-phone"></i> &nbsp; Telefono: <?php echo $edit_usu['telefono_usuario']; ?> <?php if($edit_usu['ext_usuario']!=''){ ?> ext <?php echo $edit_usu['ext_usuario']; } ?> <br/>   <i class="fa fa-envelope"></i> &nbsp; Email: <?php echo $edit_usu['email_usuario']; ?></p>
                                    </div><!-- /.col-->     
                                     <div class="col-sm-4 col-sm-offset-1    well">
                                        <h4><i class="fa fa-user"></i> &nbsp; <?php echo $edit_usu['username']; ?></h4> 
                                        <p><i class="fa fa-key"></i> &nbsp; <?php echo $edit_usu['pwd']; ?></p>                                        
                                    </div><!-- /.col-->                                  
                                </div>
                            </div>
                            <br/>            
                                <div class="form-actions fluid">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <a class="btn btn-warning" href="catalogo-usuarios.php"><i class="fa fa-arrow-circle-left"></i> Regresar a la Lista</a>
                                        </div>
                                        <div class="col-lg-6">                                        
                                            <a href="alta-usuarios.php?id=<?php echo $edit_usu['id_usuario']; ?>" class="btn btn-info pull-right"><i class="fa fa-pencil" ></i> Modificar</a>                                         </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include ("includes/footer.php"); ?>
            <?php include ("includes/js.php"); ?>
            <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
            <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
            <script src="scripts/custom/table-advanced.js"></script>
            <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
            <script>
                                            jQuery(document).ready(function () {
                                                App.init(); // initlayout and core plugins
                                                TableAdvanced.init();
                                            });
            </script>
            <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>