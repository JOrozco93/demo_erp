<?php

session_start();

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

$id_usuario = $_SESSION['id_usuario'];

require_once("config.php");

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $name; ?></title>
    <?php include ("includes/css.php"); ?>
    <link href="plugins/data-tables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
    <style>
        .error {
            color: #f00;
        }
    </style>
    <!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <?php include ("includes/header.php"); ?>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php include ("includes/menu.php"); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="theme-panel hidden-xs hidden-sm">
                    <div class="toggler-close"></div>
                </div>
                <!-- END STYLE CUSTOMIZER -->
                
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">Configuración</h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-cogs"></i>
                                <a href="inicio.php">
                                    Dashboard
                                </a>
                                <i class="fa fa-angle-right"></i>
                            </li>  
                            <li>
                                <a href="configuracion.php">
                                    Configuración
                                </a>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-offset-3 col-md-6">
                        <!-- portlet box blue -->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <h4>Datos fiscales</h4>
                            </div>
                            <div class="portlet-body">
                                <form id="df-form" method="post" action="domicilio-fiscal-save.php">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">        
                                                <label for="razon_social">Razón Social</label>
                                                <input type="text" id="razon_social" name="razon_social" placeholder="Razon Social" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">        
                                                <label for="rfc">RFC</label>
                                                <input type="text" id="rfc" name="rfc" placeholder="RFC" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="calle">Calle</label>
                                                <input type="text" id="calle" name="calle" placeholder="Calle" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="no_exterior"># Exterior</label>
                                                <input type="text" id="no_exterior" name="no_exterior" placeholder="# Exterior" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="no_interior"># Interior</label>
                                                <input type="text" id="no_interior" name="no_interior" placeholder="# Interior" class="form-control">
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="colonia">Colonia</label>
                                                <input type="text" id="colonia" name="colonia" placeholder="Colinia" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="referencia">Referencia</label>
                                                <input type="text" id="referencia" name="referencia" placeholder="Referencia" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="localidad">Localidad</label>
                                                <input type="text" id="localidad" name="localidad" placeholder="Localidad" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="municipio">Municipio</label>
                                                <input type="text" id="municipio" name="municipio" placeholder="Municipio" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="estado">Estado</label>
                                                <input type="text" id="estado" name="estado" placeholder="Estado" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="codigo_postal">Código Postal</label>
                                                <input type="text" id="codigo_postal" name="codigo_postal" placeholder="Código Postal" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="id_pais">País</label>
                                                <select class="form-control" id="id_pais" name="id_pais">
                                                    <option value="<?php echo $GLOBAL_id_pais; ?>"><?php echo $GLOBAL_pais; ?></option>
                                                    <option value="" disabled="">--------------------</option>
                                                    <?php
                                                        $sql3 = "SELECT * FROM pais ORDER BY nombre ASC";
                                                        $query3 = consulta($sql3);
                                                        $num3 = mysql_num_rows($query3);
                                                        if ($num3 > 0) {
                                                            while ($row3 = mysql_fetch_array($query3)) { ?>
                                                                <option value="<?php echo $row3['id_pais']; ?>"><?php echo $row3['nombre']; ?></option>
                                                            <?php }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>        
                                </form>                              
                            </div>
                            <div class="portlet-body well" style="margin-bottom: 0;">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button type="button" id="df-cancel-button" class="btn btn-danger pull-left"><i class="fa fa-times"></i> Cancelar</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" id="df-save-button" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- portlet box blue -->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
    </div>
    
    <?php include ("includes/footer.php"); ?>
    <?php include ("includes/js.php"); ?>

    <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="scripts/custom/table-advanced.js"></script>
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-validation/dist/jquery.validate.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins

            $("#df-form").validate({ 
                rules: { 
                    razon_social: 'required',
                    rfc: 'required'
                }, 
                messages: { 
                    razon_social: 'Se debe introducir la razón social',
                    rfc: 'Se debe introducir el rfc'
                } 
            });

            $('#df-cancel-button').click(function() {
                location.href = 'configuracion.php';
            });

            $('#df-save-button').click(function() {
                $("#df-form").submit();
            });
        });
    </script>

</body>
<!-- END BODY -->
</html>