<?php

session_start();

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
    exit();
}

$id_usuario = (int) $_SESSION['id_usuario'];

if (isset($_GET['id'])) {
    $id_domicilio_fiscal = (int) $_GET['id'];
} else {
    $id_domicilio_fiscal = 0;
}

if (!$id_domicilio_fiscal) {
    header('location: configuracion.php');
    exit();
}

require_once('config.php');

try {
	$sql = "
		DELETE FROM domicilios_fiscales
		WHERE id_domicilio_fiscal = :id_domicilio_fiscal
	";

	$stmt = $pdo->prepare($sql);

	$stmt->bindParam(':id_domicilio_fiscal', $id_domicilio_fiscal, PDO::PARAM_INT);

	$result = $stmt->execute();

	if ($result) {
		$_SESSION['notification'] = array(
			'status' => 'success',
			'message' => 'El registro ha sido eliminado'
		);
	} else {
		$_SESSION['notification'] = array(
			'status' => 'error',
			'message' => 'No se ha podido eliminar el registro'
		);
	}
} catch (PDOException $e) {
	// throw $e;
}

header('location: configuracion.php');

?>