<?php

session_start();

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

$id_usuario = (int) $_SESSION['id_usuario'];

require_once('config.php');

$razon_social = filter_var($_POST['razon_social'], FILTER_SANITIZE_STRING);
$rfc = filter_var($_POST['rfc'], FILTER_SANITIZE_STRING);
$calle = filter_var($_POST['calle'], FILTER_SANITIZE_STRING);
$no_exterior = filter_var($_POST['no_exterior'], FILTER_SANITIZE_STRING);
$no_interior = filter_var($_POST['no_interior'], FILTER_SANITIZE_STRING);
$colonia = filter_var($_POST['calle'], FILTER_SANITIZE_STRING);
$referencia = filter_var($_POST['referencia'], FILTER_SANITIZE_STRING);
$localidad = filter_var($_POST['localidad'], FILTER_SANITIZE_STRING);
$municipio = filter_var($_POST['municipio'], FILTER_SANITIZE_STRING);
$estado = filter_var($_POST['estado'], FILTER_SANITIZE_STRING);
$codigo_postal = filter_var($_POST['codigo_postal'], FILTER_SANITIZE_STRING);
$id_pais = filter_var($_POST['id_pais'], FILTER_SANITIZE_NUMBER_INT);

try {
	$sql = "INSERT INTO domicilios_fiscales(razon_social, rfc, calle, no_exterior, no_interior, colonia, referencia, localidad, municipio, estado, codigo_postal, id_pais, id_usuario) VALUES (:razon_social, :rfc, :calle, :no_exterior, :no_interior, :colonia, :referencia, :localidad, :municipio, :estado, :codigo_postal, :id_pais, :id_usuario)";

	$stmt = $pdo->prepare($sql);

	$stmt->bindParam(':razon_social', $razon_social, PDO::PARAM_STR);
	$stmt->bindParam(':rfc', $rfc, PDO::PARAM_STR);
	$stmt->bindParam(':calle', $calle, PDO::PARAM_STR);
	$stmt->bindParam(':no_exterior', $no_exterior, PDO::PARAM_STR);
	$stmt->bindParam(':no_interior', $no_interior, PDO::PARAM_STR);
	$stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
	$stmt->bindParam(':referencia', $referencia, PDO::PARAM_STR);
	$stmt->bindParam(':localidad', $localidad, PDO::PARAM_STR);
	$stmt->bindParam(':municipio', $municipio, PDO::PARAM_STR);
	$stmt->bindParam(':estado', $estado, PDO::PARAM_STR);
	$stmt->bindParam(':codigo_postal', $codigo_postal, PDO::PARAM_STR);
	$stmt->bindParam(':id_pais', $id_pais, PDO::PARAM_INT);
	$stmt->bindParam(':id_usuario', $id_usuario, PDO::PARAM_INT);

	$result = $stmt->execute();

	if ($result) {
		$_SESSION['notification'] = array(
			'status' => 'success',
			'message' => 'El registro ha sido almacenado con exito'
		);
	} else {
		$_SESSION['notification'] = array(
			'status' => 'error',
			'message' => 'No se ha podido almacenar el registro'
		);
	}
} catch (PDOException $e) {
	// throw $e;
}

header('location: configuracion.php');

?>