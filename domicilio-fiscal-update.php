<?php

session_start();

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
    exit();
}

$id_usuario = (int) $_SESSION['id_usuario'];

if (!isset($_POST['id_domicilio_fiscal'])) {
	header('location: configuracion.php');
	exit();
}

$id_domicilio_fiscal = (int) $_POST['id_domicilio_fiscal'];

require_once('config.php');

$razon_social = filter_var($_POST['razon_social'], FILTER_SANITIZE_STRING);
$rfc = filter_var($_POST['rfc'], FILTER_SANITIZE_STRING);
$calle = filter_var($_POST['calle'], FILTER_SANITIZE_STRING);
$no_exterior = filter_var($_POST['no_exterior'], FILTER_SANITIZE_STRING);
$no_interior = filter_var($_POST['no_interior'], FILTER_SANITIZE_STRING);
$colonia = filter_var($_POST['calle'], FILTER_SANITIZE_STRING);
$referencia = filter_var($_POST['referencia'], FILTER_SANITIZE_STRING);
$localidad = filter_var($_POST['localidad'], FILTER_SANITIZE_STRING);
$municipio = filter_var($_POST['municipio'], FILTER_SANITIZE_STRING);
$estado = filter_var($_POST['estado'], FILTER_SANITIZE_STRING);
$codigo_postal = filter_var($_POST['codigo_postal'], FILTER_SANITIZE_STRING);
$id_pais = filter_var($_POST['id_pais'], FILTER_SANITIZE_NUMBER_INT);

try {
	$sql = "
		UPDATE
			domicilios_fiscales
		SET
			razon_social = :razon_social,
			rfc = :rfc,
			calle = :calle,
			no_exterior = :no_exterior,
			no_interior = :no_interior,
			colonia = :colonia,
			referencia = :referencia,
			localidad = :localidad,
			municipio = :municipio,
			estado = :estado,
			codigo_postal = :codigo_postal,
			id_pais = :id_pais,
			id_usuario = :id_usuario
		WHERE
			id_domicilio_fiscal = :id_domicilio_fiscal
	";

	$stmt = $pdo->prepare($sql);

	$stmt->bindParam(':razon_social', $razon_social, PDO::PARAM_STR);
	$stmt->bindParam(':rfc', $rfc, PDO::PARAM_STR);
	$stmt->bindParam(':calle', $calle, PDO::PARAM_STR);
	$stmt->bindParam(':no_exterior', $no_exterior, PDO::PARAM_STR);
	$stmt->bindParam(':no_interior', $no_interior, PDO::PARAM_STR);
	$stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
	$stmt->bindParam(':referencia', $referencia, PDO::PARAM_STR);
	$stmt->bindParam(':localidad', $localidad, PDO::PARAM_STR);
	$stmt->bindParam(':municipio', $municipio, PDO::PARAM_STR);
	$stmt->bindParam(':estado', $estado, PDO::PARAM_STR);
	$stmt->bindParam(':codigo_postal', $codigo_postal, PDO::PARAM_STR);
	$stmt->bindParam(':id_pais', $id_pais, PDO::PARAM_INT);
	$stmt->bindParam(':id_usuario', $id_usuario, PDO::PARAM_INT);
	$stmt->bindParam(':id_domicilio_fiscal', $id_domicilio_fiscal, PDO::PARAM_INT);

	$result = $stmt->execute();

	if ($result) {
		$_SESSION['notification'] = array(
			'status' => 'success',
			'message' => 'El registro ha sido actualizado con exito'
		);
	} else {
		$_SESSION['notification'] = array(
			'status' => 'error',
			'message' => 'No se ha podido actualizar el registro'
		);
	}
} catch (PDOException $e) {
	// throw $e;
}

header('location: configuracion.php');

?>