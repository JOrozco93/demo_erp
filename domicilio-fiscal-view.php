<?php

session_start();

if (($_SESSION['id_usuario'] == NULL)) {
    header('location: logout.php');
    exit();
}

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['id'])) {
    $id_domicilio_fiscal = (int) $_GET['id'];
} else {
    $id_domicilio_fiscal = 0;
}

if (!$id_domicilio_fiscal) {
    header('location: configuracion.php');
    exit();
}

require_once("config.php");

$stmt = $pdo->prepare("
    SELECT
        domicilios_fiscales.id_domicilio_fiscal as id_domicilio_fiscal,
        domicilios_fiscales.razon_social as razon_social,
        domicilios_fiscales.rfc as rfc,
        domicilios_fiscales.calle as calle,
        domicilios_fiscales.no_exterior as no_exterior,
        domicilios_fiscales.no_interior as no_interior,
        domicilios_fiscales.colonia as colonia,
        domicilios_fiscales.referencia as referencia,
        domicilios_fiscales.localidad as localidad,
        domicilios_fiscales.municipio as municipio,
        domicilios_fiscales.estado as estado,
        domicilios_fiscales.codigo_postal as codigo_postal,
        pais.nombre as pais,
        domicilios_fiscales.creado as creado,
        domicilios_fiscales.modificado as modificado
    FROM
        domicilios_fiscales 
    INNER JOIN pais ON domicilios_fiscales.id_pais = pais.id_pais
    WHERE id_domicilio_fiscal = :id_domicilio_fiscal
    LIMIT 1
");

$stmt->bindParam(':id_domicilio_fiscal', $id_domicilio_fiscal, PDO::PARAM_STR);

$result = $stmt->execute();

if (!$result) {
    header('location: configuracion.php');
    exit();
}

$datos_fiscales = $stmt->fetch(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $name; ?></title>
    <?php include ("includes/css.php"); ?>
    <link href="plugins/data-tables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
    <style>
        .error {
            color: #f00;
        }
    </style>
    <!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed">
    <?php include ("includes/header.php"); ?>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php include ("includes/menu.php"); ?>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="theme-panel hidden-xs hidden-sm">
                    <div class="toggler-close"></div>
                </div>
                <!-- END STYLE CUSTOMIZER -->
                
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">Configuración</h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-cogs"></i>
                                <a href="inicio.php">
                                    Dashboard
                                </a>
                                <i class="fa fa-angle-right"></i>
                            </li>  
                            <li>
                                <a href="configuracion.php">
                                    Configuración
                                </a>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-offset-3 col-md-6">
                        <!-- portlet box blue -->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <h4>Datos fiscales</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Razón Social</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['razon_social']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Rfc</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['rfc']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Calle</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['calle']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label># Exterior</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['no_exterior']; ?></p>
                                    </div>
                                    <div class="col-md-6">
                                        <label># Interior</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['no_interior']; ?></p>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Colonia</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['colonia']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Referencia</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['referencia']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Localidad</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['localidad']; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Municipio</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['municipio']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Estado</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['estado']; ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Código Postal</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['codigo_postal']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>País</label>
                                        <p class="form-control" style="background: #eee;"><?php echo $datos_fiscales['pais']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body well" style="margin-bottom: 0;">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button type="button" id="df-cancel-button" class="btn btn-danger pull-left"><i class="fa fa-times"></i> Cancelar</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" id="df-edit-button" data-id-domicilio-fiscal="<?php echo $datos_fiscales['id_domicilio_fiscal']; ?>" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i> Editar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- portlet box blue -->
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
    </div>
    
    <?php include ("includes/footer.php"); ?>
    <?php include ("includes/js.php"); ?>

    <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="scripts/custom/table-advanced.js"></script>
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="plugins/jquery-validation/dist/jquery.validate.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins

            $('#df-cancel-button').click(function() {
                location.href = 'configuracion.php';
            });

            $('#df-edit-button').click(function() {
                var id_domicilio_fiscal = $(this).data('id-domicilio-fiscal');
                
                location.href = 'domicilio-fiscal-edit.php?id=' + id_domicilio_fiscal;
            });
        });
    </script>

</body>
<!-- END BODY -->
</html>