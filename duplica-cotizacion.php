<?php
	
	if (!isset($_SESSION)) {
    	session_start();
	}

	if (($_SESSION['id_usuario'] == NULL)) {
		header("Location: logout.php");
	}

	require_once("config.php");
	require_once("includes/funciones_BD.php");
	require_once("includes/validacion.php");

	$id_usuario = $_SESSION['id_usuario'];

	$id_cotizacio = 0;

	if (isset($_GET['id_cotizacion'])) {
	    $id_cotizacion = $_GET['id_cotizacion'];
	}

	if ($id_cotizacion) {
		$cotizacion = $pdo->query("SELECT * FROM cotizaciones WHERE id_cotizacion = $id_cotizacion LIMIT 1")->fetch(PDO::FETCH_ASSOC);

		$conceptos = $pdo->query("SELECT * FROM conceptos WHERE id_cotizacion = $id_cotizacion LIMIT 1")->fetchAll(PDO::FETCH_ASSOC);

		// echo '<pre>' . print_r($cotizacion, 1) . '</pre>';
		// echo '<pre>' . print_r($conceptos, 1) . '</pre>';

		$fields = '';
		$values = '';

		foreach ($cotizacion as $field => $value) {
			if ($field != 'id_cotizacion') {
				$fields .= "$field, ";
				$values .= "'$value', ";
			}
		}

		// remove trailing ',' from $fields and $values
		$fields = preg_replace('/, $/', '', $fields);
		$values = preg_replace('/, $/', '', $values);

		// insert pulicated quotation
		$stmt = $pdo->prepare("INSERT INTO cotizaciones ($fields) VALUES ($values)");
		$stmt->execute();

		$id_cotizacion_new = $pdo->lastInsertId();

		foreach ($conceptos as $concepto) {
			$fields = '';
			$values = '';
			
			foreach ($concepto as $field => $value) {
				if ($field != 'id_concepto') {
					if ($field == 'id_cotizacion') {
						$fields .= "$field, ";
						$values .= "'$id_cotizacion_new', ";
					} else {
						$fields .= "$field, ";
						$values .= "'$value', ";
					}
				}
			}

			// remove trailing ',' from $fields and $values
			$fields = preg_replace('/, $/', '', $fields);
			$values = preg_replace('/, $/', '', $values);

			// insert pulicated concept
			$sql = "INSERT INTO conceptos ($fields) VALUES ($values)";

			$stmt = $pdo->prepare($sql);
			$stmt->execute();
		}

		header("Location: detalle-cotizacion.php?id=$id_cotizacion_new");
		exit;
	}

	header("Location: listado-cotizaciones.php");
	exit;

?>