-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2017 at 01:55 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `tipo` int(11) DEFAULT '1',
  `activo` tinyint(1) DEFAULT '1',
  `aprobado` tinyint(1) DEFAULT '0',
  `motivo_aprobado` text,
  `nombre_cliente` varchar(256) DEFAULT NULL,
  `divisa_cliente` int(11) DEFAULT NULL,
  `tipo_costo` varchar(2) DEFAULT NULL,
  `motivo_costo` text,
  `depositos` double NOT NULL DEFAULT '0',
  `saldo` double NOT NULL DEFAULT '0',
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `razon_social` varchar(256) DEFAULT NULL,
  `rfc` varchar(20) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `nombre_autorizado` varchar(256) DEFAULT NULL,
  `email_autorizado` varchar(128) DEFAULT NULL,
  `telefono_autorizado` varchar(64) DEFAULT NULL,
  `ext_autorizado` varchar(32) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `tipo`, `activo`, `aprobado`, `motivo_aprobado`, `nombre_cliente`, `divisa_cliente`, `tipo_costo`, `motivo_costo`, `depositos`, `saldo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `razon_social`, `rfc`, `id_domicilio`, `nombre_autorizado`, `email_autorizado`, `telefono_autorizado`, `ext_autorizado`, `id_usuario_alta`, `fecha_alta`, `id_log`) VALUES
(1, 2, 0, 1, 'Es un buen cliente', 'Sams', 2, 'VB', 'Este es un cliente de prueba', 0, 0, 'Marco Martinez', 'marco@test.com', '324452342332', '12344', 'SSM121212M3A', 'Sams de Mexico, SA d', 6, 'Pedro Palma', 'pedro@test.com', '24346345432', '2342', 1, '2016-01-11 20:28:57', 0),
(2, 2, 0, 1, NULL, 'Instalaciones modernas', 2, 'VB', 'jlñka sjfñlkjsañ fasldkjf lasdkf', 0, 0, 'fdfffffffffffffffff', 'ssssssssssssss', '32465635345655', NULL, 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 9, 'asdf', NULL, NULL, 'sss', 1, '2016-01-11 20:44:14', 0),
(3, 2, 1, 1, NULL, 'Celular Express', 2, 'VA', 'asdfsfdaafad', 0, 1820.98, 'asdf', 'jma_hernandez@hotmail.com', '8987988', '241', 'Celular Express', 'CEL010101XYZ', 10, 'Pedro Perez', 'perez@perez.com', '3221332', '241', 1, '2016-01-11 20:44:52', 0),
(4, 1, 0, 0, NULL, 'Vista Hermosa', 2, 'VA', 'Sin motivo aparente', 0, 0, 'asdf', 'ssssssssssssss', '233333', '123', 'razon social nnnnnnnnnnnnnnnnnnn', 'rfc klsadfljalfjdkas', 11, 'dkljlfsdñ', 'dldkldakldf', '22222', '3232', 1, '2016-01-11 21:06:20', 48),
(5, 2, 0, 2, NULL, 'Paquito Lopez', 2, 'VB', 'Precio de siempre', 0, 0, 'Paco Lopez Lopez', 'paco@lopez.com ', NULL, NULL, 'TEst', NULL, 12, 'Juan José López López', 'pepe@lopez.com', '324546', NULL, 1, '2016-01-11 23:30:38', 0),
(6, 2, 1, 1, NULL, 'Bodegas de frutas del centro', 2, 'VA', 'Cliente con 200 bodegas en el país', 0, 783834.18, 'Francisco Solera', 'example@example.com', '323322332', NULL, 'Bodegas del centro, SA de CV', 'AAA010101BBB', 13, 'Pablo Pardo', 'pardo@test.com', '2343523', '222', 1, '2016-01-12 18:07:59', 0),
(7, 2, 0, 1, NULL, 'Juan Perez', 2, 'VB', 'Dale chance', 0, 0, 'Juan Perez', 'juanito@hotmail.com', NULL, NULL, NULL, NULL, 14, NULL, NULL, NULL, NULL, 1, '2016-02-01 06:51:45', 0),
(8, 2, 1, 1, NULL, 'Jacobo Altamirano', 2, 'VA', NULL, 0, 209.5, 'Jacobo Altamirano', 'test@example.com', NULL, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, 7, '2016-02-27 16:59:47', 0),
(9, 2, 1, 1, NULL, 'Sistemas de Victoria', 2, 'VB', 'Ninguno por el momento', 0, 1449.9, 'Miguel Angel Amaro Hernández', 'jma_hernandez@hotmail.com', '8341265342', NULL, 'Miguel Angel Amaro Hernández', 'VIC010101SYS', 16, 'Miguel Angel Amaro Hernández', 'jma_hernandez@hotmail.com', '8341265342', NULL, 1, '2016-04-09 02:37:29', 0),
(10, 2, 1, 1, NULL, 'Fernando Montes de Oca', 2, 'VA', 'local', 0, 0, 'fernando', 'fermoca@example.com', '1234451233', NULL, 'Fernando Montes de Oca', 'FMO010101XYZ', 17, NULL, NULL, NULL, NULL, 1, '2016-05-27 03:24:48', 0),
(11, 1, 1, 0, NULL, 'miguel angel', 2, 'VA', 'prueba', 0, 0, 'prueba', 'prueba', '1234', NULL, 'prueba', 'prueba', 20, 'prueba', 'prueba', '1234', NULL, 1, '2016-10-20 03:21:19', 11),
(12, 1, 1, 0, NULL, 'prueba', 2, 'VA', 'prueba', 0, 0, 'prueba', 'prueba', NULL, NULL, NULL, NULL, 21, NULL, NULL, NULL, NULL, 1, '2016-10-20 03:22:38', 12);

-- --------------------------------------------------------

--
-- Table structure for table `componentes`
--

CREATE TABLE `componentes` (
  `id_componente` int(11) NOT NULL,
  `id_componente_grupo` int(11) DEFAULT NULL,
  `nombre_componente` varchar(128) DEFAULT NULL,
  `VA` double(10,2) NOT NULL,
  `VB` double(10,2) NOT NULL,
  `VC` double(10,2) NOT NULL DEFAULT '0.00',
  `VD` double(10,2) NOT NULL DEFAULT '0.00',
  `VE` double(10,2) NOT NULL DEFAULT '0.00',
  `VF` double(10,2) NOT NULL DEFAULT '0.00',
  `precio` double(10,2) NOT NULL DEFAULT '0.00',
  `impuesto` double(10,2) NOT NULL DEFAULT '0.00',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `componentes`
--

INSERT INTO `componentes` (`id_componente`, `id_componente_grupo`, `nombre_componente`, `VA`, `VB`, `VC`, `VD`, `VE`, `VF`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 2, 'Flecha 2.8', 19.20, 12.00, 10.44, 9.08, 8.25, 7.50, 19.20, 0.00, 1, NULL),
(2, 2, 'Flecha 3.4', 22.53, 14.08, 12.25, 10.65, 9.68, 8.80, 22.53, 0.00, 1, NULL),
(3, 2, 'Flecha 3.9', 26.63, 16.64, 14.47, 12.58, 11.44, 10.40, 26.63, 0.00, 1, NULL),
(4, 2, 'Flecha 4.5', 30.72, 19.20, 16.70, 14.52, 13.20, 12.00, 30.72, 0.00, 1, NULL),
(5, 4, 'Guias 2 10 par', 128.53, 80.33, 69.85, 60.74, 55.20, 43.65, 128.53, 0.00, 1, 0),
(6, 4, 'Guias 2 11 par', 140.56, 87.85, 76.39, 66.43, 60.39, 54.90, 140.56, 0.00, 1, 0),
(7, 4, 'Guias 2 12 par', 152.60, 95.37, 82.93, 72.12, 65.56, 59.60, 152.60, 0.00, 1, 0),
(8, 4, 'Guias 2 13 par', 164.63, 102.89, 89.47, 77.80, 70.73, 64.30, 164.63, 0.00, 1, 0),
(9, 3, 'Tornillo Cabeza de Hongo 3/8 x 1', 0.51, 0.32, 0.28, 0.24, 0.22, 0.20, 0.51, 0.00, 1, 0),
(10, 3, 'Tornilo Cabeza Plana 1/4 x 1/2', 0.26, 0.16, 0.14, 0.12, 0.11, 0.10, 0.26, 0.00, 1, NULL),
(11, 3, 'Tornillo Hezagonal 3/8 x 2', 0.51, 0.32, 0.28, 0.24, 0.22, 0.20, 0.51, 0.00, 1, 0),
(12, 3, 'Tornillo Punta Broca', 0.26, 0.16, 0.14, 0.12, 0.11, 0.10, 0.26, 0.00, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `componentes_grupos`
--

CREATE TABLE `componentes_grupos` (
  `id_grupo_componente` int(11) NOT NULL,
  `grupo_componente` varchar(128) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `componentes_grupos`
--

INSERT INTO `componentes_grupos` (`id_grupo_componente`, `grupo_componente`, `activo`, `id_log`) VALUES
(2, 'Flechas', 1, 2),
(3, 'Tornillos', 1, 0),
(4, 'Guias', 1, 4),
(5, 'Sellos', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `conceptos`
--

CREATE TABLE `conceptos` (
  `id_concepto` int(11) NOT NULL,
  `nombre_concepto` varchar(128) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `id_puerta` int(11) DEFAULT NULL,
  `m_ancho` double NOT NULL DEFAULT '0',
  `m_alto` double NOT NULL DEFAULT '0',
  `f_ancho` double NOT NULL DEFAULT '0',
  `in_ancho` double NOT NULL DEFAULT '0',
  `f_alto` double NOT NULL DEFAULT '0',
  `in_alto` double NOT NULL DEFAULT '0',
  `id_pmodelo` int(11) DEFAULT NULL,
  `id_pcolor` int(11) DEFAULT NULL,
  `id_pmovimiento` int(11) DEFAULT NULL,
  `id_ptextura` int(11) DEFAULT NULL,
  `id_psello` int(11) DEFAULT NULL,
  `id_pmotor` int(11) DEFAULT NULL,
  `id_ventana` int(11) DEFAULT NULL,
  `id_vcolor` int(11) DEFAULT NULL,
  `nventanas` int(11) NOT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `id_grupo_componente` int(11) DEFAULT NULL,
  `id_kit` int(11) DEFAULT NULL,
  `id_cotizacion` int(11) DEFAULT NULL,
  `precio_unitario` double(10,2) NOT NULL DEFAULT '0.00',
  `importe` double(10,2) NOT NULL DEFAULT '0.00',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conceptos`
--

INSERT INTO `conceptos` (`id_concepto`, `nombre_concepto`, `cantidad`, `id_ptipo`, `id_puerta`, `m_ancho`, `m_alto`, `f_ancho`, `in_ancho`, `f_alto`, `in_alto`, `id_pmodelo`, `id_pcolor`, `id_pmovimiento`, `id_ptextura`, `id_psello`, `id_pmotor`, `id_ventana`, `id_vcolor`, `nventanas`, `id_componente`, `id_grupo_componente`, `id_kit`, `id_cotizacion`, `precio_unitario`, `importe`, `id_log`) VALUES
(1, NULL, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, 3, NULL, 1, 9.39, 9.39, 1),
(2, NULL, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 10, 3, NULL, 2, 4.79, 4.79, 2),
(3, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 6, 2, 1, 2, NULL, NULL, NULL, 4, 37908.36, 37908.36, 3),
(4, NULL, 1, 2, 10, 0, 0, 0, 0, 0, 0, 10, 1, 2, 1, 1, 6, 3, 1, 2, NULL, NULL, NULL, 5, 45521.35, 45521.35, 4),
(5, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 11, 3, NULL, 5, 9.39, 18.79, 5),
(6, NULL, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 5, 2739.97, 2739.97, 6),
(7, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 5, 0, 0, 0, NULL, NULL, NULL, 7, 84857.81, 84857.81, 7),
(8, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 5, 0, 0, 0, NULL, NULL, NULL, 8, 84857.81, 84857.81, 8),
(9, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 5, 0, 0, 0, NULL, NULL, NULL, 9, 84857.81, 84857.81, 9),
(10, NULL, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 9, 353.66, 353.66, 10),
(11, NULL, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 10, 353.66, 353.66, 11),
(12, NULL, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 14, 353.66, 353.66, 12),
(13, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 15, 353.66, 707.33, 13),
(14, NULL, 1, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 16, 353.66, 353.66, 14),
(15, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 17, 353.66, 707.33, 15),
(16, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 18, 353.66, 707.33, 16),
(17, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 19, 353.66, 707.33, 17),
(18, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, 3, NULL, 20, 9.39, 18.79, 18),
(19, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 21, 353.66, 707.33, 19),
(20, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, 3, NULL, 22, 9.39, 18.79, 20),
(21, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, 3, NULL, 23, 9.39, 18.79, 21),
(22, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, 3, NULL, 25, 10.20, 20.40, 22),
(23, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, 3, NULL, 27, 10.20, 20.40, 23),
(24, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 28, 10.20, 20.40, 24),
(25, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 29, 400.00, 800.00, 25),
(26, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 30, 400.00, 800.00, 26),
(27, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, 2, NULL, 30, 200.00, 400.00, 27),
(28, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 31, 200.00, 400.00, 28),
(29, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, 2, NULL, 31, 300.00, 600.00, 29),
(30, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, 3, NULL, 31, 10.00, 20.00, 30),
(31, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 10, 3, NULL, 31, 5.00, 10.00, 31),
(32, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 5, 0, 0, 0, NULL, NULL, NULL, 32, 84857.81, 84857.81, 32),
(33, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 6, 0, 0, 0, NULL, NULL, NULL, 34, 37871.52, 37871.52, 33),
(34, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 8, 0, 0, 0, NULL, NULL, NULL, 35, 56307.91, 56307.91, 34),
(36, NULL, 2, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, NULL, 36, 360.00, 720.00, 36);

-- --------------------------------------------------------

--
-- Table structure for table `cotizaciones`
--

CREATE TABLE `cotizaciones` (
  `id_cotizacion` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `numero_cotizacion` varchar(64) DEFAULT NULL,
  `descripcion` text,
  `id_termino_venta` int(11) NOT NULL,
  `autorizada` tinyint(1) DEFAULT '0',
  `id_usuario_autoriza` int(11) DEFAULT NULL,
  `autorizada_fecha` datetime DEFAULT NULL,
  `autorizada_rechazo` text,
  `id_usuario_autoriza_rechazo` int(11) DEFAULT NULL,
  `autorizada_rechazo_fecha` datetime DEFAULT NULL,
  `pagada` tinyint(4) DEFAULT '0',
  `pagoconsaldo` double NOT NULL DEFAULT '0',
  `url_pago` varchar(500) DEFAULT NULL,
  `id_usuario_pendiente` int(11) DEFAULT NULL,
  `pendiente_fecha` datetime DEFAULT NULL,
  `pendiente_motivo` varchar(255) DEFAULT NULL,
  `id_usuario_pagada` int(11) DEFAULT NULL,
  `pagada_fecha` datetime DEFAULT NULL,
  `aprobada_motivo` varchar(255) DEFAULT NULL,
  `id_usuario_pagada_rechazo` int(11) DEFAULT NULL,
  `pagada_rechazo_fecha` datetime DEFAULT NULL,
  `rechazada_motivo` varchar(255) DEFAULT NULL,
  `pagada_rechazo` text,
  `pagada_comprobante` text,
  `precio_venta` int(11) DEFAULT NULL,
  `id_divisa` int(11) DEFAULT NULL,
  `tipo_cambio` double DEFAULT NULL,
  `id_impuesto` int(11) NOT NULL,
  `produccion` int(11) DEFAULT '0',
  `produccion_fecha` datetime DEFAULT NULL,
  `despacho` int(11) DEFAULT '0',
  `tipo_despacho` int(11) DEFAULT '0',
  `despacho_fecha` datetime DEFAULT NULL,
  `num_guia` varchar(25) DEFAULT NULL,
  `subtotal` double NOT NULL DEFAULT '0',
  `impuestos` double DEFAULT '0',
  `total` double DEFAULT '0',
  `id_vendedor` int(11) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `comentarios_cotizacion` text,
  `factura_serie` varchar(50) NOT NULL,
  `factura_folio` int(11) NOT NULL,
  `factura_rfc_emisor` varchar(255) NOT NULL,
  `factura_rfc_receptor` varchar(255) NOT NULL,
  `factura_uuid` varchar(255) NOT NULL,
  `factura_xml_url` varchar(255) NOT NULL,
  `factura_pdf_url` varchar(255) NOT NULL,
  `id_log` int(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cotizaciones`
--

INSERT INTO `cotizaciones` (`id_cotizacion`, `id_cliente`, `numero_cotizacion`, `descripcion`, `id_termino_venta`, `autorizada`, `id_usuario_autoriza`, `autorizada_fecha`, `autorizada_rechazo`, `id_usuario_autoriza_rechazo`, `autorizada_rechazo_fecha`, `pagada`, `pagoconsaldo`, `url_pago`, `id_usuario_pendiente`, `pendiente_fecha`, `pendiente_motivo`, `id_usuario_pagada`, `pagada_fecha`, `aprobada_motivo`, `id_usuario_pagada_rechazo`, `pagada_rechazo_fecha`, `rechazada_motivo`, `pagada_rechazo`, `pagada_comprobante`, `precio_venta`, `id_divisa`, `tipo_cambio`, `id_impuesto`, `produccion`, `produccion_fecha`, `despacho`, `tipo_despacho`, `despacho_fecha`, `num_guia`, `subtotal`, `impuestos`, `total`, `id_vendedor`, `fecha`, `comentarios_cotizacion`, `factura_serie`, `factura_folio`, `factura_rfc_emisor`, `factura_rfc_receptor`, `factura_uuid`, `factura_xml_url`, `factura_pdf_url`, `id_log`, `activo`) VALUES
(1, 6, '20161114HS8W27K0SNI', 'Descripción', 1, 1, NULL, NULL, NULL, NULL, NULL, 2, 0, 'comprobantes/cotizacion_1/comprobante1.jpg', NULL, NULL, NULL, 1, '2016-11-14 11:59:19', 'motivo', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 1, '2016-11-25 12:09:48', 1, 1, '2016-12-02 15:35:31', '12345', 9.39, 1.5, 10.89, 1, '2016-11-14 23:28:16', 'comentario', '1', 1, 'HPG130304SK6', 'AAA010101BBB', 'A1E55F91-8D54-4CE7-A9F4-4786901A4F77', 'facturas/HPG130304SK6/emitido/xml/2016/11/HPG130304SK6_11_AAA010101BBB.xml', 'facturas/HPG130304SK6/emitido/pdf/2016/11/HPG130304SK6_11_AAA010101BBB.pdf', NULL, 1),
(2, 6, '20161114HSYRD6OH9CA', 'Descripción', 6, 1, NULL, NULL, NULL, NULL, NULL, 2, 0, 'comprobantes/cotizacion_2/comprobante2.jpg', NULL, NULL, NULL, 1, '2016-12-22 12:06:21', 'motivo', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, 4.79, 0.77, 5.56, 1, '2016-11-14 23:30:38', 'comentarios', '', 0, '', '', '', '', '', NULL, 1),
(3, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-14 23:31:25', NULL, '', 0, '', '', '', '', '', NULL, 0),
(4, 3, '20161114HSEHTXLOF50', 'Descripción', 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, 37908.36, 6065.34, 43973.7, 1, '2016-11-15 03:21:41', NULL, '', 0, '', '', '', '', '', NULL, 1),
(5, 8, '20161114HS51QLNJPH9', 'Descripción', 6, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, 48280.11, 7724.82, 56004.93, 1, '2016-11-15 03:30:41', NULL, '', 0, '', '', '', '', '', NULL, 1),
(6, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-25 23:46:59', NULL, '', 0, '', '', '', '', '', NULL, 0),
(7, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-25 23:55:50', NULL, '', 0, '', '', '', '', '', NULL, 0),
(8, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 00:10:03', NULL, '', 0, '', '', '', '', '', NULL, 0),
(9, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 00:14:57', NULL, '', 0, '', '', '', '', '', NULL, 0),
(10, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 00:36:05', NULL, '', 0, '', '', '', '', '', NULL, 0),
(11, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:06:11', NULL, '', 0, '', '', '', '', '', NULL, 0),
(12, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:06:17', NULL, '', 0, '', '', '', '', '', NULL, 0),
(13, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:06:57', NULL, '', 0, '', '', '', '', '', NULL, 0),
(14, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:07:15', NULL, '', 0, '', '', '', '', '', NULL, 0),
(15, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:14:54', NULL, '', 0, '', '', '', '', '', NULL, 0),
(16, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:18:32', NULL, '', 0, '', '', '', '', '', NULL, 0),
(17, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:20:23', NULL, '', 0, '', '', '', '', '', NULL, 0),
(18, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:21:43', NULL, '', 0, '', '', '', '', '', NULL, 0),
(19, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:32:53', NULL, '', 0, '', '', '', '', '', NULL, 0),
(20, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:41:46', NULL, '', 0, '', '', '', '', '', NULL, 0),
(21, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 01:56:21', NULL, '', 0, '', '', '', '', '', NULL, 0),
(22, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 02:54:31', NULL, '', 0, '', '', '', '', '', NULL, 0),
(23, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:04:54', NULL, '', 0, '', '', '', '', '', NULL, 0),
(24, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:12:38', NULL, '', 0, '', '', '', '', '', NULL, 0),
(25, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:12:51', NULL, '', 0, '', '', '', '', '', NULL, 0),
(26, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:22:08', NULL, '', 0, '', '', '', '', '', NULL, 0),
(27, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:22:14', NULL, '', 0, '', '', '', '', '', NULL, 0),
(28, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:25:40', NULL, '', 0, '', '', '', '', '', NULL, 0),
(29, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:27:58', NULL, '', 0, '', '', '', '', '', NULL, 0),
(30, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-11-26 03:41:54', NULL, '', 0, '', '', '', '', '', NULL, 0),
(31, 3, '20161125HS8TLY2IS5H', 'descripción', 6, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, 1030, 164.8, 1194.8, 1, '2016-11-26 03:44:19', NULL, '', 0, '', '', '', '', '', NULL, 1),
(32, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-12-23 00:06:53', NULL, '', 0, '', '', '', '', '', NULL, 0),
(33, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-12-23 00:31:09', NULL, '', 0, '', '', '', '', '', NULL, 0),
(34, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-12-23 00:31:15', NULL, '', 0, '', '', '', '', '', NULL, 0),
(35, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2016-12-23 00:32:19', NULL, '', 0, '', '', '', '', '', NULL, 0),
(36, 6, '20161222HSVXLPMSHJN', 'descripción', 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 1, 0, NULL, 0, 1, NULL, NULL, 720, 115.2, 835.2, 1, '2016-12-23 00:40:23', NULL, '', 0, '', '', '', '', '', NULL, 1),
(37, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 0, NULL, NULL, 0, 0, 0, 1, '2017-03-09 23:35:37', NULL, '', 0, '', '', '', '', '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `depositos`
--

CREATE TABLE `depositos` (
  `id_deposito` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `monto_deposito` text,
  `fecha_deposito` text,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT '1',
  `id_log` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `depositos`
--

INSERT INTO `depositos` (`id_deposito`, `id_cliente`, `monto_deposito`, `fecha_deposito`, `fecha`, `id_usuario_alta`, `activo`, `id_log`) VALUES
(1, 7, '2034', '4342332-4-32', '2016-03-27 07:03:33', 1, 0, 0),
(2, 5, '4034', '16-02-01', '2016-03-27 07:03:19', 1, 1, 0),
(3, 6, '200', '14-02-02', '2016-03-27 07:02:54', 1, 1, 0),
(4, 6, '10000', '16-04-04', '2016-03-27 07:03:07', 1, 1, 0),
(5, 8, '1000', '2016-03-15', '2016-03-17 12:15:40', 1, 1, 0),
(6, 1, '12500.50', '2016-04-11', '2016-04-30 01:50:12', 1, 1, 9),
(7, 1, '12500.50', '2016-04-04', '2016-04-07 02:45:46', 1, 1, 0),
(8, 1, '120', '2016-04-08', '2016-04-30 02:52:59', 1, 1, 16),
(9, 6, '250', '2016-04-01', '2016-04-30 02:49:29', 1, 1, 14),
(10, 6, '89.90', '2016-04-02', '2016-04-30 01:51:11', 1, 1, 0),
(11, 9, '1000', '2016-04-29', '2016-04-30 02:25:46', NULL, 1, 0),
(12, 9, '250', '2016-04-29', '2016-04-30 02:45:06', NULL, 1, 0),
(13, 9, '499', '2016-04-29', '2016-04-30 02:47:47', NULL, 1, 0),
(14, 9, '199.90', '2016-04-29', '2016-04-30 02:49:29', NULL, 1, 0),
(15, 8, '3500.95', '2016-04-29', '2016-04-30 02:50:42', NULL, 1, 0),
(16, 8, '180.25', '2016-04-29', '2016-04-30 02:52:59', NULL, 1, 0),
(17, 3, '1820.98', '2016-04-29', '2016-04-30 02:54:30', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `divisas`
--

CREATE TABLE `divisas` (
  `id_divisa` int(11) NOT NULL,
  `abbrev` varchar(5) DEFAULT NULL,
  `nombre` varchar(128) DEFAULT NULL,
  `tipo_cambio` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `divisas`
--

INSERT INTO `divisas` (`id_divisa`, `abbrev`, `nombre`, `tipo_cambio`, `activo`, `id_log`) VALUES
(1, 'USD', 'Dolar Americano', 1, 1, 0),
(2, 'MXN', 'Peso Mexicano', 18.42, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `domicilios`
--

CREATE TABLE `domicilios` (
  `id_domicilio` int(11) NOT NULL,
  `calle` varchar(256) DEFAULT NULL,
  `no_exterior` varchar(32) DEFAULT NULL,
  `no_interior` varchar(32) DEFAULT NULL,
  `colonia` varchar(256) DEFAULT NULL,
  `localidad` varchar(256) DEFAULT NULL,
  `referencia` text,
  `municipio` varchar(256) DEFAULT NULL,
  `estado` varchar(64) DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `codigo_postal` varchar(32) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `domicilios`
--

INSERT INTO `domicilios` (`id_domicilio`, `calle`, `no_exterior`, `no_interior`, `colonia`, `localidad`, `referencia`, `municipio`, `estado`, `id_pais`, `codigo_postal`, `fecha_alta`) VALUES
(1, 'Francisco Javier Mina', '312', 'N/A', 'Valles de Huinalá', 'Bodega industrial', 'Monterrey', 'Apodaca', 'Nuevo León', 117, '66600', '2016-01-08 11:11:00'),
(2, 'Test', NULL, '221', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 11:11:53'),
(3, '33333333333', '22', '45555555555', 'Las pruebas', 'salkdjf', 'Casa sola', 'klsdf', 'alksdf', 17, '3452', '2016-01-08 11:22:25'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 11:23:10'),
(5, NULL, 'ext', 'int', NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-08 12:29:56'),
(6, 'Av. de la Palma', '234', '21', 'Palmira', 'Edificio blanco', 'Ciudad de México', 'Alvaro Obregón', 'Distrito Federal', 117, '03630', '2016-01-11 20:28:57'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 20:42:12'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 20:42:45'),
(9, NULL, 'asdfsda', NULL, NULL, NULL, NULL, NULL, 'asdf', 102, NULL, '2016-01-11 20:44:14'),
(10, 'Sin calle conocida', '123', '456', 'Primavera', 'Monterrey', '12345678', 'Monterrey', 'Nuevo Leon', 117, '64000', '2016-01-11 20:44:52'),
(11, 'asdfasdfasd', 'askdlfja', 'lasl', 'aslkfkdas', 'aslkdlk', NULL, 'lkasjafñlkajsdf', 'askflksadjfdsal', 113, '77777', '2016-01-11 21:06:20'),
(12, 'Callejón de los Milagros', '123', NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-01-11 23:30:38'),
(13, 'Prueba', '2345', '24', 'Ninguno', 'Ninguno', '12345678', 'Monterrey', 'Nuevo León', 117, '2456654', '2016-01-12 18:07:59'),
(14, 'jkashdfilhsdaf lu', '2323', '34', 'jdf ñasdfj kasdh', NULL, NULL, NULL, NULL, 117, NULL, '2016-02-01 06:51:44'),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-02-27 16:59:47'),
(16, 'Manzanero', '1024', '00', 'Lopez Mateos', 'Victoria', '12345678', 'Victoria', 'Tamaulipas', 117, '87020', '2016-04-09 02:37:29'),
(17, 'Mazapan', '512', '64', 'Centro Monterrey', 'Monterrey', '12345678', 'Monterrey', 'Nuevo Leon', 117, '64000', '2016-05-27 03:24:48'),
(18, 'naola', '36', '24', 'lopez mateos', '12345678', 'victoria', 'victoria', 'tamaulipas', 117, '87020|', '2016-08-26 23:23:13'),
(19, 'Naola', '36', '24', 'Lopez Mateos', '12345678', 'Victoria', 'Victoria', 'Tamaulipas', 117, '87020', '2016-08-27 00:16:43'),
(20, 'prueba', 'prueba', 'prueba', 'prueba', 'prueba', 'prueba', 'prueba', 'prueba', 117, 'prueba', '2016-10-20 03:21:19'),
(21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 117, NULL, '2016-10-20 03:22:38');

-- --------------------------------------------------------

--
-- Table structure for table `domicilios_fiscales`
--

CREATE TABLE `domicilios_fiscales` (
  `id_domicilio_fiscal` int(11) NOT NULL,
  `razon_social` varchar(255) NOT NULL,
  `rfc` varchar(50) NOT NULL,
  `calle` varchar(255) NOT NULL,
  `no_exterior` varchar(50) NOT NULL,
  `no_interior` varchar(50) NOT NULL,
  `colonia` varchar(255) NOT NULL,
  `referencia` varchar(50) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  `municipio` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `codigo_postal` varchar(50) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `domicilios_fiscales`
--

INSERT INTO `domicilios_fiscales` (`id_domicilio_fiscal`, `razon_social`, `rfc`, `calle`, `no_exterior`, `no_interior`, `colonia`, `referencia`, `localidad`, `municipio`, `estado`, `codigo_postal`, `id_pais`, `id_usuario`, `creado`, `modificado`) VALUES
(1, 'Razón Social de Prueba 2016', 'RSP160512XXX', 'Mazapan', '512', '24', 'Mazapan', '12345678', 'Laredo', 'Laredo', 'Tamaulipas', '68000', 117, 1, '2016-05-13 04:34:41', '2016-05-14 00:42:03'),
(2, 'Razón Social de Prueba 2', 'RSP222222XXX', 'Naola', '36', '24', 'Naola', '123456', 'Victoria', 'Tamaulipas', 'Tamaulipas', '64000', 2, 1, '2016-05-13 21:03:50', '2016-05-13 23:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `impuestos`
--

CREATE TABLE `impuestos` (
  `id_impuesto` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `porcentaje` double DEFAULT NULL,
  `id_log` int(11) DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `impuestos`
--

INSERT INTO `impuestos` (`id_impuesto`, `nombre`, `porcentaje`, `id_log`, `activo`) VALUES
(1, 'Mexico', 16, 0, 1),
(3, 'prueba 1', 34, NULL, 0),
(4, 'Mexico', 16, 0, 0),
(5, 'USA', 12.4, 0, 1),
(6, 'Canada', 11, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `incidencias`
--

CREATE TABLE `incidencias` (
  `id_incidencia` int(11) NOT NULL,
  `id_cotizacion` int(11) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descripcion` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `autorizada` tinyint(11) DEFAULT '0',
  `autorizada_rechazo` text,
  `motivo` varchar(200) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `id_usuario_resolvio` int(11) DEFAULT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `id_log` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `incidencias`
--

INSERT INTO `incidencias` (`id_incidencia`, `id_cotizacion`, `fecha`, `descripcion`, `status`, `autorizada`, `autorizada_rechazo`, `motivo`, `id_usuario_alta`, `id_usuario_resolvio`, `activo`, `id_log`) VALUES
(2, 2, '2016-10-03 22:43:34', 'Esto es una prueba y ha sido editada por tercera vez', 1, 0, NULL, '', 1, NULL, 1, 0),
(3, 69, '2016-10-03 22:42:34', 'Nuevamente esto es otra prueba', 1, 0, NULL, '', 1, NULL, 1, 0),
(4, 4, '2016-10-01 04:10:33', 'nueva incidencia', 1, 0, NULL, 'aceptado', 1, NULL, 1, 0),
(5, 1, '2017-03-09 23:36:12', NULL, 0, 0, NULL, NULL, 1, NULL, 0, 0),
(6, 1, '2017-03-09 23:36:07', 'TEST', 0, 0, NULL, NULL, 1, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kit`
--

CREATE TABLE `kit` (
  `id_kit` int(11) NOT NULL,
  `nombre_kit` text,
  `VA` double(10,2) NOT NULL DEFAULT '0.00',
  `VB` double(10,2) NOT NULL DEFAULT '0.00',
  `VC` double(10,2) NOT NULL DEFAULT '0.00',
  `VD` double(10,2) NOT NULL DEFAULT '0.00',
  `VE` double(10,2) NOT NULL DEFAULT '0.00',
  `VF` double(10,2) NOT NULL DEFAULT '0.00',
  `tipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `precio` double(10,2) DEFAULT '0.00',
  `impuesto` double(10,2) DEFAULT '0.00',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kit`
--

INSERT INTO `kit` (`id_kit`, `nombre_kit`, `VA`, `VB`, `VC`, `VD`, `VE`, `VF`, `tipo`, `id_proveedor`, `precio`, `impuesto`, `activo`, `id_log`) VALUES
(1, 'Kit One', 148.75, 140.00, 138.50, 135.00, 132.25, 130.00, NULL, 1, 148.75, 0.00, 1, 0),
(2, 'Kit X-25', 710.99, 700.00, 698.00, 695.00, 692.50, 690.00, NULL, 1, 710.99, 0.00, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kit_componente`
--

CREATE TABLE `kit_componente` (
  `id_kit_componente` int(11) NOT NULL,
  `id_kit` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad_kit_componente` int(11) DEFAULT '1',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kit_componente`
--

INSERT INTO `kit_componente` (`id_kit_componente`, `id_kit`, `id_componente`, `cantidad_kit_componente`, `activo`, `id_log`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 1, 5, 1, 1, 2),
(3, 1, 9, 2, 1, 3),
(4, 2, 1, 1, 1, 4),
(5, 2, 3, 1, 1, 5),
(6, 2, 8, 4, 1, 6),
(7, 2, 9, 12, 1, 7),
(8, 2, 12, 2, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `evento` varchar(32) DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `descripcion` text,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id_log`, `id_usuario`, `evento`, `ip`, `descripcion`, `fecha`) VALUES
(1, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 23:01:29'),
(2, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 1 ', '2016-01-06 23:02:01'),
(3, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 2 ', '2016-01-07 15:50:47'),
(4, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 3 ', '2016-01-07 15:51:33'),
(5, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al usuario con el id 2 ', '2016-01-07 15:51:38'),
(6, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 15:52:09'),
(7, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 15:52:34'),
(8, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 15:53:44'),
(9, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 4 ', '2016-01-07 15:54:24'),
(10, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 15:59:34'),
(11, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 5 ', '2016-01-07 16:00:13'),
(12, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 16:16:38'),
(13, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 4 ', '2016-01-07 16:17:27'),
(14, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 16:33:11'),
(15, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 17:13:43'),
(16, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 18:57:09'),
(17, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 3 ', '2016-01-07 18:57:17'),
(18, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un usuario con el id: 6 ', '2016-01-07 19:36:05'),
(19, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 19:36:39'),
(20, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 6 ', '2016-01-07 19:37:03'),
(21, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 5 ', '2016-01-07 19:39:33'),
(22, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al usuario con el id: 1 ', '2016-01-07 19:39:48'),
(23, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 11:11:00'),
(24, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 11:11:53'),
(25, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al proveedor con el id 1 ', '2016-01-08 11:21:58'),
(26, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 11:22:26'),
(27, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 11:23:11'),
(28, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 11:43:00'),
(29, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 11:43:56'),
(30, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 11:45:09'),
(31, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 12:27:57'),
(32, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 3 ', '2016-01-08 12:28:26'),
(33, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 4 ', '2016-01-08 12:28:41'),
(34, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 1 ', '2016-01-08 12:29:31'),
(35, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un proveedor con el id:  ', '2016-01-08 12:29:56'),
(36, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al proveedor con el id: 5 ', '2016-01-08 12:31:09'),
(37, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 1 ', '2016-01-11 20:28:57'),
(38, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 2 ', '2016-01-11 20:44:14'),
(39, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 3 ', '2016-01-11 20:44:52'),
(40, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 4 ', '2016-01-11 21:06:20'),
(41, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 21:08:18'),
(42, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 3 ', '2016-01-11 21:08:31'),
(43, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 3 ', '2016-01-11 21:09:03'),
(44, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 21:09:16'),
(45, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 21:24:54'),
(46, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 21:25:46'),
(47, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 21:26:11'),
(48, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 4 ', '2016-01-11 21:27:26'),
(49, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 5 ', '2016-01-11 23:30:38'),
(50, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un cliente con el id: 6 ', '2016-01-12 18:08:00'),
(51, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 18:17:15'),
(52, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 18:20:55'),
(53, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 2 ', '2016-01-12 18:21:08'),
(54, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 6 ', '2016-01-13 04:22:51'),
(55, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 04:26:43'),
(56, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 04:26:53'),
(57, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 04:28:40'),
(58, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 04:31:09'),
(59, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 4 ', '2016-01-13 06:51:29'),
(60, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al prospecto con el id 4 ', '2016-01-13 06:52:01'),
(61, 1, 'Eliminar', '::1', 'El usuario con el id 1 elimino al cliente con el id 6 ', '2016-01-13 06:58:42'),
(62, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 06:59:18'),
(63, 1, 'Modificar', '::1', 'El usuario con el id: 1  modifico al cliente con el id: 2 ', '2016-01-13 06:59:36'),
(64, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-14 02:30:09'),
(65, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-14 02:30:42'),
(66, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un color con el id: 0 ', '2016-01-14 02:31:32'),
(67, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 13:51:39'),
(68, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 13:51:52'),
(69, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un multiplicador con el id: 0 ', '2016-01-18 13:52:02'),
(70, 1, 'Alta', '::1', 'El usuario con el id: 1  creo un color con el id: 0 ', '2016-01-18 13:52:25');

-- --------------------------------------------------------

--
-- Table structure for table `multiplicadores`
--

CREATE TABLE `multiplicadores` (
  `id_multiplicador` int(11) NOT NULL,
  `nombre_multiplicador` varchar(16) DEFAULT NULL,
  `valor` double DEFAULT '0',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `multiplicadores`
--

INSERT INTO `multiplicadores` (`id_multiplicador`, `nombre_multiplicador`, `valor`, `activo`, `id_log`) VALUES
(1, 'VA', 2.5, 1, 0),
(2, 'VB', 1.66, 1, 0),
(3, 'VC', 1.392, 1, 0),
(4, 'VD', 1.75, 1, 0),
(5, 'VE', 0, 1, 0),
(6, 'VF', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pais`
--

INSERT INTO `pais` (`id_pais`, `nombre`) VALUES
(1, 'Afganistán'),
(2, 'Albania'),
(3, 'Alemania'),
(4, 'Andorra'),
(5, 'Angola'),
(6, 'Antigua y Barbuda'),
(7, 'Arabia Saudita'),
(8, 'Argelia'),
(9, 'Argentina'),
(10, 'Armenia'),
(11, 'Australia'),
(12, 'Austria'),
(13, 'Azerbaiyán'),
(14, 'Bahamas'),
(15, 'Bangladesh'),
(16, 'Barbados'),
(17, 'Baréin'),
(18, 'Bélgica'),
(19, 'Belice'),
(20, 'Benín'),
(21, 'Bielorrusia'),
(22, 'Birmania'),
(23, 'Bolivia'),
(24, 'Bosnia-Herzegovina'),
(25, 'Botsuana'),
(26, 'Brasil'),
(27, 'Brunéi'),
(28, 'Bulgaria'),
(29, 'Burkina Faso'),
(30, 'Burundi'),
(31, 'Bután'),
(32, 'Cabo Verde'),
(33, 'Camboya'),
(34, 'Camerún'),
(35, 'Canadá'),
(36, 'Chad'),
(37, 'Chile'),
(38, 'China'),
(39, 'Chipre'),
(40, 'Ciudad del Vaticano'),
(41, 'Colombia'),
(42, 'Comoras'),
(43, 'Corea del Norte'),
(44, 'Corea del Sur'),
(45, 'Costa de Marfil'),
(46, 'Costa Rica'),
(47, 'Croacia'),
(48, 'Cuba'),
(49, 'Dinamarca'),
(50, 'Dominica'),
(51, 'Ecuador'),
(52, 'Egipto'),
(53, 'El Salvador'),
(54, 'Emiratos Árabes Unidos'),
(55, 'Eritrea'),
(56, 'Eslovaquia'),
(57, 'Eslovenia'),
(58, 'España'),
(59, 'Estados Unidos'),
(60, 'Estonia'),
(61, 'Etiopia'),
(62, 'Filipinas'),
(63, 'Finlandia'),
(64, 'Fiyi'),
(65, 'Francia'),
(66, 'Gabón'),
(67, 'Gambia'),
(68, 'Georgia'),
(69, 'Ghana'),
(70, 'Granada'),
(71, 'Grecia'),
(72, 'Guatemala'),
(73, 'Guyana'),
(74, 'Guinea'),
(75, 'Guinea ecuatorial'),
(76, 'Guinea-Bisáu'),
(77, 'Haití'),
(78, 'Holanda'),
(79, 'Honduras'),
(80, 'Hungría'),
(81, 'India'),
(82, 'Indonesia'),
(83, 'Irak'),
(84, 'Irán'),
(85, 'Irlanda'),
(86, 'Islandia'),
(87, 'Islas Marshall'),
(88, 'Islas Salomón'),
(89, 'Israel'),
(90, 'Italia'),
(91, 'Jamaica'),
(92, 'Japón'),
(93, 'Jordania'),
(94, 'Kazajistán'),
(95, 'Kenia'),
(96, 'Kirguistán'),
(97, 'Kiribati'),
(98, 'Kuwait'),
(99, 'Laos'),
(100, 'Lesoto'),
(101, 'Letonia'),
(102, 'Libano'),
(103, 'Liberia'),
(104, 'Libia'),
(105, 'Liechtenstein'),
(106, 'Lituania'),
(107, 'Luxemburgo'),
(108, 'Madagascar'),
(109, 'Malasia'),
(110, 'Malaui'),
(111, 'Maldivas'),
(112, 'Malí'),
(113, 'Malta'),
(114, 'Marruecos'),
(115, 'Mauricio'),
(116, 'Mauritania'),
(117, 'México'),
(118, 'Micronesia'),
(119, 'Moldavia'),
(120, 'Mónaco'),
(121, 'Mongolia'),
(122, 'Montenegro'),
(123, 'Mozambique'),
(124, 'Namibia'),
(125, 'Nauru'),
(126, 'Nepal'),
(127, 'Nicaragua'),
(128, 'Níger'),
(129, 'Nigeria'),
(130, 'Noruega'),
(131, 'Nueva Zelanda'),
(132, 'Omán'),
(133, 'Pakistán'),
(134, 'Palaos'),
(135, 'Panamá'),
(136, 'Papua Nueva Guinea'),
(137, 'Paraguay'),
(138, 'Perú'),
(139, 'Polonia'),
(140, 'Portugal'),
(141, 'Qatar'),
(142, 'Reino Unido'),
(143, 'República Centroafricana'),
(144, 'República Checa'),
(145, 'República de Macedonia'),
(146, 'República del Congo'),
(147, 'República Democratica del Congo'),
(148, 'República Dominicana'),
(149, 'República Sudafricana'),
(150, 'Ruanda'),
(151, 'Rumania'),
(152, 'Rusia'),
(153, 'Samoa'),
(154, 'San Cristóbal y Nieves'),
(155, 'San Marino'),
(156, 'San Vicente y las Granadinas'),
(157, 'Santa Lucía'),
(158, 'Santo Tomé y Príncipe'),
(159, 'Senegal'),
(160, 'Serbia'),
(161, 'Seychelles'),
(162, 'Sierra Leona'),
(163, 'Singapur'),
(164, 'Siria'),
(165, 'Somalia'),
(166, 'Sri Lanka'),
(167, 'Suazilandia'),
(168, 'Sudán'),
(169, 'Sudán del Sur'),
(170, 'Suecia'),
(171, 'Suiza'),
(172, 'Surinam'),
(173, 'Tailandia'),
(174, 'Tanzania'),
(175, 'Tayikistán'),
(176, 'Timor Oriental'),
(177, 'Togo'),
(178, 'Tonga'),
(179, 'Trinidad y Tobago'),
(180, 'Túnez'),
(181, 'Turkmenistán'),
(182, 'Turquía'),
(183, 'Tuvalu'),
(184, 'Ucrania'),
(185, 'Uganda'),
(186, 'Uruguay'),
(187, 'Uzbekistán'),
(188, 'Vanuatu'),
(189, 'Venezuela'),
(190, 'Vietnam'),
(191, 'Yemen'),
(192, 'Yibuti'),
(193, 'Zambia'),
(194, 'Zimbawe');

-- --------------------------------------------------------

--
-- Table structure for table `permisos`
--

CREATE TABLE `permisos` (
  `id_permiso` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permisos`
--

INSERT INTO `permisos` (`id_permiso`, `descripcion`, `activo`, `id_log`) VALUES
(1, 'Administrador', 1, NULL),
(2, 'Director', 1, NULL),
(3, 'Director Produccion', 1, NULL),
(4, 'Direcctor Ventas', 1, NULL),
(5, 'Cliente', 1, NULL),
(6, 'Vendedor', 1, NULL),
(7, 'Produccion', 1, NULL),
(8, 'Despacho', 1, NULL),
(9, 'Finanzas', 1, NULL),
(10, 'Servicio al Cliente', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `tasa_IVA` int(11) NOT NULL,
  `precio_Unitario` double NOT NULL,
  `unidad` text NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `tasa_IVA`, `precio_Unitario`, `unidad`, `id_usuario`, `activo`) VALUES
(3, 'prueba3', 'prueba3', 2, 18, 'prueba3', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `tipo` varchar(16) DEFAULT NULL,
  `nombre_contacto` varchar(256) DEFAULT NULL,
  `email_contacto` varchar(128) DEFAULT NULL,
  `telefono_contacto` varchar(64) DEFAULT NULL,
  `ext_contacto` varchar(32) DEFAULT NULL,
  `id_domicilio` int(11) DEFAULT NULL,
  `id_usuario_alta` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `nombre`, `tipo`, `nombre_contacto`, `email_contacto`, `telefono_contacto`, `ext_contacto`, `id_domicilio`, `id_usuario_alta`, `fecha_alta`, `activo`, `id_log`) VALUES
(1, 'HS puertas de garaje', 'Nacional', 'Guillermo Ortiz', 'test@hs.com', '8112341000', '112', 1, 1, '2016-01-08 11:11:00', 1, 34),
(2, 'testing', 'Nacional', '0', NULL, NULL, NULL, 2, 1, '2016-01-08 11:11:53', 1, 24),
(3, 'Pruebas de desarrollo', 'Nacional', 'jñlksdfaj', 'esteesuncorreoelectonicomuylargoparaprobarlabd@testingdecorreo.com', '342332423', '232', 3, 1, '2016-01-08 11:22:26', 1, 32),
(4, 'Juan Javier Jaime Gabriel', 'Nacional', NULL, NULL, NULL, NULL, 4, 1, '2016-01-08 11:23:10', 1, 33);

-- --------------------------------------------------------

--
-- Table structure for table `puerta`
--

CREATE TABLE `puerta` (
  `id_puerta` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `m_ancho` double DEFAULT NULL,
  `m_alto` double DEFAULT NULL,
  `f_ancho` double DEFAULT NULL,
  `in_ancho` double DEFAULT NULL,
  `f_alto` double DEFAULT NULL,
  `in_alto` double DEFAULT NULL,
  `nventanas` int(11) NOT NULL DEFAULT '0',
  `precio` double(10,2) NOT NULL DEFAULT '0.00',
  `VA` double(10,2) NOT NULL DEFAULT '0.00',
  `VB` double(10,2) NOT NULL DEFAULT '0.00',
  `VC` double(10,2) NOT NULL DEFAULT '0.00',
  `VD` double(10,2) NOT NULL DEFAULT '0.00',
  `VE` double(10,2) NOT NULL DEFAULT '0.00',
  `VF` double(10,2) NOT NULL DEFAULT '0.00',
  `impuesto` double(10,2) NOT NULL DEFAULT '0.00',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta`
--

INSERT INTO `puerta` (`id_puerta`, `id_ptipo`, `m_ancho`, `m_alto`, `f_ancho`, `in_ancho`, `f_alto`, `in_alto`, `nventanas`, `precio`, `VA`, `VB`, `VC`, `VD`, `VE`, `VF`, `impuesto`, `activo`, `id_log`) VALUES
(1, 1, 2.44, 2.1, 8, 7, 0, 0, 2, 956.00, 956.00, 568.00, 473.00, 411.00, 374.00, 340.00, 0.00, 1, 0),
(2, 1, 2.44, 2.4, 8, 8, 0, 0, 0, 1048.00, 1048.00, 635.00, 529.00, 460.00, 418.00, 380.00, 0.00, 1, 0),
(3, 1, 2.44, 2.7, 8, 9, 0, 0, 0, 1105.00, 1105.00, 718.00, 598.00, 520.00, 473.00, 430.00, 0.00, 1, 0),
(4, 1, 3.05, 2.1, 10, 7, 0, 0, 0, 1094.00, 1094.00, 701.00, 584.00, 508.00, 462.00, 420.00, 0.00, 1, 0),
(5, 1, 3.05, 2.4, 10, 8, 0, 0, 0, 1161.00, 1161.00, 802.00, 668.00, 581.00, 528.00, 480.00, 0.00, 1, 0),
(6, 1, 3.05, 2.7, 10, 9, 0, 0, 0, 1217.00, 1217.00, 868.00, 724.00, 629.00, 572.00, 520.00, 0.00, 1, 0),
(7, 1, 3.67, 2.1, 12, 7, 0, 0, 0, 1350.00, 1350.00, 818.00, 682.00, 593.00, 539.00, 490.00, 0.00, 1, 0),
(8, 1, 3.67, 2.4, 12, 8, 0, 0, 0, 1450.00, 1450.00, 935.00, 779.00, 678.00, 616.00, 560.00, 0.00, 1, 0),
(9, 1, 3.67, 2.7, 12, 9, 0, 0, 0, 1500.00, 1500.00, 1069.00, 891.00, 774.00, 704.00, 640.00, 0.00, 1, 0),
(10, 2, 2.44, 2.74, 8, 9, 0, 0, 0, 1243.00, 1243.00, 777.00, 676.00, 588.00, 534.00, 486.00, 0.00, 1, 0),
(11, 2, 2.44, 3.05, 8, 10, 0, 0, 0, 1313.00, 1313.00, 820.00, 713.00, 620.00, 564.00, 513.00, 0.00, 1, 0),
(12, 2, 2.44, 3.35, 8, 11, 0, 0, 0, 1501.00, 1501.00, 938.00, 816.00, 710.00, 645.00, 586.00, 0.00, 1, 0),
(13, 2, 2.44, 3.66, 8, 12, 0, 0, 0, 1689.00, 1689.00, 1055.00, 918.00, 798.00, 725.00, 659.00, 0.00, 1, 0),
(14, 2, 2.44, 3.96, 8, 13, 0, 0, 0, 1854.00, 1854.00, 1159.00, 1008.00, 876.00, 797.00, 724.00, 0.00, 1, 0),
(15, 2, 2.44, 4.27, 8, 14, 0, 0, 0, 2020.00, 2020.00, 1263.00, 1098.00, 955.00, 868.00, 789.00, 0.00, 1, 0),
(16, 1, 2, 1, 3, 4, 5, 6, 0, 1000.00, 1000.00, 1001.00, 1002.00, 1003.00, 1004.00, 1005.00, 0.00, 0, 0),
(17, 1, 2, 1, 3, 4, 5, 6, 0, 1000.00, 1001.00, 1002.00, 1003.00, 1004.00, 1005.00, 1006.00, 0.00, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_color`
--

CREATE TABLE `puerta_color` (
  `id_pcolor` int(11) NOT NULL,
  `id_ptipo` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `porcentaje` float NOT NULL DEFAULT '0',
  `por_default` int(11) DEFAULT '0',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_color`
--

INSERT INTO `puerta_color` (`id_pcolor`, `id_ptipo`, `nombre_color`, `descripcion`, `porcentaje`, `por_default`, `activo`, `id_log`) VALUES
(1, 0, 'Blanco', 'Color blanco', 0, 1, 1, 0),
(3, 0, 'Café', 'Color café', 0, 0, 1, 0),
(4, 0, 'Blanco nieve', 'Blanco nieve', 0, 0, 1, 0),
(5, 0, 'Almendra', 'Almendra', 10, 0, 1, 0),
(7, 0, 'Gris', 'Gris claro', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_componente`
--

CREATE TABLE `puerta_componente` (
  `id` int(11) NOT NULL,
  `id_puerta` int(11) DEFAULT NULL,
  `id_componente` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_componente`
--

INSERT INTO `puerta_componente` (`id`, `id_puerta`, `id_componente`, `cantidad`, `activo`, `id_log`) VALUES
(4, 1, 2, 10, 1, 0),
(12, 2, 2, 8, 1, 0),
(13, 2, 3, 2, 1, 0),
(14, 3, 2, 6, 1, 0),
(15, 1, 4, 8, 1, 0),
(18, 3, 6, 8, 1, 0),
(19, 11, 2, 4, 1, 0),
(20, 11, 5, 2, 1, 0),
(24, 11, 6, 8, 1, 0),
(27, 1, 5, 2, 1, 0),
(28, 1, 6, 2, 1, 0),
(29, 1, 3, 12, 1, 0),
(30, 4, 2, 2, 1, 0),
(31, 4, 3, 4, 1, 0),
(32, 4, 4, 8, 1, 0),
(33, 2, 6, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_modelo`
--

CREATE TABLE `puerta_modelo` (
  `id_pmodelo` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_modelo` text,
  `descripcion` text,
  `porcentaje` float NOT NULL DEFAULT '0',
  `por_default` int(11) DEFAULT '0',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_modelo`
--

INSERT INTO `puerta_modelo` (`id_pmodelo`, `id_ptipo`, `nombre_modelo`, `descripcion`, `porcentaje`, `por_default`, `activo`, `id_log`) VALUES
(1, 1, 'Classica', 'Puerta por default', 0, 1, 1, 0),
(2, 1, 'Adonnia', NULL, 10, 0, 1, 0),
(3, 1, 'Elegantti', NULL, 10, 0, 1, 0),
(4, 1, 'Finesse', NULL, 0, 0, 1, 0),
(5, 1, 'Allure', NULL, 10, 0, 1, 0),
(6, 1, 'Finna', NULL, 10, 0, 1, 0),
(7, 1, 'Modderni', NULL, 10, 0, 1, 0),
(8, 1, 'Belissa', NULL, 10, 0, 1, 0),
(9, 1, 'Exquisitte', NULL, 10, 0, 1, 0),
(10, 2, 'HS1024', NULL, 10, 1, 1, 0),
(11, 2, 'HS1527', NULL, 10, 0, 1, 0),
(12, 2, 'HS2027', NULL, 10, 0, 1, 0),
(13, 2, 'HS2527', NULL, 10, 0, 1, 0),
(14, 2, 'Hs3027', NULL, 10, 0, 1, 0),
(15, 1, 'Prueba', 'Prueba', 0, 0, 0, 0),
(16, 1, 'Prueba', 'Prueba', 0, 0, 0, 0),
(17, 1, 'prueba 1000', 'prueba 1000', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_motor`
--

CREATE TABLE `puerta_motor` (
  `id_pmotor` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_motor` text,
  `descripcion` text,
  `precio` double DEFAULT '0',
  `impuesto` int(11) NOT NULL DEFAULT '0',
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_motor`
--

INSERT INTO `puerta_motor` (`id_pmotor`, `id_proveedor`, `nombre_motor`, `descripcion`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(5, 1, 'otro motor', 'otra descripción', 2800.83, 0, 2, 1, 0),
(6, 1, 'nombre', 'descripción', 250, 0, 2, 1, 0),
(8, 3, 'motor v', 'descripción', 1250.89, 0, 2, 1, 0),
(9, 4, 'nuevo motor', 'chido', 2500, 0, 2, 1, 0),
(10, 1, 'nombre', 'descripcion', 1000, 0, 2, 1, 0),
(11, 4, 'nombre', 'descripcion', 1000, 0, 2, 1, 0),
(12, 1, 'nombre', 'descripcion', 200, 0, 2, 1, 0),
(13, 4, 'nombre', 'descripcion', 100, 0, 2, 1, 0),
(14, 1, 'otro', 'descripcion', 1, 0, 2, 1, 0),
(15, 2, 'testing', 'testing', 1, 0, 2, 1, 0),
(16, 2, 'testing', 'testing', 1, 0, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_movimiento`
--

CREATE TABLE `puerta_movimiento` (
  `id_pmovimiento` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `nombre_movimiento` text,
  `porcentaje` float NOT NULL DEFAULT '0',
  `por_default` int(1) DEFAULT '0',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_movimiento`
--

INSERT INTO `puerta_movimiento` (`id_pmovimiento`, `id_ptipo`, `nombre_movimiento`, `porcentaje`, `por_default`, `activo`, `id_log`) VALUES
(1, 1, 'Estandar', 0, 1, 1, 0),
(2, 2, 'Estandar', 0, 1, 1, 0),
(3, 2, 'Inclinada', 10, 0, 1, 0),
(4, 2, 'Inclinada suave', 10, 0, 1, 0),
(5, 2, 'Vertical', 10, 0, 1, 0),
(6, 2, 'Riel inclinado', 10, 0, 1, 0),
(7, 2, 'Techo bajo', 10, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_sello`
--

CREATE TABLE `puerta_sello` (
  `id_psello` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_sello` text,
  `descripcion` text NOT NULL,
  `precio` double NOT NULL DEFAULT '0',
  `tipo` int(11) NOT NULL DEFAULT '0',
  `impuesto` double NOT NULL DEFAULT '0',
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_sello`
--

INSERT INTO `puerta_sello` (`id_psello`, `id_proveedor`, `nombre_sello`, `descripcion`, `precio`, `tipo`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, 1, 'Sin Sello', 'descripción', 850, 0, 0, 1, 1, 0),
(2, 1, 'Perimetral', 'descripción', 10.5, 0, 0, 2, 1, 0),
(7, 1, 'Rigido', 'Rigido', 1200, 0, 0, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_textura`
--

CREATE TABLE `puerta_textura` (
  `id_ptextura` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_textura` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `impuesto` double DEFAULT NULL,
  `default` int(11) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_textura`
--

INSERT INTO `puerta_textura` (`id_ptextura`, `id_proveedor`, `nombre_textura`, `descripcion`, `precio`, `impuesto`, `default`, `activo`, `id_log`) VALUES
(1, NULL, 'Madera', 'Textura por defecto', 0, 0, 1, 1, 0),
(2, NULL, 'Lisa', 'Textura lisa', 0, 0, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_tipo`
--

CREATE TABLE `puerta_tipo` (
  `id_ptipo` int(11) NOT NULL,
  `nombre_tipo` text,
  `default` tinyint(4) DEFAULT '2',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puerta_tipo`
--

INSERT INTO `puerta_tipo` (`id_ptipo`, `nombre_tipo`, `default`, `activo`, `id_log`) VALUES
(1, 'Residencial', 1, 1, 0),
(2, 'Industrial', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `puerta_ventana`
--

CREATE TABLE `puerta_ventana` (
  `id_puerta` int(11) DEFAULT NULL,
  `id_ventana` int(11) DEFAULT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `terminos_venta`
--

CREATE TABLE `terminos_venta` (
  `id_termino_venta` int(11) NOT NULL,
  `termino_venta` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `terminos_venta`
--

INSERT INTO `terminos_venta` (`id_termino_venta`, `termino_venta`, `descripcion`, `id_usuario`, `creado`) VALUES
(1, 'termino 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2016-11-08 02:07:38'),
(8, 'hola', 'como estas\r\nbien y tu\r\ntambien\r\nja ja\r\nok', 1, '2016-11-09 01:37:07'),
(4, 'otro termino', 'otra descripción tambien', 1, '2016-11-08 05:11:15'),
(10, 'termino  II', 'Descripción II again', 1, '2016-11-10 02:06:45'),
(6, 'termino venta', '- Todos los modelos con textura de madera en BN.\r\n- Para ver las especificaciones técnicas de cada puerta, consultar http://hspuertasdegaraje.com\r\n- No incluye envío.\r\n- Puertas completas, paneles, herrajes calibre 14 y resorte acorde al tamaño de la puerta.\r\n- Modelo con cuadros, con cuadro de ajuste.\r\n- Puertas fabricadas a la medida con tiempo de envió a 10 días hábiles.\r\n- Precios en MXN\r\n- IVA incluido.\r\n- Elegantti y exquisite comienzan en precio VE.\r\n- Pago de contado.\r\n- Validez de 15 días hábiles.\r\n- Una vez realizado el pedido, no hay cambios o devoluciones.', 1, '2016-11-09 00:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `pwd` varchar(64) DEFAULT NULL,
  `id_divisa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `nombre_usuario` varchar(256) DEFAULT NULL,
  `email_usuario` varchar(128) DEFAULT NULL,
  `telefono_usuario` varchar(32) DEFAULT NULL,
  `ext_usuario` varchar(16) DEFAULT NULL,
  `num_empleado` varchar(32) DEFAULT NULL,
  `permiso_usuario` int(11) DEFAULT NULL,
  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(1) DEFAULT '1',
  `accesos` int(11) DEFAULT '0',
  `id_log` int(11) DEFAULT '0',
  `ultimo_acceso` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `username`, `pwd`, `id_divisa`, `id_cliente`, `nombre_usuario`, `email_usuario`, `telefono_usuario`, `ext_usuario`, `num_empleado`, `permiso_usuario`, `fecha_alta`, `activo`, `accesos`, `id_log`, `ultimo_acceso`) VALUES
(1, 'admintk', 'Teknik', 2, 1, 'Teknik', 'webmaster@teknik.mx', '(81) 1931 4009', NULL, NULL, 1, '2016-01-06 18:40:29', 1, 174, 22, '2016-01-12 09:08:31'),
(2, 'test', 'test', 1, NULL, 'Test', 'test@test.com', NULL, NULL, NULL, 10, '2016-01-07 15:50:47', 1, 30, 5, '2016-01-07 15:51:38'),
(3, 'perez', 'perez', 1, NULL, 'Jose', 'pp@perez.com', '3233', '32', '3232', 5, '2016-01-07 15:51:33', 1, 6, 17, '2016-01-07 18:57:18'),
(4, 'lopez', 'juan', 2, NULL, 'Juan Lopez', 'juan@lopez.co', '345', '22', '222', 5, '2016-01-07 15:54:23', 1, 1, 13, '2016-01-07 16:17:27'),
(5, 'asdf', 'asdf', 1, NULL, 'asdfasd', 'asdf@askdf.com', NULL, NULL, '21221', 5, '2016-01-07 16:00:13', 1, 0, 21, '2016-01-07 19:39:33'),
(6, 'asdf', 'asfd', 1, NULL, 'asdf', 'test@prueba.com', '222221111', NULL, '2222', 8, '2016-01-07 19:36:05', 1, 0, 20, '2016-01-07 19:37:03'),
(7, 'ventas', 'ventas', 1, NULL, 'Ventas', 'ventas@test.com', NULL, NULL, '23425235', 6, '2016-02-27 16:54:01', 1, 2, 7, NULL),
(8, 'miguel angel', 'miguel2016', 2, NULL, 'miguel', 'example@example.com', '11-11111111', NULL, '14__', 10, '2016-10-28 01:49:05', 1, 0, 8, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ventana`
--

CREATE TABLE `ventana` (
  `id_ventana` int(11) NOT NULL,
  `id_ptipo` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `nombre_ventana` text,
  `descripcion` text,
  `precio` double DEFAULT NULL,
  `VA` double(10,2) NOT NULL DEFAULT '0.00',
  `VB` double(10,2) NOT NULL DEFAULT '0.00',
  `VC` double(10,2) NOT NULL DEFAULT '0.00',
  `VD` double(10,2) NOT NULL DEFAULT '0.00',
  `VE` double(10,2) NOT NULL DEFAULT '0.00',
  `VF` double(10,2) NOT NULL DEFAULT '0.00',
  `impuesto` double DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ventana`
--

INSERT INTO `ventana` (`id_ventana`, `id_ptipo`, `id_proveedor`, `nombre_ventana`, `descripcion`, `precio`, `VA`, `VB`, `VC`, `VD`, `VE`, `VF`, `impuesto`, `activo`, `id_log`) VALUES
(2, 1, NULL, 'Cascade', 'Cascade Window', 99.9, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00, 0, 1, 0),
(3, 2, NULL, 'Big Daddy', 'Big Daddy Window', 1200, 2.00, 4.00, 6.00, 8.00, 10.00, 12.00, 0, 1, 0),
(4, 1, NULL, 'False Window', 'False Window', 1.2, 3.00, 6.00, 9.00, 12.00, 15.00, 18.00, 0, 1, 0),
(6, 1, NULL, 'x', 'x', 2, 2.00, 3.00, 4.00, 5.00, 6.00, 7.00, 0, 0, 0),
(7, 2, NULL, 'x25', 'descripción x25', 5, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ventana_color`
--

CREATE TABLE `ventana_color` (
  `id_vcolor` int(11) NOT NULL,
  `id_ptipo` int(11) NOT NULL,
  `nombre_color` text,
  `descripcion` text,
  `precio` double NOT NULL DEFAULT '0',
  `activo` tinyint(1) DEFAULT '1',
  `id_log` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ventana_color`
--

INSERT INTO `ventana_color` (`id_vcolor`, `id_ptipo`, `nombre_color`, `descripcion`, `precio`, `activo`, `id_log`) VALUES
(1, 0, 'Blanco', 'Blanco', 0, 1, 0),
(2, 0, 'Almendra', 'Almendra', 0, 1, 0),
(3, 0, 'Azul', 'azul cielo', 0, 0, 0),
(4, 0, 'Cafe', 'Cafe', 0, 1, 0),
(5, 0, 'verde', 'verde linterna', 0, 0, 0),
(6, 0, 'azul', 'azul cielo', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `componentes`
--
ALTER TABLE `componentes`
  ADD PRIMARY KEY (`id_componente`);

--
-- Indexes for table `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
  ADD PRIMARY KEY (`id_grupo_componente`);

--
-- Indexes for table `conceptos`
--
ALTER TABLE `conceptos`
  ADD PRIMARY KEY (`id_concepto`);

--
-- Indexes for table `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- Indexes for table `depositos`
--
ALTER TABLE `depositos`
  ADD PRIMARY KEY (`id_deposito`);

--
-- Indexes for table `divisas`
--
ALTER TABLE `divisas`
  ADD PRIMARY KEY (`id_divisa`);

--
-- Indexes for table `domicilios`
--
ALTER TABLE `domicilios`
  ADD PRIMARY KEY (`id_domicilio`);

--
-- Indexes for table `domicilios_fiscales`
--
ALTER TABLE `domicilios_fiscales`
  ADD PRIMARY KEY (`id_domicilio_fiscal`);

--
-- Indexes for table `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id_impuesto`);

--
-- Indexes for table `incidencias`
--
ALTER TABLE `incidencias`
  ADD PRIMARY KEY (`id_incidencia`);

--
-- Indexes for table `kit`
--
ALTER TABLE `kit`
  ADD PRIMARY KEY (`id_kit`);

--
-- Indexes for table `kit_componente`
--
ALTER TABLE `kit_componente`
  ADD PRIMARY KEY (`id_kit_componente`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `multiplicadores`
--
ALTER TABLE `multiplicadores`
  ADD PRIMARY KEY (`id_multiplicador`);

--
-- Indexes for table `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indexes for table `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- Indexes for table `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indexes for table `puerta`
--
ALTER TABLE `puerta`
  ADD PRIMARY KEY (`id_puerta`);

--
-- Indexes for table `puerta_color`
--
ALTER TABLE `puerta_color`
  ADD PRIMARY KEY (`id_pcolor`);

--
-- Indexes for table `puerta_componente`
--
ALTER TABLE `puerta_componente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  ADD PRIMARY KEY (`id_pmodelo`);

--
-- Indexes for table `puerta_motor`
--
ALTER TABLE `puerta_motor`
  ADD PRIMARY KEY (`id_pmotor`);

--
-- Indexes for table `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
  ADD PRIMARY KEY (`id_pmovimiento`);

--
-- Indexes for table `puerta_sello`
--
ALTER TABLE `puerta_sello`
  ADD PRIMARY KEY (`id_psello`);

--
-- Indexes for table `puerta_textura`
--
ALTER TABLE `puerta_textura`
  ADD PRIMARY KEY (`id_ptextura`);

--
-- Indexes for table `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  ADD PRIMARY KEY (`id_ptipo`);

--
-- Indexes for table `terminos_venta`
--
ALTER TABLE `terminos_venta`
  ADD PRIMARY KEY (`id_termino_venta`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indexes for table `ventana`
--
ALTER TABLE `ventana`
  ADD PRIMARY KEY (`id_ventana`);

--
-- Indexes for table `ventana_color`
--
ALTER TABLE `ventana_color`
  ADD PRIMARY KEY (`id_vcolor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `componentes`
--
ALTER TABLE `componentes`
  MODIFY `id_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `componentes_grupos`
--
ALTER TABLE `componentes_grupos`
  MODIFY `id_grupo_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `conceptos`
--
ALTER TABLE `conceptos`
  MODIFY `id_concepto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `cotizaciones`
--
ALTER TABLE `cotizaciones`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `depositos`
--
ALTER TABLE `depositos`
  MODIFY `id_deposito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `divisas`
--
ALTER TABLE `divisas`
  MODIFY `id_divisa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `domicilios`
--
ALTER TABLE `domicilios`
  MODIFY `id_domicilio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `domicilios_fiscales`
--
ALTER TABLE `domicilios_fiscales`
  MODIFY `id_domicilio_fiscal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id_impuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `incidencias`
--
ALTER TABLE `incidencias`
  MODIFY `id_incidencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kit`
--
ALTER TABLE `kit`
  MODIFY `id_kit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kit_componente`
--
ALTER TABLE `kit_componente`
  MODIFY `id_kit_componente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `multiplicadores`
--
ALTER TABLE `multiplicadores`
  MODIFY `id_multiplicador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT for table `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `puerta`
--
ALTER TABLE `puerta`
  MODIFY `id_puerta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `puerta_color`
--
ALTER TABLE `puerta_color`
  MODIFY `id_pcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `puerta_componente`
--
ALTER TABLE `puerta_componente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `puerta_modelo`
--
ALTER TABLE `puerta_modelo`
  MODIFY `id_pmodelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `puerta_motor`
--
ALTER TABLE `puerta_motor`
  MODIFY `id_pmotor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `puerta_movimiento`
--
ALTER TABLE `puerta_movimiento`
  MODIFY `id_pmovimiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `puerta_sello`
--
ALTER TABLE `puerta_sello`
  MODIFY `id_psello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `puerta_textura`
--
ALTER TABLE `puerta_textura`
  MODIFY `id_ptextura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `puerta_tipo`
--
ALTER TABLE `puerta_tipo`
  MODIFY `id_ptipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `terminos_venta`
--
ALTER TABLE `terminos_venta`
  MODIFY `id_termino_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ventana`
--
ALTER TABLE `ventana`
  MODIFY `id_ventana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ventana_color`
--
ALTER TABLE `ventana_color`
  MODIFY `id_vcolor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
