<?php

	set_time_limit(0);
	ini_set('memory_limit', '-1');

	session_start();

	require_once('config.php');

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
		if (($_SESSION['id_usuario'] == NULL)) {
    		$response = array(
    			'status' => 'unauthorized'
    		);

    		echo json_encode($response);
    		exit();
		}

		$id_usuario = $_SESSION['id_usuario'];

		// Remplasamos caracteres especiales para SQL y SAT, (/\x27/ = single quote, '/\x22/" = double quotes)
		$pattern = array('/\t/','/\x27/','/\x22/', '/\|/');
		$replacement = array(' ', chr(ord("`")) , chr(ord("`")), chr(33));

		// obtengo las variables del post
		$id_cotizacion = filter_var($_POST['id_cotizacion'], FILTER_SANITIZE_NUMBER_INT);

		// obtengo los datos de la cotización
		$cotizacion = $pdo->query("SELECT * FROM cotizaciones WHERE id_cotizacion = $id_cotizacion")->fetch(PDO::FETCH_ASSOC);

		$cotizacion_id_cotizacion = $cotizacion['id_cotizacion'];
		$cotizacion_id_cliente = $cotizacion['id_cliente'];
		$cotizacion_id_impuesto = $cotizacion['id_impuesto'];
		$cotizacion_subtotal = $cotizacion['subtotal'];
		$cotizacion_impuestos = $cotizacion['impuestos'];
		$cotizacion_total = $cotizacion['total'];

		// obtengo el impuesto
		$impuesto = $pdo->query("SELECT * FROM impuestos WHERE id_impuesto = $cotizacion_id_impuesto")->fetch(PDO::FETCH_ASSOC);

		$impuesto_porcentaje = $impuesto['porcentaje'];

		// obtengo los datos del emisor
		$domicilio_fiscal_rfc = $GLOBAL_facturando_rfc;

		// obtengo los datos del cliente
		$cliente = $pdo->query("SELECT * FROM clientes WHERE id_cliente = $cotizacion_id_cliente")->fetch(PDO::FETCH_ASSOC);

		$cliente_razon_social = trim(preg_replace('/\s\s+/', ' ', preg_replace($pattern, $replacement, $cliente['razon_social'])));

		$cliente_rfc = strtoupper(trim(preg_replace('/\s\s+/', ' ', preg_replace($pattern, $replacement, $cliente['rfc']))));
		
		$cliente_email_contacto =trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $cliente['email_contacto'])));

		$cliente_telefono_contacto = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $cliente['telefono_contacto'])));

		$cliente_id_domicilio = $cliente['id_domicilio'];

		// obtengo los datos del domicilio del cliente
		$domicilio_cliente = $pdo->query("SELECT * FROM domicilios WHERE id_domicilio = $cliente_id_domicilio")->fetch(PDO::FETCH_ASSOC);

		$domicilio_cliente_calle = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['calle'])));

		$domicilio_cliente_no_exterior = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['no_exterior'])));

		$domicilio_cliente_no_interior = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['no_interior'])));

		$domicilio_cliente_colonia = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['colonia'])));

		$domicilio_cliente_localidad = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['localidad'])));

		$domicilio_cliente_referencia = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['referencia'])));

		$domicilio_cliente_municipio =trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['municipio'])));

		$domicilio_cliente_estado = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['estado'])));

		// obtengo el país
		$pais = $pdo->query("SELECT * FROM pais WHERE id_pais = {$domicilio_cliente['id_pais']}")->fetch(PDO::FETCH_ASSOC);

		$domicilio_cliente_pais = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $pais['nombre'])));

		$domicilio_cliente_codigo_postal = trim(preg_replace('/\s\s+/', ' ',preg_replace($pattern, $replacement, $domicilio_cliente['codigo_postal'])));

		// generar el archivo xml y almacenarlo en disco

		// obtengo los conceptos de la cotización
		$conceptos = $pdo->query("SELECT * FROM conceptos WHERE id_cotizacion = $cotizacion_id_cotizacion")->fetchAll(PDO::FETCH_ASSOC);

		// establecer la fecha
		$time_offset = ((60 * 60 * 3) + (60 * 20)); //3 horas 20 minutos
		$hora = time() - $time_offset;
		$fecha = date('Y-m-d\TH:i:s', $hora);

		// hardcoded variables
		$version = 3.2;
		$moneda = 'MXN';
		$tipocambio = 1;
		$metododepago = '99';
		$tipodecomprobante = 'ingreso';
		$formadepago = 'Pago en una sola exhibicion';
		$numctapago = 'No identificado';

		$regimenfiscal = 'Regimen General De Ley Personas Morales';

		// documento xml
		$documento_xml = "";

		$documento_xml .= "<?xml version='1.0' encoding='utf-8'?>";
		$documento_xml .= "<cfdi:Comprobante xmlns:cfdi='http://www.sat.gob.mx/cfd/3' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd' version='$version' fecha='$fecha' serie='$cotizacion_id_cotizacion' Moneda='$moneda' TipoCambio='$tipocambio' formaDePago='$formadepago' subTotal='$cotizacion_subtotal' total='$cotizacion_total' metodoDePago='$metododepago' NumCtaPago='$numctapago' tipoDeComprobante='$tipodecomprobante'>";
			$documento_xml .= "<cfdi:Emisor>";
				$documento_xml .= "<cfdi:RegimenFiscal Regimen='$regimenfiscal'/>";
			$documento_xml .= "</cfdi:Emisor>";
			$documento_xml .= "<cfdi:Receptor rfc='$cliente_rfc' nombre='$cliente_razon_social'>";
				$documento_xml .= "<cfdi:Domicilio calle='$domicilio_cliente_calle' noExterior='$domicilio_cliente_no_exterior' noInterior='$domicilio_cliente_no_interior' colonia='$domicilio_cliente_colonia' localidad='$domicilio_cliente_localidad' referencia='$domicilio_cliente_referencia' municipio='$domicilio_cliente_municipio' estado='$domicilio_cliente_estado' pais='$domicilio_cliente_pais' codigoPostal='$domicilio_cliente_codigo_postal' email='$cliente_email_contacto'/>";
			$documento_xml .= "</cfdi:Receptor>";
			$documento_xml .= "<cfdi:Extras no_proveedor='0' orden_compra='0' no_pedido='0' />";
			$documento_xml .= "<cfdi:Conceptos>";

				foreach ($conceptos as $concepto) {
					if (isset($concepto['id_puerta']) && $concepto['id_puerta'] > 0) {
						$sql = "SELECT * FROM puerta_tipo WHERE activo = 1 AND id_ptipo = ". $concepto['id_ptipo'];
                        $ptipo = mysql_fetch_array(mysql_query($sql));
                        
                        $sql = "SELECT * FROM puerta WHERE activo = 1 AND id_puerta = ". $concepto['id_puerta'];
                        $puerta = mysql_fetch_array(mysql_query($sql));
                        
                        $sql = "SELECT * FROM puerta_modelo WHERE activo = 1 AND id_pmodelo = ". $concepto['id_pmodelo'];
                        $pmodelo = mysql_fetch_array(mysql_query($sql));
                        
                        $sql = "SELECT * FROM puerta_color WHERE activo = 1 AND id_pcolor = ". $concepto['id_pcolor'];
                        $pcolor = mysql_fetch_array(mysql_query($sql));
                        
                        $sql = "SELECT * FROM puerta_movimiento WHERE activo = 1 AND id_pmovimiento = ". $concepto['id_pmovimiento'];
                        $pmovimiento = mysql_fetch_array(mysql_query($sql));
                        
                        $sql = "SELECT * FROM puerta_textura WHERE activo = 1 AND id_ptextura = ". $concepto['id_ptextura'];
                        $ptextura = mysql_fetch_array(mysql_query($sql));
                        
                        $sql = "SELECT * FROM puerta_sello WHERE activo = 1 AND id_psello = ". $concepto['id_psello'];
                        $psello = mysql_fetch_array(mysql_query($sql));
                        
                        $sql = "SELECT * FROM puerta_motor WHERE activo = 1 AND id_pmotor = ". $concepto['id_pmotor'];
                        $pmotor = mysql_fetch_array(mysql_query($sql));

                        // variables para el concepto
                        $noidentificacion = $concepto['id_concepto'];
                        $cantidad = $concepto['cantidad'];
                        $unidad = 'pz';
                        $descripcion = "Puerta | Tipo: {$ptipo['nombre_tipo']} | Medidas: {$puerta['m_alto']} x {$puerta['m_ancho']} | Modelo: {$pmodelo['nombre_modelo']} | Color: {$pcolor['nombre_color']} | Movimiento: {$pmovimiento['nombre_movimiento']} | Textura: {$ptextura['nombre_textura']} | Sello: {$psello['nombre_sello']} | Motor: {$pmotor['nombre_motor']}";
                        $valor_unitario = $concepto["precio_unitario"];
                        $importe = $concepto['importe'];

                        // añadir el concepto al documento xml
                        $documento_xml .= "<cfdi:Concepto noIdentificacion='$noidentificacion' cantidad='$cantidad' unidad='$unidad' descripcion='$descripcion' valorUnitario='$valor_unitario' importe='$importe' />";

					} elseif (isset($concepto['id_componente']) && $concepto['id_componente'] > 0) {
						$sql = "SELECT * FROM componentes_grupos WHERE activo = 1 AND id_grupo_componente = ". $concepto['id_grupo_componente'];
						$gcomponente = mysql_fetch_array(mysql_query($sql));

						$sql = "SELECT * FROM componentes WHERE activo = 1 AND id_componente = ". $concepto['id_componente'];
						$componente = mysql_fetch_array(mysql_query($sql));

						// variables para el concepto
                        $noidentificacion = $concepto['id_concepto'];
                        $cantidad = $concepto['cantidad'];
                        $unidad = 'pz';
                        $descripcion = "Componente | Grupo: {$gcomponente['grupo_componente']} | Componente: {$componente['nombre_componente']}";
                        $valor_unitario = $concepto['precio_unitario'];
                        $importe = $concepto['importe'];

                        // añadir el concepto al documento xml
                        $documento_xml .= "<cfdi:Concepto noIdentificacion='$noidentificacion' cantidad='$cantidad' unidad='$unidad' descripcion='$descripcion' valorUnitario='$valor_unitario' importe='$importe' />";
					} elseif (isset($concepto['id_kit']) && $concepto['id_kit'] > 0) {
						$sql = "SELECT * FROM kit WHERE activo = 1 AND id_kit = ". $concepto['id_kit'];
						$kkit = mysql_fetch_array(mysql_query($sql));

						// variables para el concepto
                        $noidentificacion = $concepto['id_concepto'];
                        $cantidad = $concepto['cantidad'];
                        $unidad = 'pz';
                        $descripcion = "Kit: {$kkit['nombre_kit']}";
                        $valor_unitario = $concepto["precio_unitario"];;
                        $importe = $concepto['importe'];

                         // añadir el concepto al documento xml
                        $documento_xml .= "<cfdi:Concepto noIdentificacion='$noidentificacion' cantidad='$cantidad' unidad='$unidad' descripcion='$descripcion' valorUnitario='$valor_unitario' importe='$importe' />";
					}
				}

			$documento_xml .= "</cfdi:Conceptos>";
				$documento_xml .= "<cfdi:Impuestos totalImpuestosRetenidos='0' totalImpuestosTrasladados='0'>";
					// $documento_xml .= "<cfdi:Retenciones>";
						// $documento_xml .= "<cfdi:Retencion importe='0.00' impuesto='ISR' tasa='0.00' />";
					// $documento_xml .= "</cfdi:Retenciones>";
					$documento_xml .= "<cfdi:Traslados>";
						$documento_xml .= "<cfdi:Traslado importe='$cotizacion_impuestos' impuesto='IVA' tasa='16.00' />";
					$documento_xml .= "</cfdi:Traslados>";
				$documento_xml .= "</cfdi:Impuestos>";
		$documento_xml .= "</cfdi:Comprobante>";

		$documento_xml = trim($documento_xml);
		
		// echo $documento_xml;
		// exit;

		// crear el documento dom para el xml
		$cfdi = new DOMDocument();

		$cfdi->loadXML($documento_xml);
		
		$xml = $cfdi->saveXML();
		$xml = trim($xml);

		/* Se guarda el archivo en disco para qué en caso de error podamos revisarlo */
		$year = date("Y");
		$month = date("m");
		$day = date("d");

		$folder = 'webservice';

		umask(0);

		if(!(is_dir("facturas/"))) mkdir("facturas/", 0777);
		if(!(is_dir("facturas/" . $folder))) mkdir("facturas/" . $folder . "/", 0777);
		if(!(is_dir("facturas/" . $folder . "/" . $year))) mkdir("facturas/" . $folder . "/" . $year . "/", 0777);
		if(!(is_dir("facturas/" . $folder . "/" . $year . "/" . $month))) mkdir("facturas/" . $folder . "/" . $year . "/" . $month . "/", 0777);
		if(!(is_dir("facturas/" . $folder . "/" . $year . "/" . $month . "/" . $day))) mkdir("facturas/" . $folder . "/" . $year . "/" . $month . "/" . $day . "/", 0777);

		$directorio = "facturas/" . $folder . "/" . $year . "/" . $month . "/" . $day;

		$archivo = $domicilio_fiscal_rfc . '_' . $cliente_rfc . '_' . date("His");
		$nombre = $archivo . '.xml';

		$path = $directorio . '/' . $nombre;
		
		$valid = file_put_contents($path, $xml);

		// si el archivo se guardo con exito entonces procederemos a enviar el xml al servicio web
		if ($valid) {
			$client = new SoapCLient($GLOBAL_facturando_ws, array('wsdl_cache' => 0, 'trace' => 1));
			
			// echo '<pre>' . print_r($client->__getFunctions(), 1) . '</pre>';
			// exit;

			$xml = file_get_contents($path);
			$xml = base64_encode($xml);

			// echo $xml;
			// exit;
			
			try {
				$result = $client->GeneraCFDI($xml, $GLOBAL_facturando_id, $GLOBAL_facturando_rfc);
			} catch (Exception $e) {
				$response = array(
    				'status' => 'error',
    				'message' => $e->getMessage()
    			);

    			echo json_encode($response);
    			exit();
			}

			// print_r($result);
			// exit;

			// check if the response is an xml file
			if (simplexml_load_string($result)) {
				$cdfi = new DOMDocument();
				$cfdi->loadXML($result);

				$comprobante_fiscal = $cfdi->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/3', 'Comprobante')->item(0);
				$serie = $comprobante_fiscal->getAttribute('serie');
				$folio = $comprobante_fiscal->getAttribute('folio');

				$timbre_fiscal = $cfdi->getElementsByTagNameNS('http://www.sat.gob.mx/TimbreFiscalDigital', 'TimbreFiscalDigital')->item(0);
				$uuid = $timbre_fiscal->getAttribute('UUID');

				$xml = $cfdi->saveXML();
				$xml = trim($xml);

				if ($uuid != "") {
					$stmt = $pdo->prepare("UPDATE cotizaciones SET factura_serie = :factura_serie, factura_folio = :factura_folio, factura_rfc_emisor = :factura_rfc_emisor, factura_rfc_receptor = :factura_rfc_receptor, factura_uuid = :factura_uuid WHERE id_cotizacion = :id_cotizacion");
					
					$stmt->bindParam(':factura_serie', $serie);
					$stmt->bindParam(':factura_folio', $folio);
					$stmt->bindParam(':factura_rfc_emisor', $domicilio_fiscal_rfc);
					$stmt->bindParam(':factura_rfc_receptor', $cliente_rfc);
					$stmt->bindParam(':factura_uuid', $uuid);
					$stmt->bindParam(':id_cotizacion', $cotizacion_id_cotizacion);
					
					$stmt->execute();
				}

				// save the xml in hd
				$folder = 'facturas';

				// check if the directories exists
				umask(0);

				if (!is_dir($folder . "/")) { mkdir($folder . "/", 0777); }
				if (!is_dir($folder . "/" .  $domicilio_fiscal_rfc . "/")) { mkdir($folder . "/" .  $domicilio_fiscal_rfc . "/", 0777); }
				if (!is_dir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/")) { mkdir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/", 0777); }
				if (!is_dir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/xml/")) { mkdir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/xml/", 0777); }
				if (!is_dir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/xml/" . $year . "/")) { mkdir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/xml/" . $year . "/", 0777); }
				if (!is_dir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/xml/" . $year . "/" . $month . "/")) { mkdir($folder . "/" .  $domicilio_fiscal_rfc . "/emitido/xml/" . $year . "/" . $month . "/", 0777); }

				// set the path for the file
				$path = $folder . "/" . $domicilio_fiscal_rfc . "/emitido/xml/" . $year . "/" . $month . "/";
				$filename = $domicilio_fiscal_rfc . "_" . $serie . $folio . "_" . $cliente_rfc . ".xml";

				$full_path = $path . $filename;

				// check if file exist before save
				if (is_file($full_path)) {
					// delete file
					unlink($full_path);
				}

				$valid = file_put_contents($full_path, $xml);

				if ($valid && is_file($full_path)) {
					$stmt = $pdo->prepare("UPDATE cotizaciones SET factura_xml_url = :factura_xml_url WHERE id_cotizacion = :id_cotizacion");
					
					$stmt->bindParam(':factura_xml_url', $full_path);
					$stmt->bindParam(':id_cotizacion', $cotizacion_id_cotizacion);
					
					$stmt->execute();

					$response = array(
    					'status' => 'success',
    					'message' => 'El comprobante fiscal se ha creado con exito'
    				);
				}
			} else {
				$response = array(
    				'status' => 'error',
    				'message' => 'No se ha podido recuperar el XML timbrado'
    			);
			}
		} else {
			$response = array(
    			'status' => 'error',
    			'message' => 'No se ha podido crear el XML para el servicio web'
    		);
		}

		echo json_encode($response);
    	exit();
	}

	header('location: inicio.php');
	exit();
	
?>