<?php
	
	set_time_limit(0);
	ini_set('memory_limit', '-1');

	session_start();

	require_once('config.php');

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
		if (($_SESSION['id_usuario'] == NULL)) {
    		$response = array(
    			'status' => 'unauthorized'
    		);

    		echo json_encode($response);
    		exit();
		}

		$id_usuario = $_SESSION['id_usuario'];

		// get post vars
		$id_cotizacion = filter_var($_POST['id_cotizacion'], FILTER_SANITIZE_NUMBER_INT);

		// get data cotizacion
		$cotizacion = $pdo->query("SELECT * FROM cotizaciones WHERE id_cotizacion = $id_cotizacion")->fetch(PDO::FETCH_ASSOC);

		$cotizacion_id_cotizacion = $cotizacion['id_cotizacion'];
		$cotizacion_id_cliente = $cotizacion['id_cliente'];
		$cotizacion_factura_serie = $cotizacion['factura_serie'];
		$cotizacion_factura_folio = $cotizacion['factura_folio'];
		$cotizacion_factura_rfc_emisor = $cotizacion['factura_rfc_emisor'];
		$cotizacion_factura_rfc_receptor = $cotizacion['factura_rfc_receptor'];
		$cotizacion_factura_uuid = $cotizacion['factura_uuid'];

		// set the date
		$time_offset = ((60 * 60 * 3) + (60 * 20)); //3 horas 20 minutos
		$hora = time() - $time_offset;
		$fecha = date('Y-m-d\TH:i:s', $hora);

		// divide the date into vars
		$year = date("Y");
		$month = date("m");
		$day = date("d");

		// call web service

		// create soap client
		$client = new SoapCLient($GLOBAL_facturando_ws, array('wsdl_cache' => 0, 'trace' => 1));
			
		// echo '<pre>' . print_r($client->__getFunctions(), 1) . '</pre>';
		// exit;

		$factura_pdf_url = $client->getPDF($cotizacion_factura_uuid, $GLOBAL_facturando_id, $GLOBAL_facturando_rfc);

		// $factura_pdf_url;
		// exit();

		// check if the result is not empty and if si valid url
		if ($factura_pdf_url != "" && filter_var($factura_pdf_url, FILTER_VALIDATE_URL)) {
			$ch = curl_init();

			// set URL and other appropriate options  
			curl_setopt($ch, CURLOPT_URL, $factura_pdf_url);  
			curl_setopt($ch, CURLOPT_HEADER, 0);  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  

			// grab URL and pass it to the browser
			$pdf = curl_exec($ch);

			// close curl resource, and free up system resources  
			curl_close($ch);

			// save the xml in hd
			$folder = 'facturas';

			// check if the directories exists
			umask(0);

			if (!is_dir($folder . "/")) { mkdir($folder . "/", 0777); }
			if (!is_dir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/")) { mkdir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/", 0777); }
			if (!is_dir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/")) { mkdir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/", 0777); }
			if (!is_dir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/pdf/")) { mkdir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/pdf/", 0777); }
			if (!is_dir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/pdf/" . $year . "/")) { mkdir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/pdf/" . $year . "/", 0777); }
			if (!is_dir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/pdf/" . $year . "/" . $month . "/")) { mkdir($folder . "/" .  $cotizacion_factura_rfc_emisor . "/emitido/pdf/" . $year . "/" . $month . "/", 0777); }

			// set the path for the file
			$path = $folder . "/" . $cotizacion_factura_rfc_emisor . "/emitido/pdf/" . $year . "/" . $month . "/";
			$filename = $cotizacion_factura_rfc_emisor . "_" . $cotizacion_factura_serie . $cotizacion_factura_folio . "_" . $cotizacion_factura_rfc_receptor . ".pdf";

			$full_path = $path . $filename;

			// check if file exist before save
			if (is_file($full_path)) {
				// delete file
				unlink($full_path);
			}

			$valid = file_put_contents($full_path, $pdf);

			if ($valid && is_file($full_path)) {
				$stmt = $pdo->prepare("UPDATE cotizaciones SET factura_pdf_url = :factura_pdf_url WHERE id_cotizacion = :id_cotizacion");
				
				$stmt->bindParam(':factura_pdf_url', $full_path);
				$stmt->bindParam(':id_cotizacion', $cotizacion_id_cotizacion);
				
				$stmt->execute();

				$response = array(
					'status' => 'success',
					'message' => 'El archivo pdf de ha generado con exito'
				);
			}
		} else {
			$response = array(
    			'status' => 'error',
    			'message' => 'La url no es valida'
    		);
		}

		echo json_encode($response);
    	exit();
	}

	header('location: inicio.php');
	exit();

?>