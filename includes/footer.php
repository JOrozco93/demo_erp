<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2016 &copy; HS Puertas de Garage. <a href="http://teknik.mx" target="_blank" alt="Desarrollo de software en Monterrey">Desarrollo de software web por Teknik.</a>
        
    </div>

    <div class="footer-tools">

        <span class="go-top">
            <i class="fa fa-angle-up"></i>
        </span>
    </div>
</div>
<!-- END FOOTER -->