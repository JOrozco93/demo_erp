<?php   	/* Lista de Funciones diversas para el manejo de Base de Datos dentro del sistema */

/* Funciones de Base de Datos */
	
function accion($query, $salida, $requerido) {
	$editFormAction = $_SERVER['PHP_SELF'];
	if (isset($_SERVER['QUERY_STRING'])) {
		$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
	}

	if (isset($requerido)) {
		global $db;
		global $db_con;
		mysql_select_db($db, $db_con);
		$Resultado = mysql_query($query, $db_con) or die(mysql_error());

		if (isset($_SERVER['QUERY_STRING'])) {
			$salida .= (strpos($salida, '?')) ? "&" : "?";
			$salida .= $_SERVER['QUERY_STRING'];
		}
		header(sprintf("Location: %s", $salida));
	}
		  
}


/*  Funciones para acceso a datos   */

function datoRapido ($query) {
	global $db;
	global $db_con;
	mysql_select_db($db, $db_con);
	$resultado = mysql_query($query, $db_con) or die(mysql_error());
	$row = mysql_fetch_assoc($resultado);
	return $row['dato'];
}

function ingresoRapido ($query) {
	global $db;
	global $db_con;

	mysql_select_db($db, $db_con);
	$ingreso = mysql_query($query, $db_con) or die(mysql_error());
}

function ejecucionRapido ($query) {
	global $db;
	global $db_con;

	mysql_select_db($db, $db_con);
	mysql_query($query, $db_con) or die(mysql_error());
}

function consulta ($query) {
	global $db;
	global $db_con;
	mysql_select_db($db, $db_con);
	$resultado = mysql_query($query, $db_con) or die('error en la consulta: '.$query);//die(mysql_error());
	//$row = mysql_fetch_assoc($resultado);
	return $resultado;
}

function consultaRapida ($query) {
	global $db;
	global $db_con;
	mysql_select_db($db, $db_con);
	$resultado = mysql_query($query, $db_con) or die(mysql_error());
	$row = mysql_fetch_assoc($resultado);
	return $row;
}

function permisoSubseccion ($fSeccion , $fSubseccion , $fPerfil,$permiso) {
	global $db;
	global $db_con;
	mysql_select_db($db, $db_con);
	$query = sprintf("select ss.".$permiso." as dato from secciones s inner join subseccion ss on ss.id_seccion = s.id_seccion
where s.visualizacion = 1 and s.seccion = %s  and ss.subseccion = %s and s.id_perfil =%s ;",
					   GetSQLValueString($fSeccion, "text"),
					   GetSQLValueString($fSubseccion, "text"),
					   GetSQLValueString($fPerfil, "int"));

//echo $query;
	$resultado = mysql_query($query, $db_con) or die(mysql_error());
	$row = mysql_fetch_assoc($resultado);
	if($row['dato']==1){
		$rowR= true;
	}else{
		$rowR=false;
	}
	//echo $rowR;
	return $rowR;
}

function eliminarId ($query) {
	global $db;
	global $db_con;

	mysql_select_db($db, $db_con);
	mysql_query($query, $db_con) or die(mysql_error());
}

// Function to get the client IP address
//function get_ip() {
//$ip = getenv('HTTP_CLIENT_IP')?:
//getenv('HTTP_X_FORWARDED_FOR')?:
//getenv('HTTP_X_FORWARDED')?:
//getenv('HTTP_FORWARDED_FOR')?:
//getenv('HTTP_FORWARDED')?:
//getenv('REMOTE_ADDR');
//}

//Funcion para generar el log
function actualizalog($usuario,$evento,$detalle)
{	
  global $db;
  global $db_con;
  $ip = $_SERVER['REMOTE_ADDR'];
  $insertLog="INSERT INTO log (id_usuario,evento,ip,descripción) VALUES ('".$usuario."','".$evento."','".$ip."','".$detalle."')";
  $result = mysql_query($insertLog, $db_con);
  return mysql_insert_id();
}

function insertaDomicilio($calle, $no_interior, $no_exterior, $colonia, $referencia, $localidad, $municipio, $estado, $codigo_postal, $id_pais){
	global $db;
 	global $db_con;
	$sql_insert = sprintf("INSERT INTO `domicilios` (`calle`, `no_exterior`,  `no_interior`, `colonia`, `localidad`,  `referencia`, `municipio`, `estado`, `codigo_postal`, `id_pais`) 
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
                        GetSQLValueString($calle, "text"), 
                        GetSQLValueString($no_exterior, "text"), 
                        GetSQLValueString($no_interior, "text"), 
                        GetSQLValueString($colonia, "text"), 
                        GetSQLValueString($referencia, "text"),
                        GetSQLValueString($localidad, "text"), 
                        GetSQLValueString($municipio, "text"),
                        GetSQLValueString($estado, "text"), 
                        GetSQLValueString($codigo_postal, "text"),  
                        GetSQLValueString($id_pais, "int")
    );
    $result = mysql_query($sql_insert, $db_con) or die("Problemas en la consulta: " . $sql_insert);
    $id_domicilio = mysql_insert_id();
	return $id_domicilio;
}
function actualizaDomicilio($id_domicilio, $calle, $no_interior, $no_exterior, $colonia, $referencia, $localidad, $municipio, $estado, $codigo_postal, $id_pais){
	global $db;
 	global $db_con;
	$sql_update = sprintf("UPDATE `domicilios` SET  `calle`=%s, `no_exterior`=%s,  `no_interior`=%s, `colonia`=%s, `referencia`=%s, `localidad`=%s, `municipio`=%s, `estado`=%s, `codigo_postal`=%s, `id_pais`=%s WHERE id_domicilio=%s", 
                        GetSQLValueString($calle, "text"), 
                        GetSQLValueString($no_exterior, "text"), 
                        GetSQLValueString($no_interior, "text"), 
                        GetSQLValueString($colonia, "text"), 
                        GetSQLValueString($referencia, "text"),
                        GetSQLValueString($localidad, "text"), 
                        GetSQLValueString($municipio, "text"),
                        GetSQLValueString($estado, "text"), 
                        GetSQLValueString($codigo_postal, "text"),  
                        GetSQLValueString($id_pais, "int"),
                        GetSQLValueString($id_domicilio, "int")
    );
    $result1 = mysql_query($sql_update, $db_con) or die("Problemas en la consulta: " . $sql_update);
    $id_domicilio = mysql_insert_id();
	return $id_domicilio;
}

// jma: Cotizaciones
function getNumCotizaciones() {
	global $pdo;

	$sql = "SELECT COUNT(*) FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1  AND c.autorizada = 0 AND c.pagada != 2 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha  DESC";

	$result = $pdo->query($sql);

	if (count($result)) {
		$row = $result->fetch(PDO::FETCH_NUM);

		$cotizaciones = $row[0];
	} else {
		$cotizaciones = 0;
	}
	
	return $cotizaciones;
}

// jma: Ventas
function getNumVentas() {
	global $pdo;

	$sql = "SELECT COUNT(*) FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada != 2 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha DESC";

	$result = $pdo->query($sql);

	if (count($result)) {
		$row = $result->fetch(PDO::FETCH_NUM);

		$ventas = $row[0];
	} else {
		$ventas = 0;
	}
	
	return $ventas;
}

// jma: Producción
function getNumProduccion() {
	global $pdo;

	$sql = "SELECT COUNT(*) FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.produccion = 0 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha DESC";

	$result = $pdo->query($sql);

	if (count($result)) {
		$row = $result->fetch(PDO::FETCH_NUM);

		$produccion = $row[0];
	} else {
		$produccion = 0;
	}
	
	return $produccion;
}

// jma: Despacho
function getNumDespacho() {
	global $pdo;

	$sql = "SELECT COUNT(*) FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.produccion = 1 AND c.despacho = 0 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha DESC";

	$result = $pdo->query($sql);

	if (count($result)) {
		$row = $result->fetch(PDO::FETCH_NUM);

		$despacho = $row[0];
	} else {
		$despacho = 0;
	}
	
	return $despacho;
}

// jma: Despacho
function getNumTotal() {
	global $pdo;

	$sql = "SELECT COUNT(*) FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.produccion = 1 AND c.despacho = 1 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha DESC";

	$result = $pdo->query($sql);

	if (count($result)) {
		$row = $result->fetch(PDO::FETCH_NUM);

		$finanzas = $row[0];
	} else {
		$finanzas = 0;
	}
	
	return $finanzas;
}

// jma: Despacho
function getNumIncidencias() {
	global $pdo;
	
	$sql = "SELECT COUNT(*) FROM incidencias, cotizaciones WHERE incidencias.activo = 1 AND incidencias.id_cotizacion = cotizaciones.id_cotizacion ORDER BY incidencias.id_cotizacion ASC";

	$result = $pdo->query($sql);

	if (count($result)) {
		$row = $result->fetch(PDO::FETCH_NUM);

		$incidencias = $row[0];
	} else {
		$incidencias = 0;
	}
	
	return $incidencias;
}

// jma: Divisas
function getDivisas() {
	global $pdo;

	$sql = "SELECT * FROM divisas WHERE activo = :activo";

	$stmt = $pdo->prepare($sql);
	
	$stmt->bindParam(':activo', $activo);

	$activo = 1;

	$stmt->execute();
	
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// jma: Multiplicadores
function getMultiplicadores() {
	global $pdo;

	$sql = "SELECT * FROM multiplicadores WHERE activo = :activo";

	$stmt = $pdo->prepare($sql);
	
	$stmt->bindParam(':activo', $activo);

	$activo = 1;

	$stmt->execute();
	
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function getVentasPorMeses() {
	global $pdo;

	$meses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

	$result = "";

	foreach ($meses as $mes) {
		$result = $result . ',' . getVentasPorMes($mes); 
	}

	$result = substr($result, 1);
	$result = '[' . $result . ']';

	return $result;
}

function getVentasPorMes($mes) {
	global $pdo;

	$meses = array(1 => 'ENE', 2 => 'FEB', 3 => 'MAR', 4 => 'ABR', 5 => 'MAY', 6 => 'JUN', 7 => 'JUL', 8 => 'AGO', 9 => 'SEP', 10 => 'OCT', 11 => 'NOV', 12 => 'DIC');

	// $meses = array(1 => '01', 2 => '02', 3 => '03', 4 => '04', 5 => '05', 6 => '06', 7 => '07', 8 => '08', 9 => '09', 10 => '10', 11 => '11', 12 => '12');

	/*
	$sql = "
		SELECT COUNT(*) as counter
		FROM cotizaciones
		WHERE MONTH(fecha) = $mes
		GROUP BY MONTH(fecha)
	";
	*/

	$sql = "
		SELECT COUNT(*) as counter
		FROM cotizaciones
		WHERE activo = 1 AND autorizada = 1 AND pagada = 2 AND produccion = 1 AND despacho = 1 AND MONTH(fecha) = $mes
		GROUP BY MONTH(fecha)
	";


	$result = $pdo->query($sql);
	
	if (count($result)) {
		$row = $result->fetch(PDO::FETCH_ASSOC);

		if ($row['counter']) {
			$ventas = '[' . "'$meses[$mes]'" . ',' . $row['counter'] . ']';
		} else {
			$ventas = '[' . "'$meses[$mes]'" . ',' . 0 . ']';
		}
	}

	return $ventas;
}

function getPedidosEnProceso() {
	global $pdo;

	// Cotizaciones

	$sql = "
		SELECT
			c.id_cotizacion AS id_cotizacion,
			c.numero_cotizacion AS numero_cotizacion,
			c.autorizada AS autorizada,
			c.pagada AS pagada,
			c.produccion AS produccion,
			c.despacho AS despacho, 
			DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha,
			c.descripcion,
			cli.nombre_cliente AS cliente,
			c.total AS total,
			u.nombre_usuario AS vendedor
		FROM
			cotizaciones AS c,
			clientes AS cli,
			usuarios AS u
		WHERE
			c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario AND c.activo = 1
		ORDER BY
			c.fecha DESC
	";

	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getComponentsForDoors($id_puerta) {
	global $pdo;

	// $sql = "SELECT * FROM componentes";
	$sql = "SELECT * FROM componentes WHERE activo = 1 AND id_componente NOT IN (SELECT id_componente FROM puerta_componente WHERE activo = 1 AND id_puerta = $id_puerta)";
	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getComponentsByDoor($id_puerta) {
	global $pdo;

	$sql = "SELECT pc.id AS id, c.nombre_componente AS componente, pc.cantidad AS cantidad FROM puerta_componente AS pc, componentes AS c WHERE pc.id_componente = c.id_componente AND pc.activo = 1 AND pc.id_puerta = $id_puerta";
	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getDoorComponentById($id_puerta_componente) {
	global $pdo;

	$sql = "SELECT puerta_componente.id AS id, puerta_componente.id_puerta AS id_puerta, componentes.nombre_componente AS nombre_componente, puerta_componente.cantidad AS cantidad FROM puerta_componente, componentes WHERE puerta_componente.id_componente = componentes.id_componente AND id = $id_puerta_componente";
	$result = $pdo->query($sql);
	
	return $result->fetch(PDO::FETCH_ASSOC);
}

function deleteDoorComponentById($id_puerta_componente) {
	global $pdo;

	$sql = "DELETE FROM puerta_componente WHERE id = $id_puerta_componente";
	$result = $pdo->query($sql);
	
	return $result->rowCount();
}

function getCotizacionesByIdCliente($id_cliente) {
	global $pdo;

	$sql = "SELECT id_cotizacion, numero_cotizacion, autorizada, pagada, produccion, despacho, DATE_FORMAT(fecha, '%Y-%m-%d') AS fecha, descripcion, total FROM cotizaciones WHERE id_cliente = $id_cliente AND activo = 1";
	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getDepositosByIdCliente($id_cliente) {
	global $pdo;

	$sql = "SELECT id_deposito, DATE_FORMAT(fecha, '%Y-%m-%d') AS fecha, monto_deposito FROM depositos WHERE id_cliente = $id_cliente AND activo = 1";
	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getIncidenciasByIdCliente($id_cliente) {
	global $pdo;

	$sql = "SELECT incidencias.id_incidencia AS id_incidencia, cotizaciones.numero_cotizacion AS numero_cotizacion, DATE_FORMAT(incidencias.fecha, '%Y-%m-%d') AS fecha, incidencias.descripcion AS descripcion, incidencias.activo AS activo FROM incidencias, cotizaciones WHERE incidencias.activo = 1 AND incidencias.id_cotizacion = cotizaciones.id_cotizacion AND cotizaciones.id_cliente = $id_cliente";

	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getPuertasActualizarPrecios() {
	global $pdo;

	$sql = "SELECT * FROM puerta AS p, puerta_tipo AS pt WHERE p.activo = 1 AND p.id_ptipo = pt.id_ptipo";

	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getComponentesActualizarPrecios() {
	global $pdo;

	$sql = "SELECT * FROM componentes AS c, componentes_grupos AS cg WHERE c.activo = 1 AND c.id_componente_grupo = cg.id_grupo_componente";

	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getKitsActualizarPrecios() {
	global $pdo;

	$sql = "SELECT * FROM kit AS k WHERE k.activo = 1";

	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getVentanasActualizarPrecios() {
	global $pdo;

	$sql = "SELECT * FROM ventana AS v WHERE v.activo = 1";

	$result = $pdo->query($sql);
	
	return $result->fetchAll(PDO::FETCH_ASSOC);
}

?>