<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");
$id_usuario = $_SESSION['id_usuario'];
?>
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner">
        <!-- BEGIN LOGO -->
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <img src="img/menu-toggler.png" alt=""/>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <ul class="nav navbar-nav pull-left">
            <li>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
        </ul>
        <div class="top-menu">
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">

            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown dropdown-user">
              <a aria-expanded="false" href="inicio.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <span class="username"> <i class="fa fa-user fa-lg"></i> <?php echo $_SESSION['nombre_usuario']; ?> </span>
              </a>
            </li>

            <!-- END USER LOGIN DROPDOWN -->
            <!-- BEGIN TOOLBOX -->
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4') || ($_SESSION['permiso_usuario'] == '6')) { ?>
            <li class="dropdown dropdown-quick-sidebar-toggler">
                <a href="alta-clientes.php?id=0&tipo=cliente" class="dropdown-toggle username"  title="Crear cliente" data-toggle="tooltip" data-placement="bottom">
                    <i class="fa fa-user-plus"></i>
                </a>
            </li>
            <?php } ?>
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4') || ($_SESSION['permiso_usuario'] == '5') || ($_SESSION['permiso_usuario'] == '6')) { ?>
            <li class="dropdown dropdown-quick-sidebar-toggler"  title="Crear cotización" data-toggle="tooltip" data-placement="bottom">
                <a href="alta-cotizacion.php?id=0" class="dropdown-toggle username">
                    <i class="fa fa-file-text"></i>
                </a>
            </li>
            <?php } ?>

            <li class="dropdown dropdown-quick-sidebar-toggler">
                <a href="logout.php" class="dropdown-toggle username" title="Salir" data-toggle="tooltip" data-placement="bottom">
                    <i class="fa fa-power-off"></i>
                </a>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
