<?php

require_once "../util/PHPMailer/PHPMailerAutoload.php";
require_once "../config.php";


function smtp_mail($to, $from, $cc, $subject, $message, $attachments) {
    global $GLOBAL_MailHost;
    global $GLOBAL_MailUsername;
    global $GLOBAL_MailPassword;
    global $GLOBAL_MailFrom;
    global $GLOBAL_MailSMTPAuth;
    global $GLOBAL_MailPort;


    $mail = new PHPMailer;
//  CONFIGURACION 
    $mail->CharSet = 'UTF-8';                           // Charset to UTF-8
    $mail->isSMTP();                                    // Set mailer to use SMTP
    $mail->Host = $GLOBAL_MailHost;                     // Specify main and backup SMTP servers
    $mail->SMTPAuth = $GLOBAL_MailSMTPAuth;             // Enable SMTP authentication 'true' or 'false'
    $mail->Username = $GLOBAL_MailUsername;             // SMTP username
    $mail->Password = $GLOBAL_MailPassword;             // SMTP password
    $mail->SMTPSecure = 'tls';                          // Enable encryption, 'tls', 'ssl'  accepted
    $mail->Port = $GLOBAL_MailPort;                     // Set the SMTP Port
    $mail->WordWrap = 50;                               // Set word wrap to 50 characters
    $mail->isHTML(true);                                // Set email format to HTML	
    //$mail->SMTPDebug = 2;                             // Debug de la funcion	

    $from = $GLOBAL_MailUsername;  // Edit por problemas de Spam: Que siempre sea enviado desde la cuenta de mensajero.

    if ($from == '')
        $mail->From = $GLOBAL_MailFrom;
    else
        $mail->From = $from;
    $mail->FromName = 'HS Portones Automaticas';
    $recipients = explode(",", $to);
    foreach ($recipients as $key => $val) {
        $mail->addAddress($val);     // Add a recipient
    }
    if ($from != '')
        $mail->addReplyTo($GLOBAL_MailFrom);
    else
        $mail->addReplyTo($from);

    $copia = explode(",", $cc);
    foreach ($copia as $key => $val) {
        $mail->addCC($val);
    }

    foreach ($attachments as $key => $val) {
        if (is_file($val)) {
            $mail->addAttachment($val);         // Add attachments
        }
    }
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->AddAttachemt = $attachments;

    if ($mail->send()) {
        return true;
    } else {
        echo $mail->ErrorInfo;
        return false;
    }
}

?>