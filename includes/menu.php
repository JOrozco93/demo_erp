<?php

    if (!isset($_SESSION)) {
        session_start();
    }

    /* ---- PERMISOS -----

    1   Administrador
    2   Director
    3   Director Produccion
    4   Direcctor Ventas
    5   Cliente
    6   Vendedor
    7   Produccion
    8   Despacho
    9   Finanzas
    10  Servicio al Cliente

    ---------------------- */
?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper"></li>
            <li>&nbsp;</li>

            <li id="dashboard">
                <a href="inicio.php">
                    <i class="fa fa-home"></i>
                    <span class="title">Dashboard</span>
                    <!-- <span class="selected"></span> -->
                </a>
            </li>

            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4') || ($_SESSION['permiso_usuario'] == '6') || ($_SESSION['permiso_usuario'] == '9') || ($_SESSION['permiso_usuario'] == '10')) { ?>
                <li id="clientes">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span class="title">Clientes</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="listado-prospectos.php">Prospecto</a></li>
                        <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '6') || ($_SESSION['permiso_usuario'] == '9') || ($_SESSION['permiso_usuario'] == '10')) { ?>
                            <li><a href="listado-clientes.php">Cliente</a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>

            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4') || ($_SESSION['permiso_usuario'] == '5') || ($_SESSION['permiso_usuario'] == '6') || ($_SESSION['permiso_usuario'] == '9') || ($_SESSION['permiso_usuario'] == '10')) { ?>
                <li id="cotizaciones">
                    <a href="listado-cotizaciones.php">
                        <i class="fa fa-list-alt"></i>
                        <span class="title">Cotizaciones</span>
                    </a>
                </li>
            <?php } ?>

             <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4') || ($_SESSION['permiso_usuario'] == '5') || ($_SESSION['permiso_usuario'] == '6') || ($_SESSION['permiso_usuario'] == '9') || ($_SESSION['permiso_usuario'] == '10')) { ?>
                <li id="ventas">
                    <a href="listado-ventas.php">
                        <i class="fa fa-cart-plus"></i>
                        <span class="title">Ventas</span>
                    </a>
                </li>
            <?php } ?>

            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '3') || ($_SESSION['permiso_usuario'] == '7') || ($_SESSION['permiso_usuario'] == '10')) { ?>
                <li id="produccion">
                    <a href="listado-produccion.php">
                        <i class="fa fa-cog"></i>
                        <span class="title">Producción</span>
                    </a>
                </li>
            <?php } ?>

            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '3') || ($_SESSION['permiso_usuario'] == '6') || ($_SESSION['permiso_usuario'] == '8') || ($_SESSION['permiso_usuario'] == '10')) { ?>
                <li id="despacho">
                    <a href="listado-despacho.php">
                        <i class="fa fa-truck"></i>
                        <span class="title">Despacho</span>
                    </a>
                </li>
            <?php } ?>

            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '7') || ($_SESSION['permiso_usuario'] == '8') || ($_SESSION['permiso_usuario'] == '10')) { ?>
                <li id="incidencias">
                    <a href="listado-incidencias.php">
                        <i class="fa fa-comment"></i>
                        <span class="title">
                            Incidencias
                        </span>
                    </a>
                </li>
            <?php } ?>

            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2')) { ?>
                <li id="catalagos">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span class="title">Catalagos</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="catalogo-usuarios.php">Usuarios</a></li>
                        <li><a href="catalogo-proveedores.php">Proveedores</a></li>
                        <li><a href="catalogo-productos.php">Productos</a></li>
                    </ul>
                </li>
            <?php } ?>


            <!--
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2')) { ?>
                <li id="configuracion">
                    <a href="configuracion.php">
                        <i class="fa fa-cogs"></i>
                        <span class="title">
                            Configuración
                        </span>
                    </a>
                </li>
            <?php } ?>
            -->

            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2')) { ?>
                <li id="reportes">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span class="title">Reportes</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="reportes-ventas.php">Ventas</a></li>
                        <li><a href="reportes-produccion.php">Producción</a></li>
                        <li><a href="reportes-despachos.php">Despachos</a></li>
                    </ul>
                </li>
            <?php } ?>

            <li id="salir">
                <a href="logout.php">
                    <i class="fa fa-power-off"></i>
                    <span class="title">
                        Salir
                    </span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
