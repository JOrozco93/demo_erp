<?php
if (!isset($_SESSION)) {
    session_start();
}
?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">

            </li>
            <li>&nbsp;</li>
            <li id="dashboard">
                <a href="inicio.php">
                    <i class="fa fa-home"></i>
                    <span class="title">
                        Dashboard
                    </span>
<!--                    <span class="selected">
                    </span>-->
                </a>
            </li>
            <!--            <li id="cotizaciones">
                            <a href="cotizaciones.php">
                                <i class="fa fa-pencil-square-o"></i>
                                <span class="title">
                                    Cotizaciones
                                </span>
                            </a>
                      </li>-->
			  <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2')) { ?>
                <li id="autorizacion">
                    <a href="autorizaciones.php">
                        <i class="fa fa-check-square"></i>
                        <span class="title">
                            Autorizaci&oacute;n
                        </span>
    <!--                    <span class="selected">
                        </span>-->
                    </a>
                </li>
            <?php } ?>
            <li id="clientes">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span class="title">
                        Clientes
                    </span>
                    <span class="arrow">
                    </span>
                </a>
                <ul class="sub-menu">
                     <li>
                        <a href="listado-prospectos.php">
                            Prospecto
                        </a>
                    </li>
                    <li>
                        <a href="listado-clientes.php">
                            Cliente
                        </a>
                    </li>
                   
                </ul>
            </li>
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')) { ?>
                <li id="cotizaciones">
                <a href="listado-cotizaciones.php">
                    <i class="fa fa-list-alt"></i>
                    <span class="title">
                        Cotizaciones
                    </span>
              
                </a>
            </li>
            <li id="ventas">
                <a href="listado-ventas.php">
                    <i class="fa fa-cart-plus"></i>
                    <span class="title">
                        Ventas
                    </span>

                </a>
            </li>
            <?php } ?>
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '3')) { ?>
                <li id="produccion">
                    <a href="listado-produccion.php">
                        <i class="fa fa-cog"></i>
                        <span class="title">
                            Producci&oacute;n
                        </span>
                    </a>
                </li>
            <?php } ?>
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '3')) { ?>
                <li id="despacho">
                    <a href="listado-despacho.php">
                        <i class="fa fa-truck"></i>
                        <span class="title">
                           Despacho
                        </span>
                    </a>
                </li>
            <?php } ?>
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '3') || ($_SESSION['permiso_usuario'] == '4')) { ?>
                <li id="finanzas">
                    <a href="#">
                        <i class="fa fa-usd"></i>
                        <span class="title">
                            Finanzas
                        </span>
                        <span class="arrow">
                        </span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="listado-depositos.php">
                                Dep&oacute;sitos
                            </a>
                        </li>
                        <li>
                            <a href="balanceA.php">
                                Balance General Ortiz
                            </a>
                        </li>
                        <li>
                            <a href="balanceB.php">
                                Balance General HS
                            </a>
                        </li>                        
                    </ul>
                </li>
            <?php } ?>
            <li id="incidencias">
                <a href="listado-incidencias.php">
                    <i class="fa fa-comment"></i>
                    <span class="title">
                        Incidencias
                    </span>
                </a>
            </li>
            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')) { ?>
            <li id="catalagos">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span class="title">
                        Catalagos
                    </span>
                    <span class="arrow">
                    </span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="catalogo-usuarios.php">
                            Usuarios
                        </a>
                    </li>
                    <li>
                        <a href="catalogo-proveedores.php">
                            Proveedores
                        </a>
                    </li>
                    <li>
                        <a href="catalogo-puertas.php">
                            Puertas
                        </a>
                    </li>
                    <li>
                        <a href="catalogo-ventanas.php">
                            Ventanas
                        </a>
                    </li>
                    <li>
                        <a href="catalogo-herreria.php">
                            Kits
                        </a>
                    </li>
                    <li>
                        <a href="catalogo-componentes.php">
                            Componentes 
                        </a>
                    </li>
                    <li>
                        <a href="catalogo-divisas.php">
                            Divisas
                        </a>
                    </li>
                    <li>
                        <a href="catalogo-multiplicadores.php">
                            Multiplicadores
                        </a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            <li id="salir">
                <a href="logout.php">
                    <i class="fa fa-power-off"></i>
                    <span class="title">
                        Salir
                    </span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->