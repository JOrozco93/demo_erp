<?php
require_once("config.php");
$error = array(0 => "Ingrese la contrase&ntildea", 1 => "El usuario o la contrase&ntildea no son correctos.", 2 => "Ingrese el usuario");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>ERP</title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
       <!-- <link href="plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>-->
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <!--<link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>-->
        <!--<link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>-->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="css/style-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <!--<link href="css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="css/themes/red.css" rel="stylesheet" type="text/css" id="style_color"/>-->
        <link href="css/pages/login.css" rel="stylesheet" type="text/css"/>
        <!--<link href="css/custom.css" rel="stylesheet" type="text/css"/>-->
        <!-- END THEME STYLES -->
    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">

        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top: 200px;">
            <!-- BEGIN LOGIN FORM -->
            <form  method="post" class="login-form" action="includes/login.php">
                <h3 class="form-title">Iniciar Sesi&oacute;n</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>

<!--                    <span>
                        Enter any username and password.
                    </span>-->
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" id="username"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password"/>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green pull-right">
                        ACCEDER <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                    <?php
                    if (isset($_GET['id']))
                        echo $error[$_GET['id']];
                    ?>
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <!-- END LOGIN -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
                <script src="assets/plugins/respond.min.js"></script>
                <script src="assets/plugins/excanvas.min.js"></script>
                <![endif]-->
        <script src="plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!--<script src="plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>-->
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
       <!-- <script type="text/javascript" src="plugins/select2/select2.min.js"></script>-->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="scripts/core/app.js" type="text/javascript"></script>
        <!--<script src="scripts/custom/login.js" type="text/javascript"></script>-->
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
