<?php

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == NULL)) {
        header("Location: logout.php");
    }

    require_once("config.php");
    require_once("includes/funciones_BD.php");

    $numCotizaciones = getNumCotizaciones();
    $numVentas = getNumVentas();
    $numProduccion = getNumProduccion();
    $numDespacho = getNumDespacho();
    $numTotal = getNumTotal();
    $numIncidencias = getNumIncidencias();

    $divisas = getDivisas();
    $multiplicadores = getMultiplicadores();

    $pedidos_en_proceso = getPedidosEnProceso();

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php include ("includes/css.php"); ?>
        <!--<link rel="shortcut icon" href="favicon.ico"/>-->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Panel de control <small><?php echo datoRapido("SELECT descripcion AS dato FROM permisos WHERE id_permiso =".$_SESSION["permiso_usuario"]); ?></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="inicio.php">
                                        Inicio
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Dashboard
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                         <?php echo $numCotizaciones; ?>
                                    </div>
                                    <br>
                                    <div class="desc">
                                       Cotizaciones
                                    </div>
                                </div>
                                <a class="more" href="listado-cotizaciones.php">
                                    Ver más<i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $numVentas; ?>
                                    </div>
                                    <br>
                                    <div class="desc">
                                        Ventas en proceso
                                    </div>
                                </div>
                                <a class="more" href="listado-ventas-proceso.php">
                                    Ver más <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple">
                                <div class="visual">
                                    <i class="fa fa-cog"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $numProduccion; ?>
                                    </div>
                                    <br>
                                    <div class="desc">
                                        Producción
                                    </div>
                                </div>
                                <a class="more" href="listado-produccion.php">
                                    Ver más <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-truck"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $numDespacho; ?>
                                    </div>
                                    <br>
                                    <div class="desc">
                                        Despacho
                                    </div>
                                </div>
                                <a class="more" href="listado-despacho.php">
                                    Ver más <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="dashboard-stat grey">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $numIncidencias; ?>
                                    </div>
                                    <br>
                                    <div class="desc">
                                        Incidencias
                                    </div>
                                </div>
                                <a class="more" href="listado-incidencias.php">
                                    Ver más <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-hashtag"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $numTotal; ?>
                                    </div>
                                    <br>
                                    <div class="desc">
                                        Total de ventas
                                    </div>
                                </div>
                                <a class="more" href="#">
                                    Ver más<i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- END DASHBOARD STATS -->

                    <div class="row">

                        <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '9')) { ?>
                            <div class="col-md-9 col-sm-9">
                                <!-- BEGIN PORTLET-->
                                <div class="portlet solid bordered light-grey">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bar-chart-o"></i>Ventas del 2016
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="graph_statistics_loading">
                                            <img src="img/loading.gif" alt="loading"/>
                                        </div>
                                        <div id="graph_statistics_content" class="display-none">
                                            <div id="graph_statistics" class="chart">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PORTLET-->
                            </div>
                        <?php } ?>

                        <?php if (count($divisas)): ?>
                            <div class="col-md-3 col-sm-3">
                                <!-- BEGIN PORTLET-->
                                <div class="portlet solid light-grey bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-dollar"></i>Divisas
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Moneda</th>
                                                    <th>Actual</th>
                                                    <th>Sugerido</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($divisas as $divisa): ?>
                                                    <tr>
                                                        <td><?php echo $divisa['abbrev']; ?></td>
                                                        <td><?php echo $divisa['tipo_cambio']; ?></td>
                                                        <td><span id="<?php echo $divisa['abbrev']; ?>" class="abbrev"></span></td>
                                                    <tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        <br/>
                                        <button data-target="#modalDivisas" data-toggle="modal" class="btn btn-sm btn-success">
                                            <i class="fa fa-refresh"></i> Modificar Divisas
                                        </button>
                                    </div>
                                </div>
                                <!-- END PORTLET-->
                            </div>
                        <?php endif; ?>
                    </div>

                    <!--
                    <div class="col-md-3 col-sm-3">
                             <div class="portlet solid light-grey bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-calculator"></i>Multiplicadores
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Multiplicador</th>
                                                <th>Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($multiplicadores as $multiplicador): ?>
                                                <tr>
                                                    <td><?php echo $multiplicador['nombre_multiplicador']; ?></td>
                                                    <td><?php echo $multiplicador['valor']; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <br/>
                                    <button data-target="#modalMultiplicador" data-toggle="modal" class="btn btn-sm btn-success">
                                        <i class="fa fa-refresh"></i> Modificar Multiplicador
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->

                    <div class="row ">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-bell-o"></i>Pedidos en proceso
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="scroller" style="height: 400px;" data-always-visible="1" data-rail-visible="0">
                                        <table id="sample_1" class="table table-striped table-bordered table-hover table-full-width">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50px;">Status</th>
                                                    <th>No. Cotizacion</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>
                                                    <th>Cliente</th>
                                                    <th>Total</th>
                                                    <th>Vendedor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($pedidos_en_proceso as $row): ?>
                                                    <!-- ventas -->
                                                    <?php if ($row['autorizada'] == 1 && $row['pagada'] != 2 && $row['produccion'] == 0 && $row['despacho'] == 0): ?>
                                                        <tr>
                                                            <td>
                                                                <?php if ($row['autorizada'] == 1 && $row['pagada'] != 2 && $row['produccion'] == 0 && $row['despacho'] == 0): ?>
                                                                    <i class="fa fa-shopping-cart" style="color: #28b779;"></i>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"><?php echo $row['numero_cotizacion']; ?></a></td>
                                                            <td><?php echo $row['fecha']; ?></td>
                                                            <td><?php echo $row['descripcion']; ?></td>
                                                            <td><?php echo $row['cliente']; ?></td>
                                                            <td><?php echo number_format($row['total'], 2, '.', ','); ?></td>
                                                            <td><?php echo $row['vendedor']; ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    <!-- ventas -->

                                                    <!-- producción -->
                                                    <?php if ($row['autorizada'] == 1 && $row['pagada'] == 2 && $row['produccion'] == 0 && $row['despacho'] == 0): ?>
                                                        <tr>
                                                            <td>
                                                                <?php if ($row['autorizada'] == 1 && $row['pagada'] == 2 && $row['produccion'] == 0 && $row['despacho'] == 0): ?>
                                                                    <i class="fa fa-cog" style="color: #852b99;"></i>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"><?php echo $row['numero_cotizacion']; ?></a></td>
                                                            <td><?php echo $row['fecha']; ?></td>
                                                            <td><?php echo $row['descripcion']; ?></td>
                                                            <td><?php echo $row['cliente']; ?></td>
                                                            <td><?php echo number_format($row['total'], 2, '.', ','); ?></td>
                                                            <td><?php echo $row['vendedor']; ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    <!-- producción -->

                                                    <!-- despacho -->
                                                    <?php if ($row['autorizada'] == 1 && $row['pagada'] == 2 && $row['produccion'] == 1 && $row['despacho'] == 0): ?>
                                                        <tr>
                                                            <td>
                                                                <?php if ($row['autorizada'] == 1 && $row['pagada'] == 2 && $row['produccion'] == 1 && $row['despacho'] == 0): ?>
                                                                    <i class="fa fa-truck" style="color: #27a9e3;"></i>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"><?php echo $row['numero_cotizacion']; ?></a></td>
                                                            <td><?php echo $row['fecha']; ?></td>
                                                            <td><?php echo $row['descripcion']; ?></td>
                                                            <td><?php echo $row['cliente']; ?></td>
                                                            <td><?php echo number_format($row['total'], 2, '.', ','); ?></td>
                                                            <td><?php echo $row['vendedor']; ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    <!-- despacho -->
                                                <?php endforeach; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th style="width: 100px;">Status</th>
                                                    <th>No. Cotizacion</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>
                                                    <th>Cliente</th>
                                                    <th>Total</th>
                                                    <th>Vendedor</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="scroller-footer">
                                        <div class="pull-right">
                                            <a href="#">
                                                See All Records <i class="m-icon-swapright m-icon-gray"></i>
                                            </a>
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

        <!-- modalDivisas -->
        <div id="modalDivisas" aria-hidden="true" role="dialog" tabindex="-1" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title"><i class="fa fa-pencil-square-o"></i> Cambiar Estatus</h4>
                    </div>
                    <form id="monedaModal" name="monedaModal" method="post" action="alta/modificar-divisas.php">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label style="font-weight: bold;">Divisa</label>
                                    <select id="moneda" name="moneda" onchange="valorMoneda(this.value);" class="form-control">
                                        <option value="">[Seleccione Tipo de Divisa]</option>
                                        <?php foreach($divisas as $divisa): ?>
                                            <option value="<?php echo $divisa['abbrev']; ?>"><?php echo $divisa['abbrev']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label style="font-weight: bold;">Valor</label>
                                    <input type="text" id="cambio" name="cambio" class="form-control">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="modal-footer clearfix">
                            <button type="submit" id="submit" name="submit" value="aceptar" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Aceptar</button>
                            <button data-dismiss="modal" class="btn btn-danger pull-left" type="button"><i class="fa fa-times"></i> Cancelar</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- modalDivisas -->

        <!-- modalMultiplicador -->
        <div id="modalMultiplicador" aria-hidden="true" role="dialog" tabindex="-1" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title"><i class="fa fa-pencil-square-o"></i> Multiplicador</h4>
                    </div>
                    <form id="multiplicadorModal" name="monedaModal" method="post" action="alta/modificar-multiplicador.php">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label style="font-weight: bold;">Tipo de Multiplicador</label>
                                    <select id="multiplicador" name="multiplicador" class="form-control">
                                        <option value="">[Seleccione Tipo de Multilplicador]</option>
                                        <?php foreach($multiplicadores as $multiplicador): ?>
                                            <option value="<?php echo $multiplicador['nombre_multiplicador']; ?>"><?php echo $multiplicador['nombre_multiplicador']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label style="font-weight: bold;">Valor</label>
                                    <input type="text" id="valor" name="valor" class="form-control">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="modal-footer clearfix">
                            <button type="submit" id="submit" name="submit" value="aceptar" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Aceptar</button>
                            <button data-dismiss="modal" class="btn btn-danger pull-left" type="button"><i class="fa fa-times"></i> Cancelar</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- modalMultiplicador -->

        <!-- footer -->
        <div class="footer">
            <div class="footer-inner">
                2015 &copy; HS Puertas de Garage.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- footer -->

        <!-- BEGIN JAVASCRIPTS -->
        <?php include ("includes/js.php") ?>;

<!-- BEGIN GRAPHS -->
<script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<!-- END GRAPHS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="scripts/custom/index.js" type="text/javascript"></script>

<script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="plugins/select2/select2.min.js"></script>

<script>
    var ventas_por_meses = <?php echo getVentasPorMeses(); ?>;

    function showChartTooltip(x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff',
        }).appendTo("body").fadeIn(200);
    }

    function valorMoneda(valor) {
        var params = 'source_currency=' + valor + '&target_currency=MXN';

        $.ajax({
            url: 'ajax/monedaDivesas.php',
            data: params,
            type: 'GET',
            dataType: 'html',
            success: function (respuesta) {
                $('#monedaModal input[id=cambio]').val(respuesta);
            },
            error: function (xhr, status) {
                alert('Disculpe, existe un problema');
            }
        });
    }

    jQuery(document).ready(function () {

        App.init();
        Index.init();
        Index.initCharts();

        if ($('#graph_statistics').size() != 0) {

            $('#graph_statistics_loading').hide();
            $('#graph_statistics_content').show();

            var plot_statistics = $.plot(
                $('#graph_statistics'),
                [
                    {
                        data: ventas_por_meses,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0,
                        },
                        color: ['#f89f9f']
                    },
                    {
                        data: ventas_por_meses,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: "#f89f9f",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    },
                ],
                {
                    xaxis: {
                        tickLength: 0,
                        tickDecimals: 0,
                        mode: "categories",
                        min: 0,
                        font: {
                            lineHeight: 14,
                            style: "normal",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    yaxis: {
                        ticks: 10,
                        tickDecimals: 0,
                        tickColor: "#eee",
                        font: {
                            lineHeight: 14,
                            style: "normal",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    }
                }
            );

            var previousPoint = null;

            $('#graph_statistics').bind('plothover', function (event, pos, item) {

                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2);
                        var y = item.datapoint[1].toFixed(2);

                        showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' ventas');
                    }
                } else {
                    $("#tooltip").remove();

                    previousPoint = null;
                }
            });
        }

        $('.abbrev').each(function(index, element) {
            var abbrev = $(element).attr('id');
            var params = 'source_currency=' + abbrev + '&target_currency=MXN';

            $.ajax({
                url: 'ajax/monedaDivesas.php',
                data: params,
                type: 'GET',
                dataType: 'html',
                success: function (respuesta) {
                    $(element).text(respuesta);
                },
                // c?digo a ejecutar si la petici?n falla;
                error: function (xhr, status) {
                    alert('Disculpe, existi? un problema');
                },
                async: false
            });
        });

        $('#sample_1').dataTable( {
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0 ] }
            ],
            "aaSorting": [[2, 'desc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"]
            ],
            "iDisplayLength": 10,
        });

        $('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-small input-inline");
        $('#sample_1_wrapper .dataTables_length select').addClass("form-control input-small");
        $('#sample_1_wrapper .dataTables_length select').select2();

        $('#monedaModal button[id=submit]').click(function() {
            var moneda = $('#monedaModal select[id=moneda]').val();
            var cambio = $('#monedaModal input[id=cambio]').val();

            var valid = true;

            if (moneda === '') {
                valid = false;
            }

            if (cambio === '') {
                valid = false;
            }

            if (!valid) {
               return false;
            }
        });

        $('#multiplicadorModal button[id=submit]').click(function() {
            var multiplicador = $('#multiplicadorModal select[id=multiplicador]').val();
            var valor = $('#multiplicadorModal input[id=valor]').val();

            var valid = true;

            if (multiplicador === '') {
                valid = false;
            }

            if (valor === '') {
                valid = false;
            }

            if (!valid) {
                return false;
            }
        });

    });

</script>

<!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
