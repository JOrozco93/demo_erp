<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino al cliente con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE clientes SET activo = 0 WHERE id_cliente =" . $id_eliminado;
    consulta($update);
}

// Query principal
$sql = "SELECT c.id_cliente, c.nombre_cliente, c.tipo, c.motivo_costo, c.tipo_costo, c.telefono_contacto, c.email_contacto, c.aprobado, c.motivo_aprobado, u.nombre_usuario, c.saldo FROM clientes AS c, usuarios AS u WHERE c.activo = 1 AND c.tipo = 2 AND c.id_usuario_alta = u.id_usuario ORDER BY c.nombre_cliente ASC";
$query = consulta($sql);
$num = mysql_num_rows($query);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head lang="es">
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">                   
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Clientes</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="listado-clientes.php">
                                        Clientes
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="listado-clientes.php">
                                        Listado de Clientes
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-11"></div>
                                            <div class="col-lg-1">
                                            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2')) { ?> 
                                                <a href="alta-clientes.php?id=0&tipo=cliente"  title="Crear Nuevo Cliente" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                    <i class="fa fa-plus"></i>
                                                </a>   
                                            <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>Nombre del cliente</th>
                                                    <th>Tipo de costo</th>
                                                    <th>Telefono</th>  
                                                    <th>Email</th>
                                                    <!-- <th>Saldo</th> -->
                                                    <th>Compras</th>
                                                    <th>Estatus</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php while ($row = mysql_fetch_array($query)): ?>
                                                    <tr>
                                                        <td><a href="detalle-cliente.php?id=<?php echo $row['id_cliente']; ?>"><?php echo $row['nombre_cliente']; ?></a></td>
                                                        <td><a role="button" class="btn btn-default" data-container="body" onclick="$(this).popover('show')" tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_costo']; ?>"><?php echo $row['tipo_costo']; ?></a></td>
                                                        <td><?php echo $row['telefono_contacto']; ?></td>
                                                        <td><?php echo $row['email_contacto']; ?></td>
                                                        <!-- <td><php echo number_format($row['saldo'], 2, '.', ',');?></td> -->
                                                        <td>
                                                            <?php
                                                                $sql = "SELECT COUNT(*) AS total FROM cotizaciones WHERE autorizada = 1 AND pagada = 2 AND produccion = 1 AND despacho = 1 AND id_cliente = {$row['id_cliente']} AND activo = 1 GROUP BY id_cliente";
                                                                $compras = $pdo->query($sql)->fetch(PDO::FETCH_ASSOC);

                                                                echo (int) $compras['total'];
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($row['aprobado'] == 0): ?>
                                                                <button role="button" data-container="body"  tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_aprobado']; ?>" onclick="$(this).popover('show')" class="btn btn-warning">En proceso</button>
                                                            <?php endif; ?>

                                                            <?php if ($row['aprobado'] == 1): ?>
                                                                <button role="button" data-container="body"  tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_aprobado']; ?>" onclick="$(this).popover('show')" class="btn btn-success">Autorizado</button>
                                                            <?php endif; ?>

                                                            <?php if ($row['aprobado'] == 2): ?>
                                                                <button role="button" data-container="body"  tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_aprobado']; ?>" onclick="$(this).popover('show')" class="btn btn-danger">Declinado</button>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td style="min-width: 120px">
                                                            <div class="btn-group">
                                                                <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')): ?>
                                                                    <a  href="alta-clientes.php?tipo=cliente&id=<?php echo $row['id_cliente']; ?>" title="Editar cliente" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                                                        <i class="fa fa-pencil-square-o"></i>
                                                                    </a>
                                                                <?php endif; ?>

                                                                <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')): ?>
                                                                    <?php if ($row['aprobado'] == 1): ?>
                                                                        <a  href="alta-cotizacion.php?id_cliente=<?php echo $row['id_cliente']; ?>" title="Crear Nueva Cotizacion" data-toggle="tooltip" data-placement="left" class="btn btn-info">
                                                                            <i class="fa fa-file-text"></i>
                                                                        </a>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                
                                                                <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')): ?>
                                                                    <?php if ($row['aprobado'] != 1): ?>
                                                                        <a  onclick="estatus(event,'<?php echo $row['id_cliente']; ?>','<?php echo $row['tipo']; ?>');" title="Aprobar al Cliente" data-toggle="tooltip" data-placement="left" class="btn btn-warning">
                                                                            <i class="fa fa-check-square-o"></i>
                                                                        </a>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                
                                                                <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')): ?>
                                                                    <a onclick="deshabilita(event, '<?php echo $row['id_cliente']; ?>', '<?php echo $row['nombre_cliente']; ?>');" title="Eliminar Cliente" data-toggle="tooltip" data-placement="right" class="btn btn-danger">
                                                                        <i class="fa fa-times"></i>
                                                                    </a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endwhile; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Nombre del Cliente</th>
                                                    <th>Tipo</th>
                                                    <th>Telefono</th>  
                                                    <th>Email</th>
                                                    <!-- <th>Saldo</th> -->
                                                    <th>Compras</th>
                                                    <th>Estatus</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </tfoot>
                                        </table>  
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div><!-- END CONTAINER --> 

        <!-- Includes finales -->
        <?php include ("modal/status-cliente.php"); ?>
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
            jQuery(document).ready(function () {
                App.init(); // initlayout and core plugins
                TableAdvanced.init();
            });

         

            function  deshabilita(event, id, nombre) {
                event.preventDefault();
                var respuesta = confirm('\u00BFDesea eliminar a "' + nombre + '"?');
                if (respuesta) {
                    location.href = 'listado-clientes.php?borrar=' + id;
                }
            }

             function estatus(event, id_cliente, tipo) {
                event.preventDefault();
                $("#id").val(id_cliente);
                $("#tipo").val(tipo);
                $("#modal_status").modal('show');
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>