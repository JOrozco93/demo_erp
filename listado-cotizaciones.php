<?php

if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la cotizacion con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE cotizaciones SET activo = 0 WHERE id_cotizacion =" . $id_eliminado;
    consulta($update);
}

$sql = "SELECT c.autorizada_rechazo, c.id_cliente ,c.id_cotizacion, c.numero_cotizacion, c.pagada, c.url_pago, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario, c.pendiente_motivo, c.aprobada_motivo, c.rechazada_motivo FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 0 AND c.pagada != 2 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha DESC";
$query = consulta($sql);
$num = mysql_num_rows($query);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head lang="es">
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">                   
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Cotizaciones</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="listado-cotizaciones.php">
                                        Cotizaciones
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="listado-cotizaciones.php">
                                        Listado
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="row" style="margin-bottom: 5px;">
                                        <div class="col-md-12">
                                            <a href="alta-cotizacion.php" title="Crear nueva cotizacion" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>No. Cotizacion</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>  
                                                    <th>Cliente</th>                                                  
                                                    <th>Total</th>
                                                    <th>Vendedor</th>
                                                    <th>Estatus</th>
                                                    <th style="width: 200px;">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($num): ?>
                                                    <?php while ($row = mysql_fetch_array($query)): ?>
                                                        <tr>
                                                            <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"> <?php echo $row['numero_cotizacion']; ?> </a></td>
                                                            <td><?php echo $row['fecha']; ?></td>
                                                            <td><?php echo $row['descripcion']; ?></td>
                                                            <td><?php echo $row['nombre_cliente']; ?></td>
                                                            <td>$ <?php echo number_format($row['total'], 2, '.', ','); ?></td>                                                    
                                                            <td><?php echo $row['nombre_usuario']; ?></td>
                                                            <td>
                                                                <?php if ($row['pagada'] == 0): ?>
                                                                    <button class="btn btn-default">Nuevo</button>
                                                                <?php endif; ?>
                                                                
                                                                <?php if ($row['pagada'] == 1): ?>
                                                                    <button data-content="<?php echo $row['pendiente_motivo']; ?>" data-placement="top" data-trigger="focus" data-toogle="popover" onclick="$(this).popover('show')" class="btn btn-warning">Pendiente</button>
                                                                <?php endif; ?>

                                                                <?php if ($row['pagada'] == 2): ?>
                                                                    <button data-content="<?php echo $row['aprobada_motivo']; ?>" data-placement="top" data-trigger="focus" data-toogle="popover" onclick="$(this).popover('show')" class="btn btn-success">Aprobada</button>
                                                                <?php endif; ?>

                                                                <?php if ($row['pagada'] == 3): ?>
                                                                    <button data-content="<?php echo $row['rechazada_motivo']; ?>" data-placement="top" data-trigger="focus" data-toogle="popover" onclick="$(this).popover('show')" class="btn btn-danger">Rechazada</button>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td style="min-width: 100px">
                                                                <div class="btn-group">                                                            
                                                                    <a  href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>" title="Ver Cotizacion" data-toggle="tooltip" data-placement="left" class="btn btn-info">
                                                                        <i class="fa fa-file-text"></i>
                                                                    </a>
                                                                    
                                                                    <button title="Subir comprobante de pago" data-id-cliente="<?php echo $row['id_cliente']; ?>" data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" class="btn btn-success subir-comprobante">
                                                                        <i class="fa">$</i>
                                                                    </button>
                                                                    
                                                                    <!--
                                                                    <a href="duplica-cotizacion.php?id_cotizacion=<?php echo $row['id_cotizacion']; ?>" title="Duplicar Cotizacion" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                                                        <i class="fa fa-copy"></i>
                                                                    </a>
                                                                    -->
                                                                    
                                                                    <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')) { ?>
                                                                        <a onclick="deshabilita(event, '<?php echo $row['id_cliente']; ?>', '<?php echo $row['id_cotizacion']; ?>', '<?php echo $row['nombre_cliente']; ?>');" title="Eliminar cotizacion" data-toggle="tooltip" data-placement="right" class="btn btn-danger">
                                                                            <i class="fa fa-times"></i>
                                                                        </a>
                                                                    <?php } ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No. Cotizacion</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>  
                                                    <th>Cliente</th>                                                  
                                                    <th>Total</th>
                                                    <th>Vendedor</th>
                                                    <th>Estatus</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </tfoot>
                                        </table>  
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>

        <div class='notifications bottom-right'></div>
       
        <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>
        <?php include ("modal/subir-comprobante.php"); ?>
        <?php include ("modal/status-cotizacion.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
            
            $(document).ready(function () {
                App.init();
                TableAdvanced.init();

                function notify(message) {
                    $('.bottom-right').notify({
                        message: { text: message },
                        type: 'bangTidy'
                    }).show(); 
                }

                $('.subir-comprobante').click(function(e) {
                    e.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');
                    var id_cliente = $(this).data('id-cliente');
                    var total_cotizacion = 0.00;
                    var saldo_cliente = 0.00;

                    $("#form_comprobante input[id=id_cotizacion]").val(id_cotizacion);
                    $("#form_comprobante input[id=id_cliente]").val(id_cliente);
                    $("#form_comprobante input[id=total_cotizacion]").val(total_cotizacion);
                    $("#form_comprobante input[id=saldo_cliente]").val(saldo_cliente);

                    $('#form_comprobante input[id=pagoconsaldo]').val('');
                    $('#form_comprobante input[id=file]').val('');

                    $.ajax({
                        url: './ajax/getTotalCotizacion.php',
                        type: 'post',
                        dataType: 'jsonp',
                        data: { id_cotizacion: id_cotizacion },
                        async: false
                    }).done(function(data) {
                        if (!data.error) {
                            total_cotizacion = parseFloat(data.total);

                            $('#form_comprobante input[id=total_cotizacion]').val(parseFloat(data.total).toFixed(2));
                            $('#form_comprobante span[id=spantc]').text(parseFloat(data.total).formatMoney(2, '.', ','));
                        }
                    });

                    /*
                    $.ajax({
                        url: './ajax/getSaldoCliente.php',
                        type: 'post',
                        dataType: 'jsonp',
                        data: { id_cliente: id_cliente },
                        async: false
                    }).done(function(data) {
                        if (!data.error) {
                            saldo_cliente = parseFloat(data.saldo);

                            $('#form_comprobante input[id=saldo_cliente').val(parseFloat(data.saldo).toFixed(2));
                            $('#form_comprobante span[id=spansc]').text(parseFloat(data.saldo).formatMoney(2, '.', ','));
                        }
                    });
                    */

                    /*
                    if (saldo_cliente <= 0) {
                        $('#pagoconsaldo').val(0);

                        $('#pagoconsaldo').parent().hide();
                    } else {
                        if (total_cotizacion <= saldo_cliente) {
                            $('#pagoconsaldo').val(parseFloat(total_cotizacion).toFixed(2));
                        } else if (total_cotizacion > saldo_cliente) {
                            $('#pagoconsaldo').val(parseFloat(saldo_cliente).toFixed(2));
                        }

                        $('#pagoconsaldo').parent().show();   
                    }
                    */
                    
                    $("#subir-comprobante").modal('show');
                });

                $('#form_comprobante button[id=cerrar]').click(function(e) {
                    $("#subir-comprobante").modal('hide');
                });

                $('#form_comprobante button[id=guardar]').click(function(e) {
                    e.preventDefault();

                    var id_cotizacion = $("#form_comprobante input[id=id_cotizacion]").val();
                    var id_cliente = $("#form_comprobante input[id=id_cliente]").val();
                    var total_cotizacion = $("#form_comprobante input[id=total_cotizacion]").val();
                    // var saldo_cliente = $("#form_comprobante input[id=saldo_cliente]").val();

                    // console.log(id_cotizacion, id_cliente, total_cotizacion, saldo_cliente);
                    // return false;

                    var pagoconsaldo = $('#form_comprobante input[id=pagoconsaldo]').val();
                    var file = $('#form_comprobante input[id=file]').val();

                    // console.log(pagoconsaldo, file);
                    // return false;

                    total_cotizacion = parseFloat(total_cotizacion);
                    saldo_cliente = parseFloat(saldo_cliente);

                    /*
                    pagoconsaldo = parseFloat(pagoconsaldo);

                    if (isNaN(pagoconsaldo) ) {
                        notify('Para pagar con saldo debe de introducir una cantidad numerica');
                        
                        return false;
                    }

                    if (pagoconsaldo < 0) {
                        notify('Para pagar con saldo no puede introducir una cantidad negativa');

                        return false;
                    }

                    if (pagoconsaldo > saldo_cliente) {
                        notify('Para pagar con saldo no puede introducir una cantidad mayor a la de su propio saldo');

                        return false;
                    }

                    if (pagoconsaldo > total_cotizacion) {
                        notify('El pago con saldo no puede ser mayor al total de la cotización');

                        return false;    
                    }

                    var comprobante_requerido;
                    
                    if (pagoconsaldo < total_cotizacion) {
                        comprobante_requerido = true;
                    }

                    if (pagoconsaldo == total_cotizacion) {
                        comprobante_requerido = false;
                    }
                    */

                    if (file == '') {
                        notify('No se ha seleccionado un comprobante');
                        
                        return false;
                    }
                    
                    $('#form_comprobante').submit();

                    /*
                    $.ajax({
                        url: "alta/subir-comprobante.php",
                        type: "post",
                        data:  new FormData(this),
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(data) {
                            // $('#form_comprobante').get(0).reset();
                            // $("#subir-comprobante").modal("hide");
                            // $(".modal-backdrop").remove();
                           
                            // location.reload();
                            location.href = 'listado-ventas.php';
                        }, error: function() {
                            alert("error al subir archivo");
                        }
                   });
                    */
                });

            });

            function  deshabilita(event, id_cliente, id_cotizacion, nombre) {
                event.preventDefault();
                var respuesta = confirm('\u00BFDesea eliminar a "' + nombre + '"?');
                if (respuesta) {
                    location.href = 'listado-cotizaciones.php?borrar=' + id_cotizacion;
                }
            }

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>