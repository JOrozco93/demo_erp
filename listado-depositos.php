<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino el deposito con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE depositos SET activo = 0 WHERE id_deposito =" . $id_eliminado;
    consulta($update);
}

$suma = 0;

//Query principal
$sql = "SELECT * FROM clientes WHERE activo=1 AND aprobado=1 ORDER BY id_cliente";
$query = consulta($sql);
$num = mysql_num_rows($query);

$sql1 = "SELECT * FROM depositos WHERE activo=1 ORDER BY id_deposito";
$query1 = consulta($sql1);

$sql1 = "SELECT monto_deposito FROM depositos WHERE monto_deposito=$suma";
$query1 = consulta($sql1);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head lang="es">
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">                   
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Depositos</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-usd"></i>
                                    <a href="listado-depositos.php">
                                        Listado de Depositos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>                                
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-11"></div>
                                            <div class="col-lg-1">
                                                <a href="alta-depositos.php?id=0" title="Crear Nuevo Deposito" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                    <i class="fa fa-plus"></i>
                                                </a>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th style="width: 60%">Nombre del Cliente</th>
                                                <th style="width: 20%">Saldo</th>
                                                <th style="width: 20%">Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if ($num > 0) {
                                                    while ($row = mysql_fetch_array($query)) {
                                                        ?>
                                                        <tr>
                                                            <td><a href="detalle-depositos.php?id=<?php echo $row['id_cliente']; ?>"><?php echo $row['nombre_cliente']; ?> </a></td>                                                      
                                                            <td>
                                                                <?php
                                                                    // echo $query1['monto_deposito'];
                                                                    $sql = "SELECT SUM(monto_deposito) AS saldo FROM depositos WHERE id_cliente = {$row['id_cliente']} GROUP BY id_cliente";
                                                                    $result = mysql_query($sql, $db_con) or die(mysql_error());
                                                                    $suma_depositos = mysql_fetch_assoc($result);
                                                                    echo number_format($suma_depositos['saldo'], 2, '.', ',');
                                                                ?>
                                                            </td>
                                                            <td><?php echo $row['fecha_alta']; ?></td>                                                       
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Nombre del Cliente</th>
                                                <th>Saldo</th>
                                                <th>Fecha</th>
                                            </tr>
                                        </tfoot>
                                    </table>  
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div><!-- END CONTAINER --> 

        <!-- Includes finales -->
        <?php include ("modal/status-cliente.php"); ?>
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
            jQuery(document).ready(function () {
                App.init(); // initlayout and core plugins
                TableAdvanced.init();
            });

            function  deshabilita(event, id, monto_deposito) {
                event.preventDefault();
                var respuesta = confirm('\u00BFDesea eliminar el "' + monto_deposito + '"?');
                if (respuesta) {
                    location.href = 'listado-depositos.php?borrar=' + id;
                }
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>