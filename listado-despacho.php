<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la cotizacion con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE cotizacines SET activo = 0 WHERE id_cotizacion =" . $id_eliminado;
    consulta($update);
}

//Query principal
$sql = "SELECT c.*, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.produccion = 1 AND c.despacho = 0 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario AND c.despacho=0 ORDER BY c.fecha DESC";
$query = consulta($sql);
$num = mysql_num_rows($query);

//Query historial
$sql2 = "SELECT c.*, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.produccion = 1 AND c.despacho = 1 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario AND c.despacho=1 ORDER BY c.fecha DESC";
$query2 = consulta($sql2);
$num2 = mysql_num_rows($query2);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head lang="es">
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Despacho</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="listado-despacho.php">
                                        Despacho
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="listado-despacho.php">
                                        Listado
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-12">&nbsp;</div>

                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        No. Cotizacion
                                                    </th>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Descripción
                                                    </th>
                                                    <th>
                                                        Cliente
                                                    </th>
                                                    <th>
                                                        Tipo de despacho
                                                    </th>
                                                     <th>
                                                        Gu&iacute;a
                                                    </th>
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if ($num > 0) {
                                                while ($row = mysql_fetch_array($query)) {
                                                    ?>
                                                <tr>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"> <?php echo $row['numero_cotizacion']; ?> </a></td>
                                                    <td><?php echo $row['fecha']; ?></td>
                                                    <td><?php echo $row['descripcion']; ?></td>
                                                    <td><?php echo $row['nombre_cliente']; ?></td>
                                                    <td><?php if ($row['tipo_despacho'] == '1') { ?>Envío a cliente
                                                            <?php }else if ($row['tipo_despacho'] == '2') {?> Recolección en oficina
                                                            <?php }else if ($row['tipo_despacho'] == '3') {?> Instalación HS
                                                            <?php }else  {?> Sin Status <?php }?>
                                                    </td>
                                                    <td><?php echo $row['num_guia']; ?></td>
                                                    <td><?php echo $row['nombre_usuario']; ?></td>                                                    
                                                    <td style="min-width: 120px">
                                                        <div class="btn-group">
                                                            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '8')) { ?>
                                                                <a onclick="estatus(event, '<?php echo $row['id_cliente']; ?>', '<?php echo $row['id_cotizacion']; ?>');" title="Entregar pedido" data-toggle="tooltip" data-placement="left" class="btn btn-warning">
                                                                    <i class="fa fa-check-square-o"></i>
                                                                </a>
                                                            <?php } ?>
                                                            
                                                            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '8')) { ?>
                                                                <a onclick="deshabilita(event, '<?php echo $row['id_cliente']; ?>', '<?php echo $row['nombre_cliente']; ?>');" title="Eliminar cotizacion" data-toggle="tooltip" data-placement="right" class="btn btn-danger">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
                                                }
                                             }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>
                                                        No. Cotizacion
                                                    </th>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Descripción
                                                    </th>
                                                    <th>
                                                        Cliente
                                                    </th>
                                                    <th>
                                                        Tipo de despacho
                                                    </th>
                                                    <th>
                                                        Gu&iacute;a
                                                    </th>
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>

                                        <h4>Historial de despachos</h4>
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="historial_despachos">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        No. Cotizacion
                                                    </th>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Descripción
                                                    </th>
                                                    <th>
                                                        Cliente
                                                    </th>
                                                    <th>
                                                        Tipo de despacho
                                                    </th>
                                                    <th>
                                                        Gu&iacute;a
                                                    </th>
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if ($num2 > 0) {
                                                while ($row = mysql_fetch_array($query2)) {
                                                    ?>
                                                <tr>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"> <?php echo $row['numero_cotizacion']; ?> </a></td>
                                                    <td><?php echo $row['fecha']; ?></td>
                                                    <td><?php echo $row['descripcion']; ?></td>
                                                    <td><?php echo $row['nombre_cliente']; ?></td>
                                                    <td>
                                                        <?php if ($row['tipo_despacho'] == '1') { ?>Envío a cliente
                                                        <?php } else if ($row['tipo_despacho'] == '2') { ?> Recolección en oficina
                                                        <?php } else if ($row['tipo_despacho'] == '3') { ?> Instalación HS
                                                        <?php } else {?> Sin Status <?php }?>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $row['num_guia']; ?></span>
                                                        <button data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" class="btn btn-warning btn-xs pull-right edit-guide-number"><i class="fa fa-pencil-square-o"></i></button>
                                                    </td>
                                                    <td><?php echo $row['nombre_usuario']; ?></td>
                                                </tr>
                                            <?php
                                                }
                                             }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>
                                                        No. Cotizacion
                                                    </th>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Descripción
                                                    </th>
                                                    <th>
                                                        Cliente
                                                    </th>
                                                    <th>
                                                        Tipo de despacho
                                                    </th>
                                                     <th>
                                                        Gu&iacute;a
                                                    </th>
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>

                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>

        <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>
        <?php include ("modal/status-despacho.php"); ?>
        <?php include ("modal/edit-guide-number.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
            
            $(document).ready(function () {
                App.init();
                TableAdvanced.init();

                $('.edit-guide-number').click(function() {
                    var numero_guia = '';
                    var id_cotizacion = $(this).data('id-cotizacion');

                    $('#edit-guide-number-form input[id=numguia_ng]').val(numero_guia);
                    $('#edit-guide-number-form input[id=id_cotizacion_ng]').val(id_cotizacion);

                    $("#edit-guide-number-modal").modal('show');
                });

                $('#edit-guide-number-modal button[id=btn]').click(function() {
                    var numero_guia = $('#edit-guide-number-form input[id=numguia_ng]').val();

                    if (!numero_guia) {
                        return false;
                    }

                    $('#edit-guide-number-form').submit();
                });
            });

            function  deshabilita(event, id, nombre) {
                event.preventDefault();

                var respuesta = confirm('\u00BFDesea eliminar a "' + nombre + '"?');
                
                if (respuesta) {
                    location.href = 'listado-cotizaciones.php?borrar=' + id;
                }
            }

            function estatus(event, id_cliente, id_cotizacion) {
                event.preventDefault();

                $("#id_cliente").val(id_cliente);
                $("#id_cotizacion").val(id_cotizacion);
                $("#modal_status_despacho").modal('show');
            }

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
