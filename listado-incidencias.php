<?php

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == NULL)) {
        header("Location: logout.php");
    }

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    $id_usuario = $_SESSION['id_usuario'];

    if (isset($_GET['borrar'])) {
        $id_eliminado = $_GET['borrar'];
        $detalle = "El usuario con el id " . $id_usuario . " elimino al cliente con el id " . $id_eliminado . " ";
        $evento = "Eliminar";
        $id_log = actualizalog($id_usuario, $evento, $detalle);
        $update = "UPDATE incidencias SET activo = 0 WHERE id_incidencia = " . $id_eliminado;
        consulta($update);
    }

    // Query principal
    // $sql = "SELECT c.id_cotizacion, c.numero_cotizacion, c.numero_cotizacion, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo=1 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario  ORDER BY c.id_cotizacion ASC";
    $sql = "SELECT incidencias.id_incidencia AS id_incidencia, cotizaciones.numero_cotizacion AS numero_cotizacion, DATE_FORMAT(incidencias.fecha, '%Y-%m-%d') AS fecha, incidencias.descripcion AS descripcion, clientes.nombre_cliente AS nombre_cliente, incidencias.status AS status, incidencias.motivo AS motivo FROM incidencias, cotizaciones, clientes WHERE incidencias.activo = 1 AND incidencias.id_cotizacion = cotizaciones.id_cotizacion AND cotizaciones.id_cliente = clientes.id_cliente ORDER BY incidencias.id_incidencia";

    $query = consulta($sql);
    $num = mysql_num_rows($query);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head lang="es">
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">                   
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Incidencias</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="listado-incidencias.php">
                                        Incidencias
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="listado-incidencias.php">
                                        Listado de Incidencias
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div>
                                        <a href="alta-incidencia.php?id=0"  title="Crear Nuevo Cliente" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                            <i class="fa fa-plus"></i>
                                        </a> 
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table id="sample_1" class="table table-striped table-bordered table-hover table-full-width">
                                        <thead>
                                            <tr>
                                                <th>No. Cotizacion</th>
                                                <th>Fecha</th>
                                                <th>Cliente</th>  
                                                <th>Descripción</th>
                                                <th>Estatus</th>
                                                <th style="width: 200px;">Acciones</th>                                          
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($num > 0) { ?>
                                                <?php while ($row = mysql_fetch_array($query)) { ?>
                                                    <tr>
                                                        <td><a href="detalle-incidencia.php?id=<?php echo $row['id_incidencia']; ?>"> <?php echo $row['numero_cotizacion']; ?></a></td>
                                                        <td><?php echo $row['fecha']; ?></td>
                                                        <td><?php echo $row['descripcion']; ?></td>
                                                        <td><?php echo $row['nombre_cliente']; ?></td>
                                                        <td>
                                                            <button role="button" data-container="body"  tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="<?php echo $row['motivo']; ?>" onclick="$(this).popover('show')" class="btn btn-default">
                                                                <?php if ($row['status'] == 0) { ?>
                                                                    Pendiente
                                                                <?php } ?>

                                                                <?php if ($row['status'] == 1) { ?>
                                                                    Aceptado
                                                                <?php } ?>

                                                                <?php if ($row['status'] == 2) { ?>
                                                                    Rechazado
                                                                <?php } ?>

                                                                <?php if ($row['status'] == 3) { ?>
                                                                    Cerrado
                                                                <?php } ?>

                                                                <?php if ($row['status'] == 4) { ?>
                                                                    Cancelado
                                                                <?php } ?>
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <a href="alta-incidencia.php?id=<?php echo $row['id_incidencia']; ?>" title="Mostrar Incidencia" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                </a>

                                                                <a href="#" title="Enviar Incidencia" data-toggle="modal" class="btn btn-warning" onclick="estatus(event,'<?php echo $row['id_incidencia']; ?>','<?php echo $row['status']; ?>');">
                                                                    <i class="fa fa-check-square-o"></i>
                                                                </a>

                                                                <a onclick="deshabilita(event, '<?php echo $row['id_incidencia']; ?>', '<?php echo $row['descripcion']; ?>');" title="Eliminar Incidencia" data-toggle="tooltip" data-placement="right" class="btn btn-danger">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                                                            </div>                                           
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No. Cotizacion</th>
                                                <th>Fecha</th>
                                                <th>Cliente</th>
                                                <th>Descripción</th> 
                                                <th>Estatus</th>
                                                <th>Acciones</th>                                          
                                            </tr>
                                        </tfoot>
                                    </table>  
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div><!-- END CONTAINER --> 

        <!-- Includes finales -->

        <?php include ("modal/status-incidencia.php"); ?>

        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
            jQuery(document).ready(function () {
                App.init();

                $('#sample_1').dataTable({
                    "aaSorting": [[1, 'desc']],
                     "aLengthMenu": [
                        [5, 15, 20, -1],
                        [5, 15, 20, "All"] // change per page values here
                    ],
                    // set the initial value
                    "iDisplayLength": 10,
                });

                $('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-small input-inline");
                $('#sample_1_wrapper .dataTables_length select').addClass("form-control input-small");
                $('#sample_1_wrapper .dataTables_length select').select2();
            });

            function  deshabilita(event, id, descripcion) {
                event.preventDefault();
                
                var respuesta = confirm('\u00BFDesea eliminar la "' + descripcion + '"?');
                
                if (respuesta) {
                    location.href = 'listado-incidencias.php?borrar=' + id;
                }
            }

            function estatus(event, id_incidencia, status) {
                event.preventDefault();

                $("#id_modal_incidencias").val(id_incidencia);
                $("#status_modal_incidencias").val(status);

                $("#modal_status_incidencia").modal('show');
            }

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>