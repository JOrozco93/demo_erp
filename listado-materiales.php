<?php

if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("location: logout.php");
    exit();
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['id'])) {
    $id_cotizacion = (int) $_GET['id'];
} else {
    header("location: listado-produccion.php");
    exit();
}

// $sql = "SELECT * FROM cotizaciones WHERE id_cotizacion = $id_cotizacion";
// $cotizacion = consulta($sql);

$sql = "SELECT c.*, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.produccion = 0 AND c.despacho = 0 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario AND c.id_cotizacion = $id_cotizacion ORDER BY c.fecha DESC";
$cotizacion = $pdo->query($sql)->fetch(PDO::FETCH_ASSOC);

// echo '<pre>' . print_r($cotizacion, 1) . '</pre>';
// exit;

$sql = "SELECT * FROM conceptos WHERE id_cotizacion = $id_cotizacion";
$materiales = consulta($sql);

$sql = "SELECT SUM(cantidad) AS cantidad, id_puerta, id_ptipo, m_ancho, m_alto, f_ancho, in_ancho, f_alto, in_alto, id_pmodelo, id_pcolor, id_pmovimiento, id_ptextura, id_psello, id_pmotor, id_ventana, id_vcolor, nventanas FROM conceptos WHERE id_puerta IS NOT NULL AND id_cotizacion = $id_cotizacion GROUP BY id_puerta, id_ptipo, id_pmodelo, id_pcolor, id_pmovimiento, id_ptextura, id_psello, id_pmotor, id_ventana, id_vcolor ORDER BY cantidad DESC";
$puertas = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$sql = "SELECT SUM(cantidad) AS cantidad, id_kit FROM conceptos WHERE id_kit IS NOT NULL AND id_cotizacion = $id_cotizacion GROUP BY id_kit ORDER BY cantidad DESC";
$kits = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$sql = "SELECT SUM(cantidad) AS cantidad, id_componente, id_grupo_componente FROM conceptos WHERE id_componente IS NOT NULL AND id_cotizacion = $id_cotizacion GROUP BY id_componente, id_grupo_componente ORDER BY cantidad DESC";
$componentes = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

// ---------- Agrupar y Sumar todos los componentes necesarios ----------

$concentrado_componentes = array();

// -- Agregar al concentrado los componentes de cada puerta en la cotización

foreach($puertas as $row) {
    $sql = "SELECT * FROM puerta_componente WHERE id_puerta = {$row['id_puerta']}";
    $puerta_componentes = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    // echo '<pre>' . print_r($puerta_componentes, 1) . '</pre>';

    foreach($puerta_componentes as $pc) {
        $concentrado_componentes[] = array(                                                                                    
            'id_componente' => $pc['id_componente'],
            'cantidad' => $pc['cantidad'] * $row['cantidad'],
            '*' => "componente de puerta: {$pc['id_puerta']}"
        );
    }
}

// echo '<pre>' . print_r($concentrado_componentes, 1) . '</pre>';


// -- Agregar al concentrado los componentes de cada kit en la cotización

foreach($kits as $row) {
    $sql = "SELECT * FROM kit_componente WHERE id_kit = {$row['id_kit']}";
    $kit_componentes = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    // echo '<pre>' . print_r($kit_componentes, 1) . '</pre>';

    foreach($kit_componentes as $kc) {
        $concentrado_componentes[] = array(                                                                                    
            'id_componente' => $kc['id_componente'],
            'cantidad' => $kc['cantidad_kit_componente'] * $row['cantidad'],
            '*' => "componente de kit: {$kc['id_kit']}"
        );
    }
}

// echo '<pre>' . print_r($concentrado_componentes, 1) . '</pre>';

// -- Agregar al concentrado los componentes en la cotización

foreach($componentes as $c) {
    // echo '<pre>' . print_r($c, 1) . '</pre>';

    $concentrado_componentes[] = array(                                                                                    
        'id_componente' => $c['id_componente'],
        'cantidad' => $c['cantidad'],
        '*' => "componente: {$c['id_componente']}"
    );
}

// echo '<pre>' . print_r($concentrado_componentes, 1) . '</pre>';

// -- Recolectar todos los id de los componentes en el concentrado

$recolector = array();

foreach($concentrado_componentes as $componente) {
    $recolector[] = $componente['id_componente'];
}

// echo '<pre>' . print_r($recolector, 1) . '</pre>';

// -- Extraer los id unicos del recolector

$unicos = array_unique($recolector);
sort($unicos);

// echo '<pre>' . print_r($unicos, 1) . '</pre>';

// -- Totalizar la cantidad de los componentes

$componentes_totalizados = array();

foreach($unicos as $id_componente) {
    $cantidad = 0;

    foreach($concentrado_componentes as $indice => $componente) {
        if ($id_componente == $componente['id_componente']) {
            $cantidad = $cantidad + $componente['cantidad'];

            unset($concentrado_componentes[$indice]);
        }
    }

    $componentes_totalizados[] = array(
        'id_componente' => $id_componente,
        'cantidad' => $cantidad
    );
}

// echo '<pre>' . print_r($componentes_totalizados, 1) . '</pre>';

$componentes = $componentes_totalizados;

?>
 
<!DOCTYPE html>
<!--[if IE 8]><html lang="es" class="ie8 no-js"><![endif]-->
<!--[if IE 9]><html lang="es" class="ie9 no-js"><![endif]-->
<!--[if !IE]><!--><html lang="en"><!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>

        <!-- page-container -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- page-content-wrapper -->
            <div class="page-content-wrapper">
                <!-- page-content -->
                <div class="page-content">

                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Producción</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="listado-produccion.php">
                                        Producción
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="listado-produccion.php">
                                        Listado de Materiales
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->

                            <!-- COTIZACIÓN -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="well" style="background: #efefef; border-bottom: 2px solid #bfbfbf;">
                                        <h4><a href="detalle-cotizacion.php?id=<?php echo $cotizacion['id_cotizacion']; ?>"><?php echo $cotizacion['numero_cotizacion']; ?></a></h4>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div style="margin-bottom: 10px;">
                                                    <p><strong>Fecha</strong></p>
                                                    <p><?php echo $cotizacion['fecha']; ?></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div style="margin-bottom: 10px;">
                                                    <p><strong>Descripción de la cotización</strong></p>
                                                    <p><?php echo $cotizacion['descripcion']; ?></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div style="margin-bottom: 10px;">
                                                    <p><strong>Cliente</strong></p>
                                                    <p><?php echo $cotizacion['nombre_cliente']; ?></p>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div style="margin-bottom: 10px;">
                                                    <p><strong>Vendedor</strong></p>
                                                    <p><?php echo $cotizacion['nombre_usuario']; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- COTIZACIÓN -->

                            <!-- ELEMENTOS COTIZADOS -->

                            <!-- Puertas -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4>Puertas</h4>
                                        </div>
                                        <div class="panel-body">
                                            <?php if(count($puertas)): ?>
                                                <table id="table-puertas" class="table table-striped table-bordered">
                                                    <thead>
                                                        <th style="min-width: 10%;">Cantidad</th>
                                                        <th style="min-width: 90%;">Descripción</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($puertas as $row): ?>
                                                            <tr>
                                                                <td><?php echo $row['cantidad']; ?></td>
                                                                <td>
                                                                    <?php
                                                                        // tipo de puerta
                                                                        $sql = "SELECT * FROM puerta_tipo WHERE id_ptipo = {$row['id_ptipo']}  AND activo = 1";
                                                                        $ptipo = mysql_fetch_array(consulta($sql));

                                                                        // puerta
                                                                        $sql = "SELECT * FROM puerta WHERE id_puerta = {$row['id_puerta']} AND activo = 1";
                                                                        $puerta = mysql_fetch_array(consulta($sql));

                                                                        // modelo de la puerta
                                                                        $sql = "SELECT * FROM puerta_modelo WHERE id_pmodelo = {$row['id_pmodelo']} AND activo = 1";
                                                                        $pmodelo = mysql_fetch_array(consulta($sql));

                                                                        // color de la puerta
                                                                        $sql = "SELECT * FROM puerta_color WHERE id_pcolor = {$row['id_pcolor']} AND activo = 1";
                                                                        $pcolor = mysql_fetch_array(consulta($sql));

                                                                        // movimiento de la puerta
                                                                        $sql = "SELECT * FROM puerta_movimiento WHERE id_pmovimiento = {$row['id_pmovimiento']}  AND activo = 1";
                                                                        $pmovimiento = mysql_fetch_array(consulta($sql));

                                                                        // textura de la puerta
                                                                        $sql = "SELECT * FROM puerta_textura WHERE id_ptextura = {$row['id_ptextura']} AND activo = 1";
                                                                        $ptextura = mysql_fetch_array(consulta($sql));

                                                                        // sello de la puerta
                                                                        $sql = "SELECT * FROM puerta_sello WHERE id_psello = {$row['id_psello']} AND activo = 1";
                                                                        $psello = mysql_fetch_array(consulta($sql));
                                                                        
                                                                        // motor de la puerta
                                                                        $sql = "SELECT * FROM puerta_motor WHERE id_pmotor = {$row['id_pmotor']} AND activo = 1";
                                                                        $pmotor = mysql_fetch_array(consulta($sql));

                                                                        // ventana de la puerta
                                                                        $sql = "SELECT * FROM ventana WHERE id_ventana = {$row['id_ventana']} AND activo = 1";
                                                                        $pventana = mysql_fetch_array(consulta($sql));

                                                                        // color de la ventana
                                                                        $sql = "SELECT * FROM ventana_color WHERE id_vcolor = {$row['id_vcolor']} AND activo = 1";
                                                                        $pvcolor = mysql_fetch_array(consulta($sql));

                                                                        $nventanas = $row['nventanas'];
                                                                    ?>
                                                                    <div>
                                                                        <strong>Tipo:</strong> <?php echo $ptipo['nombre_tipo']; ?> |
                                                                        <strong>Medidas:</strong>

                                                                        <?php if ($row['m_ancho'] == 0 && $row['m_alto'] == 0 && $row['f_ancho'] == 0 && $row['in_ancho'] == 0 && $row['f_alto'] == 0 && $row['in_alto'] == 0) { ?>
                                                                            <?php echo $puerta['m_ancho'] . 'm' . 'x' . $puerta['m_alto'] . 'm'; ?>
                                                                        <?php } ?>

                                                                        <?php if ($row['m_ancho'] != 0 || $row['m_alto'] != 0) { ?>
                                                                            <?php echo $row['m_ancho'] . 'm' . 'x' . $row['m_alto'] . 'm'; ?>
                                                                        <?php } ?>

                                                                        <?php if ($row['f_ancho'] != 0 || $row['in_ancho'] != 0 || $row['f_alto'] != 0 || $row['in_alto'] != 0) { ?>
                                                                            <?php echo $row['f_ancho'] . '\'' . $row['in_ancho'] . '"' . 'x' . $row['f_alto'] . '\'' . $row['in_alto'] . "\""; ?>
                                                                        <?php } ?>

                                                                        |

                                                                        <strong>Modelo:</strong> <?php echo $pmodelo['nombre_modelo']; ?> |
                                                                        <strong>Color:</strong> <?php echo $pcolor['nombre_color']; ?> |
                                                                        <strong>Movimiento:</strong> <?php echo $pmovimiento['nombre_movimiento']; ?> |
                                                                        <strong>Textura:</strong> <?php echo $ptextura['nombre_textura']; ?> |
                                                                        <strong>Sello:</strong> <?php echo $psello['nombre_sello']; ?> |
                                                                        <strong>Motor:</strong> <?php echo $pmotor['nombre_motor']; ?> |
                                                                        <strong>Ventana:</strong> <?php echo $pventana['nombre_ventana'] ? $pventana['nombre_ventana'] : 'Sin Ventana'; ?> |
                                                                        <strong>Color ventana:</strong> <?php echo $pvcolor['nombre_color'] ? $pvcolor['nombre_color'] : 'Sin Color'; ?> |

                                                                        <?php if ($nventanas > 0): ?>
                                                                            <strong># ventanas:</strong> <?php echo $nventanas ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php else: ?>
                                                <div class="alert alert-warning" role="alert">No se han agregado puertas a la cotización</div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Puertas -->

                            <!-- Kits -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4>Kits</h4>
                                        </div>
                                        <div class="panel-body">
                                            <?php if (count($kits)): ?>
                                                <table id="table-kits" class="table table-striped table-bordered">
                                                    <thead>
                                                        <th style="min-width: 10%;">Cantidad</th>
                                                        <th style="min-width: 90%;">Descripción</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($kits as $row): ?>
                                                            <tr>
                                                                <td><?php echo $row['cantidad']; ?></td>
                                                                <td>
                                                                    <?php
                                                                        // kit
                                                                        $sql = "SELECT * FROM kit WHERE id_kit = {$row['id_kit']}  AND activo = 1";
                                                                        $kit = mysql_fetch_array(consulta($sql));
                                                                    ?>
                                                                    <div>
                                                                        <strong>Kit:</strong> <?php echo $kit['nombre_kit']; ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php else: ?>
                                                <div class="alert alert-warning" role="alert">No se han agregado kits a la cotización</div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Kits -->

                            <!-- Componentes -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4>Componentes</h4>
                                        </div>
                                        <div class="panel-body">
                                            <?php if(count($componentes)): ?>
                                                <table id="table-componentes" class="table table-striped table-bordered">
                                                    <thead>
                                                        <th style="min-width: 10%;">Cantidad</th>
                                                        <th style="min-width: 90%;">Descripción</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($componentes as $row): ?>
                                                            <tr>
                                                                <td><?php echo $row['cantidad']; ?></td>
                                                                <td>
                                                                    <?php
                                                                        // componente
                                                                        $sql = "SELECT componentes.nombre_componente AS nombre_componente, componentes_grupos.grupo_componente AS grupo_componente FROM componentes, componentes_grupos WHERE componentes.id_componente_grupo = componentes_grupos.id_grupo_componente AND componentes.id_componente = {$row['id_componente']} AND componentes.activo = 1";
                                                                        $componente = mysql_fetch_array(consulta($sql));
                                                                    ?>
                                                                    <div>
                                                                        <strong>Grupo:</strong> <?php echo $componente['grupo_componente']; ?> |
                                                                        <strong>Componente:</strong> <?php echo $componente['nombre_componente']; ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php else: ?>
                                                <div class="alert alert-warning" role="alert">No se han agregado componentes a la cotización</div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Componentes -->
                            
                            <!-- ELEMENTOS COTIZADOS -->
                        
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                </div>
                <!-- page-content -->
            </div>
            <!-- page-content-wrapper -->
        </div>
        <!-- page-container -->

         <!-- END CONTAINER -->
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>
        <?php include ("modal/status-produccion.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>

        <script>
            jQuery(document).ready(function () {
                App.init();

                $('#table-puertas').dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "aaSorting": [[0, 'desc']]
                });

                $('#table-kits').dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "aaSorting": [[0, 'desc']]
                });

                $('#table-componentes').dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "aaSorting": [[0, 'desc']]
                });
            });
        </script>

    </body>
</html>