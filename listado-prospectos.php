<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino al prospecto con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE clientes SET activo = 0 WHERE id_cliente =" . $id_eliminado;
    consulta($update);
}

//Query principal
$sql = "SELECT c.id_cliente, c.nombre_cliente, c.tipo, m.nombre_multiplicador, c.motivo_costo, c.telefono_contacto, c.email_contacto, c.aprobado, c.motivo_aprobado, u.nombre_usuario FROM clientes AS c, usuarios AS u, multiplicadores AS m WHERE c.activo=1 AND c.tipo=1 AND c.id_usuario_alta = u.id_usuario AND c.tipo_costo = m.nombre_multiplicador ORDER BY c.nombre_cliente ASC";
$query = consulta($sql);
$num = mysql_num_rows($query);

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head lang="es">
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">                   
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title"> Prospectos </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="listado-clientes.php">
                                        Clientes
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="listado-prospectos.php">
                                        Listado de Prospectos
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-11"></div>
                                            <div class="col-lg-1">
                                                <a href="alta-clientes.php?id=0&tipo=prospecto"  title="Crear Nuevo Prospecto" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                    <i class="fa fa-plus"></i>
                                                </a>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form method="POST" id="clienteVtasForm" name="clienteVtasForm" action="includes/clienteVtasForm.php">
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Nombre del Prospecto
                                                    </th>
                                                    <th>
                                                        Tipo
                                                    </th>
                                                    <th>
                                                        Telefono
                                                    </th>  
                                                    <th>
                                                        Email
                                                    </th>                                                  
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                    <th>
                                                        Estatus
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php                                                
                                            if ($num > 0) {
                                                while ($row = mysql_fetch_array($query)) {
                                                    ?>
                                                <tr>
                                                    <td><a href="detalle-prospecto.php?id=<?php echo $row['id_cliente']; ?>"> <?php echo $row['nombre_cliente']; ?> </a></td>
                                                    <td><a role="button" class="btn btn-default" data-container="body" onclick="$(this).popover('show')" tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_costo']; ?>">
                                                      <?php echo $row['nombre_multiplicador']; ?></a></td>
                                                    <td><?php echo $row['telefono_contacto']; ?></td>
                                                    <td><?php echo $row['email_contacto']; ?></td>
                                                    <td><?php echo $row['nombre_usuario']; ?></td>
                                                    <td>
                                                        <?php if ($row['aprobado'] == 0): ?>
                                                            <a role="button" data-container="body"  tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_aprobado']; ?>" onclick="$(this).popover('show')" class="btn btn-warning">En proceso</a>
                                                        <?php endif; ?>

                                                        <?php if ($row['aprobado'] == 1): ?>
                                                            <a role="button" data-container="body"  tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_aprobado']; ?>" onclick="$(this).popover('show')" class="btn btn-success">Autorizado</a>
                                                        <?php endif; ?>

                                                        <?php if ($row['aprobado'] == 2): ?>
                                                            <a role="button" data-container="body"  tabindex="0" data-toggle="popover" data-trigger="focus"  data-placement="top" data-content="<?php echo $row['motivo_aprobado']; ?>" onclick="$(this).popover('show')" class="btn btn-danger">Declinado</a>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td style="min-width: 120px">
                                                        <div class="btn-group" role="group">                                                            
                                                             <a  href="alta-clientes.php?tipo=prospecto&id=<?php echo $row['id_cliente']; ?>" title="Editar cliente" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                </a>
                                                            <?php if ($row['aprobado'] == '1') { ?>
                                                                <a  href="alta-cotizacion.php?cliente=<?php echo $row['id_cliente']; ?>" title="Crear Nueva Cotizacion" data-toggle="tooltip" data-placement="left" class="btn btn-info">
                                                                    <i class="fa fa-file-text"></i>
                                                                </a>
                                                            <?php } ?>
                                                            <?php if (($row['aprobado'] == '0'|| $row['aprobado'] == '2') && (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4'))) { ?>
                                                                <a  onclick="estatus(event,'<?php echo $row['id_cliente']; ?>','<?php echo $row['tipo']; ?>');" title="Aprobar al Prospecto" data-toggle="tooltip" data-placement="left" class="btn btn-warning">
                                                                    <i class="fa fa-check-square-o"></i>
                                                                </a>
                                                            <?php } ?>
                                                            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4')) { ?>
                                                                <a onclick="deshabilita(event, '<?php echo $row['id_cliente']; ?>', '<?php echo $row['nombre_cliente']; ?>');" title="Eliminar Cliente" data-toggle="tooltip" data-placement="right" class="btn btn-danger">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                                                            <?php } ?>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                            <?php                                                          
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>
                                                        Nombre del Prospecto
                                                    </th>
                                                    <th>
                                                        Tipo
                                                    </th>
                                                    <th>
                                                        Telefono
                                                    </th>  
                                                    <th>
                                                        Email
                                                    </th>                                                  
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                    <th>
                                                        Estatus
                                                    </th>
                                                    <th>
                                                        &nbsp;
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>                                        
                                    </form>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
       
        <!-- END CONTAINER -->
        <?php include ("modal/status-cliente.php"); ?>
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>
                                jQuery(document).ready(function () {
                                    App.init(); // initlayout and core plugins
                                    TableAdvanced.init();
                                });

                                function  ir(nom_cliente) {
                                    document.forms["clienteVtasForm"]["clientes"].value = nom_cliente;
                                    document.forms["clienteVtasForm"].submit();
                                }

                                function  deshabilita(event, id, nombre) {
                                    event.preventDefault();
                                    var respuesta = confirm('\u00BFDesea Eliminar al Cliente "' + nombre + '"?');
                                    if (respuesta) {
                                        location.href = 'listado-prospectos.php?borrar=' + id;
                                    }
                                }

                                function estatus(event, id_cliente, tipo) {
                                    event.preventDefault();
                                    $("#id").val(id_cliente);
                                    $("#tipo").val(tipo);
                                    $("#modal_status").modal('show');
                                }

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>