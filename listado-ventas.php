<?php
if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");
require_once("includes/validacion.php");

$id_usuario = $_SESSION['id_usuario'];

if (isset($_GET['borrar'])) {
    $id_eliminado = $_GET['borrar'];
    $detalle = "El usuario con el id " . $id_usuario . " elimino la cotizacion con el id " . $id_eliminado . " ";
    $evento = "Eliminar";
    $id_log = actualizalog($id_usuario, $evento, $detalle);
    $update = "UPDATE cotizaciones SET activo = 0 WHERE id_cotizacion = $id_eliminado";
    consulta($update);
}

//Query principal
$sql = "SELECT c.*, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada != 2 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha DESC";
$query = consulta($sql);
$num = mysql_num_rows($query);

//Query historial
$sql2 = "SELECT c.*, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario, c.url_pago FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario ORDER BY c.fecha DESC";
$query2 = consulta($sql2);
$num2 = mysql_num_rows($query2);

?> 
<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head lang="es">
        <meta charset="utf-8"/>
        <title><?php echo $name ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">                   
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Ventas</h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="listado-ventas.php">
                                        Ventas
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="listado-ventas.php">
                                        Listado
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="btn-group col-lg-12">
                                        <div class="input-group-btn">
                                            <div class="col-lg-12">&nbsp;</div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        No. Cotizacion
                                                    </th>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Descripción
                                                    </th>  
                                                    <th>
                                                        Cliente
                                                    </th>                                                  
                                                    <th>
                                                        Total
                                                    </th>
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                    <th style="width: 200px">
                                                        Acciones
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php                                                
                                            if ($num > 0) {
                                                while ($row = mysql_fetch_array($query)) {
                                                    ?>
                                                <tr>
                                                    <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"> <?php echo $row['numero_cotizacion']; ?> </a></td>
                                                    <td><?php echo $row['fecha']; ?></td>
                                                    <td><?php echo $row['descripcion']; ?></td>
                                                    <td><?php echo $row['nombre_cliente']; ?></td>
                                                    <td><?php echo '$ ' . number_format($row['total'], 2, '.', ','); ?></td>
                                                    <td><?php echo $row['nombre_usuario']; ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php if ($row['autorizada'] == '1' AND (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4') || ($_SESSION['permiso_usuario'] == '6') || ($_SESSION['permiso_usuario'] == '9'))) { ?>
                                                                <button title="Aprobar Cotización" data-id-cliente="<?php echo $row['id_cliente']; ?>" data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" class="btn btn-warning aprobar-cotizacion">
                                                                    <i class="fa fa-check-square-o"></i>
                                                                </button>
                                                            <?php } ?>
                                                            
                                                            <?php if (($_SESSION['permiso_usuario'] == '1') || ($_SESSION['permiso_usuario'] == '2') || ($_SESSION['permiso_usuario'] == '4') || ($_SESSION['permiso_usuario'] == '6') || ($_SESSION['permiso_usuario'] == '9')) { ?>
                                                                <a onclick="deshabilita(event, '<?php echo $row['id_cotizacion']; ?>', '<?php echo $row['nombre_cliente']; ?>');" title="Eliminar cotizacion" data-toggle="tooltip" data-placement="right" class="btn btn-danger">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php                                                          
                                                }
                                             }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>
                                                        No. Cotizacion
                                                    </th>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Descripción
                                                    </th>  
                                                    <th>
                                                        Cliente
                                                    </th>                                                  
                                                    <th>
                                                        Total
                                                    </th>                                                    
                                                    <th>
                                                        Vendedor
                                                    </th>
                                                    <th>
                                                        Acciones
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>  
                                        <h4>Historial de ventas</h4>
                                        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
                                            <thead>
                                                <tr>
                                                    <th>No. Cotizacion</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>  
                                                    <th>Cliente</th>                                                  
                                                    <th>Total</th>
                                                    <th>Vendedor</th>
                                                    <th>Estatus</th>                                                    
                                                    <th style="width: 200px">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php                                                
                                                    if ($num2 > 0) {
                                                        while ($row = mysql_fetch_array($query2)) {
                                                ?>
                                                            <tr>
                                                                <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>"> <?php echo $row['numero_cotizacion']; ?></a></td>
                                                                <td><?php echo $row['fecha']; ?></td>
                                                                <td><?php echo $row['descripcion']; ?></td>
                                                                <td><?php echo $row['nombre_cliente']; ?></td>
                                                                <td>$ <?php echo number_format($row['total'], 2, '.', ','); ?></td>                                                    
                                                                <td><?php echo $row['nombre_usuario']; ?></td>
                                                                <td>
                                                                    <?php if ($row['pagada'] == '1'): ?>
                                                                        Pendiente
                                                                    <?php endif; ?>                       
                                                                    <?php if ($row['pagada'] == '2'): ?>
                                                                        Aprobada
                                                                    <?php endif; ?>
                                                                    <?php if ($row['pagada'] == '3'): ?>
                                                                        Rechazada                        
                                                                    <?php endif; ?> 
                                                                </td>
                                                                <td>
                                                                    <?php if ($row['factura_uuid'] == ""): ?>
                                                                        <button data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" title="Generar Factura" class="btn btn-default facturar"><i class="fa fa-file-text-o"></i></button>
                                                                    <?php else: ?>
                                                                        <!-- xml download button -->
                                                                        <?php if ($row['factura_xml_url'] != "" && is_file($row['factura_xml_url'])): ?>
                                                                        <button data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" title="Descargar XML" class="btn btn-primary descargar-xml"><i class="fa fa-file-text-o"></i></button>
                                                                        <?php endif; ?>

                                                                        <!-- pdf download button -->
                                                                        <?php if ($row['factura_pdf_url'] != "" && is_file($row['factura_pdf_url'])): ?>
                                                                            <button data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" title="Descargar PDF" class="btn btn-info descargar-pdf"><i class="fa fa-file-pdf-o"></i></button>
                                                                        <?php else: ?>
                                                                            <button data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" title="Generar PDF" class="btn btn-warning generar-pdf"><i class="fa fa-repeat"></i></button>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>

                                                                    <button data-id-cotizacion="<?php echo $row['id_cotizacion']; ?>" title="Comentarios" class="btn btn-success comentarios-historial"><i class="fa fa-file-pdf-o"></i>
                                                                    </button>

                                                                    <!-- comprobante -->
                                                                    <!--
                                                                    <a id="comprobante" href="<?php echo $row['url_pago']; ?>" title="Descargar comprobante" target="_blank" class="btn btn-success">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    -->
                                                                </td>
                                                            </tr>
                                                <?php                                                          
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No. Cotizacion</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>  
                                                    <th>Cliente</th>                                                  
                                                    <th>Total</th>
                                                    <th>Vendedor</th>
                                                    <th>Estatus</th>                                                    
                                                    <th>Acciones</th>
                                                </tr>
                                            </tfoot>
                                        </table>  
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT -->
            </div>
        </div>
        <div class='notifications bottom-right'></div>
       
        <!-- END CONTAINER -->

        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <?php include ("modal/status-ventas.php"); ?>
        <?php include ("modal/facturar-modal.php"); ?>
        <?php include ("modal/comentarios-historial.php"); ?>
        <?php include ("modal/box-modal.php"); ?>
        <?php include ("modal/loading-modal.php"); ?>

        <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script type="text/javascript" src="plugins/select2/select2.min.js"></script>
        <script>

            function notify(message) {
                $('.bottom-right').notify({
                    message: { text: message },
                    type: 'bangTidy'
                }).show(); 
            }

            function  deshabilita(event, id, nombre) {
                event.preventDefault();
                var respuesta = confirm('\u00BFDesea eliminar a "' + nombre + '"?');
                if (respuesta) {
                    location.href = 'listado-ventas.php?borrar=' + id;
                }
            }

            function estatus(event, id_cliente,id_cotizacion) {
                event.preventDefault();
                $("#id_cliente").val(id_cliente);
                $("#id_cotizacion").val(id_cotizacion);
                $("#subir-comprobante").modal('show');
            }

            function estatusventa(event, id_cliente,id_cotizacion) {
                event.preventDefault();
                $("#id_cliente_v").val(id_cliente);
                $("#id_cotizacion_v").val(id_cotizacion);
                $("#modal_status_ventas").modal('show');
            }

            $(document).ready(function () {

                App.init();
                TableAdvanced.init();

                $('.aprobar-cotizacion').click(function(e) {
                    e.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');
                    var id_cliente = $(this).data('id-cliente');
                    var total_cotizacion = 0.00;

                    $("#form_status input[id=id_cotizacion]").val(id_cotizacion);
                    $("#form_status input[id=id_cliente]").val(id_cliente);
                    $("#form_status input[id=total_cotizacion]").val(total_cotizacion);

                    $.ajax({
                        url: './ajax/getTotalCotizacion.php',
                        type: 'post',
                        dataType: 'jsonp',
                        data: { id_cotizacion: id_cotizacion },
                        async: false
                    }).done(function(data) {
                        if (!data.error) {
                            total_cotizacion = data.total;

                            $('#form_status input[id=total_cotizacion]').val(total_cotizacion);
                            $('#form_status span[id=spantc]').text(parseFloat(total_cotizacion).formatMoney(2, '.', ','));
                        }
                    });

                    $.ajax({
                        url: './ajax/getCotizacion.php',
                        type: 'post',
                        dataType: 'jsonp',
                        data: { id_cotizacion: id_cotizacion },
                        async: false
                    }).done(function(data) {
                        if (!data.error) {
                            // $('#form_status span[id=pagoconsaldo').text(parseFloat(data.pagoconsaldo).formatMoney(2, '.', ','));
                            
                            if (data.url_pago != '') {
                                $('#form_status a[id=comprobante]').attr('href', data.url_pago);
                                $('#modal_status_ventas div#comprobante-group').show();
                            } else {
                                $('#form_status a[id=comprobante]').attr('href', '');
                                $('#modal_status_ventas div#comprobante-group').hide();
                            }

                            if (data.comentarios_cotizacion != '') {
                                $('#form_status span[id=comentarios]').text(data.comentarios_cotizacion);
                                $('#modal_status_ventas div#comentarios-group').show();
                            } else {
                                $('#form_status span[id=comentarios]').text('');
                                $('#modal_status_ventas div#comentarios-group').hide();
                            }
                        }
                    });
                    
                    $("#modal_status_ventas").modal('show');
                });

                $('#form_status button[id=cerrar]').click(function(e) {
                    $("#modal_status_ventas").modal('hide');
                });

                $('#form_status button[id=guardar]').click(function(e) {
                    e.preventDefault();

                    var id_cotizacion = $("#form_status input[id=id_cotizacion]").val();
                    var id_cliente = $("#form_status input[id=id_cliente]").val();
                    var total_cotizacion = $("#form_status input[id=total_cotizacion]").val();

                    // console.log(id_cotizacion, id_cliente, total_cotizacion);
                    // return false;
                    
                    var estatus = $('#form_status select[id=status]').val();
                    var motivo = $('#form_status textarea[id=motivo]').val();

                    // console.log(estatus, motivo);
                    // return false;

                    if (estatus == 0) {
                        notify('No se ha seleccionado un estatus');

                        return false;
                    }

                    $('#form_status').submit();

                });

                /*
                $('.facturar').click(function(e) {
                    e.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');
                    var id_domicilio_fiscal = 0;

                    $('#form-facturar input[id=id_cotizacion]').val(id_cotizacion);
                    $('#form-facturar select[id=id_domicilio_fiscal]').val(id_domicilio_fiscal);
                    
                    $('#form-facturar button[id=spinner]').hide();

                    $('#modal-facturar').modal('show');
                });


                $('#form-facturar button[id=cerrar]').click(function(e) {
                    $("#modal-facturar").modal('hide');
                });

                $('#form-facturar button[id=generar]').click(function(e) {
                    var id_cotizacion = $('#form-facturar input[id=id_cotizacion]').val();
                    var id_domicilio_fiscal = $('#form-facturar select[id=id_domicilio_fiscal]').val();

                    if (id_cotizacion == 0) {
                        return false;
                    }

                    if (id_domicilio_fiscal == 0) {
                        notify('Seleccione un domicilio fiscal');

                        return false;
                    }

                    $.ajax({
                        url: 'generar-factura.php',
                        method: 'post',
                        data: {
                            id_cotizacion: id_cotizacion,
                            id_domicilio_fiscal: id_domicilio_fiscal
                        },
                        dataType: 'json',
                        async: false,
                        beforeSend : function(xhr, opts) {
                            $('#form-facturar select[id=id_domicilio_fiscal]').prop('disabled', true);
                            
                            $('#form-facturar button[id=cerrar]').hide();
                            $('#form-facturar button[id=generar]').hide();

                            $('#form-facturar button[id=spinner]').show();
                            $('#form-facturar button[id=spinner] i.fa-spinner').addClass('fa-spin');
                        },
                        complete: function() {
                            $('#form-facturar button[id=spinner] i.fa-spinner').removeClass('fa-spin');
                            $('#form-facturar button[id=spinner]').hide();

                            $('#form-facturar button[id=generar]').show();
                            $('#form-facturar button[id=cerrar]').show();

                            $('#form-facturar select[id=id_domicilio_fiscal]').prop('disabled', false);
                        },
                        success: function(response) {
                            if (response.status == 'unauthorized') {
                                location.href = 'index.php';
                            }

                            if (response.status == 'error') {
                                alert(response.message);
                            }

                            if (response.status == 'success') {
                                alert(response.message);

                                location.href = 'listado-ventas.php';
                            }
                        }
                    });
                });
                */

                // facturar

                $('#sample_3').on('click', '.facturar', function(event) {
                    event.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');

                    if (id_cotizacion == 0) {
                        return false;
                    }

                    $.ajax({
                        url: 'generar-factura.php',
                        method: 'post',
                        data: { id_cotizacion: id_cotizacion },
                        dataType: 'json',
                        async: false,
                        beforeSend : function(xhr, opts) {
                            $('#loading-modal').modal('show');
                        },
                        success: function(response) {
                            $('#loading-modal').modal('hide');

                            if (response.status == 'unauthorized') {
                                location.href = 'index.php';
                            }

                            if (response.status == 'error') {
                                alert(response.message);
                            }

                            if (response.status == 'success') {
                                alert(response.message);

                                location.href = 'listado-ventas.php';
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('#loading-modal').modal('hide');

                            alert('Uncaught Error: ' + jqXHR.responseText);
                        }
                    });
                });

                // generar pdf

                $('#sample_3').on('click', '.generar-pdf', function(e) {
                    e.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');

                    if (!id_cotizacion) {
                        return;
                    }

                    $.ajax({
                        url: 'generar-pdf.php',
                        method: 'post',
                        dataType: 'json',
                        data: {
                            id_cotizacion: id_cotizacion
                        },
                        async: false,
                        beforeSend : function(xhr, opts) {
                            $('#loading-modal').modal('show');
                        },
                        success: function(response) {
                            $('#loading-modal').modal('hide');

                            if (response.status == 'unauthorized') {
                                location.href = 'index.php';
                            }

                            if (response.status == 'error') {
                                alert(response.message);
                            }

                            if (response.status == 'success') {
                                alert(response.message);

                                location.href = 'listado-ventas.php';
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('#loading-modal').modal('hide');

                            alert('Uncaught Error: ' + jqXHR.responseText);
                        }
                    });
                });

                // descargar xml

                $('#sample_3').on('click', '.descargar-xml', function(e) {
                    e.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');

                    if (!id_cotizacion) {
                        return false;
                    }

                    location.href = "descargar-xml.php?id_cotizacion=" + id_cotizacion;
                });

                // descargar pdf

                $('#sample_3').on('click', '.descargar-pdf', function(e) {
                    e.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');

                    if (!id_cotizacion) {
                        return false;
                    }

                    location.href = "descargar-pdf.php?id_cotizacion=" + id_cotizacion;
                });

                $('#sample_3').on('click', '.comentarios-historial', function(e) {
                    e.preventDefault();

                    var id_cotizacion = $(this).data('id-cotizacion');

                    if (!id_cotizacion) {
                        return false;
                    }

                    $.ajax({
                        url: './ajax/getTotalCotizacion.php',
                        type: 'post',
                        dataType: 'jsonp',
                        data: { id_cotizacion: id_cotizacion },
                        async: false
                    }).done(function(data) {
                        if (!data.error) {
                            total_cotizacion = data.total;

                            $('#modal_cometarios_historial span[id=comentarios-spantc]').text(parseFloat(total_cotizacion).formatMoney(2, '.', ','));
                        }
                    });

                    $.ajax({
                        url: './ajax/getCotizacion.php',
                        type: 'post',
                        dataType: 'jsonp',
                        data: { id_cotizacion: id_cotizacion },
                        async: false
                    }).done(function(data) {
                        if (!data.error) {
                            if (data.comentarios_cotizacion != '') {
                                $('#modal_cometarios_historial span[id=comentarios-comentarios]').text(data.comentarios_cotizacion);
                            } else {
                                $('#modal_cometarios_historial span[id=comentarios-comentarios]').text('---');
                            }

                            if (data.aprobada_motivo != '') {
                                $('#modal_cometarios_historial span[id=comentarios-motivos]').text(data.aprobada_motivo);
                            } else {
                                $('#modal_cometarios_historial span[id=comentarios-motivos]').text('---');
                            }

                            if (data.url_pago != '') {
                                $('#modal_cometarios_historial a[id=comentarios-comprobante]').attr('href', data.url_pago);
                                $('#modal_cometarios_historial div#comprobante-group').show();
                            } else {
                                $('#modal_cometarios_historial a[id=comentarios-comprobante]').attr('href', '#');
                                $('#modal_cometarios_historial div#comprobante-group').hide();
                            }
                        }
                    });

                    $("#modal_cometarios_historial").modal('show');
                });

            });

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>