<?php 

session_start();
$_SESSION['id_usuario'] = NULL;
$_SESSION['permiso_usuario'] = NULL;
$_SESSION['nombre_usuario'] = NULL;


unset($_SESSION['id_usuario']);
unset($_SESSION['permiso_usuario']);
unset($_SESSION['nombre_usuario']);

session_destroy();


$logoutGoTo = "index.php";
if ($logoutGoTo) {
	header("Location: $logoutGoTo");
	exit;
}

?>