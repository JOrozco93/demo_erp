<!-- cfdi-modal -->
<div class="modal fade" id="cfdi-modal" tabindex="-1" role="dialog" aria-labelledby="Status">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title">CFDI</h4>
            </div>
            <div class="modal-body">
            	<p>Procesando la solicitud, puede tomar unos minutos por favor no cancele el proceso ...</p>
            </div>
            <!--
            <div class="modal-footer">
                <button type="button" id="cerrar" class="btn btn-danger"><i class="fa fa-times"></i> Cerrar</button>
			</div>
			-->
        </div>
    </div>
</div>
<!-- cdfi-modal -->