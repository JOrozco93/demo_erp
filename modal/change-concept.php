<div class="modal fade" id="modal_change_concept" tabindex="-1" role="dialog" aria-labelledby="Status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="ModalStatus">Cambiar Precio Unitario</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="id_concepto" value="" class="form-control" />  
                        <input type="hidden" id="id_cotizacion" value="" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label>Precio Unitario</label>
                            <input type="text" id="precio_unitario" value="" class="form-control" />
                        </div>
                    </div>
                </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                <button type="button" id="save" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>