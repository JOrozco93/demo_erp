<!-- Comentarios Historial -->
<div class="modal fade" id="modal_cometarios_historial" tabindex="-1" role="dialog" aria-labelledby="Cometarios">
    <div class="modal-dialog">
        <!-- modal-content -->
        <div class="modal-content ">
            <!-- modal-header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="ModalStatus">Comentarios</h4>
            </div>
            <!-- modal-header -->
            <!-- modal-body -->
            <div class="modal-body">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-left">
                                <span style="font-weight: bold; color: #d9534f;">Total de la Cotización</span><br>
                                $ <span id="comentarios-spantc"></span>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div id="comentarios-group" class="form-group">
                    <strong>Comentarios de la Cotización</strong>
                    <div style="margin: 0; padding: 9px; border: 1px dashed #e0dfdf;">
                        <span id="comentarios-comentarios"></span>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <strong>Motivos de la Venta</strong>
                    <div style="margin: 0; padding: 9px; border: 1px dashed #e0dfdf;">
                        <span id="comentarios-motivos"></span>
                    </div>
                </div>

                <hr>

                <div id="comprobante-group" class="form-group">
                    <strong>Comprobante</strong><br>
                    <a id="comentarios-comprobante" href="#" target="_blank" class="btn btn-success"><i class="fa fa-download"></i> Descargar</a>
                </div>                    
            </div>
            <!-- modal-body -->
        </div>
        <!-- modal-content -->
    </div>
</div>