<!-- Modal Status -->
<div class="modal fade" id="edit-guide-number-modal" tabindex="-1" role="dialog" aria-labelledby="Guide">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form id="edit-guide-number-form" name="edit-guide-number-form" method="post" action="alta/edit-guide-number.php">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalStatus">Editar Numero de Guia</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-12">
                            <label>Numero Guia</label>
                            <input type="text" id="numguia_ng" name="numguia_ng" placeHolder="Numero de Guia" value="" class="form-control" /> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="id_cotizacion_ng" name="id_cotizacion_ng" value="" class="form-control" />
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="button" id="btn" name="btn" class="btn btn-primary" value="Guardar"><i class="fa fa-floppy-o"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('#edit-guide-number-modal').on('shown.bs.modal', function () {
        $('#edit-guide-number-form input[id=numguia_ng]').focus();
    });

    $('#edit-guide-number-modal').on('hidden.bs.modal', function () {
        // code goes here
    });

</script>