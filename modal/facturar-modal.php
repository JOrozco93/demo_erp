<?php 
    $sql = "SELECT * FROM domicilios_fiscales"; 
    $domicilios_fiscales = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
?>

<!-- facturar-modal -->
<div id="modal-facturar" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form id="form-facturar" method="post" action="ashwujhdaweyudhju.php">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                    <h4 class="modal-title" id="ModalStatus">Facturar</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="id_cotizacion" name="id_cotizacion" value="0" class="form-control">
                    </div> 
                    <div class="form-group">
                        <label for="status">Domicilio fiscal del emisor</label>
                        <select id="id_domicilio_fiscal" name="id_domicilio_fiscal" class="form-control">
                            <option value="0">Seleccione una domicilio fiscal ...</option>
                            <?php foreach ($domicilios_fiscales as $domicilio_fiscal): ?>
                                <option value="<?php echo $domicilio_fiscal['id_domicilio_fiscal']; ?>"><?php echo $domicilio_fiscal['razon_social']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="spinner" class="btn btn-default"><i class="fa fa-spinner"></i> Procesando</button>
                    
                    <button type="button" id="cerrar" class="btn btn-danger"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="button" id="generar" class="btn btn-success"><i class="fa fa-check"></i> Generar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- facturar-modal -->