<!-- Modal Status -->
<div class="modal fade" id="modal_imagen" tabindex="-1" role="dialog" aria-labelledby="Foto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <form action="alta/status-cliente.php" method="POST" name="form_imagen" id="form_imagen">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalStatus">Foto</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <img class="responsive" id="imagen" name="imagen"  width="100%" height="auto" />
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>