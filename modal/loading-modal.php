<div id="loading-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title text-center">Generando factura</h4>
                <p class="modal-title text-center">Porfavor espere un momento</p>
            </div>
            <div class="modal-body">
            	<p class="text-center">
            		<img src="img/round-loading.gif" alt="Loading" height="50" width="50">
            	</p>
            </div>
		</div>
	</div>
</div>