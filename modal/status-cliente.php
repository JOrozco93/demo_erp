<!-- Modal Status -->
<div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="Status">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form action="alta/status-cliente.php" method="post" name="form_status" id="form_status">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalStatus">Actualizar Status</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <select class="form-control" name = "status">
                                <option value="0">En proceso</option>
                                <option value="1">Autorizado</option>
                                <option value="2">Declinado</option>
                            </select>
                            <textarea id="motivo" name="motivo" class="form-control" placeholder="Motivo"></textarea>
                            <input type="hidden" id="id" name="id" value=""/>  
                            <input type="hidden" id="tipo" name="tipo" value=""/>  
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>