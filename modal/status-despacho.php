<!-- Modal Status -->
<div class="modal fade" id="modal_status_despacho" tabindex="-1" role="dialog" aria-labelledby="Status">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form action="alta/tipo-despacho.php" method="POST" name="form_status_despacho" id="form_status_despacho">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalStatus">Actualizar Status</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-12">
                            <label>Despacho</label>
                        </div>
                        <div class="col-md-10" id="tipo_despacho_div"></div>
                        <div class="col-md-2">
                            <a  onclick="$('#tipo_despacho').prop('disabled', false); $('#numguia').prop('disabled', false); $('#group-motivo').attr('style', 'display:true;');" title="Cambiar despacho" data-toggle="tooltip" data-placement="left" class="btn btn-warning" style="width: 100%;">
                                <i class="fa fa-exchange"></i> 
                            </a>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-12" name="divnumguia" id="divnumguia" style="display:none;">
                            <label>Numero Guia</label>
                            <input type="text" class="form-control" id="numguia" name="numguia" value="" /> 
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-12" id="group-motivo" style="display:none;">
                            <label>Motivo</label>
                            <textarea id="motivo" name="motivo" placeholder="Motivo" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="tipo_despacho_noch" name="tipo_despacho_noch" value="" class="form-control" />
                            <input type="hidden" id="id_cliente" name="id_cliente" value="" class="form-control" />
                            <input type="hidden" id="id_cotizacion" name="id_cotizacion" value="" class="form-control" />
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary" onclick="validar_vacios();" id="btn" name ="btn" value="Guardar"><i class="fa fa-floppy-o"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('#modal_status_despacho').on('shown.bs.modal', function () {

        $('#divnumguia').hide();
        $('#form_status_despacho').get(0).reset();
        $('#group-motivo').attr('style', 'display:none;');
        
        id_cotizacion = $("#id_cotizacion").val();
        html = "";

        $.ajax({
            url: './alta/id-tipo-despacho.php?id_cotizacion=' + id_cotizacion,
            type: 'post',
            dataType: 'jsonp'
        }).done(function(data) {
            $("#tipo_despacho_div").empty();

            if (!data.error) {
                $.each(data, function(index, ele) {
                    tipo = parseInt(ele.tipo_despacho);
                    
                    if (tipo == 1) {
                        html =
                            '<select onchange="numeroguia(this.value)" class="form-control" id="tipo_despacho" name="tipo_despacho" disabled>' +
                            '<option value="1" selected>Envío a cliente</option>' +
                            '<option value="2">Recolección en oficina</option>' +
                            '<option value="3">Instalación HS</option>' +
                            '</select>'
                        ;
                        
                        $('#tipo_despacho_noch').val(1);

                        $('#numguia').prop('disabled', true);
                        
                        $('#divnumguia').show();
                     } else if (tipo == 2) {
                        html =
                            '<select onchange="numeroguia(this.value)" class="form-control" id="tipo_despacho" name="tipo_despacho" disabled>' +
                            '<option value="1">Envío a cliente</option>' +
                            '<option value="2" selected>Recolección en oficina</option>' +
                            '<option value="3">Instalación HS</option>' +
                            '</select>'
                        ;
                         
                        $('#tipo_despacho_noch').val(2);

                        numeroguia(2);
                     } else if (tipo == 3) {
                        html =
                            '<select onchange="numeroguia(this.value)" class="form-control" id="tipo_despacho" name="tipo_despacho" disabled>' +
                            '<option value="1">Envío a cliente</option>'+
                            '<option value="2">Recolección en oficina</option>'+
                            '<option value="3" selected>Instalación HS</option>' +
                            '</select>'
                        ;
                        
                        $('#tipo_despacho_noch').val(3);

                        numeroguia(3);
                    }
                });
                
                $("#tipo_despacho_div").append(html);
            } else {
                // code goes here
            }
        });
    });

    $('#modal_status_despacho').on('hidden.bs.modal', function () {
        // code goes here
    });

    function numeroguia(val) {
        if (val == 1 ) {
            $('#divnumguia').show();
        } else {
            $('#divnumguia').hide();
        }
    }

</script>