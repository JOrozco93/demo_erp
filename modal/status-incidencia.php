<!-- Modal Status -->
<div class="modal fade" id="modal_status_incidencia" tabindex="-1" role="dialog" aria-labelledby="Status">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form action="alta/status-incidencia.php" method="post" name="form_incidenica" id="form_incidenica">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalStatus">Actualizar Status</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <select class="form-control" id="status_modal_incidencias" name="status">
                                <option value="0">Pendiente</option>
                                <option value="1">Aceptado</option>
                                <option value="2">Rechazado</option>
                                <option value="3">Cerrado</option>
                                <option value="4">Cancelado</option>
                            </select>
                            <textarea id="motivo" name="motivo" class="form-control" placeholder="Motivo"></textarea>                      
                            <input type="hidden" id="id_modal_incidencias" name="id" value="" />
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary" onclick="validar_vacios();" id="btn" name ="btn" value="Guardar"><i class="fa fa-floppy-o"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>