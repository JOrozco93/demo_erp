<!-- Modal Status -->
<div class="modal fade" id="modal_status_produccion" tabindex="-1" role="dialog" aria-labelledby="Status">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form action="alta/status-produccion.php" method="POST" name="form_status" id="form_status">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalStatus">Actualizar Status</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>¿ Desea enviar a despacho este pedido ?</p>
                            <input type="hidden" id="status" name="status" value="1"> 
                            <input type="hidden" id="id_cliente" name="id_cliente" value="">  
                            <input type="hidden" id="id_cotizacion" name="id_cotizacion" value="">  
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="submit" class="btn btn-success" onclick="validar_vacios();" id="btn" name ="btn" value="Guardar"><i class="fa fa-check-square-o"></i> Aceptar</button>
                </div>
            </form>
        </div>
    </div>
</div>