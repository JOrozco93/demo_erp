<!-- Modal Status -->
<div class="modal fade" id="modal_status_ventas" tabindex="-1" role="dialog" aria-labelledby="Status">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form id="form_status" name="form_status" method="post" action="alta/status-ventas.php">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalStatus">Aprobar Cotización</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="id_cotizacion" name="id_cotizacion" value="" class="form-control" readonly>
                        <input type="hidden" id="id_cliente" name="id_cliente" value="" class="form-control" readonly>
                        <input type="hidden" id="total_cotizacion" name="total_cotizacion" value="" class="form-control" readonly>
                    </div> 
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-left">
                                    <span style="font-weight: bold; color: #d9534f;">Total de la Cotización</span><br>
                                    $ <span id="spantc"></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div id="pagoconsaldo-group" class="form-group">
                        <strong>Pago con saldo</strong><br>
                        $ <span id="pagoconsaldo">XXXX</span>
                    </div>
                    -->
                    <div id="comprobante-group" class="form-group">
                        <strong>Comprobante</strong><br>
                        <a id="comprobante" href="#" target="_blank" class="btn btn-success"><i class="fa fa-download"></i> Descargar</a>
                    </div>
                    <div id="comentarios-group" class="form-group">
                        <strong>Comentarios de la Cotización</strong>
                        <div style="margin: 0; padding: 9px; border: 1px dashed #e0dfdf;">
                            <span id="comentarios">Comentarios</span>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="status">Estatus</label>
                        <select id="status" name="status" class="form-control">
                            <option value="0">Seleccione una estatus ...</option>
                            <option value="1">Pendiente</option>
                            <option value="2">Aprobada</option>
                            <option value="3">Rechazada</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="motivo">Motivo</label>    
                        <textarea id="motivo" name="motivo" class="form-control" placeholder="Motivo"></textarea>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" id="cerrar" class="btn btn-danger"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="button" id="guardar" class="btn btn-primary" value="Guardar"><i class="fa fa-floppy-o"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>