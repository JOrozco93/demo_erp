<!-- Modal Subir archivo comprobante de pago -->
<div class="modal fade" id="subir-comprobante" tabindex="-1" role="dialog" aria-labelledby="Comprobante">
    <div class="modal-dialog">
        <div class="modal-content ">
            <form id="form_comprobante" method="post" action="alta/subir-comprobante.php" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Subir comprobante de pago</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="id_cotizacion" name="id_cotizacion" value="" class="form-control" readonly>
                        <input type="hidden" id="id_cliente" name="id_cliente" value="" class="form-control" readonly>
                        <input type="hidden" id="total_cotizacion" name="total_cotizacion" value="" class="form-control" readonly>
                        <input type="hidden" id="saldo_cliente" name="saldo_cliente" value="" class="form-control" readonly>
                    </div> 
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="text-left">
                                    <span style="font-weight: bold; color: #d9534f;">Total de la Cotización</span><br>
                                    $ <span id="spantc"></span>
                                </p>
                            </div>
                            <!--
                            <div class="col-md-6">
                                <p class="text-right">
                                    <span style="font-weight: bold; color: #337ab7;">Mi Saldo</span><br>
                                    $ <span id="spansc"></span>
                                </p>
                            </div>
                            -->
                        </div>
                    </div>
                    <!--
                    <div class="form-group">
                        <label for="pagoconsaldo">Pagar con saldo</label>
                        <input type="text" id="pagoconsaldo" name="pagoconsaldo" class="form-control" placeholder="Pagar con saldo">
                    </div>
                    -->
                    <div class="form-group">
                        <label for="file">Comprobante</label>
                        <input type="file" id="file" name="file">
                    </div>
                    <div class="form-group">
                        <label for="comentarios">Comentarios</label>
                        <textarea id="comentarios" name="comentarios" class="form-control" placeholder="Comentarios"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="cerrar" class="btn btn-danger"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="button" id="guardar" class="btn btn-primary" value="Guardar"><i class="fa fa-floppy-o"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>