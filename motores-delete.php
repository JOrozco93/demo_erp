<?php

  if (!isset($_SESSION)) {
    session_start();
  }

  if (($_SESSION['id_usuario'] == null)) {
    header("Location: logout.php");
    exit;
  }

  require_once("config.php");
  require_once("includes/funciones_BD.php");
  require_once("includes/validacion.php");

  $id_usuario = $_SESSION['id_usuario'];

  $id_pmotor = $_GET['id'];

  if (!is_numeric($id_pmotor)) {
    header("location: motores-list.php");
    exit;
  }

  $sql = "DELETE FROM puerta_motor WHERE id_pmotor = :id_pmotor";

  $stmt = $pdo->prepare($sql);

  $stmt->bindParam(':id_pmotor', $id_pmotor);

  $result = $stmt->execute();

  if ($result) {
    echo "
      <script>
        alert('El registro ha sido eliminado');
        location.href = 'motores-list.php';
      </script>
    ";
  } else {
    echo "
    <script>
      alert('Ha ocurrido un error al eliminar el registro');
      location.href = 'motores-list.php';
    </scrip>
    ";
  }

?>