<?php

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == NULL)) {
        header("location: logout.php");
        exit;
    }

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    $id_usuario = $_SESSION['id_usuario'];

    $id_pmotor = $_GET['id'];

    if (!is_numeric($id_pmotor)) {
        header("location: motores-list.php");
        exit;
    }

    // motor

    $sql = "SELECT * FROM puerta_motor WHERE id_pmotor = :id_pmotor";

    $stmt = $pdo->prepare($sql);

    $stmt->bindParam(':id_pmotor', $id_pmotor);

    $stmt->execute();

    if ($stmt->rowCount() == 0) {
        header("location: motores-list.php");
        exit;
    }

    $motor = $stmt->fetch();

    // proveedor

    $id_proveedor = (isset($motor['id_proveedor'])) ? $motor['id_proveedor'] : 0;

    $sql = "SELECT * FROM proveedores WHERE id_proveedor = :id_proveedor";

    $stmt = $pdo->prepare($sql);

    $stmt->bindParam(':id_proveedor', $id_proveedor);

    $stmt->execute();

    $proveedor = $stmt->fetch();

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
        <style>
            input {
                line-height: normal;
            }
        </style>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">  

                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Motores <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-list"></i>
                                    <a href="">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="">
                                        Vizualizar motor
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="row" style="margin-bottom: 5px;">
                                        <div class="col-md-12">

                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form id="tvForm" method="post" action="terminos-venta-save.php" class="form-horizontal">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label><strong>Proveedor:</strong></label>
                                                <p><?php echo $proveedor['nombre']; ?></p>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label><strong>Nombre del motor:</strong></label>
                                                <p><?php echo $motor['nombre_motor']; ?></p>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label><strong>Descripción:</strong></label>
                                                <p><?php echo nl2br($motor['descripcion']); ?></p>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label><strong>Precio:</strong></label>
                                                <p><?php echo number_format($motor['precio'],2); ?></p>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="form-actions fluid">
                                            <div clas="row">
                                                <!-- cancel -->
                                                <div class="col-sm-6">
                                                    <a href="motores-list.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                                                </div>

                                                <!-- edit -->
                                                <div class="col-sm-6">
                                                    <a href="motores-edit.php?id=<?php echo $motor['id_pmotor']; ?>" class="btn blue btn-success pull-right">
                                                        <i class="fa fa-pencil-square-o"> Editar</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        
        </div>
        <!-- END CONTAINER -->
        
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        
        <script>

            $(document).ready(function () {
                App.init();
            });
            
        </script>

    </body>
    <!-- END BODY -->
</html>