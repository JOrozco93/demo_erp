<?php

if (!isset($_SESSION)) {
    session_start();
}

if (($_SESSION['id_usuario'] == NULL)) {
    header("Location: logout.php");
}

require_once("config.php");
require_once("includes/funciones_BD.php");

$fecha_inicial = (isset($_POST['fecha_inicial'])) ? $_POST['fecha_inicial'] : null;
$fecha_final = (isset($_POST['fecha_final'])) ? $_POST['fecha_final'] : null;

$sql = "SELECT c.*, DATE_FORMAT(c.fecha, '%Y-%m-%d') AS fecha, c.descripcion, cli.nombre_cliente, c.total, c.autorizada, u.nombre_usuario FROM cotizaciones AS c, clientes AS cli, usuarios AS u WHERE c.activo = 1 AND c.autorizada = 1 AND c.pagada = 2 AND c.produccion = 1 AND c.despacho = 1 AND c.id_cliente = cli.id_cliente AND c.id_vendedor = u.id_usuario";

$fecha_filter = null;

if ($fecha_inicial) {
    $fecha_filter = " AND fecha >= '$fecha_inicial'";
}

if ($fecha_final) {
    $fecha_filter = " AND fecha <= '$fecha_final'";   
}

if ($fecha_inicial && $fecha_final) {
    $fecha_filter = " AND fecha BETWEEN '$fecha_inicial' AND '$fecha_final'";
}

if ($fecha_filter) {
    $sql .= $fecha_filter;
}

$sql .= " ORDER BY fecha DESC";

// echo $sql;
// exit;

$ventas = consulta($sql);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->              
        <?php include ("includes/css.php"); ?>
        <!--<link rel="shortcut icon" href="favicon.ico"/>-->
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/select2/select2-metronic.css"/>
        <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css"/>
        <link rel="stylesheet" href="plugins/bootstrap-datepicker/css/datepicker.css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>
        
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Reporte de ventas
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="inicio.php">
                                        Inicio
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">
                                        Reporte de ventas
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <div class="well">
                        <form id="search-form" method="post" action="reportes-ventas.php" class="form-inline">
                            <div class="row">
                                <!-- fecha inicial -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="sr-only" for="fecha_inicial">Fecha Inicial</label>
                                        <div class="input-group">
                                            <input type="text" id="fecha_inicial" name="fecha_inicial" placeholder="Fecha Inicial" class="form-control">
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fecha final -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="sr-only" for="fecha_final">Fecha Final</label>
                                        <div class="input-group">
                                            <input type="text" id="fecha_final" name="fecha_final" placeholder="Fecha Final" class="form-control">
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- buscar -->
                                <button type="button" id="search_btn" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                            </div>
                        </form>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <a href="#" class="btn btn-primary pull-right"><i class="fa fa-download"> Descargar Excel</i></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table id="cotizaciones-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No. Cotizacion</th>
                                        <th>Fecha</th>
                                        <th>Descripción</th>
                                        <th>Cliente</th>
                                        <th>Tipo de despacho</th>
                                        <th>Guía</th>
                                        <th>Vendedor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php while($row = mysql_fetch_assoc($ventas)): ?>
                                        <tr>
                                            <td><a href="detalle-cotizacion.php?id=<?php echo $row['id_cotizacion']; ?>" target="_blank"><?php echo $row['numero_cotizacion']; ?></a></td>
                                            <td><?php echo $row['fecha']; ?></td>
                                            <td><?php echo $row['descripcion']; ?></td>
                                            <td><?php echo $row['nombre_cliente']; ?></td>
                                            <td>
                                                <?php
                                                    if ($row['tipo_despacho'] == '1') {
                                                        echo 'Envío a cliente';
                                                    } else if ($row['tipo_despacho'] == '2') {
                                                        echo 'Recolección en oficina';
                                                    } else if ($row['tipo_despacho'] == '3') {
                                                        echo 'Instalación HS';
                                                    } else {
                                                        echo 'Sin Status';
                                                    } 
                                                ?>
                                            </td>
                                            <td><?php echo $row['num_guia']; ?></td>
                                            <td><?php echo $row['nombre_usuario']; ?></td>
                                        </tr>
                                    <?php endwhile; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- page-content -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

        <!-- footer -->
        <div class="footer">
            <div class="footer-inner">
                2015 &copy; HS Puertas de Garage.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- footer -->

        <!-- notifications -->
        <div class='notifications bottom-right'></div>
        <!-- notifications -->
        
        <!-- BEGIN JAVASCRIPTS -->
        <?php include ("includes/js.php") ?>;

    <!-- BEGIN GRAPHS -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <!-- END GRAPHS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="scripts/custom/index.js" type="text/javascript"></script>

    <script type="text/javascript" src="plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="plugins/select2/select2.min.js"></script>

    <script>
        jQuery(document).ready(function () {
            App.init();

            function notify(message) {
                $('.bottom-right').notify({
                    message: { text: message },
                    type: 'bangTidy'
                }).show(); 
            }

            function isDate(date) {
                return ((new Date(date) !== "Invalid Date" && !isNaN(new Date(date))));
            }

            $('#fecha_inicial').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $('#fecha_final').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $('#search_btn').click(function() {
                var fecha_inicial = $('#fecha_inicial').val();
                var fecha_final = $('#fecha_final').val();

                // validar la fecha inicial

                if (fecha_inicial) {
                    if (!isDate(fecha_inicial)) {
                        notify('El formato de la fecha inicial es incorrecto');

                        return false;
                    }
                }

                // validar fecha final

                if (fecha_final) {
                    if (!isDate(fecha_final)) {
                        notify('El formato de la fecha final es incorrecto');

                        return false;
                    }
                }

                // verificar que la fecha inicial no sea mayor que la fecha final

                var fi = new Date(fecha_inicial);
                var ff = new Date(fecha_final);

                if (fi > ff) {
                    notify('La fecha inicial no puede ser mayor que la fecha final');

                    return;
                }

                $('#search-form').submit();
            });

            $('#cotizaciones-table').dataTable({
                bPaginate: false,
                bFilter: false
            });
        });

    </script>
    <!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>