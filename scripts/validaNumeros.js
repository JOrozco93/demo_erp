// JavaScript Document $(function() {
$(function () {
    //on KeyPress, integer value validation
    $('.integer_fields').keypress(function (event)
    {
        var validKey = [8, 9, 13];
        var charCode = (event.which) ? event.which : event.keyCode;
//		alert(charCode);
        if (validKey.indexOf(charCode) != -1)
            return true;
        if (charCode < 48 || charCode > 57)
        {
            event.preventDefault();
        }
    });

    //on KeyPress, float value validation
    $('.decimal_fields').keypress(function (event) {
        var validKey = [8, 9, 13];
        var charCode = (event.which) ? event.which : event.keyCode;
//		alert(charCode);
        if (validKey.indexOf(charCode) != -1)
            return true;
        if ((charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57))
        {
            event.preventDefault();
        }
    });
});
