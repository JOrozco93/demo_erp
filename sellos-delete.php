<?php

  if (!isset($_SESSION)) {
    session_start();
  }

  if (($_SESSION['id_usuario'] == null)) {
    header("Location: logout.php");
    exit;
  }

  require_once("config.php");
  require_once("includes/funciones_BD.php");
  require_once("includes/validacion.php");

  $id_usuario = $_SESSION['id_usuario'];

  $id_psello = $_GET['id'];

  if (!is_numeric($id_psello)) {
    header("location: sellos-list.php");
    exit;
  }

  $sql = "DELETE FROM puerta_sello WHERE id_psello = :id_psello";

  $stmt = $pdo->prepare($sql);

  $stmt->bindParam(':id_psello', $id_psello);

  $result = $stmt->execute();

  if ($result) {
    echo "
      <script>
        alert('El registro ha sido eliminado');
        location.href = 'sellos-list.php';
      </script>
    ";
  } else {
    echo "
    <script>
      alert('Ha ocurrido un error al eliminar el registro');
      location.href = 'sellos-list.php';
    </scrip>
    ";
  }

?>