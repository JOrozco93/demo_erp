<?php

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == NULL)) {
        header("Location: logout.php");
    }

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    $id_usuario = $_SESSION['id_usuario'];

    $sql = "SELECT * FROM puerta_sello INNER JOIN proveedores WHERE puerta_sello.id_proveedor = proveedores.id_proveedor";

    $sellos = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
        <style>
            input {
                line-height: normal;
            }
        </style>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">  

                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Sellos <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-list"></i>
                                    <a href="">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="">
                                        Sellos
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="row" style="margin-bottom: 5px;">
                                        <div class="col-md-12">
                                            <a href="sellos-add.php" title="Agregar sello" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table id="sellos" class="table table-striped table-bordered table-hover table-full-width">
                                        <thead>
                                            <tr>
                                                <th style="width: 20%;">Nombre</th>
                                                <th style="width: 20%;">Proveedor</th>
                                                <th style="width: 40%;">Descripción</th>
                                                <th style="width: 10%;">Precio</th>
                                                <th style="width: 10%;">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($sellos as $sello) { ?>
                                                <tr>
                                                    <td>
                                                        <a href="sellos-view.php?id=<?php echo $sello['id_psello']; ?>" title="Vizualizar sello" data-toggle="tooltip" data-placement="left">
                                                            <?php echo $sello['nombre_sello']; ?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo $sello['nombre']; ?></td>
                                                    <td><?php echo $sello['descripcion']; ?></td>
                                                    <td><?php echo number_format($sello['precio'],2); ?></td>
                                                    <td>
                                                        <!-- edit -->
                                                        <a href="sellos-edit.php?id=<?php echo $sello['id_psello']; ?>" title="Editar sello" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                        </a>

                                                        <!-- delete -->
                                                        <a href="sellos-delete.php?id=<?php echo $sello['id_psello']; ?>" title="Eliminar sello" data-toggle="tooltip" data-placement="right" class="btn btn-danger delete">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Proveedor</th>
                                                <th>Descripción</th>
                                                <th>Precio</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        
        </div>
        <!-- END CONTAINER -->
        
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        
        <script>

            $(document).ready(function () {
                App.init();
                
                $('#sellos').dataTable({
                    "aaSorting": [[ 0, 'desc' ]]
                });

                $('#sellos').on('click','.delete', function() {
                    return confirm('¿ Desea eliminar el registro ?');
                });
            });
            
        </script>
        <!-- END JAVASCRIPTS -->

    </body>
    <!-- END BODY -->
</html>