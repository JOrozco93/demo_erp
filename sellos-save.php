<?php

  if (!isset($_SESSION)) {
    session_start();
  }

  if (($_SESSION['id_usuario'] == null)) {
    header("Location: logout.php");
    exit;
  }

  require_once("config.php");
  require_once("includes/funciones_BD.php");
  require_once("includes/validacion.php");

  $id_usuario = $_SESSION['id_usuario'];

  $id_proveedor = (isset($_POST['id_proveedor'])) ? $_POST['id_proveedor'] : 0;
  $nombre_sello = (isset($_POST['nombre_sello'])) ? $_POST['nombre_sello'] : "";
  $descripcion = (isset($_POST['descripcion'])) ? $_POST['descripcion'] : "";
  $precio = (isset($_POST['precio'])) ? $_POST['precio'] : 0;

  $id_proveedor = filter_var($id_proveedor, FILTER_SANITIZE_NUMBER_INT);
  $nombre_sello = filter_var($nombre_sello, FILTER_SANITIZE_STRING);
  $descripcion = filter_var($descripcion, FILTER_SANITIZE_STRING);
  $precio = filter_var($precio, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

  $sql = "INSERT INTO puerta_sello (id_proveedor, nombre_sello, descripcion, precio)  VALUES (:id_proveedor, :nombre_sello, :descripcion, :precio)";

  $stmt = $pdo->prepare($sql);

  $stmt->bindParam(':id_proveedor', $id_proveedor);
  $stmt->bindParam(':nombre_sello', $nombre_sello);
  $stmt->bindParam(':descripcion', $descripcion);
  $stmt->bindParam(':precio', $precio);

  $result = $stmt->execute();

  if ($result) {
    echo "
      <script>
        alert('El registro se ha almacenado con éxito');
        location.href = 'sellos-list.php';
      </script>
    ";
  } else {
    echo "
    <script>
      alert('Ha ocurrido un error al almacenar el registro');
      location.href = 'sellos-list.php';
    </scrip>
    ";
  }

?>