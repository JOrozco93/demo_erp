<?php

  if (!isset($_SESSION)) {
    session_start();
  }

  if (($_SESSION['id_usuario'] == null)) {
    header("Location: logout.php");
    exit;
  }

  require_once("config.php");
  require_once("includes/funciones_BD.php");
  require_once("includes/validacion.php");

  $id_usuario = $_SESSION['id_usuario'];

  $id_termino_venta = $_GET['id'];

  if (!is_numeric($id_termino_venta)) {
    header("location: terminos-venta-list.php");
    exit;
  }

  $sql = "DELETE FROM terminos_venta WHERE id_termino_venta = :id_termino_venta";

  $stmt = $pdo->prepare($sql);

  $stmt->bindParam(':id_termino_venta', $id_termino_venta);

  $result = $stmt->execute();

  if ($result) {
    echo "
      <script>
        alert('El registro ha sido eliminado');
        location.href = 'terminos-venta-list.php';
      </script>
    ";
  } else {
    echo "
    <script>
      alert('Ha ocurrido un error al eliminar el registro');
      location.href = 'terminos_venta-list.php';
    </scrip>
    ";
  }

?>