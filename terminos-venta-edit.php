<?php

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == NULL)) {
        header("Location: logout.php");
    }

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    $id_usuario = $_SESSION['id_usuario'];

    $id_termino_venta = $_GET['id'];

    if (!is_numeric($id_termino_venta)) {
        header("location: terminos-venta-list.php");
        exit;
    }

    $sql = "SELECT * FROM terminos_venta WHERE id_termino_venta = :id_termino_venta";

    $stmt = $pdo->prepare($sql);

    $stmt->bindParam(':id_termino_venta', $id_termino_venta);

    $stmt->execute();

    if ($stmt->rowCount() == 0) {
        header("location: terminos-venta-list.php");
        exit;
    }

    $termino_venta = $stmt->fetch();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
        <style>
            input {
                line-height: normal;
            }
        </style>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">  

                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Términos de Venta <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-list"></i>
                                    <a href="">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="">
                                        Editar término de venta
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="row" style="margin-bottom: 5px;">
                                        <div class="col-md-12"></div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <form id="tvForm" method="post" action="terminos-venta-update.php?id=<?php echo $termino_venta['id_termino_venta']; ?>" class="form-horizontal">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Término de venta:</label>
                                                <input type="text" id="termino_venta" name="termino_venta" value="<?php echo $termino_venta['termino_venta']; ?>" placeholder="Término de venta" class="form-control">
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Descripcion:</label>
                                                <textarea id="descripcion" name="descripcion" placeholder="Descripción" rows="8" class="form-control"><?php echo $termino_venta['descripcion']; ?></textarea>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="form-actions fluid">
                                            <div clas="row">
                                                <div class="col-sm-6">
                                                    <a href="terminos-venta-list.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button type="button" id="btn-update" class="btn blue btn-success pull-right"><i class="fa fa-floppy-o"></i> Actualizar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        
        </div>
        <!-- END CONTAINER -->
        
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        
        <script>

            $(document).ready(function () {
                App.init();

                $('#btn-update').click(function() {
                    if ($('#termino_venta').val() == '') {
                        alert('Introduzca un término de venta');
                        $('#termino_venta').focus();
                        
                        return false;
                    }

                    if ($('#descripcion').val() == '') {
                        alert('Introduzca una descripción');
                        $('#descripcion').focus();

                        return false;
                    }

                    $('#tvForm').submit();
                });
            });
            
        </script>

    </body>
    <!-- END BODY -->
</html>