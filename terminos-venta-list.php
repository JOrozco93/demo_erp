<?php

    if (!isset($_SESSION)) {
        session_start();
    }

    if (($_SESSION['id_usuario'] == NULL)) {
        header("Location: logout.php");
    }

    require_once("config.php");
    require_once("includes/funciones_BD.php");
    require_once("includes/validacion.php");

    $id_usuario = $_SESSION['id_usuario'];

    $sql = "SELECT * FROM terminos_venta";
    $terminos_venta = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $name; ?></title>
        <?php include ("includes/css.php"); ?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="plugins/data-tables/DT_bootstrap.css" rel="stylesheet" />
        <style>
            input {
                line-height: normal;
            }
        </style>
        <!-- END PAGE LEVEL STYLES -->
    </head>
    <!-- END HEAD -->
    
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-fixed">
        <?php include ("includes/header.php"); ?>
        <div class="clearfix"></div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("includes/menu.php"); ?>
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">  

                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Términos de Venta <small></small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <i class="fa fa-list"></i>
                                    <a href="">
                                        Catalagos
                                    </a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="">
                                        Términos de Venta
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="row" style="margin-bottom: 5px;">
                                        <div class="col-md-12">
                                            <a href="terminos-venta-add.php" title="Agregar término de venta" data-toggle="tooltip" data-placement="left" class="btn btn-default pull-right">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table id="terminos-venta" class="table table-striped table-bordered table-hover table-full-width" style="table-layout: fixed;">
                                        <thead>
                                            <tr>
                                                <th style="width: 80%;">Termino de venta</th>
                                                <th style="width: 20%;">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($terminos_venta as $termino_venta) { ?>
                                                <tr>
                                                    <td>
                                                        <a href="terminos-venta-view.php?id=<?php echo $termino_venta['id_termino_venta']; ?>" title="Vizualizar término de venta" data-toggle="tooltip" data-placement="left">
                                                            <?php echo $termino_venta['termino_venta']; ?>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <!-- edit -->
                                                        <a href="terminos-venta-edit.php?id=<?php echo $termino_venta['id_termino_venta']; ?>" title="Editar término de venta" data-toggle="tooltip" data-placement="left" class="btn btn-primary">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                        </a>

                                                        <!-- delete -->
                                                        <a href="terminos-venta-delete.php?id=<?php echo $termino_venta['id_termino_venta']; ?>" title="Eliminar término venta" data-toggle="tooltip" data-placement="right" class="btn btn-danger delete">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Termino de venta</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        
        </div>
        <!-- END CONTAINER -->
        
        <?php include ("includes/footer.php"); ?>
        <?php include ("includes/js.php"); ?>

        <script src="plugins/data-tables/jquery.dataTables.min.js" type="text/javascript" ></script>
        <script src="plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
        <script src="scripts/custom/table-advanced.js"></script>
        <script src="plugins/select2/select2.min.js" type="text/javascript" ></script>
        
        <script>

            $(document).ready(function () {
                App.init();
                
                $('#terminos-venta').dataTable({
                    "aaSorting": [[ 0, 'desc' ]]
                });

                $('#terminos-venta').on('click', '.delete', function() {
                    return confirm('¿ Desea eliminar el registro ?');
                });
            });
            
        </script>
        <!-- END JAVASCRIPTS -->

    </body>
    <!-- END BODY -->
</html>