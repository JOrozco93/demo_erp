<?php

	if (!isset($_SESSION)) {
    session_start();
  }

  if (($_SESSION['id_usuario'] == null)) {
    header("Location: logout.php");
    exit;
  }

  require_once("config.php");
  require_once("includes/funciones_BD.php");
  require_once("includes/validacion.php");

  $id_usuario = $_SESSION['id_usuario'];

  $id_termino_venta = $_GET['id'];

  if (!is_numeric($id_termino_venta)) {
      header("location: terminos-venta-list.php");
      exit;
  }

  $termino_venta = (isset($_POST['termino_venta'])) ? $_POST['termino_venta'] : null;
  $descripcion = (isset($_POST['descripcion'])) ? $_POST['descripcion'] : null;

  $termino_venta = filter_var($termino_venta, FILTER_SANITIZE_STRING);
  $descripcion = filter_var($descripcion, FILTER_SANITIZE_STRING);

  $sql = "UPDATE terminos_venta SET termino_venta = :termino_venta, descripcion = :descripcion, id_usuario = :id_usuario WHERE id_termino_venta = :id_termino_venta";

 	$stmt = $pdo->prepare($sql);

 	$stmt->bindParam(':termino_venta', $termino_venta);
 	$stmt->bindParam(':descripcion', $descripcion);
 	$stmt->bindParam(':id_usuario', $id_usuario);

  $stmt->bindParam(':id_termino_venta', $id_termino_venta);

 	$result = $stmt->execute();

 	if ($result) {
 		echo "
 			<script>
 				alert('El registro se ha actualizado con éxito');
 				location.href = 'terminos-venta-list.php';
 			</script>
 		";
 	} else {
 		echo "
		<script>
			alert('Ha ocurrido un error al actualizar el registro');
			location.href = 'terminos_venta-list.php';
		</scrip>
 		";
 	}

?>