<?php
include('util/phpqrcode/qrlib.php');
// EL CBB SE DEBE GENERAR EN UN AREA NO MENOR A 2.75CM O 104PX
function generaCBB($rfcEmisor,$rfcReceptor,$total,$uuid){ 
$cadena="?re=".$rfcEmisor."&rr=".$rfcReceptor."&tt=".$total."&id=".$uuid;
//Qrcode::png('PHP QR Code :)',false, 'H', 4, 2, false);

if(is_dir("files/".$rfcEmisor."/temp/")){
$dir = "files/".$rfcEmisor."/temp/";
$dp = opendir($dir) or die ('Could not open '.$dir);
while ($file = readdir($dp)) 
	{
		if ($file != '.' && $file != '..' && (filemtime($dir.$file)) < (strtotime('-1 hour'))) {unlink($dir.$file);}
	}
closedir($dp);
}

if(!(is_dir("files/".$rfcEmisor))) 
	{ umask(0);
	  mkdir("files/".$rfcEmisor,0777);  
	  }
if(!(is_dir("files/".$rfcEmisor."/temp"))) 
	{  umask(0);
	   mkdir("files/".$rfcEmisor."/temp",0777); 
	  }
$prefijo = substr(md5(uniqid(rand())),0,6);
$file =  "files/".$rfcEmisor."/temp/".$prefijo.".png";
Qrcode::png($cadena,$file, 'H', 2, 2, true);

//copy($prefijo.".png",$file);
//unlink($prefijo.".png");

return $file;
}
?>